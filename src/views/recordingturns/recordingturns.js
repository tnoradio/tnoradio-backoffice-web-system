import React, { Component, Fragment } from "react";
import { Row } from "reactstrap";
import RecordingTurnFilter from "../../components/recordingturns/recordingTurnFilter";
import RecordingTurnSearch from "../../components/recordingturns/recordingTurnSearch";
import RecordingTurnList from "../../containers/recordingturns/visibleRecordingTurnList";
import RecordingTurnDetails from "../../containers/recordingturns/visibleRecordingTurnDetails";

class RecordingTurn extends Component {
  render() {
    return (
      <Fragment>
        <div className="post-application">
          <div className="content-overlay" />
          <RecordingTurnFilter />
          <Row className="post-app-content">
            <div className="post-app-content-area w-100">
              <div className="post-app-list-mails p-0">
                <RecordingTurnSearch />
                <RecordingTurnList />
              </div>
              <div className="post-app-mail-content d-none d-md-block">
                <RecordingTurnDetails />
              </div>
            </div>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default RecordingTurn;
