import React, { useState } from "react";
import { Formik, Field, Form } from "formik";
import { LoginAction } from "../../redux/actions/login/loginAction";
import { connect, useDispatch } from "react-redux";
import LogoTelegram from "../../assets/img/telegram-logo-circle.png";
import * as Yup from "yup";
import {
  Row,
  Col,
  FormGroup,
  Button,
  Card,
  CardBody,
  CardImg,
  CardTitle,
} from "reactstrap";
import {
  sendAuthCode,
  signIn,
  authenticateTelegram,
} from "../../redux/actions/chat/telegramActions";

/**
 * User Login form skeleton.
 */
const formSchema = Yup.object().shape({
  phoneNumber: Yup.string(),
  telegramCode: Yup.number(),
});

const Login = (props) => {
  const disptach = useDispatch();
  const [phone, setPhone] = useState("");
  const [phoneHash, setPhoneHash] = useState("");
  const [code, setCode] = useState("");
  const [isExpiredCode, setIsExpiredCode] = useState(false);
  const [isCodeSent, setIsCodeSent] = useState(false);

  function onChangePhone(value) {
    setPhone(value);
  }

  function onChangeCode(value) {
    setCode(value);
    setIsExpiredCode(false);
  }

  const sendCode = async () => {
    setIsCodeSent(true);
    const phone_code_hash = await sendAuthCode(phone);
    console.log(phone_code_hash);
    setPhoneHash(phone_code_hash);
  };

  const serviceSignIn = async (code) => {
    const result = await signIn(code, phoneHash);
    console.log(result);
    return result;
  };

  return (
    <div className="container-fluid">
      <Row className="full-height-vh">
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center"
        >
          <Card className="text-center width-400 form">
            <CardBody>
              <CardImg className="small-logo-form" top src={LogoTelegram} />
              <CardTitle className="black form-title">
                Inicia Sesión en Telegram
              </CardTitle>
              {!isCodeSent && (
                <span className="invalid-feedback">{props.loginMsg}</span>
              )}
              <Formik
                initialValues={{
                  phoneNumber: "+584123964576",
                  telegramCode: "",
                }}
                validationSchema={formSchema}
                onSubmit={async (values) => {
                  const signInResponse = await serviceSignIn(code);
                  if (signInResponse === "PHONE_CODE_EXPIRED") {
                    setIsExpiredCode(true);
                  } else {
                    if (signInResponse === "USER_MIGRATE_1") {
                      console.log("dc error");
                    } else {
                      console.log("Redirection");
                      disptach(authenticateTelegram());
                      props.setIsTelegramAuthenticated(true);
                      localStorage.setItem("isTelegramAuthenticated", true);
                    }
                  }
                }}
              >
                {({ errors, touched }) => (
                  <Form className="pt-2">
                    <FormGroup>
                      <Row>
                        <Col xs="12" style={{ padding: 0 }}>
                          <span>+584120996094</span>
                        </Col>
                        <Col>
                          <Button color="link" onClick={sendCode}>
                            Enviar Código
                          </Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="2"></Col>
                        <Col xs="8" style={{ padding: 0 }}>
                          <Field
                            type="text"
                            name="code"
                            id="telegramCode"
                            placeholder="Código de Telegram"
                            onKeyUp={(e) => onChangeCode(e.target.value)}
                            className={`form-control ${
                              errors.telegramCode &&
                              touched.telegramCode &&
                              "is-invalid"
                            }`}
                          />
                          {errors.telegramCode && touched.telegramCode ? (
                            <div className="invalid-feedback">
                              {errors.telegramCode}
                            </div>
                          ) : null}
                        </Col>
                        <Col xs="2"></Col>
                      </Row>
                      {isExpiredCode ? (
                        <Row>
                          <Col style={{ color: "red" }}>
                            El código ya expiró
                          </Col>
                        </Row>
                      ) : null}
                    </FormGroup>
                    <FormGroup>
                      <Row>
                        <Col xs="2"></Col>
                        <Col xs={8} style={{ padding: 0 }}>
                          <Button type="submit" block className="gradient-mint">
                            Entrar
                          </Button>
                        </Col>
                        <Col xs="2"></Col>
                      </Row>
                    </FormGroup>
                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

//add the follow code outside Login Component
const mapStateToProps = (state) => {
  return {
    sessionUserDetails: state.session.sessionUserDetails,
  };
};

const mapDispatchToProps = {
  loginAction: LoginAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
