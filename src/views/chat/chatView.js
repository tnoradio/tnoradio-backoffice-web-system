import React, { useState, useEffect } from "react";
import { getMe } from "../../redux/actions/chat/telegramActions";
import Chat from "./chat";
import TelegramLogin from "./telegramLogin";

const ChatView = () => {
  const [loginMsg, setLoginMsg] = useState("");

  console.log(localStorage.getItem("isTelegramAuthenticated"));

  const [isTelegramAuthenticated, setIsTelegramAuthenticated] = useState(false);
  console.log(isTelegramAuthenticated);

  useEffect(() => {
    getMe()
      .then((me) => {
        if (me === "Delete your session file and login again") {
          localStorage.setItem("isTelegramAuthenticated", false);
          setIsTelegramAuthenticated(false);
          setLoginMsg(
            "Cierra sesión en el celular y vuelve a solicitar el código"
          );
        } else {
          localStorage.setItem("isTelegramAuthenticated", true);
          setIsTelegramAuthenticated(true);
        }
      })
      .catch((e) => localStorage.setItem("isTelegramAuthenticated", false));
  }, []);

  return isTelegramAuthenticated === true ? (
    <Chat></Chat>
  ) : (
    <TelegramLogin
      loginMsg={loginMsg}
      setIsTelegramAuthenticated={setIsTelegramAuthenticated}
    ></TelegramLogin>
  );
};

export default ChatView;
