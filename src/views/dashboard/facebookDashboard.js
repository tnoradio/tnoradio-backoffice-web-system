import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  CardLink,
  CardText,
  Button,
  Badge,
} from "reactstrap";
import FacebookDashboard from "../../containers/dashboard/visibleFacebookMetricsContainer";
import ShoppingCartCard from "../../components/cards/shoppingCartCard";
import { AdvancedCardData } from "../cards/advancedCardData";

// Styling

class DashboardView extends Component {
  render() {
    return (
      <Fragment>
        <FacebookDashboard />
      </Fragment>
    );
  }
}

export default DashboardView;
