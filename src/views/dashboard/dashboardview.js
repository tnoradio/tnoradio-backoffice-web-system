import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  CardLink,
  CardText,
  Button,
  Badge,
} from "reactstrap";
import Dashboard from "../../containers/dashboard/visibleMetricsContainer";
import ShoppingCartCard from "../../components/cards/shoppingCartCard";
import { AdvancedCardData } from "../cards/advancedCardData";

// Styling

class DashboardView extends Component {
  render() {
    return (
      <Fragment>
        <Dashboard />
      </Fragment>
    );
  }
}

export default DashboardView;
