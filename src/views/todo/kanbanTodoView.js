import React, { useEffect } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import { connect } from "react-redux";
import "@atlaskit/tree";
import { fetchTodos } from "../../redux/actions/todo/todoActions";
import Column from "./kanbanBoard/column";
import { Col, Row } from "reactstrap";

const VerticalList = (props) => {
  useEffect(() => {
    console.log(props);
  }, [props]);
  const onDragEnd = (result) => {};

  const { todoTodos, inProgressTodos, forReviewTodos, completedTodos, todos } =
    props;
  const columns = [
    {
      "todo-column": {
        id: "todo-column",
        title: "Todo",
        taskIds: todoTodos?.map((todo) => todo._id),
      },
    },
    {
      "in-progress-column": {
        id: "inprogress-column",
        title: "In Progress",
        taskIds: inProgressTodos?.map((todo) => todo._id),
      },
    },
    {
      "for-review-column": {
        id: "for-review-column",
        title: "For Review",
        taskIds: forReviewTodos?.map((todo) => todo._id),
      },
    },
    {
      "completed-column": {
        id: "completed-column",
        title: "Completed",
        taskIds: completedTodos?.map((todo) => todo._id),
      },
    },
  ];

  const columnOrder = [
    "todo-column",
    "in-progress-column",
    "for-review-column",
    "completed-column",
  ];
  console.log(columnOrder);
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Row>
        {columnOrder.map((columnId, index) => {
          console.log(columnId, index);
          const column = columns[index][columnId];
          console.log(column);

          const tasks = column.taskIds.map((taskId) =>
            todos.find((todo) => todo._id === taskId)
          );

          console.log(tasks);

          return (
            <Col xs={3}>
              <Column key={column.id} column={column} tasks={tasks} />
            </Col>
          );
        })}
      </Row>
    </DragDropContext>
  );
};

const mapStateToProps = (state) => {
  console.log(state.todoApp);
  console.log(state.todoApp.todo?.filter((todo) => todo.status === "TODO"));
  console.log(
    state.todoApp.todo?.filter((todo) => todo.status === "IN_PROGRESS")
  );
  return {
    todoTodos: state.todoApp.todo?.filter((todo) => todo.status === "TODO"),
    inProgressTodos: state.todoApp.todo?.filter(
      (todo) => todo.status === "IN_PROGRESS"
    ),
    completedTodos: state.todoApp.todo?.filter(
      (todo) => todo.status === "COMPLETED"
    ),
    forReviewTodos: state.todoApp.todo?.filter(
      (todo) => todo.status === "FOR_REVIEW"
    ),
    todos: state.todoApp.todo,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchTodos: dispatch(fetchTodos()),
});

export default connect(mapStateToProps, mapDispatchToProps)(VerticalList);
