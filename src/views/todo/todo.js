import React, { Component } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  Modal,
  ModalHeader,
} from "reactstrap";
import MultipleTargetSource from "./kanbanBoard/dndMultipleTarget";
import ListTodoView from "./listTodoView";
import classnames from "classnames";
import * as Icon from "react-feather";
import AddTodo from "../../containers/todo/addTodo";

class TabsBorderBottom extends Component {
  state = {
    activeTab: "1",
    modal: false,
  };

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  toggleAddTodo() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.state.modal} toggle={this.toggleAddTodo} size="md">
          <ModalHeader toggle={() => this.toggleAddTodo()}>
            Agregar nueva tarea
          </ModalHeader>
          <AddTodo />
        </Modal>
        <Nav tabs className="nav-border-bottom">
          <Col xs={10}>
            <NavItem>
              <NavLink
                className={classnames({
                  active: this.state.activeTab === "1",
                })}
                onClick={() => {
                  this.toggle("1");
                }}
              >
                Vista de Lista
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({
                  active: this.state.activeTab === "2",
                })}
                onClick={() => {
                  this.toggle("2");
                }}
              >
                Vista de Tablero
              </NavLink>
            </NavItem>
          </Col>
          <Col xs={2}>
            <NavItem>
              <div className="form-group form-group-compose text-center">
                <button
                  type="button"
                  className="btn btn-raised btn-danger btn-block my-2 shadow-z-2"
                  onClick={() => this.toggleAddTodo()}
                >
                  <Icon.Plus size={18} className="mr-1" /> Nueva Tarea
                </button>
              </div>
            </NavItem>
          </Col>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                <MultipleTargetSource />
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <ListTodoView />
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

export default TabsBorderBottom;
