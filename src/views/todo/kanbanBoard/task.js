import React from "react";
import styled from "styled-components";
import { Draggable } from "react-beautiful-dnd";

const Container = styled.div`
  margin: 8px;
  border: 1px solid lightgrey;
  border-radius: 2px;
`;

export default class Task extends React.Component {
  render() {
    const { task, index } = this.props;
    console.log(task);
    console.log(index);
    return (
      <Draggable draggableId={task._id} index={index}>
        {(provided) => (
          <Container
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            innerRef={provided.innerRef}
          >
            {task.task}
          </Container>
        )}
      </Draggable>
    );
  }
}
