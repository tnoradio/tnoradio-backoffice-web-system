import React, { Component } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
} from "reactstrap";
import { connect } from "react-redux";
import {
  fetchTodos,
  toggleTodo,
  toggleStarredTodo,
  togglePriorityTodo,
  deleteTodo,
  setEditTodoFlag,
  updateTodo,
  toggleCompleteTodo,
  todoDetails,
} from "../../../redux/actions/todo/todoActions";
import TodoDetails from "../../../containers/todo/visibleTodoDetails";
import { fetchUsers } from "../../../redux/actions/users";

const mapList = (todoList) =>
  todoList.map((todo) => {
    return { id: todo._id, content: todo };
  });

const getItems = (count, todoList) => {
  return Array.from(todoList.map((todo) => ({ id: todo._id, content: todo })));
};

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 10;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  color: "white",

  // change background colour if dragging
  background: "gradient-purple-bliss",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: "#E4F7FA",
  //  border: "1px solid #006064",
  padding: grid,
  width: 240,
});

class multipleTarget extends Component {
  state = {
    items: getItems(this.props.todoTodos.length, this.props.todoTodos),
    selected: getItems(this.props.todoTodos.length, this.props.todoTodos),
    inProgressTodos: getItems(
      this.props.inProgressTodos.length,
      this.props.inProgressTodos
    ),
    selectedInProgressTodos: getItems(
      this.props.inProgressTodos.length,
      this.props.inProgressTodos
    ),
    forReviewTodos: getItems(
      this.props.forReviewTodos.length,
      this.props.forReviewTodos
    ),
    selectedforReviewTodosTodos: getItems(
      this.props.forReviewTodos.length,
      this.props.forReviewTodos
    ),
    completedTodos: getItems(
      this.props.completedTodos.length,
      this.props.completedTodos
    ),
    selectedCompletedTodosTodosTodos: getItems(
      this.props.completedTodos.length,
      this.props.completedTodos
    ),
    modal: false,
  };

  /**
   * A semi-generic way to handle multiple lists. Matches
   * the IDs of the droppable container to the names of the
   * source arrays stored in the state.
   */
  id2List = {
    droppable: "items",
    droppable2: "selected",
  };

  getList = (id) => {
    this.forceUpdate();
    return this.state[this.id2List[id]];
  };

  onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        this.getList(source.droppableId),
        source.index,
        destination.index
      );

      let state = { items };

      if (source.droppableId === "droppable2") {
        state = { selected: items };
      }

      this.setState(state);
    } else {
      const result = move(
        this.getList(source.droppableId),
        this.getList(destination.droppableId),
        source,
        destination
      );

      this.setState({
        items: result.droppable,
        selected: result.droppable2,
      });
    }
  };

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }
  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
      <div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} size="lg">
          <ModalHeader toggle={() => this.toggle()}>Tarea</ModalHeader>
          <ModalBody>
            <TodoDetails />
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={() => this.toggle()}>
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
        <div style={{ backgroundColor: "white", padding: "1em" }}>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Row>
              <Col sm={3}>
                <Droppable droppableId="droppable">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      <h4 className="text-primary">Abierta:</h4>
                      {this.state.items.map((item, index) => (
                        <Draggable
                          key={item.id}
                          draggableId={item.id}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <div
                              onClick={() => {
                                this.props.todoDetails(item.content._id);
                                this.toggle();
                              }}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                              className="gradient-light-blue-cyan"
                            >
                              {item.content.task}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </Col>
              <Col sm={3}>
                <Droppable droppableId="droppable2">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      <h4 className="text-primary">En Curso:</h4>
                      {this.state.selectedInProgressTodos.map((item, index) => (
                        <Draggable
                          key={item.id}
                          draggableId={item.id}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                              className="gradient-light-blue-cyan"
                              onClick={() => {
                                this.props.todoDetails(item.content._id);
                                this.toggle();
                              }}
                            >
                              {item.content.task}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </Col>
              <Col sm={3}>
                <Droppable droppableId="droppable3">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      <h4 className="text-primary">Para Revisión:</h4>
                      {this.state.selectedInProgressTodos.map((item, index) => (
                        <Draggable
                          key={item.id}
                          draggableId={item.id}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                              className="gradient-light-blue-cyan"
                              onClick={() => {
                                this.props.todoDetails(item.content._id);
                                this.toggle();
                              }}
                            >
                              {item.content.task}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </Col>
              <Col sm={3}>
                <Droppable droppableId="droppable4">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      <h4 className="text-primary">Listo:</h4>
                      {this.state.completedTodos.map((item, index) => (
                        <Draggable
                          key={item.id}
                          draggableId={item.id}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                              className="gradient-light-blue-cyan"
                              onClick={() => {
                                this.props.todoDetails(item.content._id);
                                this.toggle();
                              }}
                            >
                              {item.content.task}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </Col>
            </Row>
          </DragDropContext>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    active: state.todoApp.todoDetails,
    todoTodos: state.todoApp.todo?.filter((todo) => todo.status === "TODO"),
    inProgressTodos: state.todoApp.todo?.filter(
      (todo) => todo.status === "IN_PROGRESS"
    ),
    completedTodos: state.todoApp.todo?.filter(
      (todo) => todo.status === "COMPLETED"
    ),
    forReviewTodos: state.todoApp.todo?.filter(
      (todo) => todo.status === "FOR_REVIEW"
    ),
    todos: state.todoApp.todo,
    todo: state.todoApp.todo,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchTodos: dispatch(fetchTodos()),
  toggleTodo: (_id) => dispatch(toggleTodo(_id)),
  toggleStarredTodo: (_id) => dispatch(toggleStarredTodo(_id)),
  togglePriorityTodo: (_id) => dispatch(togglePriorityTodo(_id)),
  deleteTodo: (_id) => dispatch(deleteTodo(_id)),
  todoDetails: (_id) => dispatch(todoDetails(_id)),
  onEditClick: () => dispatch(setEditTodoFlag()),
  onChange: (_id, field, value, selectedTodo) =>
    dispatch(updateTodo(_id, field, value, selectedTodo)),
  toggleCompleteTodo: (_id) => dispatch(toggleCompleteTodo(_id)),
  fetchUsers: dispatch(fetchUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(multipleTarget);
