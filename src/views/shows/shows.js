import React, { Fragment, useState } from "react";
import { Modal, ModalHeader } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";

//Contenedores
import ShowsList from "../../containers/shows/visibleShowsList";
import ShowDetails from "../../containers/shows/visibleShowDetails";

//Componentes
import ShowsSearch from "../../components/shows/showsSearch";
import ShowsFilter from "../../components/shows/showsFilter";

const Shows = () => {
  const [open, setOpen] = useState(false);

  const toggle = () => {
    setOpen(!open);
  };

  return (
    <Fragment>
      <div className="contact-application">
        <div className="content-overlay" />
        <ShowsFilter />
        <div className="contact-app-content">
          <div className="contact-app-list-layout width-65-per-md width-60-per-xl w-100-down-md">
            <PerfectScrollbar>
              <ShowsSearch />
              <ShowsList toggleShow={toggle} />
            </PerfectScrollbar>
          </div>
        </div>
      </div>
      <Modal isOpen={open} toggle={() => setOpen(open)} size="xl">
        <ModalHeader toggle={toggle}>Actualizar Programa</ModalHeader>
        <ShowDetails toggle={toggle} />
      </Modal>
    </Fragment>
  );
};

export default Shows;
