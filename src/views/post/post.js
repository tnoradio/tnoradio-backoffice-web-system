import React, { Fragment, useState } from "react";
import { Modal, ModalHeader, Row } from "reactstrap";
import PostFilter from "../../components/post/postFilter";
import PostSearch from "../../components/post/postSearch";
import PostList from "../../containers/post/visiblePostList";
import PostDetails from "../../containers/post/visiblePostDetails";
import AddPost from "../../containers/post/addPost";
import { PlusCircle } from "react-feather";

const Post = () => {
  const [open, setOpen] = useState(false);
  const [addPostModal, setAddPostModal] = useState(false);

  const toggle = () => {
    setOpen(!open);
  };

  const toggleAddPost = () => {
    console.log("toggleAddPost");
    setAddPostModal((addPostModal) => !addPostModal);
  };

  return (
    <Fragment>
      <div className="contact-application">
        <div className="content-overlay" />
        <PostFilter />
        <Row className="contact-app-content">
          <div className="w-50 bg-white">
            <PostSearch />
          </div>
          <div className="w-50 bg-white">
            <div className="todo-search-box w-100 pl-2 pt-4">
              <PlusCircle
                size={30}
                className="primary mt-1 cursor-pointer"
                onClick={toggleAddPost}
              />
            </div>
          </div>
          <div className="w-100 bg-white">
            <PostList togglePost={toggle} />
          </div>
        </Row>
      </div>
      <Modal isOpen={open} toggle={() => setOpen(open)} size="xl">
        <ModalHeader toggle={toggle}>Actualiza tu Post</ModalHeader>
        <PostDetails toggle={toggle} />
      </Modal>
      <Modal isOpen={addPostModal} size="xl">
        <ModalHeader toggle={toggleAddPost}>Agregar un Post</ModalHeader>
        <AddPost toggle={toggleAddPost} />
      </Modal>
    </Fragment>
  );
};

export default Post;
