import React, { Component, Fragment } from "react";
import { Row } from "reactstrap";

import UsersFilter from "../../components/users/usersFilter";
import UsersSearch from "../../components/users/usersSearch";

//import UsersList from "../../components/users/usersList";
import UsersList from "../../containers/users/visibleUsersList";
import UserDetails from "../../containers/users/visibleUserDetails";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seed: 1,
      loading: false,
    };

    this.seed = this.seed.bind(this);
  }

  seed = () => {
    this.setState({ seed: Math.random() });
  };

  render() {
    return (
      <Fragment>
        <div className="contact-application">
          <div className="content-overlay" />
          <UsersFilter seed={this.seed} />
          <Row className="contact-app-content">
            <div className="contact-app-content-area w-100">
              <div className="contact-app-list-mails p-0">
                <UsersSearch />
                <UsersList key={this.state.seed} />
              </div>
              <div className="contact-app-mail-content d-none d-md-block">
                <UserDetails />
              </div>
            </div>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default Users;
