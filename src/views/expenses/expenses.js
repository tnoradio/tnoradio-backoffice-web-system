import React, { Component, Fragment } from "react";
import { Row, Modal, ModalHeader } from "reactstrap";
import ExpenseFilter from "../../components/expense/expenseFilter";
import ExpenseSearch from "../../components/expense/expenseSearch";
import ExpenseList from "../../containers/expense/visibleExpenseList";
import { PlusCircle } from "react-feather";
import AddExpense from "../../containers/expense/addExpense";
import ExpenseFilterTab from "../../containers/expense/expenseFilterTab";

class Expense extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seed: 1,
      loading: false,
      modal: false,
      activeTab: "1",
    };

    this.seed = this.seed.bind(this);
  }

  seed = () => {
    this.setState({ seed: Math.random() });
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
    this.state.modal === false && this.seed();
  };

  render() {
    return (
      <Fragment>
        <div className="content-overlay" />
        <ExpenseFilter />
        <Row className="post-app-content">
          <ExpenseFilterTab />
        </Row>
        <Row className="post-app-content">
          <div className="post-app-content-area w-50">
            <ExpenseSearch />
          </div>
          <div className="post-app-content-area w-50">
            <div className="todo-search-box w-100 pl-2 pt-4">
              <PlusCircle
                size={30}
                className="primary mt-1 cursor-pointer"
                onClick={this.toggle}
              />
            </div>
          </div>
        </Row>
        <Row className="post-app-content">
          <div className="post-app-content-area w-100">
            <ExpenseList key={this.state.seed} />
          </div>
        </Row>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
          size="lg"
        >
          <ModalHeader toggle={this.toggle}>Registrar un Gasto</ModalHeader>
          <AddExpense toggle={this.toggle} />
        </Modal>
      </Fragment>
    );
  }
}

export default Expense;
