// import external modules
import React from "react";
import { NavLink } from "react-router-dom";
import { connect, useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Input,
  FormGroup,
  Button,
  Card,
  CardBody,
  CardFooter,
} from "reactstrap";
import resetPass from "../../redux/actions/users/resetPassword";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

/**
 * User Login form skeleton.
 */
const formSchema = Yup.object().shape({
  email: Yup.string()
    .email("El correo es inválido")
    .required("Debes introducir un correo"),
});

const ForgotPassword = (props) => {
  return (
    <div className="container-fluid">
      <Row className="full-height-vh">
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center form-background"
        >
          <Card className="gradient-indigo-purple text-center width-400">
            <CardBody>
              <div className="text-center">
                <h4 className="text-uppercase text-bold-400 white py-4">
                  Solicitar nueva contraseña
                </h4>
              </div>
              <Formik
                initialValues={{
                  email: "",
                }}
                validationSchema={formSchema}
                onSubmit={(values) => {
                  console.log(values);
                }}
              >
                {({ errors, touched }) => (
                  <Form className="pt-2">
                    <FormGroup>
                      <Col md="12">
                        <Field
                          type="email"
                          className={`form-control ${
                            errors.email && touched.email && "is-invalid"
                          }`}
                          name="email"
                          id="email"
                          placeholder="Correo electrónico"
                        />
                        {errors.email && touched.email ? (
                          <div className="invalid-feedback">{errors.email}</div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup className="pt-2">
                      <Col md="12">
                        <div className="text-center mt-3">
                          <Button type="submit" block color="danger">
                            Enviar
                          </Button>
                          <Button color="secondary" block>
                            Cancelar
                          </Button>
                        </div>
                      </Col>
                    </FormGroup>
                  </Form>
                )}
              </Formik>
            </CardBody>
            <CardFooter>
              <div className="float-left white">
                <NavLink exact className="text-white" to="/login">
                  Login
                </NavLink>
              </div>
              <div className="float-right white">
                <NavLink exact className="text-white" to="/pages/register">
                  Register Now
                </NavLink>
              </div>
            </CardFooter>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default ForgotPassword;
