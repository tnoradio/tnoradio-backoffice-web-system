// import external modules
import React from "react";
import { NavLink } from "react-router-dom";
import {
  Row,
  Col,
  FormGroup,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardImg,
  CardTitle,
} from "reactstrap";
import ReCAPTCHA from "react-google-recaptcha";
import { toastr } from "react-redux-toastr";
import { useHistory } from "react-router-dom";
import { signUpUser } from "../../redux/actions/users/addUser";
import LogoTNO from "../../assets/img/logo_actual_TNO_blanco.png";
import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
import { connect } from "react-redux";
import { LoginAction } from "../../redux/actions/login/loginAction";
// recaptcha site key
const sitekey = "6Lc3nNkZAAAAAIx8ttRkJN9sZtTFv6Pd96JvZOeJ";
const recaptchaRef = React.createRef();

const Register = (props) => {
  const [isChecked, setChecked] = React.useState(true);
  //reCaptcha hooks
  const [validRecaptcha, setValidRecaptcha] = React.useState(false);
  let history = useHistory();
  const handleChecked = (e) => {
    setChecked((checked) => !checked);
  };
  //reCaptcha onChange function
  function onChange(value) {
    setValidRecaptcha(true);
  }

  /**
   * User Login form skeleton.
   */
  const formSchema = Yup.object().shape({
    name: Yup.string().required("Debes escribir tu nombre"),
    lastName: Yup.string().required("Debes escribir tu apellido"),
    email: Yup.string()
      .email("El correo es inválido")
      .required("Debes introducir un correo"),
    password: Yup.string().required("Debes introducir tu contraseña"),
    passwordConfirmation: Yup.string()
      .required("Debes confirmar tu contraseña")
      .oneOf([Yup.ref("password"), null], "Las contraseñas deben coincidir"),
  });

  const login = (email, password) => {
    // setIsLoading(true);

    const loginRequest = {
      email: {
        value: email,
      },
      password: password,
    };

    props.loginAction(loginRequest).then((result) => {
      if (result && result.success) {
        //  setIsLoading(false);
        history.push("/");
      } else {
        toastr.error("email o contraseña inválidos");
      }
    });
  };

  const signUp = async (
    name,
    lastName,
    email,
    password,
    passwordConfirmation
  ) => {
    const user = await signUpUser(
      name,
      lastName,
      "",
      email,
      "",
      "",
      [],
      [{ role: "AUDIENCE" }],
      "O",
      [],
      "",
      "",
      "",
      "",
      password,
      passwordConfirmation
    );

    login(email, password);
  };

  return (
    <div className="container-fluid">
      <Row className="full-height-vh">
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center form-background"
        >
          <Card className="gradient-purple-bliss text-center width-400">
            <CardBody>
              <CardImg
                className="logo-form"
                top
                src={LogoTNO}
                alt="La primera radio visual de Venezuela"
              />
              <CardTitle className="white py-4">Registrarse</CardTitle>
              <Formik
                initialValues={{
                  name: "",
                  lastName: "",
                  passwordConfirmation: "",
                  email: "",
                  password: "",
                }}
                validationSchema={formSchema}
                onSubmit={(values) => {
                  // const recaptchaValue = recaptchaRef.current.getValue();
                  if (validRecaptcha) {
                    signUp(
                      values.name,
                      values.lastName,
                      values.email,
                      values.password,
                      values.passwordConfirmation
                    );
                  } else {
                    toastr.warning(
                      "ReCaptcha",
                      "Verifica que eres un humano!",
                      {
                        transitionIn: "bounceInDown",
                        transitionOut: "bounceOutUp",
                      }
                    );
                  }
                }}
              >
                {({ errors, touched }) => (
                  <Form className="pt-2">
                    <FormGroup>
                      <Col md="12">
                        <Field
                          type="text"
                          className={`form-control ${
                            errors.name && touched.name && "is-invalid"
                          }`}
                          name="name"
                          id="name"
                          placeholder="Nombre"
                          required
                        />
                        {errors.name && touched.name ? (
                          <div className="invalid-feedback text-left">
                            {errors.name}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12">
                        <Field
                          type="text"
                          className={`form-control ${
                            errors.lastName && touched.lastName && "is-invalid"
                          }`}
                          name="lastName"
                          id="lastName"
                          placeholder="Apellido"
                          required
                        />
                        {errors.lastName && touched.lastName ? (
                          <div className="invalid-feedback text-left">
                            {errors.lastName}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12">
                        <Field
                          type="email"
                          className={`form-control ${
                            errors.email && touched.email && "is-invalid"
                          }`}
                          name="email"
                          id="email"
                          placeholder="Email"
                          required
                        />
                        {errors.name && touched.name ? (
                          <div className="invalid-feedback text-left">
                            {errors.email}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12">
                        <Field
                          type="password"
                          className={`form-control ${
                            errors.password && touched.password && "is-invalid"
                          }`}
                          name="password"
                          id="password"
                          placeholder="Contraseña"
                          required
                        />
                        {errors.password && touched.password ? (
                          <div className="invalid-feedback text-left">
                            {errors.password}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12">
                        <Field
                          type="password"
                          className={`form-control ${
                            errors.passwordConfirmation &&
                            touched.passwordConfirmation &&
                            "is-invalid"
                          }`}
                          name="passwordConfirmation"
                          id="passwordConfirmation"
                          placeholder="Confirmar contraseña"
                          required
                        />
                        {errors.passwordConfirmation &&
                        touched.passwordConfirmation ? (
                          <div className="invalid-feedback text-left">
                            {errors.passwordConfirmation}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    {/*} <FormGroup>
                      <Row>
                        <Col md="12">
                          <div className="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3">
                            <Input
                              type="checkbox"
                              className="custom-control-input"
                              checked={isChecked}
                              onChange={handleChecked}
                              id="rememberme"
                            />
                            <Label
                              className="custom-control-label float-left white"
                              for="rememberme"
                            >
                              I agree terms and conditions.
                            </Label>
                          </div>
                        </Col>
                      </Row>
                        </FormGroup>*/}
                    <FormGroup>
                      <Col md="12" className="recaptcha">
                        <ReCAPTCHA
                          className="text-center"
                          ref={recaptchaRef}
                          sitekey={sitekey}
                          onChange={onChange}
                        />
                      </Col>
                      <Col md="12">
                        <Button
                          type="submit"
                          color="danger"
                          //disabled={!validRecaptcha}
                          block
                          className="btn-pink btn-raised"
                        >
                          Registrarme
                        </Button>
                        <Button
                          type="button"
                          color="secondary"
                          block
                          className="btn-raised"
                        >
                          Cancelar
                        </Button>
                      </Col>
                    </FormGroup>
                  </Form>
                )}
              </Formik>
            </CardBody>
            <CardFooter>
              <div className="text-center">
                <NavLink to="/login" className="text-white">
                  ¿Ya tienes una cuenta? Inicia sesión.
                </NavLink>
              </div>
            </CardFooter>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

//add the follow code outside Login Component
const mapStateToProps = (state) => {
  console.log("state", state);
  return {
    sessionUserDetails: state.session.sessionUserDetails,
  };
};

const mapDispatchToProps = {
  loginAction: LoginAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
