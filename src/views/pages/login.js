// import external modules
import React, { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { toastr } from "react-redux-toastr";
import { useHistory } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import { LoginAction } from "../../redux/actions/login/loginAction";
import { connect } from "react-redux";
import LogoTNO from "../../assets/img/logo_actual_TNO_blanco.png";
import * as Yup from "yup";
import {
  Row,
  Col,
  Input,
  FormGroup,
  Button,
  Label,
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardFooter,
} from "reactstrap";

// recaptcha site key
const sitekey = "6Lc3nNkZAAAAAIx8ttRkJN9sZtTFv6Pd96JvZOeJ";
const recaptchaRef = React.createRef();

/**
 * User Login form skeleton.
 */
const formSchema = Yup.object().shape({
  email: Yup.string()
    .email("El correo es inválido")
    .required("Debes introducir un correo"),
  password: Yup.string().required("Debes introducir tu contraseña"),
});

const Login = (props) => {
  const [isChecked, handleChecked] = useState(true);
  let history = useHistory();
  //const [userState, handleState] = useState({user: {email: '', password: ''}});

  //reCaptcha hooks
  const [validRecaptcha, setValidRecaptcha] = useState(false);
  //const [expired, setExpired] = useState(false);

  //reCaptcha onChange function
  function onChange(value) {
    setValidRecaptcha(true);
  }

  const login = (email, password) => {
    // setIsLoading(true);

    const loginRequest = {
      email: {
        value: email,
      },
      password: password,
    };

    props.loginAction(loginRequest).then((result) => {
      if (result && result.success) {
        //  setIsLoading(false);
        history.push("/pages/user-profile");
      } else {
        toastr.error("email o contraseña inválidos");
      }
    });
  };

  return (
    <div className="container-fluid">
      <Row className="full-height-vh">
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center form-background"
        >
          <Card className="gradient-purple-bliss text-center width-400 form">
            <CardBody>
              <CardImg
                className="logo-form"
                top
                src={LogoTNO}
                alt="La primera radio visual de Venezuela"
              />
              <CardTitle className="white form-title">Iniciar Sesión</CardTitle>

              <Formik
                initialValues={{
                  email: "",
                  password: "",
                }}
                validationSchema={formSchema}
                onSubmit={(values) => {
                  const recaptchaValue = recaptchaRef.current.getValue();
                  if (validRecaptcha) {
                    login(values.email, values.password);
                  } else {
                    toastr.warning(
                      "ReCaptcha",
                      "Verifica que eres un humano!",
                      {
                        transitionIn: "bounceInDown",
                        transitionOut: "bounceOutUp",
                      }
                    );
                  }
                }}
              >
                {({ errors, touched }) => (
                  <Form className="pt-2">
                    <FormGroup>
                      <Col md="12">
                        <small className="text-muted">
                          eg.
                          <i>correo@ejemplo.com</i>
                        </small>
                        <Field
                          type="email"
                          name="email"
                          id="email"
                          placeholder="Email"
                          className={`form-control ${
                            errors.email && touched.email && "is-invalid"
                          }`}
                        />
                        {errors.email && touched.email ? (
                          <div className="invalid-feedback">{errors.email}</div>
                        ) : null}
                      </Col>
                    </FormGroup>

                    <FormGroup>
                      <Col md="12">
                        <Field
                          name="password"
                          id="password"
                          type="password"
                          placeholder="Contraseña"
                          className={`form-control ${
                            errors.password && touched.password && "is-invalid"
                          }`}
                        />
                        {errors.password && touched.password ? (
                          <div className="invalid-feedback">
                            {errors.password}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>

                    <FormGroup>
                      <Row>
                        <Col md="12">
                          <div className="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3">
                            <Input
                              type="checkbox"
                              className="custom-control-input"
                              checked={isChecked}
                              onChange={() => handleChecked(!isChecked)}
                              id="rememberme"
                            />
                            <Label
                              className="custom-control-label float-left white"
                              for="rememberme"
                            >
                              Recuérdame
                            </Label>
                          </div>
                        </Col>
                      </Row>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12" className="recaptcha">
                        <ReCAPTCHA
                          className="text-center"
                          ref={recaptchaRef}
                          sitekey={sitekey}
                          onChange={onChange}
                        />
                      </Col>
                      <Col md="12">
                        <Button type="submit" block className="gradient-mint">
                          Entrar
                        </Button>
                        <Button
                          type="button"
                          block
                          className="gradient-ibiza-sunset"
                        >
                          Cancelar
                        </Button>
                      </Col>
                    </FormGroup>
                  </Form>
                )}
              </Formik>
            </CardBody>
            <CardFooter>
              <div className="float-left">
                <NavLink to="/forgot-password" className="text-white">
                  {" "}
                  Olvidé mi contraseña
                </NavLink>
              </div>
              <div className="float-right">
                <NavLink to="/register" className="text-white">
                  Registrarme
                </NavLink>
              </div>
            </CardFooter>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

//add the follow code outside Login Component
const mapStateToProps = (state) => {
  return {
    sessionUserDetails: state.session.sessionUserDetails,
  };
};

const mapDispatchToProps = {
  loginAction: LoginAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
