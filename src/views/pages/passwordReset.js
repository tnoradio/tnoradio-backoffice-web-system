// import external modules
import React from "react";
import { NavLink } from "react-router-dom";
import { connect, useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Input,
  FormGroup,
  Button,
  Card,
  CardBody,
  CardFooter,
  Label,
} from "reactstrap";
import resetLoggedUserPass from "../../redux/actions/users/resetPassword";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

/**
 * User Login form skeleton.
 */
const formSchema = Yup.object().shape({
  password: Yup.string().required("Debes introducir la contraseña"),
  oldPassword: Yup.string().required("Debes introducir la contraseña Anterior"),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Las contraseñas deben coincidir"
  ),
});

const PasswordReset = (props) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  const user = state.session?.user;
  console.log(user);
  return (
    <div className="container-fluid">
      <Row>
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center"
        >
          <Card className="gradient-indigo-purple text-center width-400">
            <CardBody>
              <div className="text-center">
                <h4 className="text-uppercase text-bold-400 white py-4">
                  Solicitar nueva contraseña
                </h4>
              </div>
              <Formik
                initialValues={{
                  oldPassword: "",
                  password: "",
                  passwordConfirmation: "",
                }}
                validationSchema={formSchema}
                onSubmit={(values) => {
                  console.log(values);
                  dispatch(
                    resetLoggedUserPass(
                      user._id,
                      values.oldPassword,
                      values.password,
                      values.passwordConfirmation
                    )
                  );
                }}
              >
                {({ errors, touched }) => (
                  <Form className="pt-2">
                    <FormGroup>
                      <Col md="12">
                        <Label for="squareinput">Contraseña Anterior</Label>
                        <Field
                          type="password"
                          className={`form-control ${
                            errors.oldPassword &&
                            touched.oldPassword &&
                            "is-invalid"
                          }`}
                          name="oldPassword"
                          id="oldPassword"
                          placeholder="Contraseña anterior"
                        />
                        {errors.oldPassword && touched.oldPassword ? (
                          <div className="invalid-feedback">
                            {errors.oldPassword}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12">
                        <Label for="squareinput">Nueva Contraseña</Label>
                        <Field
                          type="password"
                          className={`form-control ${
                            errors.password && touched.password && "is-invalid"
                          }`}
                          name="password"
                          id="password"
                          placeholder="Contraseña"
                        />
                        {errors.password && touched.password ? (
                          <div className="invalid-feedback">
                            {errors.password}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md="12">
                        <Label for="squareinput">Verificar Contraseña</Label>
                        <Field
                          type="password"
                          className={`form-control ${
                            errors.passwordConfirmation &&
                            touched.passwordConfirmation &&
                            "is-invalid"
                          }`}
                          name="passwordConfirmation"
                          id="passwordConfirmation"
                          placeholder="Verifica tu Contraseña"
                        />
                        {errors.passwordConfirmation &&
                        touched.passwordConfirmation ? (
                          <div className="invalid-feedback">
                            {errors.passwordConfirmation}
                          </div>
                        ) : null}
                      </Col>
                    </FormGroup>
                    <FormGroup className="pt-2">
                      <Col md="12">
                        <div className="text-center mt-3">
                          <Button type="submit" block color="danger">
                            Enviar
                          </Button>
                          <Button color="secondary" block>
                            Cancelar
                          </Button>
                        </div>
                      </Col>
                    </FormGroup>
                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PasswordReset;
