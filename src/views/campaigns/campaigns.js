import React, { Component, Fragment } from "react";
import { Row } from "reactstrap";
import CampaignFilter from "../../components/campaigns/campaignFilter";
import CampaignSearch from "../../components/campaigns/campaignSearch";
import CampaignList from "../../containers/campaigns/visibleCampaignList";
import CampaignDetails from "../../containers/campaigns/visibleCampaignDetails";

class Campaign extends Component {
  render() {
    return (
      <Fragment>
        <div className="post-application">
          <div className="content-overlay" />
          <CampaignFilter />
          <Row className="post-app-content">
            <div className="post-app-content-area w-100">
              <div className="post-app-list-mails p-0">
                <CampaignSearch />
                <CampaignList />
              </div>
              <div className="post-app-mail-content d-none d-md-block">
                <CampaignDetails />
              </div>
            </div>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default Campaign;
