import React, { Component } from "react";
import { Row, Col, Form, FormGroup, Label, Input } from "reactstrap";
import Select from "react-select";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};
const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

export default class Step1 extends Component {
  state = {
    selectedAgeRange: 0,
  };

  componentDidMount() {}

  componentWillUnmount() {}

  // not required as this component has no forms or user entry
  // isValidated() {}

  ageRanges = [
    { label: "18-25", value: 1 },
    { label: "26-30", value: 2 },
    { label: "31-40", value: 3 },
    { label: "más de 40", value: 4 },
  ];

  ageGroupOptions = { label: "Rango de edad", options: this.ageRanges };

  render() {
    var ageRange = [];
    return (
      <div className="step step1">
        <Form>
          <div className="form-body">
            <Row>
              <Col md="6">
                <FormGroup>
                  <Label for="projectinput1">First Name</Label>
                  <Select
                    defaultValue={
                      this.props.getStore().age_range !== []
                        ? {
                            label: this.props.getStore().age_range,
                            value: this.props.getStore().age_range,
                          }
                        : {
                            label: this.ageRange[0].label,
                            value: this.ageRange[0].value,
                          }
                    }
                    options={this.ageGroupOptions}
                    formatGroupLabel={formatGroupLabel}
                    value={this.ageRange.find(
                      (obj) => obj.value === this.state.selectedAgeRange
                    )}
                    onChange={(e) => {
                      this.setState({ selectedAgeRange: e.value });
                      ageRange.push(this.state.selectedAgeRange);
                      this.props.updateStore({ age_range: ageRange });
                    }}
                    id="genre"
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <Label for="projectinput2">Last Name</Label>
                  <Input type="text" id="projectinput2" name="lname" />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <FormGroup>
                  <Label for="projectinput3">E-mail</Label>
                  <Input type="text" id="projectinput3" name="email" />
                </FormGroup>
              </Col>
              <Col md="6">
                <FormGroup>
                  <Label for="projectinput4">Contact Number</Label>
                  <Input type="text" id="projectinput4" name="phone" />
                </FormGroup>
              </Col>
            </Row>
          </div>
        </Form>
      </div>
    );
  }
}
