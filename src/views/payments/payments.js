import React, { Component, Fragment } from "react";
import { Row, Modal, ModalHeader } from "reactstrap";
import PaymentFilter from "../../components/payment/paymentFilter";
import PaymentSearch from "../../components/payment/paymentSearch";
import PaymentList from "../../containers/payment/visiblePaymentList";
import PerfectScrollbar from "react-perfect-scrollbar";
import { PlusCircle } from "react-feather";
import AddPayment from "../../containers/payment/addPayment";
import PaymentFilterTab from "../../containers/payment/paymentFilterTab";

class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seed: 1,
      loading: false,
      modal: false,
      activeTab: "1",
    };

    this.seed = this.seed.bind(this);
  }

  seed = () => {
    this.setState({ seed: Math.random() });
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
    this.state.modal === false && this.seed();
  };

  render() {
    return (
      <Fragment>
        <div className="content-overlay" />
        <PaymentFilter />
        <Row className="post-app-content">
          <PaymentFilterTab />
        </Row>
        <Row className="post-app-content">
          <div className="post-app-content-area w-50">
            <PaymentSearch />
          </div>
          <div className="post-app-content-area w-50">
            <div className="todo-search-box w-100 pl-2 pt-4">
              <PlusCircle
                size={30}
                className="primary mt-1 cursor-pointer"
                onClick={this.toggle}
              />
            </div>
          </div>
        </Row>
        <Row className="post-app-content">
          <div className="post-app-content-area w-100">
            <PaymentList key={this.state.seed} />
          </div>
        </Row>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
          size="lg"
        >
          <ModalHeader toggle={this.toggle}>Registrar un Pago</ModalHeader>
          <AddPayment toggle={this.toggle} />
        </Modal>
      </Fragment>
    );
  }
}

export default Payment;
