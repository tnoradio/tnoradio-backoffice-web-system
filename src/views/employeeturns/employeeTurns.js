import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import EmployeeTurnsFilter from "../../components/employeeturns/employeeTurnsFilter";
import EmployeeTurnsSearch from "../../components/employeeturns/employeeTurnsSearch";
import EmployeeTurnsList from "../../components/employeeturns/employeeTurnsList";

class EmployeeTurns extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seed: 1,
      loading: false,
    };

    this.seed = this.seed.bind(this);
  }

  seed = () => {
    this.setState({ seed: Math.random() });
  };

  render() {
    return (
      <Fragment>
        <div className="contact-application">
          <div className="content-overlay" />
          <EmployeeTurnsFilter seed={this.seed} />
          <Row className="contact-app-content">
            <div className="contact-app-content-area w-100">
              <EmployeeTurnsList key={this.state.seed} />
            </div>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default EmployeeTurns;
