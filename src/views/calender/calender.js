// import external modules
import React, { Fragment, Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  Button,
  Input,
  Card,
  CardBody,
} from "reactstrap";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import { connect } from "react-redux";
import DateTimePicker from "react-datetime";
import "react-datetime/css/react-datetime.css";

import { handleAddEvent } from "../../redux/actions/calenderAction/calenderAction";
import { fetchTodos } from "../../redux/actions/todo/todoActions";
import { fetchRecordingTurns } from "../../redux/actions/recordingTurns";
import { fetchCampaigns } from "../../redux/actions/campaigns";

//const allViews = Object.keys(Calendar.Views).map((k) => Calendar.Views[k]);
const localizer = momentLocalizer(moment);

class Calender extends Component {
  state = {
    modal: false,
    start: new Date(),
    end: new Date(),
    eventTitle: "Enter Your Title",
    events: [],
  };

  toggleModal = () => {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  };

  handleChange = (e) => {
    this.setState({
      eventTitle: e.target.value,
    });
  };

  handleStartDateTimeChange = (date) => {
    this.setState((prevState) => ({
      start: date._d,
    }));
  };

  handleEndDateTimeChange = (date) => {
    this.setState((prevState) => ({
      end: date._d,
    }));
  };

  handleSubmit = () => {
    const { handleAddEvent } = this.props;

    this.setState(
      (prevState) => {
        const { start, eventTitle, end, events } = this.state;
        return {
          events: [
            ...events,
            {
              title: eventTitle,
              allDay: true,
              start: start,
              end: end,
            },
          ],
        };
      },
      () => {
        const { start, end, eventTitle, events } = this.state;
        let param = {
          start,
          eventTitle,
          end,
        };
        handleAddEvent(param, events);
      }
    );
    this.toggleModal();
  };

  handleSlotSelect = (slotInfo) => {
    this.toggleModal();
    this.setState((prevState) => ({
      start: slotInfo.start,
      end: slotInfo.end,
      eventTitle: "Enter Your Title",
    }));
  };

  render() {
    const { calender } = this.props;
    const { modal, eventTitle, start, end } = this.state;
    //console.log(calender);
    moment.locale("es");
    return (
      <Fragment>
        <Modal isOpen={modal} toggle={this.toggleModal}>
          <ModalBody>
            <label>Event Title</label>
            <Input value={eventTitle} onChange={this.handleChange} />
            <Fragment>
              <div>
                <label>Start Date</label>
                <DateTimePicker
                  onChange={this.handleStartDateTimeChange}
                  defaultValue={calender.startDate}
                  value={start}
                />
              </div>
              <div>
                <label>End Date</label>
                <DateTimePicker
                  onChange={this.handleEndDateTimeChange}
                  defaultValue={calender.startDate}
                  value={end}
                />
              </div>
            </Fragment>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleModal}>
              Cancel
            </Button>
            <Button color="success" onClick={this.handleSubmit}>
              Submit
            </Button>
          </ModalFooter>
        </Modal>
        <Row>
          <Col xs="12">
            <div className="content-header">Calendario</div>
          </Col>
        </Row>

        <Card>
          <CardBody>
            <div style={{ height: 750 }}>
              <Calendar
                events={calender}
                //  views={allViews}
                step={60}
                defaultDate={new Date()}
                selectable={false}
                onSelectSlot={this.handleSlotSelect}
                localizer={localizer}
                eventPropGetter={(event, start, end, isSelected) => {
                  console.log(event);
                  return {
                    event,
                    start,
                    end,
                    isSelected,
                    style: {
                      backgroundColor: event.type === "TODO" ? "#009da0" : "",
                    },
                  };
                }}
              />
            </div>
          </CardBody>
        </Card>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  let todoEvents = state.todoApp.todo?.map((todo) => ({
    start: moment(todo.start_date).toDate(),
    end: moment(todo.due_date).toDate(),
    title: todo.task,
    type: "TODO",
    id: todo._id,
  }));
  let recordingTurnEvents = state.recordingTurnsApp.recordingTurns?.map(
    (recordingTurn) => ({
      start: moment(recordingTurn.startTime).toDate(),
      end: moment(recordingTurn.endTime).toDate(),
      title: recordingTurn.description,
      type: "RECORDING_TURN",
      id: recordingTurn._id,
    })
  );

  return {
    calender: todoEvents.concat(recordingTurnEvents),
  };
};

const mapDispatchToProps = (dispatch) => ({
  todos: dispatch(fetchTodos()),
  recordingTurns: dispatch(fetchRecordingTurns()),
  campaigns: dispatch(fetchCampaigns()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Calender);
