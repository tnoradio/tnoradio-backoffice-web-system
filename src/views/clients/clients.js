import React, { Component, Fragment } from "react";
import { Row } from "reactstrap";

import ClientsFilter from "../../components/clients/clientsFilter";
import ClientsSearch from "../../components/clients/clientsSearch";
import ClientsList from "../../containers/clients/visibleClientsList";
import ClientDetails from "../../containers/clients/visibleClientDetails";

class Clients extends Component {
  render() {
    return (
      <Fragment>
        <div className="contact-application">
          <div className="content-overlay" />
          <ClientsFilter />
          <Row className="contact-app-content">
            <div className="contact-app-content-area w-100">
              <div className="contact-app-list-mails p-0">
                <ClientsSearch />
                <ClientsList />
              </div>
              <div className="contact-app-mail-content d-none d-md-block">
                <ClientDetails />
              </div>
            </div>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default Clients;
