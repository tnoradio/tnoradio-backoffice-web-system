const axios = {
  defaults: { baseURL: "http://localhost:4000/api/users/" },
  get: jest.fn(() => Promise.resolve({ data: data })),
  patch: jest.fn(() => Promise.resolve({ data: data })),
  post: jest.fn(() => Promise.resolve({ data: response })),
  put: jest.fn(() => Promise.resolve({ data: data })),
};

export default axios;
