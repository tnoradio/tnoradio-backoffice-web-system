import React from "react";
import Chat from "./chatListItem";
import { useDispatch } from "react-redux";

import PropTypes from "prop-types";
import { getChatsList } from "../../redux/actions/chat/telegramActions";

const ChatList = ({ chats, openChat, chatList, getChatHistory }) => {
  const dispatch = useDispatch();
  const handleTime = (date) => {
    const days = ["lun", "mar", "mier", "jue", "vier", "sab", "dom"];
    const today = new Date();
    const time = new Date(date);
    if (today.toDateString() === time.toDateString()) {
      return time.toLocaleTimeString(); //time
    } else return days[time.getDay()];
  };
  const MINUTE_MS = 10000;

  React.useEffect(() => {
    const interval = setInterval(() => {
      localStorage.getItem("isTelegramAuthenticated") === true &&
        dispatch(getChatsList());
    }, MINUTE_MS);

    return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
  }, []);

  return (
    <div className="list-group position-relative" id="users-list">
      <div className="users-list-padding">
        {chatList &&
          chatList?.map((chat, index) => {
            const top_message = chat?.top_message;
            const name = top_message.chat.first_name
              ? top_message.chat.first_name
              : "";
            const last_name = top_message?.chat?.last_name
              ? top_message?.chat?.last_name
              : "";
            return (
              <Chat
                key={chat.chat.id}
                {...chat}
                time={handleTime(top_message.date)}
                onClick={() => {
                  getChatHistory(chat.chat.id);
                  openChat(chat.chat.id);
                }}
                thumb={chat?.thumb}
                contactName={name + " " + last_name}
                message={top_message.text}
                chatExcerpt={top_message?.text}
              />
            );
          })}
      </div>
    </div>
  );
};

ChatList.propTypes = {
  chats: PropTypes.array.isRequired,
  openChat: PropTypes.func.isRequired,
};
export default ChatList;
