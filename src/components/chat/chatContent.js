import React, { Fragment, useEffect } from "react";
// import { Button } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { MoreVertical, MessageSquare } from "react-feather";
import iso from "../../assets/img/iso.png";
import { useSelector } from "react-redux";

// import PropTypes from 'prop-types'
// import { AlertOctagon, Trash2 } from "react-feather";
const ChatContent = (props) => {
  const state = useSelector((state) => state);
  const handleTime = (date) => {
    const days = ["lun", "mar", "mier", "jue", "vier", "sab", "dom"];
    const today = new Date();
    const time = new Date(date);
    if (today.toDateString() === time.toDateString()) {
      return "hoy " + time.toLocaleTimeString(); //time
    } else return days[time.getDay()] + " " + time.toLocaleTimeString();
  };

  let { messageHistory } = state.chatApp;
  const name = messageHistory[0]?.from_user.first_name;
  useEffect(() => {}, [state.chatApp.messageHistory]);
  return (
    <Fragment>
      {state.chatApp.messageHistory?.length &&
      state.chatApp.messageHistory?.length > 0 ? (
        <div className="contact-app-content-detail">
          <div className="chat-name p-2 bg-white">
            <div className="media">
              <span className="chat-app-sidebar-toggle ft-align-justify font-large-1 mr-2 d-none d-block d-sm-block d-md-none"></span>
              <div className="media-body">
                <span>{name}</span>
                <MoreVertical
                  size={18}
                  className="ft-more-vertical float-right mt-1"
                />
              </div>
            </div>
          </div>
          <PerfectScrollbar
            containerRef={(ref) => {
              let chatScrollHandler = ref;
            }}
          >
            <section className="chat-app-window">
              <div className="badge badge-dark mb-1">Chat History</div>
              <div className="chats">
                {messageHistory.map((message) =>
                  !message?.from_user?.is_self ? (
                    <div className="chat chat-left" key={message.id}>
                      {/* <div className="chat-avatar">
                        <img
                        src={chatDetails?.thumb}
                        className="width-50 rounded-circle"
                        alt="avatar"
                />
                    </div>*/}
                      <div className="chat-body">
                        <div className="chat-content" key={message.id}>
                          <p
                            style={{
                              fontSize: "0.8em",
                              letterSpacing: "-0.5px",
                            }}
                          >
                            {handleTime(message?.date)}
                          </p>
                          <p>{message.text}</p>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className="chat" key={message.id}>
                      <div className="chat-avatar">
                        <img
                          src={iso}
                          width="40"
                          className="rounded-circle mr-1"
                          alt="avatar"
                        />
                      </div>
                      <div className="chat-body">
                        <div className="chat-content" key={message.id}>
                          <p
                            style={{
                              fontSize: "0.8em",
                              letterSpacing: "-0.5px",
                            }}
                          >
                            {handleTime(message?.date)}
                          </p>
                          <p>{message.text}</p>
                        </div>
                      </div>
                    </div>
                  )
                )}
              </div>
            </section>
          </PerfectScrollbar>
        </div>
      ) : (
        <div
          className="row h-100"
          style={{ width: "50%", float: "right", marginRight: "5rem" }}
        >
          <div className="col-sm-12 ">
            <div className="text-center">
              <MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un mensaje para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

export default ChatContent;

ChatContent.propTypes = {
  chatDetails: PropTypes.object.isRequired,
  messageHistory: PropTypes.object.isRequired,
};
