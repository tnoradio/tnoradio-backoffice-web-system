import React, { useState } from "react";
import { Row, Col, Tooltip } from "reactstrap";
import PropTypes from "prop-types";
import { CheckCircle } from "react-feather";
import DefaultTwitter from "../../assets/img/icons8-twitter-64.png";

const Client = ({
  onClientClick,
  pcustomer_id,
  pcustomer_username,
  pcustomer_isUser,
  onIsUserClick,
  active,
}) => {
  const [isUserTooltipOpen, setIsUSerTooltipOpen] = useState(false);
  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === pcustomer_id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row>
        <Col xs={3} onClick={onClientClick}>
          <img
            src={DefaultTwitter}
            className="rounded-circle width-50"
            alt={"Twitter"}
          />
        </Col>
        <Col xs={7} onClick={onClientClick}>
          <p className="mb-0 text-truncate">{pcustomer_username}</p>
          <p className="mb-0 text-muted font-small-3"></p>
        </Col>
        <Col xs={2}>
          <CheckCircle
            size={20}
            onClick={onIsUserClick}
            style={{
              color:
                pcustomer_isUser === "True" || pcustomer_isUser === true
                  ? "#00E5FF"
                  : "#78909C",
            }}
            id={`suspend-${pcustomer_id}-Toggler`}
          />
          <Tooltip
            placement="bottom"
            isOpen={isUserTooltipOpen}
            target={`suspend-${pcustomer_id}-Toggler`}
            toggle={() =>
              setIsUSerTooltipOpen((isUserTooltipOpen) => !isUserTooltipOpen)
            }
          >
            Cambiar el estatus del cliente
          </Tooltip>
        </Col>
      </Row>
    </li>
  );
};

Client.propTypes = {
  pcustomer_id: PropTypes.string,
  pcustomer_username: PropTypes.string,
  onDeleteClick: PropTypes.func,
  onClientClick: PropTypes.func,
};

export default Client;
