import React, { Fragment } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  FormGroup,
  Label,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import DefaultTwitter from "../../assets/img/icons8-twitter-64.png";

const ClientDetails = ({
  selectedClient,
  selectedAgency,
  onEditClick,
  editClientFlag,
  onChange,
  isAgency,
  onChangeIsUserClient,
}) => {
  if (isAgency == false) {
    const isUser = () => {
      return selectedClient?.pcustomer_isUser === "True" ||
        selectedClient.pcustomer_isUser === true
        ? "Ya es cliente cerrado"
        : "Aún no es cliente cerrado";
    };

    return (
      <Fragment>
        {selectedClient ? (
          <div className="contact-app-content-detail">
            <Row>
              <Col className="text-right">
                <Button
                  className="btn-outline-success mr-1 mt-1"
                  size="sm"
                  onClick={onEditClick}
                >
                  {editClientFlag ? (
                    <Icon.Check size={16} />
                  ) : (
                    <Icon.Edit2 size={16} />
                  )}
                </Button>
              </Col>
            </Row>
            <PerfectScrollbar>
              <Media>
                <Media className="col-3" left href="#">
                  <img
                    src={DefaultTwitter}
                    className="rounded-circle width-50"
                    alt={"user"}
                  />
                </Media>
                <Media body>
                  <Media heading>@{selectedClient.pcustomer_username}</Media>
                  {selectedClient.pcustomer_followers} seguidores
                </Media>
              </Media>
              <Table responsive borderless size="sm" className="mt-4">
                <tbody>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Enlace al Perfil</td>
                    <td className="col-9">
                      :
                      <a
                        href={`https://www.twitter.com/${selectedClient.pcustomer_username}`}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {" "}
                        twitter.com/{selectedClient.pcustomer_username}{" "}
                      </a>
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Biografía</td>
                    <td className="col-9">
                      {editClientFlag ? (
                        <Input
                          type="text"
                          name="pcustomer_bio"
                          id="pcustomer_bio"
                          defaultValue={selectedClient.pcustomer_bio}
                          onChange={(e) =>
                            onChange(
                              selectedClient.pcusctomer_id,
                              "bio",
                              e.target.value
                            )
                          }
                        />
                      ) : (
                        ": " + selectedClient.pcustomer_bio
                      )}
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Clasificación</td>
                    <td className="col-9">
                      {editClientFlag ? (
                        <Input
                          type="text"
                          name="tpc_label"
                          id="tpc_label"
                          defaultValue={selectedClient.tpc_label}
                          onChange={(e) =>
                            onChange(
                              selectedClient.pcustomer_id,
                              "tpc_label",
                              e.target.value
                            )
                          }
                        />
                      ) : (
                        ": " + "Potencial " + selectedClient.tpc_label
                      )}
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">
                      Frecuencia de publicación
                    </td>
                    <td className="col-9">
                      {editClientFlag ? (
                        <Input
                          type="text"
                          name="pcustomer_frequency_percent"
                          id="pcustomer_frequency_percent"
                          defaultValue={
                            selectedClient.pcustomer_frequency_percent
                          }
                          onChange={(e) =>
                            onChange(
                              selectedClient.pcustomer_id,
                              "pcustomer_frequency_percent",
                              e.target.value
                            )
                          }
                        />
                      ) : (
                        ": " +
                        Number(
                          selectedClient.pcustomer_frequency_percent
                        ).toFixed(2)
                      )}
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Puntuación</td>
                    <td className="col-9">
                      {editClientFlag ? (
                        <Input
                          type="text"
                          name="pcustomer_total"
                          id="pcustomer_total"
                          defaultValue={selectedClient.pcustomer_total}
                          onChange={(e) =>
                            onChange(
                              selectedClient.pcustomer_id,
                              "birthpcustomer_totaldate",
                              e.target.value
                            )
                          }
                        />
                      ) : (
                        ": " + Number(selectedClient.pcustomer_total).toFixed(2)
                      )}
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Red Social</td>
                    <td className="col-9">
                      {editClientFlag ? (
                        <Input
                          type="text"
                          name="pcustomer_social"
                          disabled
                          id="pcustomer_social"
                          defaultValue={selectedClient.pcustomer_social}
                          onChange={(e) =>
                            onChange(
                              selectedClient.pcustomer_id,
                              "pcustomer_social",
                              e.target.value
                            )
                          }
                        />
                      ) : (
                        ": " + selectedClient.pcustomer_social
                      )}
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Ya es Cliente?</td>
                    <td className="col-9">: {isUser()}</td>
                  </tr>
                  {/*}   <tr className="d-flex">
                  <td className="col-3 text-bold-400">Teléfono</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="phone"
                        id="phone"
                        defaultValue={selectedClient.phone}
                        onChange={(e) =>
                          onChange(selectedClient._id, "phone", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedClient.phone
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Dirección</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="address"
                        id="address"
                        defaultValue={selectedClient.address}
                        onChange={(e) =>
                          onChange(
                            selectedClient._id,
                            "address",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedClient.address
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Compañía</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="company"
                        id="company"
                        defaultValue={selectedClient.company}
                        onChange={(e) =>
                          onChange(
                            selectedClient._id,
                            "company",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedClient.company
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Departamento</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="department"
                        id="department"
                        defaultValue={selectedClient.department}
                        onChange={(e) =>
                          onChange(
                            selectedClient._id,
                            "department",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedClient.department
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Rol:</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <>
                        <Row>
                          <Col xs={5}>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="ADMIN"
                                label="Admin"
                                defaultChecked={admin}
                                onChange={() => {
                                  setAdmin((admin) => !admin);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="AUDIENCE"
                                label="Audiencia"
                                defaultChecked={audience}
                                onChange={() => {
                                  setAudience(!audience);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="EMPLOYEE"
                                label="Empleado"
                                defaultChecked={employee}
                                onChange={() => {
                                  setEmployee((employee) => !employee);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="CLIENT"
                                label="Cliente"
                                defaultChecked={client}
                                onChange={() => {
                                  setClient((client) => !client);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="PRODUCER"
                                label="Productor"
                                defaultChecked={producer}
                                onChange={() => {
                                  setProducer((producer) => !producer);
                                }}
                              />
                            </FormGroup>
                          </Col>
                          <Col xs={7}>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="POTENTIAL_CLIENT"
                                label="Cliente Potencial"
                                defaultChecked={potentialClient}
                                onChange={() => {
                                  setPotentialClient(
                                    (potentialClient) => !potentialClient
                                  );
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="HOST"
                                label="Locutor"
                                defaultChecked={host}
                                onChange={() => {
                                  setHost((host) => !host);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="PRODUCTION_ASSISTANT"
                                label="Asistente de Producción"
                                defaultChecked={productionAssistant}
                                onChange={() => {
                                  setProductionAssistant(
                                    (productionAssistant) =>
                                      !productionAssistant
                                  );
                                }}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        {/*  <Input
                                    type="text"
                                    name="role"
                                    id="role"
                                    defaultValue={
                                       selectedClient.role
                                    }
                                    onChange={e => onChange(selectedClient._id, "roles", e.target.value)}
                                 />
                      </>
                    ) : (
                      ": " + userUtils.translateRole(selectedClient.roles)
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Notas</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="notes"
                        id="notes"
                        defaultValue={selectedClient.notes}
                        onChange={(e) =>
                          onChange(selectedClient._id, "notes", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedClient.notes
                    )}
                  </td>
                </tr>*/}
                </tbody>
              </Table>
            </PerfectScrollbar>
          </div>
        ) : (
          <div className="">
            <div className="">
              <div className="text-center">
                <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
                <h4>Seleccione un usuario para mostrar los detalles.</h4>
              </div>
            </div>
          </div>
        )}
      </Fragment>
    );
  } else {
    return (
      <Fragment>
        {selectedAgency ? (
          <div className="contact-app-content-detail">
            <Row>
              <Col className="text-right">
                <Button
                  className="btn-outline-success mr-1 mt-1"
                  size="sm"
                  onClick={onEditClick}
                >
                  {editClientFlag ? (
                    <Icon.Check size={16} />
                  ) : (
                    <Icon.Edit2 size={16} />
                  )}
                </Button>
              </Col>
            </Row>
            <PerfectScrollbar>
              <Media>
                <Media className="col-3" left href="#">
                  <img
                    src={DefaultTwitter}
                    className="rounded-circle width-50"
                    alt={"user"}
                  />
                </Media>
                <Media body>
                  <Media heading>@{selectedAgency.name_screen}</Media>
                  {selectedAgency.followers_count} seguidores
                </Media>
              </Media>
              <Table responsive borderless size="sm" className="mt-4">
                <tbody>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Enlace al Perfil</td>
                    <td className="col-9">
                      :
                      <a
                        href={selectedAgency.url_twitter}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {" "}
                        {selectedAgency.url_twitter}{" "}
                      </a>
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Biografía</td>
                    <td className="col-9">
                      {editClientFlag ? (
                        <Input
                          type="text"
                          name="pcustomer_bio"
                          id="pcustomer_bio"
                          defaultValue={selectedAgency.description}
                          onChange={(e) =>
                            onChange(selectedAgency.id, "bio", e.target.value)
                          }
                        />
                      ) : (
                        ": " + selectedAgency.description
                      )}
                    </td>
                  </tr>
                  <tr className="d-flex">
                    <td className="col-3 text-bold-400">Ya es Cliente?</td>
                    <td className="col-9">: {}</td>
                  </tr>
                  {}
                </tbody>
              </Table>
            </PerfectScrollbar>
          </div>
        ) : (
          <div className="">
            <div className="">
              <div className="text-center">
                <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
                <h4>Seleccione un usuario para mostrar los detalles.</h4>
              </div>
            </div>
          </div>
        )}
      </Fragment>
    );
  }
};

ClientDetails.prototype = {
  selectedClient: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      lastName: PropTypes.string,
      bio: PropTypes.string,
      image: PropTypes.string,
      phone: PropTypes.string,
      address: PropTypes.string,
      notes: PropTypes.string,
      company: PropTypes.string,
      department: PropTypes.string,
      roles: PropTypes.array,
      gender: PropTypes.string,
      images: PropTypes.array,
      socials: PropTypes.array,
      dni: PropTypes.number,
      email: PropTypes.object.isRequired,
      isActive: PropTypes.bool,
      isStarred: PropTypes.bool,
      isDeleted: PropTypes.bool,
      updatedBy: PropTypes.string,
    }).isRequired
  ).isRequired,
};
export default ClientDetails;
