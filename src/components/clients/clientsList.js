import React from "react";
import PropTypes from "prop-types";
import Client from "./client";
import Agency from "./agency";
import PerfectScrollbar from "react-perfect-scrollbar";

const ClientsList = (props) => {
  const {
    active,
    clients,
    deleteClient,
    clientDetails,
    agencyDetails,
    isAgency,
    updateIsUserClient,
  } = props;

  if (isAgency === false) {
    return (
      <div className="contact-app-list">
        <PerfectScrollbar>
          <div id="clients-list">
            <ul className="list-group">
              {clients.map((client) => (
                <Client
                  key={client.pcustomer_id}
                  active={active}
                  {...client}
                  onDeleteClick={() => deleteClient(client.pcustomer_id)}
                  onClientClick={() => clientDetails(client.pcustomer_id)}
                  onIsUserClick={() =>
                    updateIsUserClient(client.pcustomer_user_id)
                  }
                />
              ))}
            </ul>
          </div>
        </PerfectScrollbar>
      </div>
    );
  } else {
    return (
      <div className="contact-app-list">
        <PerfectScrollbar>
          <div id="clients-list">
            <ul className="list-group">
              {clients.map((client) => (
                <Agency
                  key={client.id}
                  active={active}
                  {...client}
                  onDeleteClick={() => deleteClient(client.id)}
                  onAgencyClick={() => agencyDetails(client.id)}
                  onIsUserClick={() => updateIsUserClient(client.id)}
                />
              ))}
            </ul>
          </div>
        </PerfectScrollbar>
      </div>
    );
  }
};

ClientsList.prototype = {
  clients: PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired,
  deleteClient: PropTypes.func.isRequired,
  clientDetails: PropTypes.func.isRequired,
};
export default ClientsList;
