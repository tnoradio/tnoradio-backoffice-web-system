import React from "react";
import FilterLink from "../../containers/clients/clientsFilterLink";
import { clientVisibilityFilter } from "../../redux/actions/clients";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";

class ClientsFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }
  render() {
    return (
      <div className="contact-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="contact-app-sidebar-content">
            <div className="contact-app-menu">
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Flitrar
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={clientVisibilityFilter.SHOW_ALL}>
                  <Icon.Users size={18} className="mr-1" /> Todos
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Clasificación
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink
                  filter={clientVisibilityFilter.POTENTIAL_ADVERTISER}
                  roleValue="ADVERTISER"
                >
                  <Icon.Circle size={18} className="mr-1 danger" />
                  Potencial anunciante
                </FilterLink>
                <FilterLink
                  filter={clientVisibilityFilter.POTENTIAL_PRODUCER}
                  roleValue="PRODUCER"
                >
                  <Icon.Circle size={18} className="mr-1 warning" />
                  Potencial productor
                </FilterLink>
                <FilterLink
                  filter={clientVisibilityFilter.AGENCIES}
                  roleValue="AGENCIES"
                >
                  <Icon.Circle size={18} className="mr-1 info" />
                  Agencias de publicidad
                </FilterLink>
                <FilterLink
                  filter={clientVisibilityFilter.IS_CLIENT_USER}
                  roleValue="IS_CLIENT_USER"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Ya es Cliente
                </FilterLink>
              </ul>
            </div>
          </div>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default ClientsFilter;
