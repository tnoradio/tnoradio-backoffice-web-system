import React, { useState } from "react";
import { Row, Col, Tooltip } from "reactstrap";
import PropTypes from "prop-types";
import { CheckCircle } from "react-feather";
import DefaultTwitter from "../../assets/img/icons8-twitter-64.png";

const Agency = ({
  onAgencyClick,
  id,
  name_screen,
  isUser,
  onIsUserClick,
  active,
}) => {
  const [isUserTooltipOpen, setIsUSerTooltipOpen] = useState(false);
  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row>
        <Col xs={3} onClick={onAgencyClick}>
          <img
            src={DefaultTwitter}
            className="rounded-circle width-50"
            alt={"Twitter"}
          />
        </Col>
        <Col xs={7} onClick={onAgencyClick}>
          <p className="mb-0 text-truncate">{name_screen}</p>
          <p className="mb-0 text-muted font-small-3"></p>
        </Col>
        <Col xs={2}>
          <CheckCircle
            size={20}
            onClick={onIsUserClick}
            style={{
              color:
                isUser === "True" || isUser === true ? "#00E5FF" : "#78909C",
            }}
            id={`suspend-${id}-Toggler`}
          />
          <Tooltip
            placement="bottom"
            isOpen={isUserTooltipOpen}
            target={`suspend-${id}-Toggler`}
            toggle={() =>
              setIsUSerTooltipOpen((isUserTooltipOpen) => !isUserTooltipOpen)
            }
          >
            Cambiar el estatus del cliente
          </Tooltip>
        </Col>
      </Row>
    </li>
  );
};

Agency.propTypes = {
  id: PropTypes.string,
  name_screen: PropTypes.string,
  onDeleteClick: PropTypes.func,
  onAgencyClick: PropTypes.func,
};

export default Agency;
