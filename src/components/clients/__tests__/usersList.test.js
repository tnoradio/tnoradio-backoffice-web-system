// src/App.test.js
import React from "react";
import UsersList from "../clientsList";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";

import * as userUtils from "../../../utility/test/test-user-utils";

describe("usersList Component", () => {
  let props;

  beforeEach(() => {
    props = {
      users: userUtils.mockUsersList,
    };
  });

  xit("renders without crashing", () => {
    const tree = renderer.create(<UsersList {...props} />);

    expect(tree.toJSON()).toMatchSnapshot();
  });
});
