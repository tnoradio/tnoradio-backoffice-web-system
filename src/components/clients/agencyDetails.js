import React, { Fragment } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  FormGroup,
  Label,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import DefaultTwitter from "../../assets/img/icons8-twitter-64.png";

const AgencyDetails = ({
  selectedAgency,
  onEditClick,
  editClientFlag,
  onChange,
  onChangeIsUserClient,
}) => {
  console.log("entrooo");
  console.log(selectedAgency);
  const isUser = () => {
    return selectedAgency?.pcustomer_isUser === "True" ||
      selectedAgency.pcustomer_isUser === true
      ? "Ya es cliente cerrado"
      : "Aún no es cliente cerrado";
  };

  return (
    <Fragment>
      {selectedAgency ? (
        <div className="contact-app-content-detail">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editClientFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media>
              <Media className="col-3" left href="#">
                <img
                  src={DefaultTwitter}
                  className="rounded-circle width-50"
                  alt={"user"}
                />
              </Media>
              <Media body>
                <Media heading>@{selectedAgency.pcustomer_username}</Media>
                {selectedAgency.pcustomer_followers} seguidores
              </Media>
            </Media>
            <Table responsive borderless size="sm" className="mt-4">
              <tbody>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Enlace al Perfil</td>
                  <td className="col-9">
                    :
                    <a
                      href={`https://www.twitter.com/${selectedAgency.pcustomer_username}`}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {" "}
                      twitter.com/{selectedAgency.pcustomer_username}{" "}
                    </a>
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Biografía</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="pcustomer_bio"
                        id="pcustomer_bio"
                        defaultValue={selectedAgency.pcustomer_bio}
                        onChange={(e) =>
                          onChange(
                            selectedAgency.pcusctomer_id,
                            "bio",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedAgency.pcustomer_bio
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Clasificación</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="tpc_label"
                        id="tpc_label"
                        defaultValue={selectedAgency.tpc_label}
                        onChange={(e) =>
                          onChange(
                            selectedAgency.pcustomer_id,
                            "tpc_label",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + "Potencial " + selectedAgency.tpc_label
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">
                    Frecuencia de publicación
                  </td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="pcustomer_frequency_percent"
                        id="pcustomer_frequency_percent"
                        defaultValue={
                          selectedAgency.pcustomer_frequency_percent
                        }
                        onChange={(e) =>
                          onChange(
                            selectedAgency.pcustomer_id,
                            "pcustomer_frequency_percent",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " +
                      Number(
                        selectedAgency.pcustomer_frequency_percent
                      ).toFixed(2)
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Puntuación</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="pcustomer_total"
                        id="pcustomer_total"
                        defaultValue={selectedAgency.pcustomer_total}
                        onChange={(e) =>
                          onChange(
                            selectedAgency.pcustomer_id,
                            "birthpcustomer_totaldate",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + Number(selectedAgency.pcustomer_total).toFixed(2)
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Red Social</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="pcustomer_social"
                        disabled
                        id="pcustomer_social"
                        defaultValue={selectedAgency.pcustomer_social}
                        onChange={(e) =>
                          onChange(
                            selectedAgency.pcustomer_id,
                            "pcustomer_social",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedAgency.pcustomer_social
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Ya es Cliente?</td>
                  <td className="col-9">: {isUser()}</td>
                </tr>
                {/*}   <tr className="d-flex">
                  <td className="col-3 text-bold-400">Teléfono</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="phone"
                        id="phone"
                        defaultValue={selectedAgency.phone}
                        onChange={(e) =>
                          onChange(selectedAgency._id, "phone", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedAgency.phone
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Dirección</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="address"
                        id="address"
                        defaultValue={selectedAgency.address}
                        onChange={(e) =>
                          onChange(
                            selectedAgency._id,
                            "address",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedAgency.address
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Compañía</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="company"
                        id="company"
                        defaultValue={selectedAgency.company}
                        onChange={(e) =>
                          onChange(
                            selectedAgency._id,
                            "company",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedAgency.company
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Departamento</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="department"
                        id="department"
                        defaultValue={selectedAgency.department}
                        onChange={(e) =>
                          onChange(
                            selectedAgency._id,
                            "department",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedAgency.department
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Rol:</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <>
                        <Row>
                          <Col xs={5}>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="ADMIN"
                                label="Admin"
                                defaultChecked={admin}
                                onChange={() => {
                                  setAdmin((admin) => !admin);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="AUDIENCE"
                                label="Audiencia"
                                defaultChecked={audience}
                                onChange={() => {
                                  setAudience(!audience);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="EMPLOYEE"
                                label="Empleado"
                                defaultChecked={employee}
                                onChange={() => {
                                  setEmployee((employee) => !employee);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="CLIENT"
                                label="Cliente"
                                defaultChecked={client}
                                onChange={() => {
                                  setClient((client) => !client);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="PRODUCER"
                                label="Productor"
                                defaultChecked={producer}
                                onChange={() => {
                                  setProducer((producer) => !producer);
                                }}
                              />
                            </FormGroup>
                          </Col>
                          <Col xs={7}>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="POTENTIAL_CLIENT"
                                label="Cliente Potencial"
                                defaultChecked={potentialClient}
                                onChange={() => {
                                  setPotentialClient(
                                    (potentialClient) => !potentialClient
                                  );
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="HOST"
                                label="Locutor"
                                defaultChecked={host}
                                onChange={() => {
                                  setHost((host) => !host);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="PRODUCTION_ASSISTANT"
                                label="Asistente de Producción"
                                defaultChecked={productionAssistant}
                                onChange={() => {
                                  setProductionAssistant(
                                    (productionAssistant) =>
                                      !productionAssistant
                                  );
                                }}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        {/*  <Input
                                    type="text"
                                    name="role"
                                    id="role"
                                    defaultValue={
                                       selectedAgency.role
                                    }
                                    onChange={e => onChange(selectedAgency._id, "roles", e.target.value)}
                                 />
                      </>
                    ) : (
                      ": " + userUtils.translateRole(selectedAgency.roles)
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Notas</td>
                  <td className="col-9">
                    {editClientFlag ? (
                      <Input
                        type="text"
                        name="notes"
                        id="notes"
                        defaultValue={selectedAgency.notes}
                        onChange={(e) =>
                          onChange(selectedAgency._id, "notes", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedAgency.notes
                    )}
                  </td>
                </tr>*/}
              </tbody>
            </Table>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="">
          <div className="">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un usuario para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

AgencyDetails.prototype = {
  selectedAgency: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      name_screen: PropTypes.string,
      description: PropTypes.string,
      location: PropTypes.string,
      twitter_url: PropTypes.string,
    }).isRequired
  ).isRequired,
};
export default AgencyDetails;
