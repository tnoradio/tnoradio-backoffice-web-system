import React from "react";

import VisibleUsersList from "../../containers/users/visibleUsersList";

const App = () => (
  <div>
    <VisibleUsersList />
  </div>
);

export default App;
