import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import * as Icon from "react-feather";
import CardStatistics from "../cards/cardStatistics";
import PropTypes from "prop-types";
import ClassifiedTwitterUsersCard from "../cards/classifiedTwitterUsersCard";

// Styling

class TwitterDashboard extends Component {
  render() {
    const { statistics, usersClassifiedByTopics, updateTwitterUserBioTopic } =
      this.props;
    console.log(this.props);
    return (
      <Fragment>
        {/* Minimal statistics charts */}

        <Row className="row-eq-height">
          <Col xs={12}>
            <h4>Estadísticas Twitter</h4>
          </Col>
          {statistics.topicMetrics.data?.map((statistic) => {
            return (
              <Col sm="12" md="3">
                <CardStatistics
                  cardBgColor="gradient-blackberry"
                  statistics={Math.trunc(statistic.topic_cant_percent) + "%"}
                  text={statistic.topic_name.toUpperCase()}
                  iconSide="right"
                >
                  <Icon.PieChart size={20} strokeWidth="1.3" color="#fff" />
                </CardStatistics>
              </Col>
            );
          })}
        </Row>
        {/*} <Col xs={12}>
          <h5>Precisión del Algoritmo</h5>
        </Col>*/}
        <Col xs={12}>
          <br />
          <h5>Biografías clasificadas por Tópicos</h5>
          <ClassifiedTwitterUsersCard
            title="Biografías clasificadas por Tópicos"
            usersList={usersClassifiedByTopics.usersClassified}
            updateTwitterUserBioTopic={(user, value) => {
              console.log(user, value);
              updateTwitterUserBioTopic(user, value);
            }}
          ></ClassifiedTwitterUsersCard>
        </Col>
      </Fragment>
    );
  }
}

TwitterDashboard.propTypes = {
  statistics: PropTypes.object.isRequired,
  fetchAllTopicMetrics: PropTypes.func.isRequired,
  updateTwitterUserBioTopic: PropTypes.func,
};

export default TwitterDashboard;
