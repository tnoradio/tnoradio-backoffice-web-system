import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  CardLink,
  CardText,
  Button,
  Badge,
} from "reactstrap";
import { Link } from "react-router-dom";
import * as Icon from "react-feather";

import { AdvancedCardData } from "../../../views/cards/advancedCardData";

import CardStatistics from "../cards/cardStatistics";
import PublishedFacebookCard from "../cards/publishedFacebookCard";
import PublishedInstagramCard from "../cards/publishedInstagramCard";
import TaskStatsDonutChartCard from "../cards/taskStatsDonutChartCard";
import MonthlySalesStatisticsBarChartCard from "../cards/tasksStatisticsBarChartCard";

import PropTypes from "prop-types";
import ConversionStatsDonutChartCard from "../cards/conversionStatsDonutChartCard";

// Styling

class ClientsDashboard extends Component {
  render() {
    const {
      clients,
      tasks_users,
      producersConvertionRate,
      advertisersConvertionRate,
    } = this.props;

    return (
      <Fragment>
        {/* Task Progress */}
        <Row className="row-eq-height">
          <Col sm="12" md="6">
            <ConversionStatsDonutChartCard
              clients={clients.filter(
                (client) => client.tpc_label === "productor"
              )}
              total={
                clients.filter((client) => client.tpc_label === "productor")
                  .length
              }
              convertedTotal={
                this.props.clients.filter(
                  (client) =>
                    client.tpc_label === "productor" &&
                    (client.pcustomer_isUser === true ||
                      client.pcustomer_isUser === "True")
                ).length
              }
              cardTitle="Tasa de Conversión Productores"
              cardSubTitle="Estatus Actual"
              convertionRate={producersConvertionRate}
              isProducer={true}
            />
          </Col>
          <Col sm="12" md="6">
            <ConversionStatsDonutChartCard
              clients={clients.filter(
                (client) => client.tpc_label === "anunciante"
              )}
              convertedTotal={
                this.props.clients.filter(
                  (client) =>
                    client.tpc_label === "anunciante" &&
                    (client.pcustomer_isUser === true ||
                      client.pcustomer_isUser === "True")
                ).length
              }
              total={
                clients.filter((client) => client.tpc_label === "anunciante")
                  .length
              }
              cardTitle="Tasa de Conversión Anunciantes"
              cardSubTitle="Estatus Actual"
              convertionRate={advertisersConvertionRate}
              isProducer={false}
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

ClientsDashboard.propTypes = {
  statistics: PropTypes.object.isRequired,
  fetchAllTopicMetrics: PropTypes.func.isRequired,
};

export default ClientsDashboard;
