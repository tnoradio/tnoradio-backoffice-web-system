import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  CardLink,
  CardText,
  Button,
  Badge,
} from "reactstrap";
import { Link } from "react-router-dom";
import * as Icon from "react-feather";

import { AdvancedCardData } from "../../../views/cards/advancedCardData";

import CardStatistics from "../cards/cardStatistics";
import PublishedFacebookCard from "../cards/publishedFacebookCard";
import PublishedInstagramCard from "../cards/publishedInstagramCard";
import TaskStatsDonutChartCard from "../cards/taskStatsDonutChartCard";
import MonthlySalesStatisticsBarChartCard from "../cards/tasksStatisticsBarChartCard";

import PropTypes from "prop-types";

// Styling

class EmployeesTasksDashboard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      statistics,
      facebook,
      tasksprogress,
      tasks,
      instagram,
      tasks_users,
    } = this.props;
    console.log(tasks_users);

    return (
      <Fragment>
        {/* Task Progress */}
        <Row className="row-eq-height">
          <Col sm="12" md="6">
            <TaskStatsDonutChartCard
              taskStatsDonutChartData={tasksprogress}
              cardTitle="Progreso de tareas de empleados"
              cardSubTitle="Estatus Actual"
            />
          </Col>
          <Col sm="12" md="6">
            <MonthlySalesStatisticsBarChartCard
              taskBarChartData={tasks}
              usersList={tasks_users.users}
              cardTitle="ESTATUS DE TAREAS POR USUARIO"
              cardSubTitle=""
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

EmployeesTasksDashboard.propTypes = {
  statistics: PropTypes.object.isRequired,
  fetchAllTopicMetrics: PropTypes.func.isRequired,
};

export default EmployeesTasksDashboard;
