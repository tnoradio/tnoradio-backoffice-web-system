import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  CardLink,
  CardText,
  Button,
  Badge,
} from "reactstrap";
import { Link } from "react-router-dom";
import * as Icon from "react-feather";

import { AdvancedCardData } from "../../../views/cards/advancedCardData";

import CardStatistics from "../cards/cardStatistics";
import PublishedFacebookCard from "../cards/publishedFacebookCard";
import PublishedInstagramCard from "../cards/publishedInstagramCard";
import TaskStatsDonutChartCard from "../cards/taskStatsDonutChartCard";
import MonthlySalesStatisticsBarChartCard from "../cards/tasksStatisticsBarChartCard";

import PropTypes from "prop-types";

// Styling

class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      statistics,
      facebook,
      tasksprogress,
      tasks,
      instagram,
      tasks_users,
    } = this.props;
    console.log(tasks_users);

    return (
      <Fragment>
        {/* Minimal statistics charts */}

        <Row className="row-eq-height">
          {statistics.topicMetrics.data?.map((statistic) => {
            return (
              <Col sm="12" md="3">
                <CardStatistics
                  cardBgColor="gradient-blackberry"
                  statistics={Math.trunc(statistic.topic_cant_percent) + "%"}
                  text={statistic.topic_name.toUpperCase()}
                  iconSide="right"
                >
                  <Icon.PieChart size={20} strokeWidth="1.3" color="#fff" />
                </CardStatistics>
              </Col>
            );
          })}
        </Row>

        {/* Task Progress */}
        <Row className="row-eq-height">
          <Col sm="12" md="6">
            <TaskStatsDonutChartCard
              taskStatsDonutChartData={tasksprogress}
              cardTitle="Progreso de tareas de empleados"
              cardSubTitle="Estatus Actual"
            />
          </Col>
          <Col sm="12" md="6">
            <MonthlySalesStatisticsBarChartCard
              taskBarChartData={tasks}
              usersList={tasks_users.users}
              cardTitle="ESTATUS DE TAREAS POR USUARIO"
              cardSubTitle=""
            />
          </Col>
        </Row>

        {/* Monthly Statistics & Shopping Cart */}
        <Row className="row-eq-height">
          <Col sm="12" md="12">
            <PublishedFacebookCard
              publishedList={facebook.published}
              cardTitle="Facebook - Publicaciones"
            />
          </Col>
        </Row>
        <Row className="row-eq-height">
          <Col sm="12" md="12">
            <PublishedInstagramCard
              publishedList={instagram.published_instagram}
              cardTitle="Instagram - Publicaciones"
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

Dashboard.propTypes = {
  statistics: PropTypes.object.isRequired,
  fetchAllTopicMetrics: PropTypes.func.isRequired,
};

export default Dashboard;
