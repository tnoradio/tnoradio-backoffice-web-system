import React, { PureComponent } from "react";
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap";
import PropTypes from "prop-types";
import classnames from "classnames";

//Chsrtis JS
import ChartistGraph from "react-chartist";
//Chsrtis CSS
import "chartist/dist/chartist.min.css";
//Component specific chart CSS
import "../../../assets/scss/components/cards/monthlySalesStatisticsBarChartCard.scss";

class MonthlySalesStatisticsBarChartCard extends PureComponent {
  render() {
    var labels = [];
    var completed = [];
    var dif = [];
    this.props.taskBarChartData.tasks.forEach((data) => {
      console.log(data);
      let user = this.props.usersList.find(
        (element) => element._id === data.owner_id
      );
      labels.push(user?.name);
      completed.push(data.total_completed_percents);
      dif.push(100 - data.total_completed_percents);
    });

    var series = [completed, dif];

    var data_tasks = {
      labels,
      series,
    };

    return (
      <Card className={classnames(this.props.cardBgColor)}>
        <CardHeader>
          <CardTitle className="mb-0">{this.props.cardTitle}</CardTitle>
        </CardHeader>
        <CardBody className="pt-2 pb-0">
          <p className="font-medium-2 text-muted text-center pb-2">
            {this.props.cardSubTitle}
          </p>
        </CardBody>
        <ChartistGraph
          className="height-300 Stackbarchart mb-2"
          data={data_tasks}
          type="Bar"
          options={{
            horizontalBars: true,
            stackBars: true,
            fullWidth: true,
            axisX: {
              showGrid: false,
              showLabel: false,
            },
            axisY: {
              showGrid: false,
              showLabel: true,
              offset: 50,
            },
            chartPadding: 30,
          }}
          listener={{
            created: (data) => {
              let defs = data.svg.elem("defs");
              defs
                .elem("linearGradient", {
                  id: "linear",
                  y1: 0,
                  x1: 1,
                  y2: 0,
                  x2: 0,
                })
                .elem("stop", {
                  offset: 0,
                  "stop-color": "rgba(0, 201, 255,1)",
                })
                .parent()
                .elem("stop", {
                  offset: 1,
                  "stop-color": "rgba(17,228,183, 1)",
                });
            },
            draw: (data) => {
              if (data.type === "bar") {
                data.element.attr({
                  style: "stroke-width: 5px",
                  y1: data.y1 + 0.001,
                });
              } else if (data.type === "label") {
                data.element.attr({
                  x: 35,
                });
              }
            },
          }}
        />
      </Card>
    );
  }
}

MonthlySalesStatisticsBarChartCard.propTypes = {
  cardTitle: PropTypes.string,
  cardSubTitle: PropTypes.string,
  stackbarchartData: PropTypes.object,
  usersList: PropTypes.object,
};

export default MonthlySalesStatisticsBarChartCard;
