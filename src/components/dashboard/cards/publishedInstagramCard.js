import React, { PureComponent } from "react";
import { Card, CardHeader, CardTitle, Table, Button } from "reactstrap";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import moment from "moment";

class PublishedInstagramCard extends PureComponent {
  render() {
    const list = this.props.publishedList;
    return (
      <Card>
        <CardHeader>
          <CardTitle className="mb-0">{this.props.cardTitle}</CardTitle>
        </CardHeader>
        <Table responsive className="text-center">
          <thead>
            <tr>
              <th>#</th>
              <th>Tipo</th>
              <th>Likes</th>
              <th>Vistas</th>
              <th>Comentarios</th>
              <th>Total</th>
              <th>Link</th>
              <th>Topicos</th>
            </tr>
          </thead>
          <tbody>
            {list.map((object, i) => {
              return (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{object.type}</td>
                  <td>{object.likes}</td>
                  <td>{object.views}</td>
                  <td>{object.comments}</td>
                  <td>{object.score}</td>
                  <td>
                    <a href={object.link}>
                      <Button color="success" className="btn-round">
                        Link
                      </Button>
                    </a>
                  </td>
                  <td>
                    <small className="text-nowrap">{object.hashtags}</small>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Card>
    );
  }
}

PublishedInstagramCard.propTypes = {
  cardTitle: PropTypes.string,
  publishedList: PropTypes.object,
};

export default PublishedInstagramCard;
