import React, { PureComponent } from "react";
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap";
import PropTypes from "prop-types";
import classnames from "classnames";
//Chsrtis JS
import ChartistGraph from "react-chartist";
//Chsrtis CSS
import "chartist/dist/chartist.min.css";
//Component specific chart CSS
import "../../../assets/scss/components/cards/projectStatsDonutChartCard.scss";
import { AdvancedCardData } from "./advancedCardData";

class TaskStatsDonutChartCard extends PureComponent {
  render() {
    const taskStatsDonutChartData = this.props.taskStatsDonutChartData;
    var array = [];
    taskStatsDonutChartData.progress.map((object, i) => {
      array.push(object.count_percent);
    });

    return (
      <Card className={classnames(this.props.cardBgColor)}>
        <CardHeader>
          <CardTitle className="mb-0">{this.props.cardTitle}</CardTitle>
        </CardHeader>
        <CardBody className="pt-2 pb-0">
          <p className="font-medium-2 text-muted text-center pb-2">
            {this.props.cardSubTitle}
          </p>
        </CardBody>

        <ChartistGraph
          className="height-250 donut"
          data={{
            series: [
              {
                name: "Done",
                className: "ct-done",
                progressClassName: "bg-success",
                value: array[0] || 0,
              },
              {
                name: "Progress",
                className: "ct-progress",
                progressClassName: "bg-amber",
                value: array[1] || 0,
              },
              {
                name: "Outstanding",
                className: "ct-outstanding",
                progressClassName: "bg-deep-purple bg-lighten-1",
                value: array[2] || 0,
              },
              {
                name: "Started",
                className: "ct-started",
                progressClassName: "bg-blue",
                value: array[3] || 0,
              },
            ],
          }}
          type="Pie"
          options={{
            donut: true,
            startAngle: 0,
            labelInterpolationFnc: () => {
              let total = this.props.taskStatsDonutChartData.progress.reduce(
                (prev, progress) => {
                  if (progress.status != "TODO")
                    return prev + parseInt(progress.count_percent);
                  else return prev;
                },
                0
              );
              return total + "%";
            },
          }}
          listener={{
            draw: (data) => {
              if (data.type === "label") {
                if (data.index === 0) {
                  data.element.attr({
                    dx: data.element.root().width() / 2,
                    dy: data.element.root().height() / 2,
                  });
                } else {
                  data.element.remove();
                }
              }
            },
          }}
        />
        <CardBody>
          <div className="row">
            {taskStatsDonutChartData.progress.map((object, i) => {
              let progressClassName = [
                "bg-success",
                "bg-amber",
                "bg-deep-purple bg-lighten-1",
                "bg-blue",
              ];

              return (
                <div className={"col-6"} key={i}>
                  {[
                    <div key={i}>
                      <span className="mb-1 text-muted d-block">
                        {object.count_percent}% - {object.status}
                      </span>
                      <div className="progress" style={{ height: 5 + "px" }}>
                        <div
                          className={classnames(
                            "progress-bar",
                            progressClassName[i]
                          )}
                          role="progressbar"
                          style={{ width: object.count_percent + "%" }}
                          aria-valuenow={object.count_percent}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        />
                      </div>
                    </div>,
                  ]}
                </div>
              );
            })}
          </div>
        </CardBody>
      </Card>
    );
  }
}

TaskStatsDonutChartCard.propTypes = {
  cardTitle: PropTypes.string,
  cardSubTitle: PropTypes.string,
  stackbarchartData: PropTypes.object,
};

export default TaskStatsDonutChartCard;
