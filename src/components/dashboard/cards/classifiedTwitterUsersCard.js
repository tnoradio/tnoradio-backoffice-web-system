import React, { PureComponent } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import { Edit, Trash } from "react-feather";
import PerfectScrollbar from "react-perfect-scrollbar";
import "react-perfect-scrollbar/dist/css/styles.css";
import { ClipLoader } from "react-spinners";
import SpinnerLoader from "../../shared/LoadingSpinner";

class ClassifiedTwitterUsersCard extends PureComponent {
  state = {
    newTopic: "",
    modal: false,
    user: {},
    isUserTooltipOpen: false,
    spinner: true,
  };

  render() {
    const list = this.props.usersList;
    const oldTopics = [
      "emprendimiento",
      "salud",
      "deporte",
      "entretenimiento",
      "finanzas",
      "moda",
      "educacion",
    ];
    let groupedOptions = [];
    let topics = [];

    const toggle = () => this.setState({ modal: !this.state.modal });
    const handleTopicChange = (user, value) => {
      console.log(user, value);
      this.setState({ newTopic: value });
      this.props.updateTwitterUserBioTopic(user, value);
      this.setState({ modal: !this.state.modal });
    };
    const handleTopicDelete = () => {
      this.props.updateTwitterUserBioTopic(this.state.user, "delete");
    };
    oldTopics.forEach((topic) => topics.push({ value: topic, label: topic }));
    groupedOptions = [
      {
        label: "Tópicos",
        options: topics,
      },
    ];
    if (list.length > 0) this.setState({ spinner: false });
    return (
      <Card style={{ height: "400px" }}>
        <SpinnerLoader loading={this.state.spinner} />

        <CardHeader>
          <CardTitle className="mb-0">{this.props.cardTitle}</CardTitle>
        </CardHeader>
        <Table responsive striped className="text-center">
          <thead>
            <tr>
              <th>#</th>
              <th>Etiqueta</th>
              <th>Biografía</th>
              <th>Entrenar</th>
            </tr>
          </thead>
          <tbody style={{ overflow: "scroll", display: "contents" }}>
            {list.map((object, i) => {
              return (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{object.proc_label}</td>
                  <td>{object.proc_bio}</td>
                  <td
                    style={{
                      color: "",
                      "&:hover": {
                        cursor: "pointer",
                      },
                    }}
                  >
                    <Edit
                      className="mr-2"
                      size={18}
                      id={`${object.proc_user_id_twitter}-${i}-Toggler`}
                      onClick={() => {
                        this.setState({ modal: true });
                        this.setState({ user: object });
                      }}
                    />
                    <Trash
                      size={18}
                      id={`${object.proc_user_id_twitter}-${i}-Toggler`}
                      color="red"
                      onClick={() => {
                        this.setState({ user: object });
                        handleTopicDelete();
                      }}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <Modal
          isOpen={this.state.modal}
          toggle={toggle}
          className={this.props.className}
          size="md"
        >
          <ModalHeader toggle={toggle}>Seleccionar Tópico</ModalHeader>
          <ModalBody>
            <Select
              options={groupedOptions}
              onChange={(e) => handleTopicChange(this.state.user, e.value)}
              value={groupedOptions.filter(
                (option) => option === this.state.newTopic
              )}
            />
          </ModalBody>
        </Modal>
      </Card>
    );
  }
}

ClassifiedTwitterUsersCard.propTypes = {
  cardTitle: PropTypes.string,
  publishedList: PropTypes.object,
};

export default ClassifiedTwitterUsersCard;
