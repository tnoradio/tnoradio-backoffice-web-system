import React, { PureComponent } from "react";
import { Card, CardHeader, CardTitle, Table, Button } from "reactstrap";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import moment from "moment";

class PublishedFacebookCard extends PureComponent {
  render() {
    const facebookList = this.props.publishedList;
    return (
      <Card>
        <CardHeader>
          <CardTitle className="mb-0">{this.props.cardTitle}</CardTitle>
        </CardHeader>
        <Table responsive className="text-center">
          <thead>
            <tr>
              <th>#</th>
              <th>Fecha Publicación</th>
              <th>Tipo</th>
              <th>Likes</th>
              <th>Vistos</th>
              <th>Impr x Seguidores</th>
              <th>Impr Organicas</th>
              <th>Total</th>
              <th>Link</th>
            </tr>
          </thead>
          <tbody>
            {facebookList.map((object, i) => {
              let type = "Story";
              if (object.story === null) type = "Post";

              return (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>
                    <span>
                      {moment(object.created_time).format("DD-MM-yyyy")}
                    </span>
                  </td>
                  <td>{type}</td>
                  <td>{object.post_reaction_like}</td>
                  <td>{object.post_impressions_unique}</td>
                  <td>{object.post_impressions_fan}</td>
                  <td>{object.post_impressions_organic_unique}</td>
                  <td>
                    {parseInt(object.post_impressions_organic_unique) +
                      parseInt(object.post_reaction_like) +
                      parseInt(object.post_impressions_unique) +
                      parseInt(object.post_impressions_fan)}
                  </td>
                  <td>
                    <a href={object.link}>
                      <Button color="success" className="btn-round">
                        Link
                      </Button>
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Card>
    );
  }
}

PublishedFacebookCard.propTypes = {
  cardTitle: PropTypes.string,
  publishedList: PropTypes.object,
};

export default PublishedFacebookCard;
