import React, { PureComponent } from "react";
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap";
import PropTypes from "prop-types";
import classnames from "classnames";
//Chsrtis JS
import ChartistGraph from "react-chartist";
//Chsrtis CSS
import "chartist/dist/chartist.min.css";
//Component specific chart CSS
import "../../../assets/scss/components/cards/projectStatsDonutChartCard.scss";

class ConversionStatsDonutChartCard extends PureComponent {
  render() {
    var array = [
      this.props.convertedTotal,
      this.props.total - this.props.convertedTotal,
    ];

    return (
      <Card className={classnames(this.props.cardBgColor)}>
        <CardHeader>
          <CardTitle className="mb-0">{this.props.cardTitle}</CardTitle>
        </CardHeader>
        <CardBody className="pt-2 pb-0">
          <p className="font-medium-2 text-muted text-center pb-2">
            {this.props.cardSubTitle}
          </p>
        </CardBody>

        <ChartistGraph
          className="height-250 donut"
          data={{
            series: [
              {
                name: "Convertidos",
                className: "ct-done",
                progressClassName: "bg-success",
                value: array[0] || 0,
              },
              {
                name: "Clientes",
                className: "ct-outstanding",
                progressClassName: "bg-amber",
                value: array[1] || 0,
              },
            ],
          }}
          type="Pie"
          options={{
            donut: true,
            startAngle: 0,
            labelInterpolationFnc: () => {
              let total = parseFloat(this.props.convertionRate).toFixed(2);
              return total + "%";
            },
          }}
          listener={{
            draw: (data) => {
              if (data.type === "label") {
                if (data.index === 0) {
                  data.element.attr({
                    dx: data.element.root().width() / 2,
                    dy: data.element.root().height() / 2,
                  });
                } else {
                  data.element.remove();
                }
              }
            },
          }}
        />
        <CardBody>
          <div className="row">
            {array.map((object, i) => {
              let progressClassName = ["bg-success", "bg-deep-purple"];

              return (
                <div className={"col-6"} key={i}>
                  {[
                    <div key={i}>
                      <span className="mb-1 text-muted d-block">
                        {object} -{" "}
                        {this.props.isProducer ? "Productores" : "Anunciantes"}{" "}
                        {i === 0 ? "convertidos" : "sin convertir"}
                      </span>
                      <div className="progress" style={{ height: 5 + "px" }}>
                        <div
                          className={classnames(
                            "progress-bar",
                            progressClassName[i]
                          )}
                          role="progressbar"
                          style={{ width: object }}
                          aria-valuenow={this.props.convertionRate}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        />
                      </div>
                    </div>,
                  ]}
                </div>
              );
            })}
          </div>
          <p className="white bg-warning">
            Data extraída de la red social Twitter.
          </p>
        </CardBody>
      </Card>
    );
  }
}

ConversionStatsDonutChartCard.propTypes = {
  cardTitle: PropTypes.string,
  cardSubTitle: PropTypes.string,
  stackbarchartData: PropTypes.object,
};

export default ConversionStatsDonutChartCard;
