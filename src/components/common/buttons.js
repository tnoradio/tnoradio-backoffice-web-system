import { XSquare } from "react-feather";

const Buttons = ({ options, onDeleteOption, editFlag, keyLabel }) => {
  const colors = [
    "primary",
    "secondary",
    "success",
    "info",
    "warning",
    "danger",
  ];
  const color = colors[Math.floor(Math.random() * colors.length)];

  return (
    <div className="btn-toolbar mt-3">
      {options.map((option, index) => (
        <div key={`${keyLabel}_${index}`} className="mr-3 mb-4">
          <span className="border border-primary p-2 mr-2">
            {option.label}
            {editFlag ? (
              <XSquare
                size={24}
                className="ml-2 cursor-pointer"
                color="#FF586B"
                onClick={() => {
                  onDeleteOption(option);
                }}
              />
            ) : null}
          </span>
        </div>
      ))}
    </div>
  );
};

export default Buttons;
