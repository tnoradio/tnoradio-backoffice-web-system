const groupedOptions = (options, label) => {
  switch (label) {
    case "Géneros":
      const genres = options.map((genre) => ({
        label: genre.genre,
        value: genre.id,
      }));
      return [{ label: label, options: genres || [] }];
    case "Sub Géneros":
      const subGenres = options.map((subGenre) => ({
        label: subGenre.subgenre,
        value: subGenre.id,
      }));
      return [{ label: label, options: subGenres || [] }];
    case "Clasificación":
      const pgs = options.map((pg) => ({
        label: pg.showPGClassification,
        value: pg.id,
      }));
      return [{ label: label, options: pgs || [] }];
    case "Tipo":
      const types = options.map((type) => ({
        label: type.showType,
        value: type.id,
      }));
      return [{ label: label, options: types || [] }];
    case "Target":
      const targets = options.map((target) => ({
        label: target.label,
        value: target.value,
      }));
      return [{ label: label, options: targets || [] }];
    case "Rango de edades":
      const ages = options.map((age) => ({
        label: age.label,
        value: age.value,
      }));
      return [{ label: label, options: ages || [] }];
    case "Locutor (es)":
      const hosts = options.map((user) => ({
        label: user.name + " " + user.lastName,
        value: user._id,
      }));
      return [{ label: label, options: hosts || [] }];
    case "Productor (es)":
      const producers = options.map((user) => ({
        label: user.name + " " + user.lastName,
        value: user._id,
      }));
      return [{ label: label, options: producers || [] }];
    default:
      return [{ label: label, options: options || [] }];
  }
};

export default groupedOptions;
