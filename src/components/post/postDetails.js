import React, { Fragment, useState, useEffect } from "react";
import { Row, Col, Media, Table, Button, Input } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import { useDispatch } from "react-redux";
import MyDropzone from "../shared/MyDropzone";
import updatePostImage from "../../redux/actions/posts/updatePostImage";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import parse from "html-react-parser";
import { Tag } from "react-feather";
import Tags from "../../containers/post/tags";

const PostDetails = ({ selectedPost, onEditClick, editPostFlag }) => {
  const dispatch = useDispatch();
  const [imagePostFiles, setPostImageFiles] = useState([]);
  const [toggleTag, setToggleTag] = React.useState(false);
  const [postTags, setPostTags] = React.useState([]);
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  useEffect(() => {
    if (
      selectedPost !== undefined &&
      imagePostFiles[0] !== undefined &&
      imagePostFiles[0] !== null
    )
      dispatch(
        updatePostImage(
          imagePostFiles[0],
          "main",
          "main",
          selectedPost.slug,
          "main",
          selectedPost._id,
          selectedPost.image.substring(selectedPost.image.indexOf("=") + 1)
        )
      );
  }, [imagePostFiles]);

  useEffect(() => {
    transformContent();
  }, [selectedPost]);

  const onEditorStateChange = (editorState) => {
    setEditorState(editorState);
    selectedPost.text = JSON.stringify(
      convertToRaw(editorState.getCurrentContent())
    );
  };

  const onPostTagsChange = () => {
    selectedPost.tags = postTags;
  };

  const onDeleteTag = (tag) => {
    const newTags = selectedPost?.tags.filter((t) => t !== tag);
    selectedPost.tags = newTags;
    setPostTags(newTags);
  };

  const transformContent = () => {
    try {
      const content = JSON.parse(selectedPost?.text);
      content.entityMap === undefined
        ? (content.entityMap = {})
        : (content.entityMap = content.entityMap);
      const convertedContent = convertFromRaw(content);
      setEditorState(EditorState.createWithContent(convertedContent));
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Fragment>
      {selectedPost ? (
        <div className="contact-app-content-detail p-2">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={() => onEditClick(selectedPost, editPostFlag)}
              >
                {editPostFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media className="col-12" left href="#">
              {editPostFlag ? (
                <MyDropzone
                  layout={"post"}
                  image={selectedPost.image}
                  setImageFiles={setPostImageFiles}
                  disabled={false}
                ></MyDropzone>
              ) : (
                <MyDropzone
                  layout={"post"}
                  image={selectedPost.image}
                  setImageFiles={setPostImageFiles}
                  disabled={true}
                ></MyDropzone>
              )}
            </Media>

            <Table responsive borderless size="sm" className="mt-1">
              <tbody>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Título:</td>
                  <td className="col-10">
                    {editPostFlag ? (
                      <Input
                        type="text"
                        name="title"
                        id="title"
                        defaultValue={selectedPost.title}
                        onChange={(e) => (selectedPost.title = e.target.value)}
                      />
                    ) : (
                      <h4>{selectedPost.title}</h4>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Subtítulo:</td>
                  <td className="col-10">
                    {editPostFlag ? (
                      <Input
                        type="textarea"
                        name="subtitle"
                        id="subtitle"
                        defaultValue={selectedPost.subtitle}
                        onChange={(e) =>
                          (selectedPost.subtitle = e.target.value)
                        }
                      />
                    ) : (
                      <p className="pl-2 pr-1">{selectedPost.subtitle}</p>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Resúmen:</td>
                  <td className="col-10">
                    {editPostFlag ? (
                      <Input
                        type="textarea"
                        name="summary"
                        id="summary"
                        defaultValue={selectedPost.summary}
                        onChange={(e) =>
                          (selectedPost.summary = e.target.value)
                        }
                      />
                    ) : (
                      <p className="pl-2 pr-1">{selectedPost.summary}</p>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  {editPostFlag ? (
                    <>
                      <td className="col-2 text-bold-400">Contenido:</td>
                      <td className="col-10">
                        <Editor
                          editorState={editorState}
                          onEditorStateChange={onEditorStateChange}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                        />
                      </td>
                    </>
                  ) : (
                    <>
                      <td className="col-2 text-bold-400">Texto:</td>
                      <td className="col-10">
                        {editorState.getCurrentContent && (
                          <div className="pl1 pr-2">
                            {parse(
                              draftToHtml(
                                convertToRaw(editorState.getCurrentContent())
                              )
                            )}
                          </div>
                        )}
                      </td>
                    </>
                  )}
                </tr>
                {Array.isArray(selectedPost.tags) && (
                  <tr className="d-flex">
                    <td className="col-2 text-bold-400">Etiquetas:</td>
                    <td className="col-10">
                      {selectedPost.tags.map((tag) => (
                        <span
                          key={`${tag}_${selectedPost._id}`}
                          className={
                            "badge " +
                            (tag === "SALUD" ? "badge-info" : "") +
                            (tag === "EMPRENDIMIENTO" ? "badge-warning" : "") +
                            (tag === "CINE" ? "badge-success" : "") +
                            (tag === "ENTRETENIMIENTO" ? "badge-info" : "") +
                            (tag === "CULTURA" ? "badge-warning" : "") +
                            (tag === "HISTORIA" ? "badge-success" : "") +
                            (tag === "ECONOMIA" ? "badge-info" : "") +
                            (tag === "DEPORTE" ? "badge-warning" : "") +
                            (tag === "TEATRO" ? "badge-success" : "") +
                            (tag === "EDUCACIÓN" ? "badge-info" : "") +
                            (tag === "MATERNIDAD" ? "badge-warning" : "") +
                            (tag === "MUSICA" ? "badge-purple" : "") +
                            (tag === "TECNOLOGÍA" ? "badge-purple" : "") +
                            (tag === "TECNOLOGIA" ? "badge-purple" : "") +
                            (tag === "FINANZAS" ? "badge-success" : "") +
                            (tag === "FRANQUICIAS" ? "badge-info" : "") +
                            " mr-1"
                          }
                        >
                          {tag}{" "}
                          {editPostFlag === true && (
                            <Icon.X
                              className="cursor-pointer"
                              onClick={() => onDeleteTag(tag)}
                            />
                          )}
                        </span>
                      ))}
                      <br />
                      <br />
                      {editPostFlag && (
                        <Button
                          className="btn-square"
                          size="md"
                          outline
                          color="primary"
                          onClick={() =>
                            setToggleTag((toggleTag) => !toggleTag)
                          }
                        >
                          <Tag size={16} color="#009DA0" />
                          {"  "}
                          Agregar Etiqueta
                        </Button>
                      )}
                    </td>
                    <Tags
                      postTags={selectedPost?.tags}
                      setToggleTag={setToggleTag}
                      toggleTag={toggleTag}
                      setPostTags={setPostTags}
                      onChange={onPostTagsChange}
                    />
                  </tr>
                )}
              </tbody>
            </Table>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un post para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

PostDetails.prototype = {
  selectedPost: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      owner_id: PropTypes.string,
      slug: PropTypes.string,
      summary: PropTypes.string,
      text: PropTypes.string,
      image: PropTypes.string,
      publish_date: PropTypes.string,
      tags: PropTypes.array,
      images: PropTypes.array,
      approved: PropTypes.bool,
      isActive: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
};
export default PostDetails;
