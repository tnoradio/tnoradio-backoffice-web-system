import React, { Fragment, useState, useEffect } from "react";
import { Media } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as postUtils from "../../utility/posts/postUtils";
import { useDispatch, useSelector } from "react-redux";
import { getPostProfileImage } from "../../redux/actions/posts/fetchPostImage";
import MyDropzone from "../shared/MyDropzone";
import { updatePostProfileImage } from "../../redux/actions/posts/updatePostImage";

const PostImage = ({ post }) => {
  const [data, setData] = React.useState({});
  const dispatch = useDispatch();

  const [imageProfileFiles, setProfileImageFiles] = useState([]);

  const state = useSelector((state) => state);

  useEffect(() => {
    if (
      state.postApp.fetchPostImage.postProfileImage !== {} &&
      state.postApp.fetchPostImage.postProfileImage !== undefined &&
      state.postApp.fetchPostImage.postProfileImage.file !== undefined &&
      state.postApp.fetchPostImage.postProfileImage.file.data
    )
      setData(
        Buffer.from(
          state.postApp.fetchPostImage.postProfileImage.file.data,
          "binary"
        ).toString("base64")
      );
  }, [state.postApp.fetchPostImage]);

  useEffect(() => {
    if (post !== undefined) {
      dispatch(getPostProfileImage("profile", post.slug));
    }
  }, [post]);

  useEffect(() => {
    //console.log(imageProfileFiles);
    if (
      post !== undefined &&
      imageProfileFiles[0] !== undefined &&
      imageProfileFiles[0] !== null
    )
      dispatch(
        updatePostProfileImage(
          imageProfileFiles[0],
          "profile",
          "profile",
          post.slug,
          "profiles",
          post._id
        )
      );
  }, [imageProfileFiles]);

  return (
    <Fragment>
      {post ? (
        <div className="contact-app-content-detail">
          <PerfectScrollbar>
            <Media>
              <Media className="rounded-circle width-70" href="#">
                <MyDropzone
                  layout={"post"}
                  image={`data:image/jpeg;base64,${data}`}
                  setImageFiles={setProfileImageFiles}
                  disabled={true}
                ></MyDropzone>
              </Media>
            </Media>
          </PerfectScrollbar>
        </div>
      ) : null}
    </Fragment>
  );
};

PostImage.prototype = {
  post: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      owner_id: PropTypes.string,
      slug: PropTypes.string,
      summary: PropTypes.string,
      text: PropTypes.string,
      image: PropTypes.string,
      publish_date: PropTypes.string,
      tags: PropTypes.array,
      images: PropTypes.array,
      approved: PropTypes.bool,
      isActive: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
};
export default PostImage;
