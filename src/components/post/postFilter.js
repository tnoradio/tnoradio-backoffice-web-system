import React from "react";
import FilterLink from "../../containers/post/postFilterLink";
import { postVisibilityFilter } from "../../redux/actions/posts";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";

class PostFilter extends React.Component {
  render() {
    return (
      <div className="contact-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="contact-app-sidebar-content">
            <div className="contact-app-menu">
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Flitrar
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={postVisibilityFilter.SHOW_ALL}>
                  <Icon.Layers size={18} className="mr-1" /> Todos
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.UNAPPROVED_POST}>
                  <Icon.AlertOctagon
                    size={18}
                    style={{ color: "#FF586B" }}
                    className="mr-1"
                  />{" "}
                  Deshabilitados
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Etiquetas
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={postVisibilityFilter.HEALTH_POST}>
                  <Icon.Circle size={18} className="mr-1 primary" /> Salud
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.CULTURE_POST}>
                  <Icon.Circle size={18} className="mr-1 warning" /> Cultura
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.ENTREPRENEURSHIP_POST}>
                  <Icon.Circle size={18} className="mr-1 danger" />{" "}
                  Emprendimiento
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.TECHNOLOGY_POST}>
                  <Icon.Circle size={18} className="mr-1 blue" /> Tecnología
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.EVENTS_POST}>
                  <Icon.Circle size={18} className="mr-1 pink" /> Eventos
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.SPORTS_POST}>
                  <Icon.Circle size={18} className="mr-1 blue" /> Deportes
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.ENTERTAINMENT_POST}>
                  <Icon.Circle size={18} className="mr-1 purple" />{" "}
                  Entretenimiento
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.CINEMA_POST}>
                  <Icon.Circle size={18} className="mr-1 danger" /> Cine
                </FilterLink>
                <FilterLink filter={postVisibilityFilter.FRANCHISE_POST}>
                  <Icon.Circle size={18} className="mr-1 danger" /> Franquicias
                </FilterLink>
              </ul>
            </div>
          </div>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default PostFilter;
