import React from "react";

import VisiblePostList from "../../containers/post/visiblePostList";

const App = () => <VisiblePostList />;

export default App;
