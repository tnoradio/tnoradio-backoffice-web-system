import React from "react";
import PropTypes from "prop-types";
import Post from "./post";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useDispatch } from "react-redux";
import { updatePost } from "../../redux/actions/posts";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import Spinner from "../spinner/spinner";

const PostList = ({
  active,
  posts,
  toggleStarredPost,
  deletePost,
  postDetails,
  isUserAdmin,
  postsCount,
  fetchNextPostPage,
}) => {
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = React.useState(0);
  const [prevPage, setPrevPage] = React.useState(0);
  const totalPages = Math.ceil(postsCount / 20);
  const [loading, setLoading] = React.useState(posts === undefined);

  const handlePageClick = (e, i) => {
    e.preventDefault();
    setPrevPage(currentPage);
    const pageDifference = i - (currentPage + 1);
    setLoading(true);
    pageDifference > 0 &&
      posts.length < i * 20 &&
      fetchNextPostPage(pageDifference * 20, currentPage + 1);
    setLoading(false);
    setCurrentPage(i - 1);
  };

  const handlePrevPageClick = (e) => {
    e.preventDefault();
    setCurrentPage((currentPage) => currentPage - 1);
    setPrevPage((currentPage) => currentPage - 1);
  };

  const handleNextPageClick = (e) => {
    e.preventDefault();
    setCurrentPage(currentPage + 1);
    setPrevPage(currentPage - 1);
    fetchNextPostPage(20, currentPage);
  };

  const handleDotsClick = (e) => {
    setPrevPage(currentPage);
    setCurrentPage(currentPage - 4);
  };

  const fabricatePaginationArray = (currentPage) => {
    return [
      currentPage - 2,
      currentPage - 1,
      currentPage,
      currentPage + 1,
      currentPage + 2,
    ];
  };

  console.log("posts", posts);

  return (
    <div className="contact-app-list bg-white">
      {loading && <Spinner />}

      <div id="users-list">
        <ul className="list-group">
          {posts.map((post, index) => {
            return (
              <Post
                key={post._id}
                active={active}
                {...post}
                post={post}
                isUserAdmin={isUserAdmin}
                onStarredClick={() => {
                  dispatch(
                    updatePost(post._id, "starred", !post.starred, post)
                  );
                  toggleStarredPost(post._id);
                }}
                onDeleteClick={() => deletePost(post._id)}
                onPostClick={() => postDetails(post._id)}
                onApproveClick={() => {
                  dispatch(
                    updatePost(post._id, "approved", !post.approved, post)
                  );
                }}
              />
            );
          })}
        </ul>
      </div>
      {/*<div className="d-flex justify-content-center p-2 m-3">
          <Pagination aria-label="Posts page navigation" disabled={currentPage <= 0}>
            <PaginationItem disabled={prevPage === 0} >
              <PaginationLink previous onClick={e => handlePrevPageClick(e)} href="#">
              </PaginationLink>
            </PaginationItem>
            <PaginationItem active={0 === currentPage}>
              <PaginationLink  onClick={e => handlePrevPageClick(e, 1)} href="#">
                1
              </PaginationLink>
            </PaginationItem>
            {currentPage >= 6 && 
            <PaginationItem>
              <PaginationLink onClick={e => handleDotsClick(e)} href="#">
                ...
              </PaginationLink>
            </PaginationItem>}
            {currentPage < 6 && [2,3,4,5,6,7].map((page) => (
              <PaginationItem active={page -1 === currentPage} key={page}>
                <PaginationLink onClick={e => handlePageClick(e, page)} href="#">
                  {page}
                </PaginationLink>
              </PaginationItem>
              ))
            }
            {currentPage >= 6 && fabricatePaginationArray(currentPage).map((page, i) => (
              <PaginationItem active={page - 1 === currentPage} key={i}>
                <PaginationLink onClick={e => handlePageClick(e, page)} href="#">
                  {page}
                </PaginationLink>
              </PaginationItem>
              ))
            }
            <PaginationItem disabled={currentPage === totalPages - 2}>
              <PaginationLink onClick={e => handleNextPageClick(e)} next href="#">
              </PaginationLink>
            </PaginationItem>
          </Pagination>
        </div>*/}
    </div>
  );
};

PostList.prototype = {
  post: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      owner_id: PropTypes.string,
      slug: PropTypes.string,
      summary: PropTypes.string,
      text: PropTypes.string,
      image: PropTypes.string,
      publish_date: PropTypes.string,
      tags: PropTypes.array,
      images: PropTypes.array,
      isActive: PropTypes.bool,
      approved: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
  active: PropTypes.string.isRequired,
  isUserAdmin: PropTypes.bool.isRequired,
  postsCount: PropTypes.number.isRequired,
  toggleStarredPost: PropTypes.func.isRequired,
  fetchNextPostPage: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  postDetails: PropTypes.func.isRequired,
};
export default PostList;
