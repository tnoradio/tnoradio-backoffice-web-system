import React from "react";
import { Row, Col } from "reactstrap";
import PropTypes from "prop-types";
import { CheckCircle, Trash } from "react-feather";

const Post = ({
  onPostClick,
  onApproveClick,
  onDeleteClick,
  _id,
  title,
  tags,
  active,
  approved,
  isActive,
  isUserAdmin,
}) => {
  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === _id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row className="post-list-group-item">
        {isUserAdmin && (
          <Col xs={1}>
            <CheckCircle
              size={18}
              onClick={onApproveClick}
              style={{ color: approved ? "green" : "#495057" }}
            />
            <Trash
              size={18}
              onClick={onDeleteClick}
              style={{ color: active ? "#FF586B" : "#495057" }}
            />
          </Col>
        )}
        <Col xs={11} onClick={onPostClick}>
          <p className="mb-0 post-title">{isActive ? <s>{title}</s> : title}</p>
          <p className="mb-0 font-small-3">
            {tags.map((tag, index) => (
              <span
                key={`${tag}_${index}`}
                className={
                  "badge " +
                  (tag === "FRANQUICIAS" ? "badge-info" : "") +
                  (tag === "CULTURA" ? "badge-danger" : "") +
                  (tag === "SALUD" ? "badge-success" : "") +
                  (tag === "EMPRENDIMIENTO" ? "badge-success" : "") +
                  (tag === "ENTRETENIMIENTO" ? "badge-warning" : "") +
                  (tag === "TECNOLOGÍA" ? "badge-success" : "") +
                  (tag === "TECNOLOGIA" ? "badge-success" : "") +
                  (tag === "DEPORTE" ? "badge-info" : "") +
                  (tag === "CINE" ? "badge-purple" : "") +
                  (tag === "EVENTOS" ? "badge-pink" : "") +
                  (tag === "EDUCACIÓN" ? "badge-pink" : "") +
                  (tag === "EDUCACION" ? "badge-purple" : "") +
                  " mr-1"
                }
              >
                {tag}
              </span>
            ))}
          </p>
        </Col>
      </Row>
    </li>
  );
};

Post.propTypes = {
  _id: PropTypes.string,
  title: PropTypes.string,
  owner_id: PropTypes.string,
  slug: PropTypes.string,
  summary: PropTypes.string,
  text: PropTypes.string,
  publish_date: PropTypes.string,
  tags: PropTypes.array,
  images: PropTypes.array,
  approved: PropTypes.bool,
  starred: PropTypes.bool,
  deleted: PropTypes.bool,
  isActive: PropTypes.bool,
  onStarredClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onPostClick: PropTypes.func,
};

export default Post;
