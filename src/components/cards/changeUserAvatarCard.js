import React, { useState, PureComponent } from "react";
import { Row, Col, Card, CardBody, Button, CardLink } from "reactstrap";
import PropTypes from "prop-types";

import * as Icon from "react-feather";

//Component specific chart CSS
import "../../assets/scss/components/cards/userStatisticChartCard.scss";

class ChangeUserAvatarCard extends PureComponent {
  constructor(props) {
    super(props);
    this.image = {
      preview: this.props.currentImageUrl,
      raw: "",
    };
  }

  handleChange = (e) => {
    if (e.target.files.length) {
      this.setState({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0],
      });
    }
  };

  handleUpload = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("image", this.image.raw);

    await fetch("YOUR_URL", {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
      },
      body: formData,
    });
  };

  render() {
    return (
      <Card>
        <CardBody>
          <Row className="d-flex mb-3 py-2">
            <Col sm="12" className="text-center">
              <label htmlFor="upload-button">
                {this.image.preview ? (
                  <img
                    src={this.image.preview}
                    className="bg-danger width-100 rounded-circle img-fluid mb-4"
                  />
                ) : (
                  <></>
                )}
              </label>
            </Col>
            <Col sm="12" className="text-center">
              <input
                type="file"
                id="upload-button"
                style={{ display: "none" }}
                onChange={this.handleChange}
              />
              {/*}   <Button className="btn btn-outline-danger" onClick={this.handleUpload}>Subir Foto</Button>*/}
              <CardLink
                onClick={this.handleUpload}
                onChange={this.handleChange}
                href="#"
              >
                Sube tu avatar
              </CardLink>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}

ChangeUserAvatarCard.propTypes = {
  currentImageUrl: PropTypes.string,
};

export default ChangeUserAvatarCard;
