import React from "react";
import PropTypes from "prop-types";
import Todo from "./todo";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useDispatch } from "react-redux";
import { destroyTodo, updateTodo } from "../../redux/actions/todo/todoActions";

const TodoList = ({
  todo,
  active,
  toggleTodo,
  toggleStarredTodo,
  togglePriorityTodo,
  deleteTodo,
  todoDetails,
}) => {
  const dispatch = useDispatch();
  return (
    <div className="todo-app-list">
      <PerfectScrollbar>
        <div id="users-list">
          <ul className="list-group">
            {todo.map((todo) => (
              <Todo
                key={todo._id}
                active={active}
                {...todo}
                onCheckboxChange={() => {
                  dispatch(
                    updateTodo(todo._id, "completed", !todo.completed, todo)
                  );
                  toggleTodo(todo._id);
                }}
                onStarredClick={() => {
                  dispatch(
                    updateTodo(todo._id, "starred", !todo.starred, todo)
                  );
                  toggleStarredTodo(todo._id);
                }}
                onPriorityClick={() => {
                  dispatch(
                    updateTodo(todo._id, "priority", !todo.priority, todo)
                  );
                  togglePriorityTodo(todo._id);
                }}
                onDeleteClick={() => {
                  dispatch(destroyTodo(todo._id));
                  //deleteTodo(todo._id);
                }}
                onTodoClick={() => todoDetails(todo._id)}
              />
            ))}
          </ul>
        </div>
      </PerfectScrollbar>
    </div>
  );
};

TodoList.prototype = {
  todo: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      full_name: PropTypes.string.isRequired,
      image: PropTypes.string,
      task: PropTypes.string,
      start_date: PropTypes.number.isRequired,
      due_date: PropTypes.number.isRequired,
      project: PropTypes.string,
      priority: PropTypes.bool.isRequired,
      tag: PropTypes.bool.isRequired,
      comments: PropTypes.string,
      completed: PropTypes.bool.isRequired,
      starred: PropTypes.bool.isRequired,
      deleted: PropTypes.bool.isRequired,
    }).isRequired
  ).isRequired,
  toggleStarredTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  todoDetails: PropTypes.func.isRequired,
};
export default TodoList;
