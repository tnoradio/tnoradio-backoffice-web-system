import React, { useEffect } from "react";
import { Label, Row, Col, CustomInput } from "reactstrap";
import PropTypes from "prop-types";
import { Star, Trash } from "react-feather";
import { useDispatch } from "react-redux";
import { getUserImage } from "../../redux/actions/users/fetchUserImage";

const Todo = ({
  onCheckboxChange,
  onTodoClick,
  onStarredClick,
  onPriorityClick,
  onDeleteClick,
  _id,
  task,
  full_name,
  completed,
  image,
  project,
  starred,
  priority,
  tags,
  active,
}) => {
  const dispatch = useDispatch();
  const [data, setData] = React.useState(""); //imageData
  var Buffer = require("buffer").Buffer;
  let imageFile, imageData;

  useEffect(async () => {
    imageFile = await dispatch(getUserImage("profile", image));
    imageData =
      imageFile !== undefined
        ? Buffer.from(imageFile.file.data, "binary").toString("base64")
        : "";
    setData(imageData);
  }, [_id]);
  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === _id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row className="todo-list-group-item">
        <Col xl={1} xs={1}>
          <Label check>
            <CustomInput
              type="checkbox"
              onChange={onCheckboxChange}
              id={"exampleCustomCheckbox-" + _id}
              defaultChecked={completed}
            />
          </Label>
        </Col>
        <Col xl={1} xs={1}>
          <Star
            size={20}
            onClick={onStarredClick}
            style={{ color: starred ? "#FFC107" : "#495057" }}
          />
          <Trash
            size={20}
            onClick={onDeleteClick}
            style={{ color: priority ? "#FF586B" : "#495057" }}
          />
        </Col>
        <Col xl={10} xs={9} onClick={onTodoClick}>
          <p className="mb-0 text-truncate">
            {completed ? <s>{task}</s> : task}
            <img
              src={
                data !== ""
                  ? `data:image/jpeg;base64,${data}`
                  : "https://randomuser.me/api/portraits/lego/1.jpg"
              }
              className="rounded-circle width-50 float-right d-none d-xl-block"
              alt={full_name}
            />
          </p>
          <p className="mb-0 font-small-3">
            {tags?.map((tag) => (
              <span
                className={
                  "badge " +
                  (tag === "Eventos" ? "badge-info" : "") +
                  (tag === "Edición" ? "badge-warning" : "") +
                  (tag === "Cobranzas" ? "badge-success" : "") +
                  (tag === "Ventas" ? "badge-info" : "") +
                  (tag === "Producción" ? "badge-warning" : "") +
                  (tag === "Servicio Técnico" ? "badge-success" : "") +
                  (tag === "Promociones" ? "badge-info" : "") +
                  " mr-1"
                }
              >
                {tag}
              </span>
            ))}
          </p>
        </Col>
      </Row>
    </li>
  );
};

Todo.propTypes = {
  task: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  project: PropTypes.string.isRequired,
  starred: PropTypes.bool.isRequired,
  onCheckboxChange: PropTypes.func.isRequired,
  onStarredClick: PropTypes.func.isRequired,
  onPriorityClick: PropTypes.func.isRequired,
  onTodoClick: PropTypes.func.isRequired,
};

export default Todo;
