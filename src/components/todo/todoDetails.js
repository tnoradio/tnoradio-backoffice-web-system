import React, { Fragment, useEffect } from "react";
import {
  Row,
  Col,
  Media,
  Button,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Label,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import fetchUserImage from "../../redux/actions/users/fetchUserImage";
import AddTag from "../../containers/todo/addTag";
import { Tag } from "react-feather";
import moment from "moment";
import { destroyTodo, updateTodo } from "../../redux/actions/todo/todoActions";

const TodoDetails = ({
  selectedTodo,
  toggleStarredTodo,
  togglePriorityTodo,
  toggleCompleteTodo,
  onEditClick,
  editTodoFlag,
  onChange,
  team,
}) => {
  const dispatch = useDispatch();
  const [data, setData] = React.useState({}); //imageData
  const [toggleTag, setToggleTag] = React.useState(false);
  const [tags, addTag] = React.useState([]);
  const Buffer = require("buffer").Buffer;
  let state = useSelector((state) => state);

  useEffect(() => {
    if (
      state.userApp.fetchUserImage.userImage &&
      state.userApp.fetchUserImage.userImage.file &&
      state.userApp.fetchUserImage.userImage.file.data
    )
      setData(
        (data) =>
          (data = Buffer.from(
            state.userApp.fetchUserImage.userImage.file.data,
            "binary"
          ).toString("base64"))
      );
  }, [state.userApp.fetchUserImage]);

  useEffect(() => {
    if (selectedTodo?.owner_id) {
      const slug = team.find(
        (owner) => owner._id === selectedTodo.owner_id
      ).slug;
      dispatch(fetchUserImage("profile", slug));

      if (selectedTodo?.tags) {
        addTag(() => [...selectedTodo.tags]);
      }
    }
  }, [selectedTodo]);

  useEffect(() => {}, [tags]);

  return (
    <>
      <Fragment>
        {selectedTodo ? (
          <div className="todo-app-content-detail">
            <Row>
              <Col className="">
                <Icon.Star
                  size={20}
                  className="my-2 mx-1"
                  onClick={() => {
                    toggleStarredTodo(selectedTodo._id);
                    dispatch(
                      updateTodo(
                        selectedTodo._id,
                        "starred",
                        !selectedTodo.starred,
                        selectedTodo
                      )
                    );
                  }}
                  style={{
                    color: selectedTodo.starred ? "#FFC107" : "#495057",
                  }}
                />
                <Icon.AlertCircle
                  size={20}
                  className="my-2 mx-1"
                  onClick={() => {
                    togglePriorityTodo(selectedTodo._id);
                    dispatch(
                      updateTodo(
                        selectedTodo._id,
                        "priority",
                        !selectedTodo.priority,
                        selectedTodo
                      )
                    );
                  }}
                  style={{
                    color: selectedTodo.priority ? "#FF586B" : "#495057",
                  }}
                />
                <Icon.Check
                  size={20}
                  className="my-2 mx-1"
                  onClick={() => {
                    toggleCompleteTodo(selectedTodo._id);

                    let state = "COMPLETED";
                    if (selectedTodo.completed === true) {
                      selectedTodo.completed = false;
                      state = "TODO";
                    } else {
                      selectedTodo.completed = true;
                    }

                    onChange(selectedTodo._id, "status", state, selectedTodo);
                  }}
                  style={{
                    color: selectedTodo.completed ? "#00C853" : "#78909C",
                  }}
                />
              </Col>
              <Col className="text-right">
                <Button
                  className="btn-outline-success mr-1 mt-1"
                  size="sm"
                  onClick={onEditClick}
                >
                  {editTodoFlag ? (
                    <Icon.Check size={16} />
                  ) : (
                    <Icon.Edit2 size={16} />
                  )}
                </Button>
              </Col>
            </Row>
            <PerfectScrollbar>
              <Row>
                <Col>
                  {editTodoFlag ? (
                    <Input
                      type="text"
                      name="task"
                      id="task"
                      defaultValue={selectedTodo.task}
                      onChange={(e) => {
                        onChange(
                          selectedTodo._id,
                          "task",
                          e.target.value,
                          selectedTodo
                        );
                      }}
                    />
                  ) : (
                    <p className="lead">
                      {selectedTodo.completed === true ? (
                        <s>{selectedTodo.task}</s>
                      ) : (
                        selectedTodo.task
                      )}
                    </p>
                  )}
                </Col>
              </Row>
              <Row className="mt-1">
                <Col>
                  <Row>
                    <Col sm="3">
                      <Media
                        object
                        src={`data:image/jpeg;base64,${data}`}
                        alt="Generic placeholder image"
                        className="img-fluid rounded-circle width-35"
                      />
                    </Col>

                    <Col sm="9">
                      {editTodoFlag ? (
                        <Input
                          type="select"
                          name="owner"
                          id="owner"
                          className="w-80"
                          value={
                            team.find(
                              (owner) => owner._id === selectedTodo.owner_id
                            )._id
                          }
                          onChange={(e) =>
                            onChange(
                              selectedTodo._id,
                              "owner_id",
                              e.target.value,
                              selectedTodo
                            )
                          }
                        >
                          {team.map((user) => (
                            <option key={user._id} value={user._id}>
                              {user.name + " " + user.lastName}
                            </option>
                          ))}
                        </Input>
                      ) : (
                        team.find(
                          (owner) => selectedTodo.owner_id === owner._id
                        ).name +
                        " " +
                        team.find(
                          (owner) => selectedTodo.owner_id === owner._id
                        ).lastName
                      )}
                    </Col>
                  </Row>
                </Col>
                <Col>
                  <Row>
                    <Col sm="3">
                      <Icon.Calendar size={19} />
                    </Col>
                    <Col sm="9" className="text-truncate">
                      {editTodoFlag ? (
                        <Input
                          type="date"
                          className="w-80"
                          name="due_date"
                          id="due_date"
                          defaultValue={selectedTodo.due_date}
                          onChange={(e) =>
                            onChange(
                              selectedTodo._id,
                              "due_date",
                              e.target.value,
                              selectedTodo
                            )
                          }
                        />
                      ) : (
                        moment(selectedTodo.due_date).format("DD/MM/yyyy")
                      )}
                    </Col>
                  </Row>
                </Col>
              </Row>
              <hr style={{ border: "1px solid #e6ecf5" }} />
              <Row>
                <Col>
                  <Label for="comments">Comentarios:</Label>
                  {editTodoFlag ? (
                    <Input
                      type="textarea"
                      rows="4"
                      name="comments"
                      id="comments"
                      defaultValue={selectedTodo.comments}
                      onChange={(e) =>
                        onChange(
                          selectedTodo._id,
                          "comments",
                          e.target.value,
                          selectedTodo
                        )
                      }
                    />
                  ) : (
                    ` ${selectedTodo.comments}`
                  )}
                </Col>
              </Row>
              <hr style={{ border: "1px solid #e6ecf5" }} />
              <Row>
                <Col sm="1">
                  <Icon.Briefcase size={20} />
                </Col>
                <Col sm="5">
                  {editTodoFlag ? (
                    <Input
                      type="select"
                      name="project"
                      id="project"
                      className="w-80"
                      defaultValue={selectedTodo.project}
                      onChange={(e) =>
                        onChange(
                          selectedTodo._id,
                          "project",
                          e.target.value,
                          selectedTodo
                        )
                      }
                    >
                      <option key="PROD" value="PROD">
                        Producción
                      </option>
                      <option key="OPE" value="OPE">
                        Operación
                      </option>
                      <option key="ADMIN" value="ADMIN">
                        Administración
                      </option>
                      <option key="SALES" value="SALES">
                        Mercadeo y Ventas
                      </option>
                    </Input>
                  ) : (
                    selectedTodo.project
                  )}
                </Col>
                <Col sm="1">
                  <Icon.Activity size={20} />
                </Col>
                <Col sm="5">
                  {editTodoFlag ? (
                    <Input
                      type="select"
                      name="status"
                      id="status"
                      className="w-80"
                      defaultValue={selectedTodo.status}
                      onChange={(e) => {
                        if (
                          e.target.value === "COMPLETED" &&
                          selectedTodo.completed === false
                        ) {
                          toggleCompleteTodo(selectedTodo._id);
                        }
                        if (
                          e.target.value !== "COMPLETED" &&
                          selectedTodo.completed === true
                        ) {
                          toggleCompleteTodo(selectedTodo._id);
                        }
                        onChange(
                          selectedTodo._id,
                          "status",
                          e.target.value,
                          selectedTodo
                        );
                      }}
                    >
                      <option key="TODO" value="TODO">
                        Abierta
                      </option>
                      <option key="IN_PROGRESS" value="IN_PROGRESS">
                        En Curso
                      </option>
                      <option key="FOR_REVIEW" value="FOR_REVIEW">
                        Para Revisión
                      </option>
                      <option key="COMPLETED" value="COMPLETED">
                        Completado
                      </option>
                    </Input>
                  ) : selectedTodo.status === "COMPLETED" ? (
                    "Completado"
                  ) : selectedTodo.status === "IN_PROGRESS" ? (
                    "En curso"
                  ) : selectedTodo.status === "FOR_REVIEW" ? (
                    "Para revisión"
                  ) : (
                    "Abierta"
                  )}
                </Col>
              </Row>
              <hr style={{ border: "1px solid #e6ecf5" }} />
              <Row>
                <Col sm="1">
                  <Icon.Tag size={20} />
                </Col>
                <Col sm="5">
                  {editTodoFlag ? (
                    <>
                      <Row>
                        <p className="mb-0 font-small-3">
                          {tags.map((tag) => (
                            <span
                              className={
                                "badge " +
                                (tag === "Eventos" ? "badge-info" : "") +
                                (tag === "Edición" ? "badge-warning" : "") +
                                (tag === "Cobranzas" ? "badge-success" : "") +
                                (tag === "Ventas" ? "badge-info" : "") +
                                (tag === "Producción" ? "badge-warning" : "") +
                                (tag === "Servicio Técnico"
                                  ? "badge-success"
                                  : "") +
                                (tag === "Promociones" ? "badge-info" : "") +
                                " mr-1"
                              }
                            >
                              {tag}
                            </span>
                          ))}
                        </p>
                      </Row>
                      <br></br>
                      <Row>
                        <Button
                          className="btn-square"
                          size="md"
                          outline
                          color="primary"
                          onClick={() =>
                            setToggleTag((toggleTag) => !toggleTag)
                          }
                        >
                          <Tag size={16} color="#009DA0" />
                          {"  "}
                          Agregar Etiqueta
                        </Button>
                      </Row>
                      <Modal
                        isOpen={toggleTag}
                        toggle={() => setToggleTag((toggleTag) => !toggleTag)}
                      >
                        <ModalHeader>
                          <Label>Agregar Etiqueta</Label>
                        </ModalHeader>
                        <ModalBody>
                          <AddTag addTag={addTag} setToggleTag={setToggleTag} />
                        </ModalBody>
                      </Modal>
                    </>
                  ) : (
                    <p className="mb-0 font-small-3">
                      {tags.map((tag) => (
                        <span
                          className={
                            "badge " +
                            (tag === "Eventos" ? "badge-info" : "") +
                            (tag === "Edición" ? "badge-warning" : "") +
                            (tag === "Cobranzas" ? "badge-success" : "") +
                            (tag === "Ventas" ? "badge-info" : "") +
                            (tag === "Producción" ? "badge-warning" : "") +
                            (tag === "Servicio Técnico"
                              ? "badge-success"
                              : "") +
                            (tag === "Promociones" ? "badge-info" : "") +
                            " mr-1"
                          }
                        >
                          {tag}
                        </span>
                      ))}
                    </p>
                  )}
                </Col>
              </Row>
              <hr style={{ border: "1px solid #e6ecf5" }} />
              <Row>
                <Col>
                  <p className="font-small-2 mb-0">
                    <span className="text-bold-400">
                      {selectedTodo.full_name}
                    </span>{" "}
                    creó la tarea el{" "}
                    {moment(selectedTodo.due_date).format("DD/MM/yyyy")}.
                  </p>
                  <p className="font-small-2 mb-0">
                    <span className="text-bold-400">
                      {selectedTodo.full_name}
                    </span>{" "}
                    la agregó a {selectedTodo.project}.
                  </p>
                  <p className="font-small-2 mb-0">
                    <span className="text-bold-400">
                      {selectedTodo.full_name}
                    </span>{" "}
                    la etiquetó {selectedTodo.tag}.
                  </p>
                </Col>
              </Row>
            </PerfectScrollbar>
          </div>
        ) : (
          <div className="row h-100">
            <div className="col-sm-12 my-auto">
              <div className="text-center">
                <Icon.CheckSquare size={84} color="#ccc" className="pb-3" />
                <h4>Selecciona una tarea para ver los detalles.</h4>
              </div>
            </div>
          </div>
        )}
      </Fragment>
    </>
  );
};

TodoDetails.prototype = {
  selectedTodo: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      full_name: PropTypes.string.isRequired,
      image: PropTypes.string,
      task: PropTypes.string,
      start_date: PropTypes.number.isRequired,
      due_date: PropTypes.number.isRequired,
      project: PropTypes.string,
      priority: PropTypes.bool.isRequired,
      tag: PropTypes.bool.isRequired,
      comments: PropTypes.string,
      completed: PropTypes.bool.isRequired,
      starred: PropTypes.bool.isRequired,
      deleted: PropTypes.bool.isRequired,
    }).isRequired
  ).isRequired,
  starred: PropTypes.bool.isRequired,
  priority: PropTypes.bool.isRequired,
  toggleStarredTodo: PropTypes.func.isRequired,
  togglePriorityTodo: PropTypes.func.isRequired,
};
export default TodoDetails;
