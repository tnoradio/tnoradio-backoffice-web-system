import React from "react";
import FilterLink from "../../containers/todo/todoFilterLink";
import { todoVisiblityFilter } from "../../redux/actions/todo/todoActions";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";

class TodoFilter extends React.Component {
  render() {
    return (
      <div className="todo-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="todo-app-sidebar-content">
            <div className="todo-app-menu">
              <ul className="list-group list-group-messages">
                <FilterLink filter={todoVisiblityFilter.SHOW_ALL}>
                  <Icon.Users size={18} className="mr-1" /> Todas
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Áreas
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={todoVisiblityFilter.ADMIN_TODO}>
                  <Icon.Briefcase size={18} className="mr-1" /> Administración
                </FilterLink>
                <FilterLink filter={todoVisiblityFilter.OPE_TODO}>
                  <Icon.Briefcase size={18} className="mr-1" /> Operaciones
                </FilterLink>
                <FilterLink filter={todoVisiblityFilter.PROD_TODO}>
                  <Icon.Briefcase size={18} className="mr-1" /> Producción
                </FilterLink>
                <FilterLink filter={todoVisiblityFilter.SALES_TODO}>
                  <Icon.Briefcase size={18} className="mr-1" /> Ventas
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Filters
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={todoVisiblityFilter.STARRED_TODO}>
                  <Icon.Star size={18} className="mr-1" /> Starred
                </FilterLink>
                <FilterLink filter={todoVisiblityFilter.PRIORITY_TODO}>
                  <Icon.AlertCircle size={18} className="mr-1" /> Urgente
                </FilterLink>
                <FilterLink filter={todoVisiblityFilter.COMPLETED_TODO}>
                  <Icon.Check size={18} className="mr-1" /> Listo
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Tags
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink
                  filter={todoVisiblityFilter.SALES_TODO}
                  departmentValue="ADMIN"
                >
                  <Icon.Tag size={18} className="mr-1 info" />
                  Ventas
                </FilterLink>
                <FilterLink
                  filter={todoVisiblityFilter.POST_PRODUCTION_TODO}
                  departmentValue="PROD"
                >
                  <Icon.Tag size={18} className="mr-1 warning" />
                  Edición
                </FilterLink>
                <FilterLink
                  filter={todoVisiblityFilter.EVENTS_TODO}
                  departmentValue="PROD"
                >
                  <Icon.Tag size={18} className="mr-1 success" />
                  Eventos
                </FilterLink>
                <FilterLink
                  filter={todoVisiblityFilter.COLLECTIONS_TODO}
                  departmentValue="ADMIN"
                >
                  <Icon.Tag size={18} className="mr-1 info" />
                  Cobranzas
                </FilterLink>
                <FilterLink
                  filter={todoVisiblityFilter.TECHNICAL_SUPPORT_TODO}
                  departmentValue="OPE"
                >
                  <Icon.Tag size={18} className="mr-1 warning" />
                  Servicio Técnico
                </FilterLink>
                <FilterLink
                  filter={todoVisiblityFilter.PROM_TODO}
                  departmentValue="PROD"
                >
                  <Icon.Tag size={18} className="mr-1 success" />
                  Promociones
                </FilterLink>
              </ul>
            </div>
          </div>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default TodoFilter;
