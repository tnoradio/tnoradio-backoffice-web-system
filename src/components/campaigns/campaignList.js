import React from "react";
import PropTypes from "prop-types";
import Campaign from "./campaign";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useDispatch } from "react-redux";
import { updateCampaign } from "../../redux/actions/campaigns";

const CampaignList = (props) => {
  const {
    active,
    campaigns,
    toggleStarredCampaign,
    deleteCampaign,
    campaignDetails,
  } = props;
  const dispatch = useDispatch();
  return (
    <div className="post-app-list">
      <PerfectScrollbar>
        <div id="campaigns-list">
          <ul className="list-group">
            {campaigns.map((campaign) => {
              return (
                <Campaign
                  key={campaign._id}
                  active={active}
                  {...campaign}
                  onStarredClick={() => {
                    dispatch(
                      updateCampaign(
                        campaign._id,
                        "starred",
                        !campaign.starred,
                        campaign
                      )
                    );
                    toggleStarredCampaign(campaign._id);
                  }}
                  onDeleteClick={() => deleteCampaign(campaign._id)}
                  onCampaignClick={() => campaignDetails(campaign._id)}
                />
              );
            })}
          </ul>
        </div>
      </PerfectScrollbar>
    </div>
  );
};

CampaignList.prototype = {
  campaigns: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      channel: PropTypes.array,
      price: PropTypes.number,
      description: PropTypes.string,
      date: PropTypes.string,
      startTime: PropTypes.string,
      endTime: PropTypes.string,
      status: PropTypes.string,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
  toggleStarredCampaign: PropTypes.func.isRequired,
  deleteCampaign: PropTypes.func.isRequired,
  campaignDetails: PropTypes.func.isRequired,
};
export default CampaignList;
