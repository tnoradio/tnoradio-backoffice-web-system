import React from "react";
import { Modal, ModalHeader } from "reactstrap";
import FilterLink from "../../containers/campaigns/campaignFilterLink";
import { campaignVisibilityFilter } from "../../redux/actions/campaigns";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";
import AddCampaignWizard from "../../containers/campaigns/addCampaign/addCampaignWizard";

class CampaignFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  render() {
    return (
      <div className="post-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="post-app-sidebar-content">
            <div className="post-app-menu">
              <div className="form-group form-group-compose text-center">
                <button
                  type="button"
                  className="btn btn-raised btn-danger btn-block my-2 shadow-z-2"
                  onClick={this.toggle}
                >
                  <Icon.Edit size={18} className="mr-1" /> Crear Campaña
                </button>
              </div>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Flitrar
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={campaignVisibilityFilter?.SHOW_ALL}>
                  <Icon.Cast size={18} className="mr-1" /> Campañas
                </FilterLink>
                <FilterLink filter={campaignVisibilityFilter?.DELETED_CAMPAIGN}>
                  <Icon.Trash
                    size={18}
                    style={{ color: "#FF586B" }}
                    className="mr-1"
                  />{" "}
                  Eliminadas
                </FilterLink>
                <FilterLink filter={campaignVisibilityFilter?.STARRED_CAMPAIGN}>
                  <Icon.Star
                    size={18}
                    style={{ color: "#FFC107" }}
                    className="mr-1"
                  />{" "}
                  Destacadas
                </FilterLink>

                <FilterLink
                  filter={campaignVisibilityFilter?.ACTIVE_CAMPAIGN}
                  genreValue="ACTIVE"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Activas
                </FilterLink>
              </ul>
            </div>
          </div>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={this.props.className}
            size="xl"
          >
            <ModalHeader toggle={this.toggle}>Crear una Campaña</ModalHeader>
            <AddCampaignWizard />
          </Modal>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default CampaignFilter;
