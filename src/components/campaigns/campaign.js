import React from "react";
import { Row, Col } from "reactstrap";
import PropTypes from "prop-types";
import { Star, Trash } from "react-feather";
import moment from "moment";

const Campaign = ({
  onCampaignClick,
  onStarredClick,
  onDeleteClick,
  _id,
  name,
  starred,
  isActive,
  start_date,
  end_date,
  active,
}) => {
  let startDate = moment(start_date).format("DD/MM/yyyy");
  let endDate = moment(end_date).format("DD/MM/yyyy");

  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === _id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row className="post-list-group-item">
        <Col xs={1}>
          <Star
            size={20}
            onClick={onStarredClick}
            style={{ color: starred ? "#FFC107" : "#495057" }}
          />
        </Col>
        <Col xs={4} onClick={onCampaignClick}>
          <p className="mb-0 text-truncate">{name}</p>
        </Col>
        <Col xs={2} onClick={onCampaignClick}>
          <p className="mb-0 text-truncate">
            {active ? <span>{startDate}</span> : <span>{startDate} </span>}
          </p>
        </Col>
        <Col xs={2} onClick={onCampaignClick}>
          <p className="mb-0 text-truncate">{endDate}</p>
        </Col>
        <Col xs={1}>
          <Trash
            size={20}
            onClick={onDeleteClick}
            style={{ color: active ? "#FF586B" : "#495057" }}
          />
        </Col>
      </Row>
    </li>
  );
};

Campaign.propTypes = {
  _id: PropTypes.string,
  price: PropTypes.number,
  owner_id: PropTypes.string,
  start_date: PropTypes.string,
  end_date: PropTypes.string,
  channel: PropTypes.array,
  starred: PropTypes.bool,
  deleted: PropTypes.bool,
  isActive: PropTypes.bool,
  owner: PropTypes.string,
  payments: PropTypes.array,
  onStarredClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onCampaignClick: PropTypes.func,
};

export default Campaign;
