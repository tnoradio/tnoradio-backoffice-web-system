import React, { Fragment, useState, useEffect } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  ListGroup,
  ListGroupItem,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  Modal,
  CustomInput,
} from "reactstrap";
import dateFormat from "dateformat";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import moment from "moment";
import { fetchPayments } from "../../redux/actions/payments";
import AddPayment from "../../containers/payment/addPayment";
import { useHistory } from "react-router-dom";

const CampaignDetails = ({
  selectedCampaign,
  onEditClick,
  editCampaignFlag,
  onChange,
}) => {
  const state = useSelector((state) => state);
  const [paymentsModal, setPaymentsModal] = React.useState(false);
  const [campaignPayments, setCampaignPayments] = React.useState([]);
  const [userPayments, setUserPayments] = React.useState([]);
  const [paymentsData, setPaymentsData] = React.useState([]);
  const dispatch = useDispatch();
  let history = useHistory();

  const options = [
    { value: false, label: "Inactiva" },
    { value: true, label: "Activa" },
  ];

  const handleStatusChange = (status) => {
    onChange(selectedCampaign._id, "isActive", status);
  };

  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  const addPaymentSubmit = (paymentId) => {
    var payments = campaignPayments;
    payments.push(paymentId);
    setPaymentsModal(false);
    setCampaignPayments(payments);
    onChange(selectedCampaign._id, "payments", payments);
  };

  const onDeletePaymentClick = (payment) => {
    var paymentsAux = [];
    campaignPayments.map((item) => {
      if (item._id !== payment._id) {
        paymentsAux.push(item);
      }
    });
    setCampaignPayments(paymentsAux);
    onChange(selectedCampaign._id, "payments", paymentsAux);
  };
  useEffect(() => {
    dispatch(fetchPayments());
    // Safe to add dispatch to the dependencies array
  }, [dispatch]);
  useEffect(() => {
    let payments;

    if (selectedCampaign?.payments) {
      payments = new Date(selectedCampaign?.payments);
      setCampaignPayments(payments);
    }
    if (selectedCampaign?.payments) {
      let userPaymentsList = state.paymentApp.payments.filter(
        (payment) => payment.owner_id === selectedCampaign.owner_id
      );

      setUserPayments(userPaymentsList);

      let paymentsDataList = userPayments.filter((userPayment) =>
        selectedCampaign.payments.find((payment) => userPayment._id === payment)
      );

      setPaymentsData(paymentsDataList);
    }
  }, [selectedCampaign]);

  return (
    <Fragment>
      {selectedCampaign ? (
        <div className="contact-app-content-detail">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editCampaignFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media heading>{selectedCampaign.name}</Media>

            <Table responsive borderless size="md" className="mt-4">
              <tbody style={{ height: "600px" }}>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Nombre</td>
                  <td className="col-9">
                    {editCampaignFlag ? (
                      <Input
                        type="text"
                        name="name"
                        id="name"
                        defaultValue={selectedCampaign.name}
                        onChange={(e) =>
                          onChange(selectedCampaign._id, "name", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedCampaign.name
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Monto</td>
                  <td className="col-5">
                    {editCampaignFlag ? (
                      <Input
                        type="text"
                        name="price"
                        id="price"
                        disabled={!isUserAdmin}
                        defaultValue={selectedCampaign.price}
                        onChange={(e) =>
                          onChange(
                            selectedCampaign._id,
                            "price",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      `: ${selectedCampaign.price}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Fecha de Inicio</td>
                  <td className="col-5">
                    {editCampaignFlag ? (
                      <Input
                        type="date"
                        name="start_date"
                        id="start_date"
                        value={dateFormat(
                          selectedCampaign.start_date,
                          "yyyy-mm-dd"
                        )}
                        onChange={(e) => {
                          console.log(e.target.value);
                          onChange(
                            selectedCampaign._id,
                            "start_date",
                            e.target.value
                          );
                        }}
                      />
                    ) : (
                      dateFormat(selectedCampaign.start_date, "dd-mm-yyyy")
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Fecha Fin</td>
                  <td className="col-5">
                    {editCampaignFlag ? (
                      <Input
                        type="date"
                        name="end_date"
                        id="end_date"
                        value={dateFormat(
                          selectedCampaign.end_date,
                          "yyyy-mm-dd"
                        )}
                        onChange={(e) =>
                          onChange(
                            selectedCampaign._id,
                            "end_date",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      dateFormat(selectedCampaign.end_date, "dd-mm-yyyy")
                    )}
                  </td>
                </tr>

                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Estatus</td>
                  <td className="col-5">
                    {editCampaignFlag ? (
                      <Select
                        options={options}
                        disabled={!isUserAdmin}
                        onChange={(e) => handleStatusChange(e.value)}
                        value={options.filter(
                          (option) => option.value === selectedCampaign.isActive
                        )}
                      />
                    ) : (
                      `: ${selectedCampaign.isActive ? "Activa" : "Inactiva"}`
                    )}
                  </td>
                </tr>
                {/*}   <tr className="d-flex">
                  <td className="col-3 text-bold-400">Pagos</td>
                  <td className={editCampaignFlag ? "col-5" : "col-7"}>
                    {editCampaignFlag ? (
                      <>
                        <Row>
                          <Col>
                            <ListGroup>
                              {paymentsData?.map((payment) => {
                                return (
                                  <ListGroupItem key={payment._id}>
                                    <Row>
                                      <Col xs={10}>
                                        <span>
                                          {moment(payment.payment_date).format(
                                            "DD/MM/yyyy"
                                          )}
                                        </span>
                                        {"  "}
                                        <span>{payment.amount}</span>{" "}
                                      </Col>
                                      <Col xs={2}>
                                        <Icon.Trash
                                          size={18}
                                          className="float-right mt-1 mb-2 width-25 d-block"
                                          style={{
                                            color: "#FF586B",
                                            cursor: "pointer",
                                          }}
                                          onClick={() => {
                                            onDeletePaymentClick(payment);
                                          }}
                                        />
                                      </Col>
                                    </Row>
                                  </ListGroupItem>
                                );
                              })}
                            </ListGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs={12}>
                            <br></br>
                            <Button
                              className="btn-square float-right"
                              size="sm"
                              outline
                              color="primary"
                              onClick={() => history.push("/payments")}
                            >
                              <Icon.DollarSign size={16} color="#009DA0" />
                              {"   "}
                              Agregar Pago
                            </Button>
                          </Col>
                        </Row>
                        <Modal
                          isOpen={paymentsModal}
                          toggle={() => setPaymentsModal(!paymentsModal)}
                        >
                          <ModalHeader>
                            <Label>Agregar Pago</Label>
                          </ModalHeader>
                          <ModalBody>
                            <AddPayment></AddPayment>
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              className="btn-square"
                              outline
                              color="primary"
                              onClick={addPaymentSubmit}
                              type="submit"
                            >
                              Agregar
                            </Button>{" "}
                          </ModalFooter>
                        </Modal>
                      </>
                    ) : (
                      <Row>
                        <Col>
                          <ListGroup>
                            {paymentsData.map((payment) => {
                              return (
                                <ListGroupItem key={payment._id}>
                                  <Row>
                                    <Col xs={10}>
                                      <span>
                                        Fecha:{"       "}
                                        {moment(payment.payment_date).format(
                                          "DD/MM/yyyy"
                                        )}
                                      </span>
                                      {"       "}|{"       "}
                                      <span>Monto: {payment.amount}</span>{" "}
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              );
                            })}
                          </ListGroup>
                        </Col>
                      </Row>
                    )}
                  </td>
                          </tr>*/}
              </tbody>
            </Table>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione una pauta para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

CampaignDetails.prototype = {
  selectedCampaign: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      amount: PropTypes.number,
      owner_id: PropTypes.string,
      show_owner: PropTypes.number,
      concept: PropTypes.string,
      currency: PropTypes.string,
      campaign_date: PropTypes.string,
      receipts: PropTypes.array,
      reconciled: PropTypes.bool,
      isActive: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
};
export default CampaignDetails;
