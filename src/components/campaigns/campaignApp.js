import React from "react";

import VisibleCampaignList from "../../containers/campaigns/visibleCampaignList";

const App = () => (
  <div>
    <VisibleCampaignList />
  </div>
);

export default App;
