import { FormGroup, CustomInput } from "reactstrap";
import { useState } from "react";

const ShowTarget = ({ onChange, selectedShow }) => {
  const [target, setTarget] = useState(selectedShow?.target || []);
  const handleTargetChange = (selected_target) => {
    let new_targets = target;
    if (target?.includes(selected_target) === false) {
      new_targets.push(selected_target);
    } else {
      new_targets = target?.filter((target) => target !== selected_target);
    }
    setTarget(new_targets);
    onChange(selectedShow.id, "target", new_targets);
  };
  return (
    <>
      <FormGroup check className="px-0">
        <CustomInput
          type="checkbox"
          id="alto"
          label="Alto"
          defaultChecked={selectedShow.target?.some(
            (target) => target === "alto"
          )}
          onChange={() => handleTargetChange("alto")}
        />
      </FormGroup>
      <FormGroup check className="px-0">
        <CustomInput
          type="checkbox"
          id="medio"
          label="Medio"
          defaultChecked={selectedShow.target?.some(
            (target) => target === "medio"
          )}
          onChange={() => handleTargetChange("medio")}
        />
      </FormGroup>
      <FormGroup check className="px-0">
        <CustomInput
          type="checkbox"
          id="bajo"
          label="Bajo"
          defaultChecked={selectedShow.target?.some(
            (target) => target === "bajo"
          )}
          onChange={() => handleTargetChange("bajo")}
        />
      </FormGroup>
    </>
  );
};

export default ShowTarget;
