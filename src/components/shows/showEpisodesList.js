import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Tooltip,
  ModalFooter,
} from "reactstrap";
import { Edit2, Eye, PlusSquare, ArrowRight } from "react-feather";
import { episodesUrl, miniaturesUrl } from "../../config";
import { useState } from "react";
import ShowEpisodeDetails from "./showEpisodeDetails";
import AddShowEpisode from "../../containers/shows/addShowEpisode";
import { addSeason } from "../../redux/actions/shows/addSeason";
import MyDropzone from "../shared/MyDropzone";
import Accordion from "../accordion/Accordion";
import { updateShowEpisode } from "../../redux/actions/shows/updateShowEpisode";

const ShowEpisodesList = ({
  seasons,
  showEditFlag,
  editShowFlag,
  episodeTags,
  onUpdateSeason,
  showSlug,
  showId,
}) => {
  const [selectedEpisodeId, setSelectedEpisodeId] = useState(null);
  const [selectedEpisodeState, setSelectedEpisodeState] = useState(null);
  const [modalEpisodes, setModalEpisodes] = useState(false);
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const toggleTooltip = () => {
    setTooltipOpen(!tooltipOpen);
  };

  const onEditClick = (episode) => {
    setSelectedEpisodeId(episode.id);
    setSelectedEpisodeState(episode);
  };

  const toggleModalEpisodes = () => {
    setModalEpisodes(!modalEpisodes);
  };

  const onEpisodeAdd = (episode) => {
    console.log(episode);
    toggleModalEpisodes();

    const episodeSeason = seasons.find(
      (s) => s.season_number === episode.season
    );

    episodeSeason?.episodes
      ? episodeSeason.episodes.push(episode)
      : (episodeSeason.episodes = [episode]);

    const updatedSeasons = seasons.map((s) =>
      s.id === episodeSeason.id ? episodeSeason : s
    );

    onUpdateSeason(updatedSeasons);
  };

  const onAddSeason = (season) => {
    const addedSeason = addSeason(season);
    const updatedSeasons = [...seasons, addedSeason];
    onUpdateSeason(updatedSeasons);
  };

  const onSaveChangesClick = async (episode, season) => {
    setSelectedEpisodeId(null);
    const updatedEpisode = await updateShowEpisode(episode);
    const updatedEpisodes = season.episodes.map((ep) =>
      ep.id === updatedEpisode.id ? updatedEpisode : ep
    );

    const updatedSeason = {
      ...season,
      episodes: updatedEpisodes,
    };

    const updatedSeasons = seasons.map((s) =>
      s.id === updatedSeason.id ? updatedSeason : s
    );

    onUpdateSeason(updatedSeasons);
  };

  return (
    <>
      {showEditFlag && (
        <div className="">
          <Button
            className="btn-outline-primary"
            outline
            size="sm"
            onClick={toggleModalEpisodes}
            id={`Tooltip-AddEpisode`}
          >
            Agregar Episodio <PlusSquare size={20} />
          </Button>
          <Tooltip
            placement={"top"}
            isOpen={tooltipOpen}
            target={`Tooltip-AddEpisode`}
            toggle={toggleTooltip}
          >
            Agregar Episodio
          </Tooltip>
          <Modal isOpen={modalEpisodes} toggle={toggleModalEpisodes} size="lg">
            <ModalHeader toggle={toggleModalEpisodes}>
              Agregar Episodio
            </ModalHeader>
            <AddShowEpisode
              seasons={seasons}
              episodeTags={episodeTags}
              showSlug={showSlug}
              onAddSeason={onAddSeason}
              onAddEpisode={onEpisodeAdd}
              toggleModalEpisodes={toggleModalEpisodes}
            />
          </Modal>
        </div>
      )}
      <Accordion>
        {seasons.map((season) => (
          <Accordion.AccordionItem
            key={season.id}
            render={() => (
              <div className="accordion-header d-flex justify-content-between mt-2 cursor-pointer primary">
                <h5>Temporada {season.season_number}</h5>
                <div>
                  <Button className="outline-primary" outline size="sm">
                    Ver episodios <ArrowRight size={20} />
                  </Button>
                </div>
              </div>
            )}
          >
            <table className="table table-striped mb-0">
              <tbody>
                {season.episodes?.map((episode) => (
                  <tr key={episode.id} className="p-2">
                    <td className="align-content-center text-center col-1">
                      {episode.episode_number}
                    </td>
                    <td className="align-content-center text-center col-3">
                      <MyDropzone
                        layout="show"
                        style={{ width: "60px", height: "auto" }}
                        image={`${miniaturesUrl}/${showSlug}/showEpisodeMiniature_${episode.id}`}
                        disabled
                      />
                    </td>
                    <td className="align-content-center text-left col-3">
                      <span className="font-weight-bold">{episode.title}</span>
                      <br />
                      <span>{episode.description}</span>
                    </td>
                    <td className="align-content-center text-center col-1">
                      <Button
                        className="align-content-center text-center mb-0 btn-outline-primary"
                        size="sm"
                        href={`${episodesUrl}/${showSlug}/${episode.url}`}
                        target="_blank"
                      >
                        <Eye size={24} />
                      </Button>
                    </td>
                    <td className="align-content-center text-center col-1">
                      {showEditFlag && (
                        <Button
                          className="btn-outline-success mb-0"
                          size="sm"
                          onClick={() => onEditClick(episode)}
                        >
                          <Edit2 size={24} />
                        </Button>
                      )}
                    </td>

                    {selectedEpisodeId === episode.id && (
                      <Modal
                        isOpen={true}
                        toggle={() => setSelectedEpisodeId(null)}
                        size="lg"
                      >
                        <ModalHeader toggle={() => setSelectedEpisodeId(null)}>
                          Editar Episodio
                        </ModalHeader>
                        <ModalBody>
                          <ShowEpisodeDetails
                            seasons={seasons}
                            season={season}
                            onEditSeason={onUpdateSeason}
                            editShowFlag={true}
                            episodeTags={episodeTags}
                            episode={selectedEpisodeState}
                            setSelectedEpisodeState={setSelectedEpisodeState}
                            showSlug={showSlug}
                          />
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            className="btn-outline-success"
                            size="sm"
                            onClick={() =>
                              onSaveChangesClick(selectedEpisodeState, season)
                            }
                          >
                            Guardar
                          </Button>
                        </ModalFooter>
                      </Modal>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          </Accordion.AccordionItem>
        ))}
      </Accordion>
    </>
  );
};

export default ShowEpisodesList;
