import { Row, Col, FormGroup, CustomInput } from "reactstrap";
import { useState } from "react";

const ShowAgeRange = ({ onChange, selectedShow }) => {
  const [age_range, setAgeRanges] = useState(selectedShow?.age_range || []);
  const handleAgeRangeChange = (selected_age_range) => {
    let new_age_range;
    if (age_range?.includes(selected_age_range) === false) {
      new_age_range = age_range;
      new_age_range.push(selected_age_range);
    } else {
      new_age_range = age_range?.filter(isNotAgeRange);

      function isNotAgeRange(age) {
        return age !== selected_age_range;
      }
    }

    setAgeRanges(new_age_range);
    onChange(selectedShow.id, "age_range", new_age_range);
  };
  return (
    <Row>
      <Col xs={6}>
        <FormGroup check className="px-0">
          <CustomInput
            type="checkbox"
            id="18-25"
            label="18-25"
            onChange={() => handleAgeRangeChange("18-25")}
            defaultChecked={selectedShow.age_range?.some(
              (age_range) => age_range === "18-25"
            )}
          />
        </FormGroup>
        <FormGroup check className="px-0">
          <CustomInput
            type="checkbox"
            id="26-35"
            label="26-35"
            onChange={() => handleAgeRangeChange("26-35")}
            defaultChecked={selectedShow.age_range?.some(
              (age_range) => age_range === "26-35"
            )}
          />
        </FormGroup>
      </Col>
      <Col xs={6}>
        <FormGroup check className="px-0">
          <CustomInput
            type="checkbox"
            id="36-45"
            label="36-45"
            onChange={() => handleAgeRangeChange("36-45")}
            defaultChecked={selectedShow.age_range?.some(
              (age_range) => age_range === "36-45"
            )}
          />
        </FormGroup>
        <FormGroup check className="px-0">
          <CustomInput
            type="checkbox"
            id="46 and more"
            label="46 and more"
            onChange={() => handleAgeRangeChange("46 and more")}
            defaultChecked={selectedShow.age_range?.some(
              (age_range) => age_range === "46 and more"
            )}
          />
        </FormGroup>
      </Col>
    </Row>
  );
};
export default ShowAgeRange;
