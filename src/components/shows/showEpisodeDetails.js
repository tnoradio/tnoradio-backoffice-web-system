import { useState, useEffect } from "react";
import { Input, Button, Media } from "reactstrap";
import ImageUploader from "./ImageUploader";
import TagsManager from "./tagsManager";
import Select from "react-select";
import formatGroupLabel from "../common/formatGroupLabel";
import { updateShowEpisode } from "../../redux/actions/shows/updateShowEpisode";
import addTagToEpisode from "../../redux/actions/shows/addTagToEpisode";
import fetchShowFileNames from "../../redux/actions/shows/fetchShowEpisodesFileNames";
import { getEpisodeTags } from "../../redux/actions/shows/getEpisodeTags";
import { episodesUrl, filesUrl, miniaturesUrl } from "../../config";

const ShowEpisodeDetails = ({
  seasons,
  episode,
  season,
  onEditSeason,
  editShowFlag,
  showSlug,
  setSelectedEpisodeState,
}) => {
  const [selectedFileName, setSelectedFileName] = useState("");
  const [episodeTags, setEpisodeTags] = useState([]);
  const [fileNames, setFileNames] = useState([]);
  const [episodeImageFiles, setEpisodeImageFiles] = useState([]);

  useEffect(() => {
    if (episode) {
      const fetchEpisodeData = async () => {
        const [fileNames, episodeTags] = await Promise.all([
          fetchShowFileNames(showSlug),
          getEpisodeTags(showSlug),
        ]);
        setFileNames(fileNames);
        setEpisodeTags(episodeTags);
      };

      fetchEpisodeData();
    }
  }, [showSlug, episode, season]);

  const onEpisodeTitleChange = async (e) => {
    setSelectedEpisodeState({ ...episode, title: e.target.value });
  };

  const onEpisodeNumberChange = async (e, episode, season) => {
    setSelectedEpisodeState({ ...episode, number: e.target.value });
  };

  const onEpisodeDescriptionChange = async (e) => {
    setSelectedEpisodeState({ ...episode, description: e.target.value });
  };

  const onChangeFileName = (e) => {
    const updatedFileName = e.value;

    episode.url = updatedFileName;

    const episodeIndex = season.episodes.findIndex(
      (ep) => ep.id === episode.id
    );

    if (episodeIndex !== -1) {
      season.episodes[episodeIndex] = { ...episode, url: updatedFileName };
      setSelectedFileName(updatedFileName);
      updateShowEpisode(season.episodes[episodeIndex]);
    }
  };

  const onUpdateMiniatureImageUrl = (e) => {
    setSelectedEpisodeState({ ...episode, miniature: e });
  };

  const handleAddTag = async (tag) => {
    const updatedEpisode = await addTagToEpisode(tag, episode.id);

    const updatedEpisodes = season.episodes.map((ep) =>
      ep.id === updatedEpisode.id ? updatedEpisode : ep
    );

    const updatedSeason = {
      ...season,
      episodes: updatedEpisodes,
    };

    const updatedSeasons = seasons.map((s) =>
      s.id === updatedSeason.id ? updatedSeason : s
    );

    onEditSeason(updatedSeasons);
  };

  const handleRemoveTag = (tagId) => {
    episode.tags = episode.tags.filter((tag) => tag.id !== tagId);
    // Update the state accordingly
  };

  return (
    <>
      {editShowFlag ? (
        <div className="border border-primary rounded m-2 p-2" key={episode.id}>
          <table className="w-100">
            <thead>
              <tr>
                <th className="col-2">Episodio Nº</th>
                <th className="col-10">Título</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="col-2">
                  <Input
                    className="w-100"
                    type="number"
                    value={episode.episode_number || 0}
                    onChange={(e) => onEpisodeNumberChange(e)}
                    name="episodeTitle"
                    id="episodeTitle"
                  />
                </td>
                <td className="col-10">
                  <Input
                    className="w-100"
                    type="text"
                    value={episode.title || ""}
                    onChange={onEpisodeTitleChange}
                    name="episodeTitle"
                    id="episodeTitle"
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <table className="w-100">
            <thead>
              <th className="col-12">Descripción</th>
            </thead>
            <tbody>
              <tr>
                <td className="col-12">
                  <Input
                    className="w-100"
                    type="textarea"
                    value={episode.description || ""}
                    onChange={onEpisodeDescriptionChange}
                    name="episodeDescription"
                    id="episodeDescription"
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <table className="w-100">
            <thead>
              <th className="col-12">Enlace</th>
            </thead>
            <tbody>
              <tr>
                <td className="col-12">
                  <Select
                    defaultValue={{
                      label: episode.url,
                      value: episode.url,
                    }}
                    options={fileNames?.map((fileName) => {
                      return {
                        label: fileName.File_Name,
                        value: fileName.File_Name,
                      };
                    })}
                    formatGroupLabel={formatGroupLabel}
                    value={fileNames?.find(
                      (obj) => obj.File_Name === selectedFileName
                    )}
                    onChange={(e) => onChangeFileName(e, episode, season)}
                    id="fileNames"
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <table className="w-100">
            <thead>
              <th className="col-12">Miniatura</th>
            </thead>
            <tbody>
              <tr>
                <td className="col-12">
                  <Media className="col-2" left href="#">
                    <ImageUploader
                      imageFiles={episodeImageFiles}
                      onUpdateImage={onUpdateMiniatureImageUrl}
                      setImageFiles={setEpisodeImageFiles}
                      imageCategory="shows"
                      imageType="ondemand_episode_miniature"
                      imageName={`showEpisodeMiniature_${episode.id}`}
                      showSlug={showSlug}
                      fileUrlPath={`${miniaturesUrl}/${showSlug}/showEpisodeMiniature_${episode.id}`}
                      layout="show"
                      editShowFlag={editShowFlag}
                    />
                  </Media>
                </td>
              </tr>
            </tbody>
          </table>
          <table className="w-100">
            <thead>
              <th>Etiquetas</th>
            </thead>
            <tbody>
              <tr></tr>
            </tbody>
          </table>
        </div>
      ) : (
        <>
          <div
            className="border border-primary rounded m-2 p-2"
            key={episode.id}
          >
            <table className="w-100">
              <thead>
                <tr>
                  <th>Episodio Nº</th>
                  <th>Título</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <span>{episode.episode_number}</span>
                  </td>
                  <td>
                    <span>{episode.title}</span>
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="w-100">
              <thead>
                <th>Descripción</th>
              </thead>
              <tbody className="w-100">
                <tr>
                  <td>
                    <span>{episode.description}</span>
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="w-100">
              <thead>
                <th>Enlace</th>
              </thead>
              <tbody>
                <tr>{episode.url}</tr>
              </tbody>
            </table>
            <table className="w-100">
              <thead>
                <th>Miniatura</th>
              </thead>
              <tbody>
                <tr>{`${miniaturesUrl}/${showSlug}/${episode.miniature}`}</tr>
              </tbody>
            </table>
            <table className="w-100">
              <thead>
                <th>Etiquetas</th>
              </thead>
              <tbody>
                <tr>
                  {episode.tags?.map((tag) => {
                    return (
                      <span key={tag.id}>
                        <Button color="primary" outline>
                          {tag.name}
                        </Button>

                        {"  "}
                      </span>
                    );
                  })}
                </tr>
              </tbody>
            </table>
          </div>
        </>
      )}
    </>
  );
};
export default ShowEpisodeDetails;
