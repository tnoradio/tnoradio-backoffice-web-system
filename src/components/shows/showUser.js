import { useState } from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
} from "reactstrap";
import Select from "react-select";
import { Trash, Mic } from "react-feather";
import groupedOptions from "../common/groupedOptions";
import formatGroupLabel from "../common/formatGroupLabel";

const ShowUser = ({
  usersList,
  onEditShowUser,
  showUsers,
  modalActionText,
  usersListText,
  noSelectedErrorText,
  duplicatedErrorText,
}) => {
  const [showUserModal, setShowUserModal] = useState(false);
  const [showUser, setShowUser] = useState({});
  const [errorShowUser, setErrorShowUser] = useState(false);
  const [errorDuplicatedShowUser, setErrorDuplicatedShowUser] = useState(false);

  const addShowUser = (showUser) => {
    const updatedShowUsers = [...showUsers];
    const existingShowUserIndex = updatedShowUsers.findIndex(
      (existingShowUser) => existingShowUser.userId === showUser.value._id
    );

    if (existingShowUserIndex === -1) {
      updatedShowUsers.push({ userId: showUser.value });
    } else {
      setErrorDuplicatedShowUser(true);
      return;
    }
    onEditShowUser(updatedShowUsers);
    setShowUserModal(false);
    resetShowUserErrorStates();
  };

  const onDeleteShowUserClick = (userId) => {
    const updatedShowUsers = showUsers.filter(
      (showUser) => showUser.userId !== userId
    );
    onEditShowUser(updatedShowUsers);
  };

  const resetShowUserErrorStates = () => {
    setErrorShowUser(false);
    setErrorDuplicatedShowUser(false);
  };

  return (
    <>
      <Row>
        <Col>
          <ListGroup>
            {showUsers?.map((showUser) => {
              return (
                <ListGroupItem key={`showUser_${showUser.userId}`}>
                  <Row>
                    <Col xs={10}>
                      {usersList?.map((user) => {
                        if (user._id === showUser.userId) {
                          return (
                            <div key={user._id}>
                              <span>{user.name}</span>{" "}
                              <span>{user.lastName}</span>{" "}
                            </div>
                          );
                        } else return "";
                      })}
                    </Col>
                    <Col xs={2}>
                      <Trash
                        size={18}
                        className="float-right mt-1 mb-2 width-25 d-block"
                        style={{
                          color: "#FF586B",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          onDeleteShowUserClick(showUser.userId);
                        }}
                      />
                    </Col>
                  </Row>
                </ListGroupItem>
              );
            })}
          </ListGroup>
        </Col>
        <Col xs={12}>
          <br></br>
          <Button
            className="btn-square float-right"
            size="sm"
            outline
            color="primary"
            onClick={() => setShowUserModal(true)}
          >
            <Mic size={16} color="#009DA0" />
            {"  "}
            {modalActionText}
          </Button>
        </Col>
        <Col xs={2}></Col>
        {errorShowUser ? (
          <div
            className="invalid-feedback"
            style={{ display: "block", textAlign: "center" }}
          >
            {noSelectedErrorText}
          </div>
        ) : null}
      </Row>
      <Modal
        isOpen={showUserModal}
        toggle={() => setShowUserModal((state) => !state)}
      >
        <ModalHeader>
          <Label>{modalActionText}</Label>
        </ModalHeader>
        <ModalBody>
          <Row>
            {usersList ? (
              <Col xs={12}>
                <FormGroup>
                  <Select
                    defaultValue={usersList[0]?.user}
                    options={groupedOptions(usersList, usersListText)}
                    formatGroupLabel={formatGroupLabel}
                    value={usersList.find((obj) => {
                      return obj === showUser;
                    })}
                    onChange={(e) => {
                      console.log(e);
                      setShowUser(e);
                    }}
                  />
                </FormGroup>
              </Col>
            ) : (
              <Col xs={11}></Col>
            )}
            {errorShowUser ? (
              <div
                className="invalid-feedback"
                style={{
                  display: "block",
                  textAlign: "center",
                }}
              >
                {noSelectedErrorText}
              </div>
            ) : null}
            {errorDuplicatedShowUser ? (
              <div
                className="invalid-feedback"
                style={{
                  display: "block",
                  textAlign: "center",
                }}
              >
                {duplicatedErrorText}
              </div>
            ) : null}
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button
            className="btn-square"
            outline
            color="primary"
            onClick={() => addShowUser(showUser)}
          >
            Agregar
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ShowUser;
