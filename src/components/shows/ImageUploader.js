import React, { useEffect } from "react";
import MyDropzone from "../shared/MyDropzone";
import updateImage from "../../redux/actions/shows/updateShowImage";

const ImageUploader = ({
  imageFiles,
  setImageFiles,
  imageType,
  imageCategory,
  imageName,
  showSlug,
  fileUrlPath,
  layout,
  editShowFlag,
  onUpdateImage,
}) => {
  useEffect(() => {
    const handleImageUpdate = async (imageFile) => {
      if (imageFile) {
        try {
          const response = await updateImage(
            imageFile,
            imageCategory,
            imageType,
            imageName,
            showSlug
          );
          onUpdateImage(response);
        } catch (err) {
          console.log(err);
        }
      }
    };

    if (imageFiles[0]) {
      handleImageUpdate(imageFiles[0]);
    }
  }, [
    imageFiles,
    imageType,
    imageCategory,
    imageName,
    showSlug,
    onUpdateImage,
  ]);

  return (
    <MyDropzone
      layout={layout}
      image={fileUrlPath}
      setImageFiles={setImageFiles}
      disabled={!editShowFlag}
    />
  );
};

export default ImageUploader;
