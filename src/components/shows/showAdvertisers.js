import React, { Fragment } from "react";
import { Col, Row, ListGroup, ListGroupItem, Button } from "reactstrap";
import { deleteShowAdvertiser } from "../../redux/actions/shows";
import { useDispatch } from "react-redux";
import { Trash, User } from "react-feather";
import AdvertisersModal from "./AdvertisersModal";

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

// const thumbs = this.state.files.map(file => (
//    <div style={thumb} key={file.name}>
//      <div style={thumbInner}>
//        <img src={file.preview} style={img} alt={file.name}/>
//      </div>
//    </div>
//  ));

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 150,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};

const ShowAdvertisers = ({ editShowFlag, showAdvertisers, showId }) => {
  var Buffer = require("buffer").Buffer;
  const [advertisersModal, setAdvertisersModal] = React.useState(false);
  const [updateShowAdvertisers, setUpdateShowAdvertisers] = React.useState(
    showAdvertisers || []
  );
  const dispatch = useDispatch();
  const onDeleteAdvertiserClick = (advertiser) => {
    dispatch(deleteShowAdvertiser(advertiser.id));
  };

  function showAdvertiserImageUrl(advertiser) {
    if (advertiser.image)
      return `data:image/jpeg;base64,${Buffer.from(
        advertiser.image?.data,
        "binary"
      ).toString("base64")}`;
    else if (advertiser.imagefile !== undefined)
      return advertiser.imageFile?.preview;
  }

  return (
    <>
      {editShowFlag ? (
        <Row>
          <Col>
            <ListGroup>
              {showAdvertisers?.map((advertiser, index) => {
                return (
                  <ListGroupItem key={`${advertiser.name}_${index}`}>
                    <Row>
                      <Col xs={6}>
                        <p>Nombre: {advertiser.name}</p>
                        {"  "}
                        <p>Sitio: {advertiser.website}</p>{" "}
                      </Col>
                      <Col xs={4}>
                        <div style={thumbsContainer}>
                          <div style={thumb} key={advertiser.name}>
                            <div style={thumbInner}>
                              <img
                                src={showAdvertiserImageUrl(advertiser)}
                                style={img}
                                alt={advertiser?.name}
                              />
                            </div>
                          </div>
                        </div>
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            onDeleteAdvertiserClick(advertiser);
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={12}>
            <br></br>
            <Button
              className="btn-square float-right"
              size="sm"
              outline
              color="primary"
              onClick={() => setAdvertisersModal(true)}
            >
              <User size={16} color="#009DA0" />
              {"   "}
              Agregar Anunciante
            </Button>
          </Col>
          <AdvertisersModal
            advertisersModal={advertisersModal}
            setAdvertisersModal={setAdvertisersModal}
            showAdvertisers={updateShowAdvertisers}
            setShowAdvertisers={setUpdateShowAdvertisers}
            showId={showId}
          ></AdvertisersModal>
        </Row>
      ) : (
        <Row>
          <Col>
            {showAdvertisers !== undefined && showAdvertisers !== null ? (
              <ListGroup>
                {showAdvertisers?.map((advertiser, index) => {
                  return (
                    <ListGroupItem key={advertiser.name + index}>
                      <Row>
                        <Col xs={10}>
                          <p>Nombre: {advertiser.name}</p>
                          {"  "}
                          <p>Sitio: {advertiser.website}</p>{" "}
                        </Col>
                        <Col xs={4}>
                          <div style={thumbsContainer}>
                            <div style={thumb} key={advertiser.name}>
                              <div style={thumbInner}>
                                <img
                                  src={showAdvertiserImageUrl(advertiser)}
                                  style={img}
                                  alt={advertiser.name}
                                />
                              </div>
                            </div>
                          </div>
                        </Col>{" "}
                        *
                      </Row>
                    </ListGroupItem>
                  );
                })}
              </ListGroup>
            ) : (
              <></>
            )}
          </Col>
        </Row>
      )}
    </>
  );
};

export default ShowAdvertisers;
