import React from "react";
import { components } from "react-select";
import { Button } from "reactstrap";

const CustomMenuList = (props) => {
  return (
    <>
      <components.MenuList {...props}>{props.children}</components.MenuList>
      <div style={{ padding: "8px", textAlign: "center" }}>
        <Button color="link" onClick={props.selectProps.onAddTagClick}>
          Agregar Etiqueta
        </Button>
      </div>
    </>
  );
};

export default CustomMenuList;
