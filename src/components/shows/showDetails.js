import { Fragment, useEffect, useState } from "react";
import {
  Media,
  Table,
  Input,
  FormGroup,
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  Button
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";
import ShowAdvertisers from "./showAdvertisers";
import {
  fetchShowTypes,
  fetchShowGenres,
  fetchShowPGClass,
} from "../../redux/actions/shows";
import fetchUsers from "../../redux/actions/users/fetchUsers";
import { useDispatch, useSelector } from "react-redux";
import updateShowImage from "../../redux/actions/shows/updateShowImage";
import { filesUrl, miniaturesUrl } from "../../config";
import fetchShowSubGenres from "../../redux/actions/shows/fetchShowSubGenres";
import fetchShowAdvertisers from "../../redux/actions/shows/fetchShowAdvertisers";
import {
  ShowPGs,
  ShowTypes,
  ShowUser,
  ShowSchedules,
  ShowGenre,
  GenresButtons,
  ShowSocials,
  ShowTarget,
  ShowAgeRange,
  ShowGenders,
  ImageUploader,
} from "../shows";
import ShowEpisodesList from "./showEpisodesList";

const ShowDetails = ({ selectedShow, onEditClick, editShowFlag, onChange }) => {
  const dispatch = useDispatch();
  const [showHosts, setShowHosts] = useState(selectedShow?.hosts || []);
  const [showGenres, setShowGenres] = useState(selectedShow?.genres || []);
  const [showSubGenres, setShowSubGenres] = useState(
    selectedShow?.subgenres || []
  );
  const [showGenre, setShowGenre] = useState(selectedShow?.genre || "");
  const [showProducers, setShowProducers] = useState(
    selectedShow?.producers || []
  );

  const [showSocials, setShowSocials] = useState(selectedShow?.socials || []);
  const [showSchedules, setShowSchedules] = useState(
    selectedShow?.showSchedules || []
  );
  const [bannerImageFiles, setBannerImageFiles] = useState([]);
  const [miniBannerImageFiles, setMiniBannerImageFiles] = useState([]);
  const [onDemandVerticalImageFiles, setOnDemandVerticalImageFiles] = useState(
    []
  );
  const [onDemandSquareImageFiles, setOnDemandSquareImageFiles] = useState([]);
  const [miniSiteBannerImageFiles, setMiniiSiteBannerImageFiles] = useState([]);
  const [
    responsiveMiniSiteBannerImageFiles,
    setResponsiveMiniiSiteBannerImageFiles,
  ] = useState([]);
  const [responsiveBannerImageFiles, setResponsiveBannerImageFiles] = useState(
    []
  );
  const [seasonsState, setSeasonsState] = useState(selectedShow?.seasons || []);

  const { genres, showPGs, showTypes, subgenres, usersList } = useSelector(
    (state) => ({
      genres: state.showApp.showGenres || [],
      showPGs: state.showApp.showPGs || [],
      showTypes: state.showApp.showTypes || [],
      subgenres: state.showApp.showSubGenres || [],
      usersList: state.userApp.users || [],
    })
  );

  useEffect(() => {
    const handleImageUpdate = (imageFile, imageType, imageKey) => {
      if (selectedShow && imageFile) {
        dispatch(
          updateShowImage(
            imageFile,
            imageType,
            selectedShow.showSlug,
            imageKey,
            selectedShow.id
          )
        );
      }
    };

    const imageFilesAndTypes = [
      { files: bannerImageFiles, type: "showBanner", key: "banners" },
      {
        files: miniBannerImageFiles,
        type: "showMiniBanner",
        key: "mini_banners",
      },
      {
        files: miniSiteBannerImageFiles,
        type: "showMiniSiteBanner",
        key: "mini_site_banners",
      },
      {
        files: responsiveMiniSiteBannerImageFiles,
        type: "responsiveMiniSiteBanner",
        key: "responsive_mini_site_banners",
      },
      {
        files: responsiveBannerImageFiles,
        type: "showResponsiveBanner",
        key: "responsive_banners",
      },
      {
        files: onDemandSquareImageFiles,
        type: "ondemand_square",
        key: "shows",
      },
    ];

    imageFilesAndTypes.forEach(({ files, type, key }) =>
      handleImageUpdate(files[0], type, key)
    );
  }, [
    bannerImageFiles,
    miniBannerImageFiles,
    miniSiteBannerImageFiles,
    responsiveMiniSiteBannerImageFiles,
    responsiveBannerImageFiles,
    onDemandSquareImageFiles,
    selectedShow,
    dispatch,
  ]);

  useEffect(() => {
    if (selectedShow?.id) {
      dispatch(fetchShowTypes());
      dispatch(fetchShowGenres());
      dispatch(fetchShowSubGenres());
      dispatch(fetchShowPGClass());
      dispatch(fetchShowAdvertisers(selectedShow.id));
      dispatch(fetchUsers());
    }
  }, [dispatch, selectedShow]);

  const onEditHost = (hosts) => {
    setShowHosts((prevHosts) => {
      const updatedHosts = [...hosts];
      onChange(selectedShow.id, "hosts", updatedHosts);
      return updatedHosts;
    });
  };

  const onEditProduer = (produers) => {
    setShowProducers((prevProducers) => {
      const updatedProducers = [...produers];
      onChange(selectedShow.id, "producers", updatedProducers);
      return updatedProducers;
    });
  };

  const onEditGenres = (genres) => {
    setShowGenres((prevGenres) => {
      const updatedGenres = [...genres];
      onChange(selectedShow.id, "genres", updatedGenres);
      onChange(selectedShow.id, "genre", genres[0]?.genre || "");
      setShowGenre(genres[0]?.genre || "");
      return updatedGenres;
    });
  };

  const onEditSubGenres = (subgenres) => {
    setShowSubGenres((prevSubGenres) => {
      const updatedSubGenres = [...subgenres];
      onChange(selectedShow.id, "subgenres", updatedSubGenres);
      return updatedSubGenres;
    });
  };

  const onEditShowSocials = (socials) => {
    setShowSocials((prevSocials) => {
      const updatedSocials = [...socials];
      onChange(selectedShow.id, "socials", updatedSocials);
      return updatedSocials;
    });
  };

  const onEditSchedules = (schedulesState) => {
    setShowSchedules(schedulesState);
    onChange(selectedShow.id, "showSchedules", schedulesState);
  };

  const onEditSeason = (seasons) => {
    setSeasonsState((prevSeasons) => {
      const updatedSeasons = [...seasons];
      onChange(selectedShow.id, "seasons", updatedSeasons);
      return updatedSeasons;
    });
  };

  return (
    <Fragment>
      {selectedShow ? (
        <div className="contact-app-content-detail pl-2 pr-2">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editShowFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media>
              <Media body>
                <Media heading>{selectedShow?.name}</Media>
                {selectedShow?.genre}
              </Media>
            </Media>
            <Table responsive borderless size="sm" className="mt-4">
              <tbody>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Nombre:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <Input
                        type="text"
                        name="name"
                        id="name"
                        defaultValue={selectedShow.name}
                        onChange={(e) =>
                          onChange(selectedShow.id, "name", e.target.value)
                        }
                      />
                    ) : (
                      " " + selectedShow.name
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Slug:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <Input
                        type="text"
                        name="slug"
                        id="showSlug"
                        defaultValue={selectedShow.showSlug}
                        onChange={(e) =>
                          onChange(selectedShow.id, "showSlug", e.target.value)
                        }
                      />
                    ) : (
                      " " + selectedShow.showSlug
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Sinopsis:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <Input
                        type="textarea"
                        id="synopsis"
                        rows="5"
                        name="synopsis"
                        defaultValue={selectedShow.synopsis}
                        onChange={(e) =>
                          onChange(selectedShow.id, "synopsis", e.target.value)
                        }
                      />
                    ) : (
                      " " + selectedShow.synopsis
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Género:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <ShowGenre
                        genres={genres}
                        showGenres={showGenres}
                        onEditGenres={onEditGenres}
                        showGenre={showGenre}
                        keyLabel="genre"
                        isEdit={editShowFlag}
                      />
                    ) : null}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400"></td>
                  <td className="col-10">
                    <GenresButtons
                      editFlag={editShowFlag}
                      showGenres={showGenres}
                      genres={genres}
                      onEditGenres={onEditGenres}
                      keyLabel="genre"
                    />
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Sub Géneros:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <ShowGenre
                        genres={subgenres}
                        showGenres={showSubGenres}
                        onEditGenres={onEditSubGenres}
                        keyLabel="subgenre"
                        isEdit={editShowFlag}
                      />
                    ) : null}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400"></td>
                  <td className="col-10">
                    <GenresButtons
                      editFlag={editShowFlag}
                      showGenres={showSubGenres}
                      genres={subgenres}
                      onEditGenres={onEditSubGenres}
                      keyLabel="subgenre"
                    />
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Clasificación:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <ShowPGs
                        showPGs={showPGs}
                        onChange={onChange}
                        selectedShow={selectedShow}
                      />
                    ) : (
                      " " + selectedShow.pgClassification
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Tipo:</td>
                  <td className="col-10">
                    {editShowFlag ? (
                      <ShowTypes
                        showTypes={showTypes}
                        onChange={onChange}
                        selectedShow={selectedShow}
                      />
                    ) : (
                      " " + selectedShow.type
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Target:</td>
                  <td className="col-10" style={{ zIndex: 0 }}>
                    {editShowFlag ? (
                      <FormGroup>
                        <Row>
                          <Col xs={6}>
                            <ShowTarget
                              onChange={onChange}
                              selectedShow={selectedShow}
                            />
                          </Col>
                        </Row>
                      </FormGroup>
                    ) : (
                      selectedShow.target?.map((target) => (
                        <span>{`${target} `}</span>
                      ))
                    )}
                    <br />
                    <span className="font-small-2 teal">
                      **Nivel socio-económico al que va dirigido el programa**
                    </span>
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Géneros:</td>
                  <td className="col-10" style={{ zIndex: 0 }}>
                    {editShowFlag ? (
                      <ShowGenders
                        onChange={onChange}
                        selectedShow={selectedShow}
                      />
                    ) : (
                      selectedShow.target_gender?.map((gender) => (
                        <span>{` ${gender}`}</span>
                      ))
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Rango de edades:</td>
                  <td className="col-10" style={{ zIndex: 0 }}>
                    {editShowFlag ? (
                      <ShowAgeRange
                        onChange={onChange}
                        selectedShow={selectedShow}
                      />
                    ) : (
                      selectedShow.age_range?.map((age_range) => (
                        <span>{` ${age_range}`}</span>
                      ))
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Locutor (es)</td>
                  <td className="col-10 col-lg-4">
                    {editShowFlag ? (
                      <ShowUser
                        showUsers={showHosts || []}
                        usersList={usersList}
                        onEditShowUser={onEditHost}
                        modalActionText="Agregar Locutor"
                        usersListText="Locutor (es)"
                        noSelectedErrorText="Debe indicar al menos un Locutor"
                        duplicatedErrorText="El locutor ya existe para este Programa"
                      />
                    ) : (
                      <ListGroup>
                        {showHosts?.map((host) => {
                          return (
                            <ListGroupItem key={"host " + host.userId}>
                              <Row>
                                <Col>
                                  {usersList?.map((user) => {
                                    if (user._id === host.userId) {
                                      return (
                                        <div key={user._id}>
                                          {user.name +
                                            " " +
                                            user.lastName +
                                            "\n"}
                                          <br></br>
                                        </div>
                                      );
                                    } else return "";
                                  })}
                                </Col>
                              </Row>
                            </ListGroupItem>
                          );
                        })}
                      </ListGroup>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Productor (es)</td>
                  <td className="col-10 col-lg-4">
                    {editShowFlag ? (
                      <ShowUser
                        showUsers={showProducers || []}
                        usersList={usersList}
                        onEditShowUser={onEditProduer}
                        modalActionText="Agregar Productor"
                        usersListText="Productor (es)"
                        noSelectedErrorText="Debe indicar al menos un Productor"
                        duplicatedErrorText="El productor ya existe para este Programa"
                      />
                    ) : (
                      <ListGroup>
                        {showProducers?.map((producer) => {
                          return (
                            <ListGroupItem key={"producer " + producer.userId}>
                              <Row>
                                <Col>
                                  {usersList?.map((user) => {
                                    if (user._id === producer.userId) {
                                      return (
                                        <div key={user._id}>
                                          {user.name +
                                            " " +
                                            user.lastName +
                                            "\n"}
                                          <br></br>
                                        </div>
                                      );
                                    } else return "";
                                  })}
                                </Col>
                              </Row>
                            </ListGroupItem>
                          );
                        })}
                      </ListGroup>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Horario (s)</td>
                  <td className="col-8">
                    {editShowFlag ? (
                      <ShowSchedules
                        showSchedules={showSchedules}
                        onEditSchedules={onEditSchedules}
                      />
                    ) : (
                      <Row>
                        <Col className="col-10">
                          <ListGroup className="d-flex flex-row flex-wrap">
                            {showSchedules?.map((showSchedule) => {
                              return (
                                <ListGroupItem
                                  key={`${showSchedule.weekDay}_${showSchedule.startTime}_${showSchedule.endTime}`}
                                  className="flex-grow-1 mr-2 mb-2"
                                >
                                  <span>{showSchedule.weekDay}</span>{" "}
                                  <span>de {showSchedule.startTime}</span>{" "}
                                  <span>a {showSchedule.endTime}</span>
                                </ListGroupItem>
                              );
                            })}
                          </ListGroup>
                        </Col>
                      </Row>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Redes Sociales</td>
                  <td className="col-10 col-lg-4">
                    {editShowFlag ? (
                      <ShowSocials
                        showSocials={showSocials}
                        onSocialsEdit={onEditShowSocials}
                      />
                    ) : (
                      <Row>
                        <Col>
                          <ListGroup>
                            {showSocials?.map((social) => {
                              return (
                                <ListGroupItem
                                  key={social.socialNetwork + social.userName}
                                >
                                  <Row>
                                    <Col xs={10}>
                                      <span>{social.socialNetwork}</span>
                                      {"  "}
                                      <span>@{social.userName}</span>{" "}
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              );
                            })}
                          </ListGroup>
                        </Col>
                      </Row>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Anunciantes</td>
                  <td className="col-10 col-lg-4">
                    <Row>
                      <Col>
                        <ShowAdvertisers
                          editShowFlag={editShowFlag}
                          showId={selectedShow?.id}
                        ></ShowAdvertisers>
                      </Col>
                    </Row>
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Correo:</td>
                  <td className="col-10 col-lg-4">
                    {editShowFlag ? (
                      <Input
                        type="email"
                        name="name"
                        id="name"
                        defaultValue={selectedShow.email}
                        onChange={(e) =>
                          onChange(selectedShow.id, "email", e.target.value)
                        }
                      />
                    ) : (
                      " " + selectedShow.email
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Minisite Banner</td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={responsiveBannerImageFiles}
                        setImageFiles={setMiniiSiteBannerImageFiles}
                        imageType="showMiniSiteBanner"
                        imageKey="show_mini_site_banner"
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${filesUrl}/shows/minisite`}
                        layout="show"
                        editShowFlag={editShowFlag}
                        updateImage={updateShowImage}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">
                    Minisite Responsive Banner
                  </td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={responsiveMiniSiteBannerImageFiles}
                        setImageFiles={setResponsiveMiniiSiteBannerImageFiles}
                        imageType="showResponsiveMiniSiteBanner"
                        imageKey="responsive_banners"
                        selectedShow={selectedShow}
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${filesUrl}/shows/responsive_minisite`}
                        layout="show"
                        editShowFlag={editShowFlag}
                        updateImage={updateShowImage}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Show Banner</td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={bannerImageFiles}
                        setImageFiles={setBannerImageFiles}
                        imageType="showBanner"
                        imageKey="banners"
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${filesUrl}/shows/banner`}
                        layout="show"
                        editShowFlag={editShowFlag}
                        updateImage={updateShowImage}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Responsive Banner</td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={responsiveBannerImageFiles}
                        setImageFiles={setResponsiveBannerImageFiles}
                        imageType="showResponsiveBanner"
                        imageKey="responsive_banners"
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${filesUrl}/shows/responsive_banner`}
                        layout="show"
                        editShowFlag={editShowFlag}
                        updateImage={updateShowImage}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Mini Banner</td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={miniBannerImageFiles}
                        setImageFiles={setMiniBannerImageFiles}
                        imageType="showMiniBanner"
                        imageKey="mini_banners"
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${filesUrl}/shows/mini_banner`}
                        layout="show"
                        editShowFlag={editShowFlag}
                        updateImage={updateShowImage}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">
                    OnDemand Banner Cuadrado
                  </td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={onDemandSquareImageFiles}
                        setImageFiles={setOnDemandSquareImageFiles}
                        imageType="ondemandSquare"
                        imageKey="ondemand_square"
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${filesUrl}/shows/ondemand_square`}
                        layout="show"
                        editShowFlag={editShowFlag}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">
                    OnDemand Banner Vertical
                  </td>
                  <td className="col-7">
                    <Media className="col-2" left href="#">
                      <ImageUploader
                        imageFiles={onDemandVerticalImageFiles}
                        setImageFiles={setOnDemandVerticalImageFiles}
                        imageType="ondemandVertical"
                        imageKey="ondemand_vertical"
                        selectedShowId={selectedShow?.id}
                        selectedShowSlug={selectedShow?.showSlug}
                        fileUrlPath={`${miniaturesUrl}/shows/ondemand_vertical`}
                        layout="show"
                        editShowFlag={editShowFlag}
                      />
                    </Media>
                  </td>
                  <td className="col-2"></td>
                </tr>
                <tr className="d-flex">
                  <td className="col-2 text-bold-400">Episodios</td>
                  <td className="col-10">
                    <ShowEpisodesList
                      seasons={seasonsState}
                      showEditFlag={editShowFlag}
                      showSlug={selectedShow?.showSlug}
                      showId={selectedShow?.id}
                      onUpdateSeason={onEditSeason}
                    />
                  </td>
                </tr>
              </tbody>
            </Table>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un Show para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

export default ShowDetails;
