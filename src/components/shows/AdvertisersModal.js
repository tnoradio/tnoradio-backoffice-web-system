/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import Dropzone from "react-dropzone";

import "react-datetime/css/react-datetime.css";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

import { Trash } from "react-feather";
import { useDispatch } from "react-redux";
import { addShowAdvertiser } from "../../redux/actions/shows";

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 150,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const advertiserFormSchema = Yup.object().shape({
  name: Yup.string().required("Debes escribir un nombre."),
  webSite: Yup.string().required("Debes escribir un sitio web."),
  imageUrl: Yup.string().required("Debes indicar uan imagen."),
});

const AdvertisersModal = ({
  advertisersModal,
  setAdvertisersModal,
  showAdvertisers,
  setShowAdvertisers,
  showId,
}) => {
  //console.log(showAdvertisers);
  const dispatch = useDispatch();
  const [isErrorName, setIsErrorName] = React.useState(false);
  const [isErrorImage, setIsErrorImage] = React.useState(false);
  const [isErrorWebsite, setIsErrorWebsite] = React.useState(false);
  const [advertiserName, setAdvertiserName] = React.useState("");
  const [advertiserWebsite, setAdvertiserWebsite] = React.useState("");
  const [files, setFiles] = React.useState([]);
  const [images, setImages] = React.useState([]);
  const [advertisersState, setAdvertisersState] = React.useState([]);

  React.useEffect(() => {
    setAdvertisersState(showAdvertisers);
  }, []);

  //console.log(advertisersState);
  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} alt={file.name} />
      </div>
    </div>
  ));

  const setImage = (image) => {
    var imageFile = [];
    var images = [];
    if (image[0].type === "image/jpg" || image[0].type === "image/jpeg") {
      imageFile = image.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      );
      images.push({
        name: "advertiser",
        imageUrl: "advertisers/advertiser_" + advertiserName + ".jpg",
      });
      //console.log("Image file -> {0}", imageFile);
      //console.log("Image -> {0}", images);
      setIsErrorImage(false);
      setFiles(imageFile);
      setImages(images);
    }
  };

  const addAdvertiserSubmit = () => {
    if (advertiserName !== "") {
      if (advertiserWebsite !== "") {
        if (files.length > 0) {
          var ads = advertisersState;
          //console.log(advertisersState);
          //console.log(ads);
          ads.push({
            name: advertiserName,
            website: advertiserWebsite,
            imageFile: files[0],
            imageUrl: images[0].imageUrl,
          });
          //console.log(ads);
          setAdvertisersModal((advertisersModal) => !advertisersModal);
          setAdvertiserName("");
          setAdvertiserWebsite("");
          setFiles([]);
          setImages([]);
          setAdvertisersState(ads);
          //console.log(advertisersState);
          setShowAdvertisers(advertisersState);
          //console.log(advertisersState);
          dispatch(
            addShowAdvertiser(
              advertiserName,
              advertiserWebsite,
              files[0],
              showId
            )
          );
        } else {
          setIsErrorName(false);
          setIsErrorImage(true);
          setIsErrorWebsite(false);
        }
      } else {
        setIsErrorName(false);
        setIsErrorImage(false);
        setIsErrorWebsite(true);
      }
    } else {
      setIsErrorName(true);
      setIsErrorImage(false);
      setIsErrorWebsite(false);
    }
  };
  return (
    <Modal
      isOpen={advertisersModal}
      toggle={() =>
        setAdvertisersModal((advertisersModal) => !advertisersModal)
      }
    >
      <ModalHeader>
        <Label>Agregar Anunciante</Label>
      </ModalHeader>
      <Formik
        initialValues={{
          name: "",
          webSite: "",
          imageUrl: "",
        }}
        validationSchema={advertiserFormSchema}
        // onSubmit={
        //    values => {
        //    addSocialSubmit(values);
        //    console.log(values);
        // }}
      >
        {({}) => (
          <Form>
            <ModalBody>
              <Row>
                <Col xs={12}>
                  <FormGroup>
                    <Label for="name">Nombre del Anunciante</Label>
                    <Field
                      className={`form-control ${
                        isErrorName ? "is-invalid" : ""
                      }`}
                      type="text"
                      onChange={(e) => {
                        setAdvertiserName(e.currentTarget.value);
                      }}
                      value={advertiserName}
                      name="name"
                      required
                    />
                    {isErrorName ? (
                      <div className="invalid-feedback">
                        Debes escribir un nombre para el anunciante.
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <FormGroup>
                    <Label for="name">Sitio Web del Anunciante</Label>
                    <Field
                      className={`form-control ${
                        isErrorWebsite ? "is-invalid" : ""
                      }`}
                      type="text"
                      onChange={(e) => {
                        setAdvertiserWebsite(e.currentTarget.value);
                      }}
                      value={advertiserWebsite}
                      name="name"
                      required
                    />
                    {isErrorWebsite ? (
                      <div className="invalid-feedback">
                        Debes escribir sitio web para el anunciante.
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <FormGroup>
                    <Row>
                      <Col xs={2}></Col>
                      <Col xs={8}>
                        <Label>Imagen del Anunciante</Label>
                      </Col>
                      <Col xs={2}></Col>
                    </Row>
                    <Row>
                      <Col xs={2}></Col>
                      <Col xs={8}>
                        <Dropzone
                          onDrop={(acceptedFiles) => setImage(acceptedFiles)}
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()}>
                                <input {...getInputProps()} />
                                {thumbs.length > 0 ? (
                                  <div></div>
                                ) : (
                                  <p>
                                    Arrastra la imagen aquí o haz click en este
                                    espacio para seleccionarla.
                                  </p>
                                )}
                                <div style={thumbsContainer}>{thumbs}</div>
                              </div>
                            </section>
                          )}
                        </Dropzone>
                      </Col>
                      <Col xs={2}></Col>
                    </Row>
                    {isErrorImage ? (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block", textAlign: "center" }}
                      >
                        {"Debe seleccionar una imagen para el Patrocinador"}
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button
                className="btn-square"
                outline
                color="primary"
                onClick={() => addAdvertiserSubmit()}
                type="submit"
              >
                Agregar
              </Button>{" "}
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export default AdvertisersModal;
