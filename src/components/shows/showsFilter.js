import React from "react";
import { Modal, ModalHeader } from "reactstrap";
import FilterLink from "../../containers/shows/showsFilterLink";
import { showsVisibilityFilter } from "../../redux/actions/shows";
import AddShowWizard from "../../containers/shows/addShow/addShowWizard";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";

class ShowsFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }
  render() {
    return (
      <div className="contact-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="contact-app-sidebar-content">
            <div className="contact-app-menu">
              <div className="form-group form-group-compose text-center">
                <button
                  type="button"
                  className="btn btn-raised btn-danger btn-block my-2 shadow-z-2"
                  onClick={this.toggle}
                >
                  <Icon.UserPlus size={18} className="mr-1" /> Nuevo Programa
                </button>
              </div>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Flitrar
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={showsVisibilityFilter.SHOW_ALL}>
                  <Icon.PlayCircle size={18} className="mr-1" /> Todos
                </FilterLink>
                <FilterLink filter={showsVisibilityFilter.DELETED_SHOW}>
                  <Icon.Trash
                    size={18}
                    style={{ color: "#FF586B" }}
                    className="mr-1"
                  />{" "}
                  Eliminados
                </FilterLink>
                <FilterLink filter={showsVisibilityFilter.ENABLED_SHOW}>
                  <Icon.CheckCircle
                    size={18}
                    style={{ color: "green" }}
                    className="mr-1"
                  />{" "}
                  Habilitado
                </FilterLink>
                <FilterLink filter={showsVisibilityFilter.ACTIVE_SHOW}>
                  <Icon.Star
                    size={18}
                    style={{ color: "yellow" }}
                    className="mr-1"
                  />{" "}
                  Activo
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Géneros
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink
                  filter={showsVisibilityFilter.MOTIVATIONAL_SHOW}
                  genreValue="MOTIVATIONAL"
                >
                  <Icon.Circle size={18} className="mr-1 danger" />
                  Motivacional
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.BUSINESS_SHOW}
                  genreValue="BUSINESS"
                >
                  <Icon.Circle size={18} className="mr-1 warning" />
                  Economía y Negocios
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.MAGAZINE_SHOW}
                  genreValue="MAGAZINE"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Revista
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.HEALTH_SHOW}
                  genreValue="HEALTH"
                >
                  <Icon.Circle size={18} className="mr-1 danger" />
                  Salud
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.ENTREPRENEURSHIP_SHOW}
                  genreValue="ENTREPRENEURSHIP"
                >
                  <Icon.Circle size={18} className="mr-1 warning" />
                  Emprendimiento
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.CULTURE_SHOW}
                  genreValue="CULTURE"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Cultura
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.ENTERTAINMENT_SHOW}
                  genreValue="ENTERTAINMENT"
                >
                  <Icon.Circle size={18} className="mr-1 danger" />
                  Entretenimiento
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.MUSIC_SHOW}
                  genreValue="MUSIC"
                >
                  <Icon.Circle size={18} className="mr-1 warning" />
                  Musical
                </FilterLink>
                <FilterLink
                  filter={showsVisibilityFilter.SPORTS_SHOW}
                  genreValue="SPORTS"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Deportes
                </FilterLink>
              </ul>
            </div>
          </div>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={this.props.className}
            size="lg"
          >
            <ModalHeader toggle={this.toggle}>Agregar un Programa</ModalHeader>
            <AddShowWizard />
          </Modal>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default ShowsFilter;
