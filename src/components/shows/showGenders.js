import { FormGroup, CustomInput } from "reactstrap";
import { useState } from "react";

const ShowGenders = ({ onChange, selectedShow }) => {
  const [genders, setGenders] = useState(selectedShow?.target_gender || []);

  const handleGenderChange = (selected_gender) => {
    const updatedGenders = genders ? [...genders] : [];

    if (!updatedGenders.includes(selected_gender)) {
      updatedGenders.push(selected_gender);
    } else {
      updatedGenders.splice(updatedGenders.indexOf(selected_gender), 1);
    }

    setGenders(updatedGenders);
    onChange(selectedShow.id, "target_gender", updatedGenders);
  };

  return (
    <FormGroup>
      <FormGroup check className="px-0">
        <CustomInput
          type="checkbox"
          id="female"
          label="Femenino"
          onChange={() => handleGenderChange("female")}
          defaultChecked={selectedShow.target_gender?.some(
            (gender) => gender === "female"
          )}
        />
      </FormGroup>
      <FormGroup check className="px-0">
        <CustomInput
          type="checkbox"
          id="male"
          label="Masculino"
          onChange={() => handleGenderChange("male")}
          defaultChecked={selectedShow.target_gender?.some(
            (gender) => gender === "male"
          )}
        />
      </FormGroup>
      <FormGroup check className="px-0">
        <CustomInput
          type="checkbox"
          id="other"
          label="Otro"
          onChange={() => handleGenderChange("other")}
          defaultChecked={selectedShow.target_gender?.some(
            (gender) => gender === "other"
          )}
        />
      </FormGroup>
    </FormGroup>
  );
};
export default ShowGenders;
