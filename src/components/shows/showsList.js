import React from "react";
import PropTypes from "prop-types";
import Show from "./show";
import PerfectScrollbar from "react-perfect-scrollbar";

const ShowsList = ({
  active,
  shows,
  toggleEnabledShow,
  deleteShow,
  showDetails,
  toggleActiveShow,
}) => {
  return (
    <div className="contact-app-list">
      <PerfectScrollbar>
        <div id="shows-list">
          <ul className="list-group">
            {shows.map((show) => (
              <Show
                key={show.id}
                active={active}
                {...show}
                onEnabledShow={() => toggleEnabledShow(show.id)}
                onDeleteClick={() => deleteShow(show.id)}
                onShowClick={() => showDetails(show.id)}
                onStarClick={() => toggleActiveShow(show.id)}
              />
            ))}
          </ul>
        </div>
      </PerfectScrollbar>
    </div>
  );
};

ShowsList.prototype = {
  shows: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      synopsis: PropTypes.string,
      showSchedules: PropTypes.array,
      initDate: PropTypes.string,
      email: PropTypes.string,
      pgClassification: PropTypes.string,
      producer: PropTypes.string,
      socials: PropTypes.array,
      isEnabled: PropTypes.bool,
      hosts: PropTypes.array,
      images: PropTypes.array,
      seasons: PropTypes.array,
      colors: PropTypes.array,
      genres: PropTypes.array,
      subgenres: PropTypes.array,
      isStared: PropTypes.bool,
      isDeleted: PropTypes.bool,
      isActive: PropTypes.bool,
    }).isRequired
  ).isRequired,
  toggleEnabledShow: PropTypes.func.isRequired,
  toggleActiveShow: PropTypes.func.isRequired,
  deleteShow: PropTypes.func.isRequired,
  showDetails: PropTypes.func.isRequired,
};

export default ShowsList;
