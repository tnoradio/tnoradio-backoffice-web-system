import { useState } from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { SocialNetworks } from "../../utility/common-data/SocialMedia";
import { Trash, AtSign } from "react-feather";
import formatGroupLabel from "../common/formatGroupLabel";
import Select from "react-select";

const ShowSocials = ({ showSocials, onSocialsEdit }) => {
  const [socialsModal, setSocialsModal] = useState(false);
  const [social, setSocial] = useState();
  const [userNameSocial, setUserNameSocial] = useState();

  const filterSocials = (socials, socialToRemove) => {
    return socials.filter((item) => {
      return (
        item.userName !== socialToRemove.userName &&
        item.name !== socialToRemove.name
      );
    });
  };

  const onDeleteSocialClick = (social) => {
    const updatedSocials = filterSocials(showSocials, social);
    onSocialsEdit(updatedSocials);
  };

  const addSocialSubmit = () => {
    let socials = showSocials;
    if (userNameSocial !== "" && social !== undefined) {
      socials.push({
        name: social.label,
        url: social.url,
        userName: userNameSocial,
      });
      setSocialsModal(false);
      onSocialsEdit(socials);
    }
  };

  return (
    <>
      <Row>
        <Col>
          <ListGroup>
            {showSocials?.map((social) => {
              return (
                <ListGroupItem key={social.name + social.userName}>
                  <Row>
                    <Col xs={10}>
                      <span className="primary">{social.name}</span>
                      {"  "}
                      <span>@{social.userName}</span>{" "}
                    </Col>
                    <Col xs={2}>
                      <Trash
                        size={18}
                        className="float-right mt-1 mb-2 width-25 d-block"
                        style={{
                          color: "#FF586B",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          onDeleteSocialClick(social);
                        }}
                      />
                    </Col>
                  </Row>
                </ListGroupItem>
              );
            })}
          </ListGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <br></br>
          <Button
            className="btn-square float-right"
            size="sm"
            outline
            color="primary"
            onClick={() => setSocialsModal(true)}
          >
            <AtSign size={16} color="#009DA0" />
            {"   "}
            Agregar Red Social
          </Button>
        </Col>
      </Row>
      <Modal
        isOpen={socialsModal}
        toggle={() => setSocialsModal(!socialsModal)}
      >
        <ModalHeader>
          <Label>Agregar Red Social</Label>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <Select
                  defaultValue={SocialNetworks[0].label}
                  options={SocialNetworks}
                  formatGroupLabel={formatGroupLabel}
                  value={SocialNetworks.find((obj) => obj === social)}
                  onChange={(e) => setSocial(e)}
                  id="socialNetwork"
                  name="socialNetwork"
                />
                <Label for="name">Nombre de Usuario</Label>
                <Input
                  type="text"
                  id="userNamme"
                  name="userNamme"
                  defaultValue={""}
                  onChange={(e) => setUserNameSocial(e.target.value)}
                />
              </FormGroup>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button
            className="btn-square"
            outline
            color="primary"
            onClick={addSocialSubmit}
            type="submit"
          >
            Agregar
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </>
  );
};
export default ShowSocials;
