import groupedOptions from "../common/groupedOptions";
import formatGroupLabel from "../common/formatGroupLabel";
import Select from "react-select";

const ShowTypes = ({ showTypes, onChange, selectedShow }) => {
  return (
    <Select
      defaultValue={{
        label: selectedShow.type,
        value: selectedShow.type,
      }}
      options={groupedOptions(showTypes, "Tipo")}
      formatGroupLabel={formatGroupLabel}
      value={showTypes.find((obj) => obj.value === selectedShow.type)}
      onChange={(e) => {
        onChange(selectedShow.id, "type", e.value);
      }}
      id="showType"
    />
  );
};

export default ShowTypes;
