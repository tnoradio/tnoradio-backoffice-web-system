const customStyles = {
  control: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    zIndex: 900,
  }),
  option: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    backgroundColor: state.isSelected ? "#009DA0" : "#fff",
    color: state.isSelected ? "#fff" : "#000",
  }),
};

export default customStyles;
