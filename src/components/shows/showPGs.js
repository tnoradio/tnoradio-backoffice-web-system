import groupedOptions from "../common/groupedOptions";
import formatGroupLabel from "../common/formatGroupLabel";
import Select from "react-select";

const ShowPGs = ({ showPGs, onChange, selectedShow }) => {
  return (
    <Select
      defaultValue={{
        label: selectedShow.pgClassification,
        value: selectedShow.pgClassification,
      }}
      options={groupedOptions(showPGs, "Clasificación")}
      formatGroupLabel={formatGroupLabel}
      value={showPGs.find((pg) => pg.value === selectedShow.pgClassification)}
      onChange={(e) => {
        onChange(selectedShow.id, "pgClassification", e.value);
        selectedShow.pgClassification = e.label;
      }}
      id="pg"
    />
  );
};

export default ShowPGs;
