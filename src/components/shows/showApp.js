import React from "react";

import VisibleShowsList from "../../containers/shows/visibleShowsList";

const App = () => (
  <div>
    <VisibleShowsList />
  </div>
);

export default App;
