import React, { useState } from "react";
import { Badge, Button } from "reactstrap";
import { Trash } from "react-feather";

const TagsManager = ({ episodeTags, onAddTag, onRemoveTag }) => {
  const [newTag, setNewTag] = useState("");
  const [tags, setTags] = useState(episodeTags);

  const handleAddTag = () => {
    if (newTag.trim()) {
      setTags([...tags, newTag]);
      onAddTag(newTag);
      setNewTag("");
    }
  };

  const handleRemoveTag = (tag) => {
    setTags(tags.filter((t) => t.id !== tag));
    onRemoveTag(tag.id);
  };

  return (
    <div>
      <div className="tag-list">
        {tags?.map((tag) => (
          <span key={tag.id} className="tag">
            <Button outline color="primary">
              {tag.name}
              <Badge color="transparent">
                <Trash
                  style={{ color: "#FF586B" }}
                  size={16}
                  className="cursor-pointer"
                  onClick={handleRemoveTag(tag)}
                />
              </Badge>
            </Button>{" "}
          </span>
        ))}
      </div>
      <div className="tag-input">
        <input
          type="text"
          value={newTag}
          onChange={(e) => setNewTag(e.target.value)}
          placeholder="Agregar una etiqueta"
        />
        <button onClick={handleAddTag}>Agregar</button>
      </div>
    </div>
  );
};

export default TagsManager;
