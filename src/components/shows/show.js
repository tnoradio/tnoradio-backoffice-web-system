import React from "react";
import { Row, Col } from "reactstrap";
import PropTypes from "prop-types";
import { Star, Trash, CheckCircle } from "react-feather";

const Show = ({
  onShowClick,
  onEnabledShow,
  onDeleteClick,
  onStarClick,
  id,
  name,
  genre,
  isEnabled,
  isActive,
  active,
}) => (
  <li
    className={
      "list-group-item list-group-item-action no-border " +
      (active === id ? "bg-grey bg-lighten-4" : "")
    }
  >
    <Row>
      <Col xs={10} onClick={onShowClick}>
        <p className="mb-0 text-truncate">{name}</p>
        <p className="mb-0 text-muted font-small-3">{genre}</p>
      </Col>
      <Col xs={1} className="d-flex flex-column">
        <CheckCircle
          size={18}
          onClick={onEnabledShow}
          className="float-right width-25 d-block"
          style={{ color: isEnabled ? "green" : "#495057" }}
        />
        <Trash
          size={18}
          onClick={onDeleteClick}
          className="float-right width-25 d-block mt-2"
          style={{ color: "#FF586B" }}
        />
      </Col>
      <Col xs={1} className="d-flex flex-column">
        <Star
          size={18}
          onClick={onStarClick}
          className="float-right width-25 d-block"
          style={{ color: isActive ? "yellow" : "#495057" }}
        />
      </Col>
    </Row>
  </li>
);

Show.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  genre: PropTypes.string,
  genres: PropTypes.array,
  subgenres: PropTypes.array,
  synopsis: PropTypes.string,
  showSchedules: PropTypes.array,
  initDate: PropTypes.string,
  email: PropTypes.string,
  pgClassification: PropTypes.string,
  producer: PropTypes.string,
  isEnabled: PropTypes.bool,
  isActive: PropTypes.bool,
  isDeleted: PropTypes.bool,
  hosts: PropTypes.array,
  socials: PropTypes.array,
  images: PropTypes.array,
  onEnabledShow: PropTypes.func,
  onShowClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onStarClick: PropTypes.func,
};

export default Show;
