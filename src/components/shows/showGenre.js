import { useState } from "react";
import Select from "react-select";
import groupedOptions from "../common/groupedOptions";
import formatGroupLabel from "../common/formatGroupLabel";
import customStyles from "./customStyles";
import Buttons from "../common/buttons";

const genresList = (showGenres, genres, keyLabel) =>
  showGenres?.map((genre) => ({
    value: genre.id,
    label: keyLabel === "genre" ? genre?.genre : genre?.subgenre,
  })) || [];

const onDeleteGenreClick = (genre, showGenres, onEditGenres) => {
  const updatedGenres = showGenres.filter((item) => item.id !== genre.value);
  onEditGenres(updatedGenres);
};

const ShowGenre = ({ onEditGenres, genres, showGenres, keyLabel }) => {
  const [selectedGenre, setSelectedGenre] = useState({
    label: keyLabel === "genre" ? genres[0]?.genre : genres[0]?.subgenre,
    value: genres[0]?.id,
  });

  const addGenre = (genre) => {
    const updatedGenres = [...showGenres];
    if (
      !updatedGenres.find((existingGenre) => existingGenre.id === genre.value)
    ) {
      keyLabel === "genre"
        ? updatedGenres.push({ id: genre.value, genre })
        : updatedGenres.push({ id: genre.value, subgenre: genre.label });
      console.log(updatedGenres);
      onEditGenres(updatedGenres);
    }
  };

  return (
    <Select
      defaultValue={selectedGenre}
      styles={customStyles}
      options={groupedOptions(
        genres,
        keyLabel === "genre" ? "Géneros" : "Sub Géneros"
      )}
      formatGroupLabel={formatGroupLabel}
      value={selectedGenre}
      onChange={(e) => {
        setSelectedGenre(e);
        addGenre(e);
      }}
      id={keyLabel}
    />
  );
};

export default ShowGenre;

export const GenresButtons = ({
  editFlag,
  showGenres,
  genres,
  onEditGenres,
  keyLabel,
}) => {
  return (
    <Buttons
      options={genresList(showGenres, genres, keyLabel)}
      onDeleteOption={(genre) =>
        onDeleteGenreClick(genre, showGenres, onEditGenres)
      }
      editFlag={editFlag}
    />
  );
};
