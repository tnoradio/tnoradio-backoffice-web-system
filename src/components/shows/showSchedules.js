import { useState } from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  CustomInput,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
} from "reactstrap";
import moment from "moment";
import { Trash, Clock } from "react-feather";
import DateTime from "react-datetime";

const ShowSchedules = ({ onEditSchedules, showSchedules }) => {
  const [errorCrash, setErrorCrash] = useState(false);
  const [weekDay, setWeekDay] = useState("lunes");
  const [start, setStart] = useState("");
  const [modalSchedule, setModalSchedule] = useState(false);
  const [end, setEnd] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [weekdays, setWeekdays] = useState({
    lunes: false,
    martes: false,
    miercoles: false,
    jueves: false,
    viernes: false,
    sabado: false,
    domingo: false,
  });

  const handleAddSchedule = () => {
    if (start === "" || end === "") {
      setErrorCrash(true);
      setModalSchedule(true);
      return;
    }

    const newSchedule = {
      weekDay,
      startTime: start,
      endTime: end,
      startDate,
      endDate,
    };

    const updatedSchedules = [...showSchedules];

    if (verifySchedule(updatedSchedules, newSchedule)) {
      updatedSchedules.push(newSchedule);
      setErrorCrash(false);
      setModalSchedule(false);
    } else {
      setErrorCrash(true);
      setModalSchedule(true);
    }

    resetScheduleForm();

    onEditSchedules(updatedSchedules);
  };

  const verifySchedule = (schedules, newSchedule) => {
    // Check if the new schedule is valid
    const isOverlapping = (existingSchedule) => {
      return (
        existingSchedule.weekDay === newSchedule.weekDay &&
        ((newSchedule.startDate < existingSchedule.endDate &&
          newSchedule.endDate > existingSchedule.startDate) ||
          (newSchedule.startDate >= existingSchedule.startDate &&
            newSchedule.startDate < existingSchedule.endDate))
      );
    };

    // Check for overlaps with any existing schedule
    const hasOverlap = schedules.some(isOverlapping);

    // If there's no overlap or the schedules array is empty, return true
    return !hasOverlap || schedules.length === 0;
  };

  const resetScheduleForm = () => {
    setWeekdays({
      lunes: false,
      martes: false,
      miercoles: false,
      jueves: false,
      viernes: false,
      sabado: false,
      domingo: false,
    });
    setStartDate(new Date());
    setEndDate(new Date());
  };

  const isInvalidTime = (start, end) => {
    if (start <= end) {
      return false;
    } else {
      return true;
    }
  };

  const handleEndDateTimeChange = (date) => {
    let endTime = moment(date._d).format("HH:mm");
    setEnd(endTime + ":00");
    setEndDate(date._d);
  };

  const handleStartDateTimeChange = (date) => {
    let startTime = moment(date._d).format("HH:mm");
    setStart(startTime + ":00");
    setStartDate(date._d);
  };

  const onDeleteScheduleClick = (showSchedule) => {
    const updatedSchedules = showSchedules.filter((item) => {
      return (
        item.weekDay !== showSchedule.weekDay ||
        item.startTime !== showSchedule.startTime ||
        item.endTime !== showSchedule.endTime
      );
    });

    onEditSchedules(updatedSchedules);
  };

  const handleChangeRadioInput = (weekday) => {
    const updatedWeekdays = { ...weekdays };
    updatedWeekdays[weekday] = true;
    for (const day in updatedWeekdays) {
      if (day !== weekday) {
        updatedWeekdays[day] = false;
      }
    }
    setWeekdays(updatedWeekdays);
    setWeekDay(weekday);
  };

  return (
    <>
      <Row>
        <Col>
          <ListGroup>
            {showSchedules?.map((showSchedule) => {
              return (
                <ListGroupItem
                  key={`${showSchedule.id}-${showSchedule.weekDay}-${showSchedule.startTime}-${showSchedule.endTime}`}
                >
                  <Row>
                    <Col xs={10}>
                      <span>{showSchedule.weekDay}</span>{" "}
                      <span>de {showSchedule.startTime}</span>{" "}
                      <span>a {showSchedule.endTime}</span>
                    </Col>
                    <Col xs={2}>
                      <Trash
                        size={18}
                        onClick={() => {
                          onDeleteScheduleClick(showSchedule);
                        }}
                        className="float-center mt-1 mb-2 width-25 d-block"
                        style={{
                          color: "#FF586B",
                          cursor: "pointer",
                        }}
                      />
                    </Col>
                  </Row>
                </ListGroupItem>
              );
            })}
          </ListGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <br></br>
          <Button
            className="btn-square float-right"
            size="sm"
            outline
            color="primary"
            onClick={() => setModalSchedule(true)}
          >
            <Clock size={16} color="#009DA0" />
            {"  "}
            Agregar Horario
          </Button>
          <br></br>
        </Col>
      </Row>
      {/*errorNotScheduleSelected ? (
        <div
          className="invalid-feedback"
          style={{ display: "block", textAlign: "center" }}
        >
          {"Debe indicar al menos un horario para el Programa"}
        </div>
      ) : null*/}
      <Modal
        isOpen={modalSchedule}
        toggle={() => setModalSchedule(!modalSchedule)}
      >
        <ModalHeader>
          <Label>Agregar Horario</Label>
        </ModalHeader>
        <ModalBody>
          <FormGroup tag="fieldset">
            <h6>Seleccione día de la semana</h6>
            <Row>
              <Col xs={6}>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="monday"
                    name="customRadio"
                    label="Lunes"
                    onClick={() => handleChangeRadioInput("lunes")}
                  />
                </FormGroup>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="tuesday"
                    name="customRadio"
                    label="Martes"
                    onClick={() => handleChangeRadioInput("martes")}
                  />
                </FormGroup>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="wednesday"
                    name="customRadio"
                    label="Miércoles"
                    onClick={() => handleChangeRadioInput("miercoles")}
                  />
                </FormGroup>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="thursday"
                    name="customRadio"
                    label="Jueves"
                    onClick={() => handleChangeRadioInput("jueves")}
                  />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="friday"
                    name="customRadio"
                    label="Viernes"
                    onClick={() => handleChangeRadioInput("viernes")}
                  />
                </FormGroup>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="saturday"
                    name="customRadio"
                    label="Sábado"
                    onClick={() => handleChangeRadioInput("sabado")}
                  />
                </FormGroup>
                <FormGroup check className="px-0">
                  <CustomInput
                    type="radio"
                    id="sunday"
                    name="customRadio"
                    label="Domingo"
                    onClick={() => handleChangeRadioInput("domingo")}
                  />
                </FormGroup>
              </Col>
            </Row>
          </FormGroup>
          <Row>
            <Col xs={6}>
              <div>
                <Label>Hora de Inicio</Label>
                <DateTime
                  name="start"
                  className={""}
                  dateFormat={false}
                  onChange={(e) => handleStartDateTimeChange(e)}
                  value={startDate}
                  // inputProps={ '00:00:00' }
                />
              </div>
            </Col>
            <Col xs={6}>
              <div>
                <Label>Hora de Finalización</Label>
                <DateTime
                  name="end"
                  className={""}
                  dateFormat={false}
                  onChange={(e) => handleEndDateTimeChange(e)}
                  value={endDate}
                  // inputProps={ '00:00:00' }
                />
              </div>
            </Col>
          </Row>
          <Row>
            {errorCrash ? (
              <div
                className="invalid-feedback"
                style={{
                  display: "block",
                  textAlign: "center",
                }}
              >
                {
                  "Debe indicar una hora de inicio y fin válida. Existe choque de horarios"
                }
              </div>
            ) : null}
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button
            className="btn-square"
            outline
            color="primary"
            onClick={handleAddSchedule}
            size="sm"
            disabled={isInvalidTime(start, end)}
          >
            Agregar
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ShowSchedules;
