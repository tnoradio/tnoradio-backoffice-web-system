import React, { Fragment, useState, useEffect } from "react";
import { Media } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { getUserImage } from "../../redux/actions/users/fetchUserImage";
import MyDropzone from "../../components/shared/MyDropzone";
import { updateUserProfileImage } from "../../redux/actions/users/updateUserImage";

const UserImage = ({ user }) => {
  const Buffer = require("buffer").Buffer;
  const [data, setData] = React.useState({});
  const dispatch = useDispatch();
  const [imageProfileFiles, setProfileImageFiles] = useState([]);

  useEffect(() => {
    getUserProfileImage();
  }, [user.slug]);

  const getUserProfileImage = async () => {
    const imageFile = await getUserImage("profile", user.slug);
    const imageData = Buffer.from(imageFile?.file.data, "binary").toString(
      "base64"
    );
    setData(imageData);
  };

  useEffect(() => {
    //console.log(imageProfileFiles);
    if (
      user !== undefined &&
      imageProfileFiles[0] !== undefined &&
      imageProfileFiles[0] !== null
    )
      dispatch(
        updateUserProfileImage(
          imageProfileFiles[0],
          "profile",
          "profile",
          user.slug,
          "profiles",
          user._id
        )
      );
  }, [imageProfileFiles]);

  return (
    <Fragment>
      {user ? (
        <div className="contact-app-content-detail">
          <PerfectScrollbar>
            <Media className="rounded-circle width-70" href="#">
              <MyDropzone
                layout={"user"}
                image={`data:image/jpeg;base64,${data}`}
                setImageFiles={setProfileImageFiles}
                disabled={true}
              ></MyDropzone>
            </Media>
          </PerfectScrollbar>
        </div>
      ) : null}
    </Fragment>
  );
};

UserImage.prototype = {
  user: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      lastName: PropTypes.string,
      bio: PropTypes.string,
      image: PropTypes.string,
      phone: PropTypes.string,
      address: PropTypes.string,
      notes: PropTypes.string,
      company: PropTypes.string,
      department: PropTypes.string,
      roles: PropTypes.array,
      gender: PropTypes.string,
      images: PropTypes.array,
      socials: PropTypes.array,
      dni: PropTypes.number,
      email: PropTypes.object.isRequired,
      isActive: PropTypes.bool,
      isStarred: PropTypes.bool,
      isDeleted: PropTypes.bool,
      updatedBy: PropTypes.string,
    }).isRequired
  ).isRequired,
};
export default UserImage;
