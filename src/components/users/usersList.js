import React from "react";
import PropTypes from "prop-types";
import User from "./user";
import PerfectScrollbar from "react-perfect-scrollbar";

const UsersList = (props) => {
  const { active, users, toggleStarredUser, deleteUser, userDetails } = props;

  return (
    <div className="contact-app-list">
      <PerfectScrollbar>
        <div id="users-list">
          <ul className="list-group">
            {users.map((user) => (
              <User
                key={user._id}
                active={active}
                {...user}
                onStarredClick={() => toggleStarredUser(user._id)}
                onDeleteClick={() => deleteUser(user._id)}
                onUserClick={() => userDetails(user._id)}
              />
            ))}
          </ul>
        </div>
      </PerfectScrollbar>
    </div>
  );
};

UsersList.prototype = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      lastName: PropTypes.string,
      bio: PropTypes.bio,
      image: PropTypes.string,
      department: PropTypes.string,
      phone: PropTypes.string,
      address: PropTypes.string,
      notes: PropTypes.string,
      company: PropTypes.string,
      roles: PropTypes.array,
      socials: PropTypes.array,
      dni: PropTypes.number,
      gender: PropTypes.string,
      slug: PropTypes.string,
      images: PropTypes.array,
      email: PropTypes.object.isRequired,
      isActive: PropTypes.bool,
      isStarred: PropTypes.bool,
      isDeleted: PropTypes.bool,
      updatedBy: PropTypes.string,
    }).isRequired
  ).isRequired,
  toggleStarredUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  userDetails: PropTypes.func.isRequired,
};
export default UsersList;
