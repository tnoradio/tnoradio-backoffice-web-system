import React from "react";
import { Modal, ModalHeader } from "reactstrap";
import FilterLink from "../../containers/users/usersFilterLink";
import { userVisibilityFilter } from "../../redux/actions/users";
import AddUser from "../../containers/users/addUser";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";

class UsersFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
    this.state.modal === false && this.props.seed();
  }

  render() {
    return (
      <div className="contact-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="contact-app-sidebar-content">
            <div className="contact-app-menu">
              <div className="form-group form-group-compose text-center">
                <button
                  type="button"
                  className="btn btn-raised btn-danger btn-block my-2 shadow-z-2"
                  onClick={this.toggle}
                >
                  <Icon.UserPlus size={18} className="mr-1" /> Nuevo Usuario
                </button>
              </div>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Flitrar
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={userVisibilityFilter.SHOW_ALL}>
                  <Icon.Users size={18} className="mr-1" /> Todos
                </FilterLink>
                <FilterLink filter={userVisibilityFilter.DELETED_USER}>
                  <Icon.Trash
                    size={18}
                    style={{ color: "#FF586B" }}
                    className="mr-1"
                  />{" "}
                  Eliminados
                </FilterLink>
                <FilterLink filter={userVisibilityFilter.STARRED_USER}>
                  <Icon.Star
                    size={18}
                    style={{ color: "#FFC107" }}
                    className="mr-1"
                  />{" "}
                  Destacados
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Rol de Usuario
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink
                  filter={userVisibilityFilter.EMPLOYEE_USER}
                  roleValue="EMPLOYEE"
                >
                  <Icon.Circle size={18} className="mr-1 danger" />
                  Empleado
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.CLIENT_USER}
                  roleValue="CLIENT"
                >
                  <Icon.Circle size={18} className="mr-1 warning" />
                  Cliente
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.POTENTIAL_CLIENT_USER}
                  roleValue="POTENTIAL_CLIENT"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Cliente Potencial
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.AUDIENCE_USER}
                  roleValue="AUDIENCE"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Audiencia
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.ADMIN_USER}
                  roleValue="ADMIN"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Administrador
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.PRODUCER_USER}
                  roleValue="PRODUCER"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Productor
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.HOST_USER}
                  roleValue="HOST"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Locutor
                </FilterLink>
                <FilterLink
                  filter={userVisibilityFilter.PRODUCTION_ASSISTANT_USER}
                  roleValue="PRODUCTION_ASSISTANT"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Asistente de Producción
                </FilterLink>
              </ul>
            </div>
          </div>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={this.props.className}
            size="lg"
          >
            <ModalHeader toggle={this.toggle}>Agregar un Usuario</ModalHeader>
            <AddUser toggle={this.toggle} />
          </Modal>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default UsersFilter;
