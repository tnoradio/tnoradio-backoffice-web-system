import React, { useEffect } from "react";
import { Row, Col } from "reactstrap";
import PropTypes from "prop-types";
import { Star, Trash } from "react-feather";
import * as userUtils from "../../utility/users/userUtils";
import { getUserImage } from "../../redux/actions/users/fetchUserImage";

const User = ({
  onUserClick,
  onStarredClick,
  onDeleteClick,
  _id,
  name,
  lastName,
  roles,
  isStarred,
  active,
  slug,
}) => {
  const [data, setData] = React.useState(""); //imageData
  var Buffer = require("buffer").Buffer;

  useEffect(() => {
    getUserProfileImage();
  }, [slug]);

  const getUserProfileImage = async () => {
    const imageFile = await getUserImage("profile", slug);
    const imageData = Buffer.from(imageFile?.file.data, "binary").toString(
      "base64"
    );
    setData(imageData);
  };

  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === _id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row>
        <Col xs={3} onClick={onUserClick}>
          <img
            src={`data:image/jpeg;base64,${data}`}
            className="rounded-circle width-50"
            alt={name}
            lazy={true}
          />
        </Col>
        <Col xs={7} onClick={onUserClick}>
          <p className="mb-0 text-truncate">
            {name} {lastName}
          </p>
          <p className="mb-0 text-muted font-small-3">
            {userUtils.translateRole(roles)}
          </p>
        </Col>
        <Col xs={2}>
          <Trash
            size={18}
            onClick={onDeleteClick}
            className="float-right mt-1 mb-2 width-25 d-block"
            style={{ color: "#FF586B" }}
          />
          <Star
            size={18}
            onClick={onStarredClick}
            className="float-right width-25 d-block"
            style={{ color: isStarred ? "#FFC107" : "#495057" }}
          />
        </Col>
      </Row>
    </li>
  );
};

User.propTypes = {
  _id: PropTypes.string,
  name: PropTypes.string,
  lastName: PropTypes.string,
  bio: PropTypes.string,
  department: PropTypes.string,
  image: PropTypes.string,
  phone: PropTypes.string,
  address: PropTypes.string,
  notes: PropTypes.string,
  company: PropTypes.string,
  roles: PropTypes.array,
  gender: PropTypes.string,
  images: PropTypes.array,
  socials: PropTypes.array,
  dni: PropTypes.number,
  email: PropTypes.object.isRequired,
  isActive: PropTypes.bool,
  isStarred: PropTypes.bool,
  isDeleted: PropTypes.bool,
  updatedBy: PropTypes.string,
  onStarredClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onUserClick: PropTypes.func,
};

export default User;
