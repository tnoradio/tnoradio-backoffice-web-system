import React, { Fragment, useState, useEffect } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  FormGroup,
  CustomInput,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import * as userUtils from "../../utility/users/userUtils";
import { useDispatch, useSelector } from "react-redux";
import fetchUserImage from "../../redux/actions/users/fetchUserImage";
import { getUserProfileImage } from "../../redux/actions/users/fetchUserImage";
import MyDropzone from "../../components/shared/MyDropzone";
import updateUserImage from "../../redux/actions/users/updateUserImage";

let roles = [];
const UserDetails = ({ selectedUser, onEditClick, editUserFlag, onChange }) => {
  const [internImageData, setInternImageData] = React.useState({});
  const [websiteImageData, setWebsiteImageData] = React.useState({});
  const dispatch = useDispatch();
  const [potentialClient, setPotentialClient] = useState(false);
  const [admin, setAdmin] = useState(false);
  const [employee, setEmployee] = useState(false);
  const [client, setClient] = useState(false);
  const [host, setHost] = useState(false);
  const [audience, setAudience] = useState(false);
  const [producer, setProducer] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [intern, setIntern] = useState(false);
  const [productionAssistant, setProductionAssistant] = useState(false);
  const [imageProfileFiles, setProfileImageFiles] = useState([]);
  const [imageTalentFiles, setTalentImageFiles] = useState([]);
  const state = useSelector((state) => state);
  const Buffer = require("buffer").Buffer;

  useEffect(() => {
    if (
      state.userApp.fetchUserImage.userImage !== {} &&
      state.userApp.fetchUserImage.userImage !== undefined &&
      state.userApp.fetchUserImage.userImage.file !== undefined &&
      state.userApp.fetchUserImage.userImage.file?.data
    )
      setWebsiteImageData(
        Buffer.from(
          state.userApp.fetchUserImage.userImage.file?.data || "",
          "binary"
        ).toString("base64")
      );
  }, [state.userApp.fetchUserImage.userImage]);

  useEffect(() => {
    if (
      state.userApp.fetchUserImage.userProfileImage !== {} &&
      state.userApp.fetchUserImage.userProfileImage !== undefined &&
      state.userApp.fetchUserImage.userProfileImage.file !== undefined &&
      state.userApp.fetchUserImage.userProfileImage.file?.data
    )
      setInternImageData(
        Buffer.from(
          state.userApp.fetchUserImage.userProfileImage.file?.data || "",
          "binary"
        ).toString("base64")
      );
  }, [state.userApp.fetchUserImage.userProfileImage]);

  useEffect(() => {
    if (selectedUser !== undefined) {
      roles = [];
      selectedUser.roles.forEach((userRole) => {
        roles.push({ role: userRole.role });
      });

      setIsActive(selectedUser?.isActive);

      if (selectedUser.roles.some((role) => role.role === "AUDIENCE") === true)
        setAudience(true);
      if (selectedUser.roles.some((role) => role.role === "INTERN") === true)
        setIntern(true);
      if (selectedUser.roles.some((role) => role.role === "ADMIN") === true)
        setAdmin(true);
      if (selectedUser.roles.some((role) => role.role === "EMPLOYEE") === true)
        setEmployee(true);
      if (selectedUser.roles.some((role) => role.role === "CLIENT") === true)
        setClient(true);
      if (selectedUser.roles.some((role) => role.role === "HOST") === true)
        setHost(true);
      if (selectedUser.roles.some((role) => role.role === "PRODUCER") === true)
        setProducer(true);
      if (
        selectedUser.roles.some(
          (role) => role.role === "PRODUCTION_ASSISTANT"
        ) === true
      )
        setProductionAssistant(true);
      if (
        selectedUser.roles.some((role) => role.role === "POTENTIAL_CLIENT") ===
        true
      )
        setPotentialClient(true);

      //Get user images
      dispatch(fetchUserImage("profile", selectedUser.slug));
      dispatch(getUserProfileImage("talent", selectedUser.slug));
    }
  }, [selectedUser]);

  useEffect(() => {
    if (audience === true) {
      if (roles.some((role) => role.role === "AUDIENCE") === false) {
        roles.push({ role: "AUDIENCE" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "AUDIENCE") === true) {
        roles = roles.filter((role) => role.role !== "AUDIENCE");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [audience]);

  useEffect(() => {
    if (admin === true) {
      if (roles.some((role) => role.role === "ADMIN") === false) {
        roles.push({ role: "ADMIN" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "ADMIN") === true) {
        roles = roles.filter((role) => role.role !== "ADMIN");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [admin]);

  useEffect(() => {
    if (employee === true) {
      if (roles.some((role) => role.role === "EMPLOYEE") === false) {
        roles.push({ role: "EMPLOYEE" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      //console.log("Es falso employee");
      if (roles.some((role) => role.role === "EMPLOYEE") === true) {
        roles = roles.filter((role) => role.role !== "EMPLOYEE");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [employee]);

  useEffect(() => {
    if (client === true) {
      if (roles.some((role) => role.role === "CLIENT") === false) {
        roles.push({ role: "CLIENT" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "CLIENT") === true) {
        roles = roles.filter((role) => role.role !== "CLIENT");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [client]);

  useEffect(() => {
    if (potentialClient === true) {
      if (roles.some((role) => role.role === "POTENTIAL_CLIENT") === false) {
        roles.push({ role: "POTENTIAL_CLIENT" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "POTENTIAL_CLIENT") === true) {
        roles = roles.filter((role) => role.role !== "POTENTIAL_CLIENT");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [potentialClient]);

  useEffect(() => {
    if (host === true) {
      if (roles.some((role) => role.role === "HOST") === false) {
        roles.push({ role: "HOST" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "HOST") === true) {
        roles = roles.filter((role) => role.role !== "HOST");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [host]);

  useEffect(() => {
    if (producer === true) {
      if (roles.some((role) => role.role === "PRODUCER") === false) {
        roles.push({ role: "PRODUCER" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "PRODUCER") === true) {
        roles = roles.filter((role) => role.role !== "PRODUCER");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [producer]);

  useEffect(() => {
    if (productionAssistant === true) {
      if (
        roles.some((role) => role.role === "PRODUCTION_ASSISTANT") === false
      ) {
        roles.push({ role: "PRODUCTION_ASSISTANT" });
        onChange(selectedUser._id, "roles", roles);
      }
    } else {
      if (roles.some((role) => role.role === "PRODUCTION_ASSISTANT") === true) {
        roles = roles.filter((role) => role.role !== "PRODUCTION_ASSISTANT");
        onChange(selectedUser._id, "roles", roles);
      }
    }
  }, [productionAssistant]);

  useEffect(() => {
    //console.log(imageProfileFiles);
    if (
      selectedUser !== undefined &&
      imageProfileFiles[0] !== undefined &&
      imageProfileFiles[0] !== null
    )
      dispatch(
        updateUserImage(
          imageProfileFiles[0],
          "profile",
          "profile",
          selectedUser.slug,
          "profiles",
          selectedUser._id
        )
      );
  }, [imageProfileFiles]);

  useEffect(() => {
    if (
      selectedUser !== undefined &&
      imageTalentFiles[0] !== undefined &&
      imageTalentFiles[0] !== null
    )
      dispatch(
        updateUserImage(
          imageTalentFiles[0],
          "talent",
          "talent",
          selectedUser.slug,
          "talents",
          selectedUser._id
        )
      );
  }, [imageTalentFiles]);

  useEffect(() => {}, [isActive, selectedUser]);

  return (
    <Fragment>
      {selectedUser ? (
        <div className="contact-app-content-detail">
          <Row>
            <Col className="text-left">
              <Media heading>
                {selectedUser.name} {selectedUser.lastName}
              </Media>
              {userUtils.translateRole(selectedUser.roles)}
            </Col>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editUserFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media className="mt-4">
              <Media>
                <Media className="col-4" left href="#">
                  Perfil
                  {editUserFlag ? (
                    <MyDropzone
                      layout={"user"}
                      image={`data:image/jpeg;base64,${internImageData}`}
                      setImageFiles={setProfileImageFiles}
                      disabled={false}
                    ></MyDropzone>
                  ) : (
                    <MyDropzone
                      layout={"user"}
                      image={`data:image/jpeg;base64,${internImageData}`}
                      setImageFiles={setProfileImageFiles}
                      disabled={true}
                    ></MyDropzone>
                  )}
                </Media>
                <Media className="col-4" left href="#">
                  Website
                  {editUserFlag ? (
                    <MyDropzone
                      layout={"user"}
                      image={`data:image/jpeg;base64,${websiteImageData}`}
                      setImageFiles={setTalentImageFiles}
                      disabled={false}
                    ></MyDropzone>
                  ) : (
                    <MyDropzone
                      layout={"user"}
                      image={`data:image/jpeg;base64,${websiteImageData}`}
                      setImageFiles={setTalentImageFiles}
                      disabled={true}
                    ></MyDropzone>
                  )}
                </Media>
                <Media className="col-4" left href="#"></Media>
              </Media>
            </Media>
            <Table responsive borderless size="sm" className="mt-4">
              <tbody>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Nombre</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="name"
                        id="name"
                        defaultValue={selectedUser.name}
                        onChange={(e) =>
                          onChange(selectedUser._id, "name", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.name
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Apellido</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="lastName"
                        id="lastName"
                        defaultValue={selectedUser.lastName}
                        onChange={(e) =>
                          onChange(selectedUser._id, "lastName", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.lastName
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Slug para Enlace</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="slug"
                        id="slug"
                        defaultValue={selectedUser.slug}
                        onChange={(e) =>
                          onChange(selectedUser._id, "slug", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.slug
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Cumpleaños</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="date"
                        name="birthdate"
                        id="birthdate"
                        defaultValue={
                          new Date(selectedUser.birthdate).toLocaleDateString(
                            "en-GB"
                          ) || new Date(Date.now())
                        }
                        onChange={(e) =>
                          onChange(
                            selectedUser._id,
                            "birthdate",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " +
                      new Date(selectedUser.birthdate).toLocaleDateString(
                        "en-GB"
                      )
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Email</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="email"
                        disabled
                        id="email"
                        defaultValue={selectedUser.email.value}
                        onChange={(e) =>
                          onChange(selectedUser._id, "email", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.email.value
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Biografía</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="textarea"
                        name="bio"
                        id="bio"
                        defaultValue={selectedUser.bio}
                        onChange={(e) =>
                          onChange(selectedUser._id, "bio", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.bio
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Teléfono</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="phone"
                        id="phone"
                        defaultValue={selectedUser.phone}
                        onChange={(e) =>
                          onChange(selectedUser._id, "phone", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.phone
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Dirección</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="address"
                        id="address"
                        defaultValue={selectedUser.address}
                        onChange={(e) =>
                          onChange(selectedUser._id, "address", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.address
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Compañía</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="company"
                        id="company"
                        defaultValue={selectedUser.company}
                        onChange={(e) =>
                          onChange(selectedUser._id, "company", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.company
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Departamento</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="department"
                        id="department"
                        defaultValue={selectedUser.department}
                        onChange={(e) =>
                          onChange(
                            selectedUser._id,
                            "department",
                            e.target.value
                          )
                        }
                      />
                    ) : (
                      ": " + selectedUser.department
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Rol:</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <>
                        <Row>
                          <Col xs={5}>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="ADMIN"
                                label="Admin"
                                defaultChecked={admin}
                                onChange={() => {
                                  setAdmin((admin) => !admin);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="AUDIENCE"
                                label="Audiencia"
                                defaultChecked={audience}
                                onChange={() => {
                                  setAudience(!audience);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="EMPLOYEE"
                                label="Empleado"
                                defaultChecked={employee}
                                onChange={() => {
                                  setEmployee((employee) => !employee);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="CLIENT"
                                label="Cliente"
                                defaultChecked={client}
                                onChange={() => {
                                  setClient((client) => !client);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="PRODUCER"
                                label="Productor"
                                defaultChecked={producer}
                                onChange={() => {
                                  setProducer((producer) => !producer);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="INTERN"
                                label="Pasante"
                                onChange={() => {
                                  setIntern((intern) => !intern);
                                }}
                              />
                            </FormGroup>
                          </Col>
                          <Col xs={7}>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="POTENTIAL_CLIENT"
                                label="Cliente Potencial"
                                defaultChecked={potentialClient}
                                onChange={() => {
                                  setPotentialClient(
                                    (potentialClient) => !potentialClient
                                  );
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="HOST"
                                label="Locutor"
                                defaultChecked={host}
                                onChange={() => {
                                  setHost((host) => !host);
                                }}
                              />
                            </FormGroup>
                            <FormGroup check className="px-0">
                              <CustomInput
                                type="checkbox"
                                id="PRODUCTION_ASSISTANT"
                                label="Asistente de Producción"
                                defaultChecked={productionAssistant}
                                onChange={() => {
                                  setProductionAssistant(
                                    (productionAssistant) =>
                                      !productionAssistant
                                  );
                                }}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        {/*  <Input
                                    type="text"
                                    name="role"
                                    id="role"
                                    defaultValue={
                                       selectedUser.role
                                    }
                                    onChange={e => onChange(selectedUser._id, "roles", e.target.value)}
                                 />*/}
                      </>
                    ) : (
                      ": " + userUtils.translateRole(selectedUser.roles)
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Usuario Activo</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <FormGroup check className="px-0">
                        <CustomInput
                          type="checkbox"
                          id="isActive"
                          label="Activo"
                          defaultChecked={selectedUser?.isActive}
                          onChange={() => {
                            setIsActive((isActive) => !isActive);
                            onChange(
                              selectedUser._id,
                              "isActive",
                              !selectedUser?.isActive
                            );
                          }}
                        />
                      </FormGroup>
                    ) : (
                      <>: {isActive === true ? "Si" : "No"}</>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Notas</td>
                  <td className="col-9">
                    {editUserFlag ? (
                      <Input
                        type="text"
                        name="notes"
                        id="notes"
                        defaultValue={selectedUser.notes}
                        onChange={(e) =>
                          onChange(selectedUser._id, "notes", e.target.value)
                        }
                      />
                    ) : (
                      ": " + selectedUser.notes
                    )}
                  </td>
                </tr>
              </tbody>
            </Table>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="">
          <div className="mt-2">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un usuario para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

UserDetails.prototype = {
  selectedUser: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      lastName: PropTypes.string,
      bio: PropTypes.string,
      image: PropTypes.string,
      phone: PropTypes.string,
      address: PropTypes.string,
      notes: PropTypes.string,
      company: PropTypes.string,
      department: PropTypes.string,
      roles: PropTypes.array,
      gender: PropTypes.string,
      images: PropTypes.array,
      socials: PropTypes.array,
      dni: PropTypes.number,
      email: PropTypes.object.isRequired,
      isActive: PropTypes.bool,
      isStarred: PropTypes.bool,
      isDeleted: PropTypes.bool,
      updatedBy: PropTypes.string,
    }).isRequired
  ).isRequired,
};
export default UserDetails;
