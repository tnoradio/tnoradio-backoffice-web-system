import React from "react";

import VisiblePaymentList from "../../containers/payment/visiblePaymentList";

const App = () => (
  <div>
    <VisiblePaymentList />
  </div>
);

export default App;
