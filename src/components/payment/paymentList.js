import PropTypes from "prop-types";
import Payment from "./payment";
import { useDispatch } from "react-redux";
import { updatePayment } from "../../redux/actions/payments";
import { useState } from "react";
import { Table, Label, FormGroup, Input } from "reactstrap";

const PaymentList = ({
  active,
  payments,
  toggleStarredPayment,
  deletePayment,
  paymentDetails,
}) => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="post-app-list">
      <div id="payments-list">
        <Table responsive>
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Concepto</th>
              <th>Mon</th>
              <th>Monto</th>
            </tr>
          </thead>
          <tbody>
            {payments.map((payment) => {
              return (
                <Payment
                  key={payment._id}
                  active={active}
                  payment_date={payment.payment_date}
                  {...payment}
                  onStarredClick={() => {
                    dispatch(
                      updatePayment(
                        payment._id,
                        "starred",
                        !payment.starred,
                        payment
                      )
                    );
                    toggleStarredPayment(payment._id);
                  }}
                  onDeleteClick={() => deletePayment(payment._id)}
                  isOpen={isOpen}
                  toggle={toggle}
                  onPaymentClick={() => {
                    paymentDetails(payment._id);
                    toggle();
                  }}
                />
              );
            })}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

PaymentList.prototype = {
  payments: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      amount: PropTypes.number,
      owner_id: PropTypes.string,
      show_owner: PropTypes.number,
      receipts: PropTypes.array,
      payment_date: PropTypes.string,
      concept: PropTypes.string,
      currecy: PropTypes.string,
      reconciled: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
  toggleStarredPayment: PropTypes.func.isRequired,
  deletePayment: PropTypes.func.isRequired,
  paymentDetails: PropTypes.func.isRequired,
};
export default PaymentList;
