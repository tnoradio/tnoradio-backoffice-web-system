import React, { Component, Fragment } from "react";
import {
  Row,
  Modal,
  ModalHeader,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import classnames from "classnames";
import { paymentVisibilityFilter } from "../../redux/actions/payments";

class PaymentTabs extends Component {
  state = {
    activeTab: "0",
  };

  toggleTab = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  render() {
    const { onClick } = this.props;

    return (
      <Nav tabs className="nav-border-bottom">
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "0",
            })}
            onClick={() => {
              this.toggleTab("0");
              onClick(paymentVisibilityFilter.SHOW_ALL);
            }}
          >
            Todo
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "1",
            })}
            onClick={() => {
              this.toggleTab("1");
              onClick(paymentVisibilityFilter.USD_PAYMENT);
            }}
          >
            USD
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "2",
            })}
            onClick={() => {
              this.toggleTab("2");
              onClick(paymentVisibilityFilter.BS_PAYMENT);
            }}
          >
            Bs.
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "3",
            })}
            onClick={() => {
              this.toggleTab("3");
              onClick(paymentVisibilityFilter.EURO_PAYMENT);
            }}
          >
            Euro
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "4",
            })}
            onClick={() => {
              this.toggleTab("4");
              onClick(paymentVisibilityFilter.CRYPTO_PAYMENT);
            }}
          >
            Crypto
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "5",
            })}
            onClick={() => {
              this.toggleTab("5");
              onClick(paymentVisibilityFilter.OTHER_PAYMENT);
            }}
          >
            Otro
          </NavLink>
        </NavItem>
      </Nav>
    );
  }
}

export default PaymentTabs;
