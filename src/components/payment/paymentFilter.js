import React from "react";
import FilterLink from "../../containers/payment/paymentFilterLink";
import { paymentVisibilityFilter } from "../../redux/actions/payments";
import * as Icon from "react-feather";

class PaymentFilter extends React.Component {
  render() {
    return (
      <div className="post-app-sidebar float-left d-none d-xl-block">
        <div className="post-app-sidebar-content">
          <div className="post-app-menu">
            <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
              Flitrar
            </h6>
            <ul className="list-group list-group-messages">
              <FilterLink filter={paymentVisibilityFilter.SHOW_ALL}>
                <Icon.DollarSign size={18} className="mr-1" /> Pagos
              </FilterLink>
              <FilterLink filter={paymentVisibilityFilter.DELETED_PAYMENT}>
                <Icon.Trash
                  size={18}
                  style={{ color: "#FF586B" }}
                  className="mr-1"
                />{" "}
                Eliminados
              </FilterLink>
              <FilterLink filter={paymentVisibilityFilter.STARRED_PAYMENT}>
                <Icon.Star
                  size={18}
                  style={{ color: "#FFC107" }}
                  className="mr-1"
                />{" "}
                Destacados
              </FilterLink>
              <FilterLink
                filter={paymentVisibilityFilter.NOT_RECONCILED_PAYMENT}
                genreValue="NO CONCILIADO"
              >
                <Icon.Circle size={18} className="mr-1 danger" />
                No conciliado
              </FilterLink>
              <FilterLink
                filter={paymentVisibilityFilter.RECONCILED_PAYMENT}
                genreValue="CONCILIADO"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Conciliado
              </FilterLink>
            </ul>
            <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
              Conceptos
            </h6>
            <ul className="list-group list-group-messages">
              <FilterLink
                filter={paymentVisibilityFilter.POST_PROD_PAYMENT}
                genreValue="POST PRODUCCIÓN"
              >
                <Icon.Circle size={18} className="mr-1 info" />
                Post Producción
              </FilterLink>
              <FilterLink
                filter={paymentVisibilityFilter.RECORDING_STUDIO_PAYMENT}
                genreValue="RECORDING STUDIO"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Estudio de grabación
              </FilterLink>
              <FilterLink
                filter={paymentVisibilityFilter.SHOW_PAYMENT}
                genreValue="SHOW"
              >
                <Icon.Circle size={18} className="mr-1 warning" />
                Programa
              </FilterLink>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentFilter;
