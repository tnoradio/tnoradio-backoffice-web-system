import React, { Fragment, useState, useEffect } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  FormGroup,
  Label,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import MyFilesDropzone from "../shared/MyFilesDropzone";
import { fetchShowById } from "../../redux/actions/shows/fetchShowDetail";
import dateFormat from "dateformat";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { destroyFile } from "../../redux/actions/payments";
import { fetchRecordingTurns } from "../../redux/actions/recordingTurns";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

const PaymentDetails = ({
  selectedPayment,
  onEditClick,
  editPaymentFlag,
  onChange,
}) => {
  let showList = [];
  let turnsList = [];
  let showGroupOptions = [];
  let turnsGroupOptions = [];
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  const shows = state.showApp.shows;
  const recordingTurns = state.recordingTurnsApp.recordingTurns;
  const [receiptFiles, setReceiptFiles] = useState([]);
  const [showName, setShowName] = useState("");
  const [turnDescription, setTurnDescription] = useState("");
  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  if (shows) {
    shows.forEach((show) => {
      show.producers.forEach((producer) => {
        if (producer.userId === state.session?.user._id || isUserAdmin)
          showList.push({ value: show.id, label: show.name });
      });
      if (showList.some((show) => show.id).length === 0)
        show.hosts.forEach((host) => {
          if (host.userId === state.session?.user._id)
            showList.push({ value: show.id, label: show.name });
        });
    });

    showGroupOptions = [
      {
        label: "Programas",
        options: showList,
      },
    ];
  } else {
    showList = [];
  }
  if (recordingTurns) {
    recordingTurns.forEach((recordingTurn) => {
      if (recordingTurn.owner_id === state.session?.user._id)
        turnsList.push({
          value: recordingTurn._id,
          label: recordingTurn.description,
        });
    });

    turnsGroupOptions = [
      {
        label: "Pautas",
        options: turnsList,
      },
    ];
  } else {
    turnsList = [];
  }
  useEffect(() => {}, [receiptFiles]);

  useEffect(() => {
    dispatch(fetchRecordingTurns());
  }, []);

  useEffect(() => {
    fetchShowName();
    fetchTurnDescription();
  }, [selectedPayment]);

  useEffect(() => {
    if (
      selectedPayment &&
      selectedPayment.receipts &&
      selectedPayment.receipts.length !== 0
    )
      setReceiptFiles(selectedPayment.receipts);
  }, [selectedPayment]);

  const fetchShowName = async () => {
    if (selectedPayment && selectedPayment.show_owner) {
      const res = await fetchShowById(selectedPayment.show_owner);
      setShowName(res?.name);
    }
  };
  const fetchTurnDescription = () => {
    if (
      selectedPayment &&
      selectedPayment.recording_turn_owner &&
      recordingTurns !== undefined
    ) {
      const description = recordingTurns.filter(
        (turn) => turn._id === selectedPayment.recording_turn_owner
      )[0]?.description;
      setTurnDescription(description);
    }
  };

  const onDelete = (fileId) => {
    try {
      var filtered = receiptFiles.filter((file) => file.fileId !== fileId);
      onChange(selectedPayment._id, "receipts", filtered, selectedPayment);
      setReceiptFiles(filtered);
      destroyFile(fileId);
    } catch (e) {}
  };

  return (
    <Fragment>
      {selectedPayment ? (
        <div className="contact-app-content-detail">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editPaymentFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media heading>{selectedPayment.concept}</Media>
            Pago por el Programa {showName}
            <Table responsive borderless size="sm" className="mt-4">
              <tbody>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Concepto</td>
                  <td className="col-9">
                    {editPaymentFlag ? (
                      <Input
                        type="text"
                        name="concept"
                        id="concept"
                        defaultValue={selectedPayment.concept}
                        onChange={(e) =>
                          onChange(
                            selectedPayment._id,
                            "concept",
                            e.target.value,
                            selectedPayment
                          )
                        }
                      />
                    ) : (
                      ": " + selectedPayment.concept
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Monto</td>
                  <td className="col-9">
                    {editPaymentFlag ? (
                      <Input
                        type="text"
                        name="amount"
                        id="amount"
                        defaultValue={selectedPayment.amount}
                        onChange={(e) =>
                          onChange(
                            selectedPayment._id,
                            "amount",
                            e.target.value,
                            selectedPayment
                          )
                        }
                      />
                    ) : (
                      `: ${selectedPayment.amount}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Moneda</td>
                  <td className="col-9">
                    {editPaymentFlag ? (
                      <FormGroup
                        tag="fieldset"
                        onChange={(e) =>
                          onChange(
                            selectedPayment._id,
                            "currency",
                            e.target.id,
                            selectedPayment
                          )
                        }
                      >
                        <FormGroup check>
                          <Label check>
                            <Input
                              type="radio"
                              name="currency"
                              id="USD"
                              value="USD"
                              defaultChecked={
                                selectedPayment.currency === "USD"
                              }
                            />{" "}
                            Dólares
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check>
                            <Input
                              type="radio"
                              name="currency"
                              id="Bs."
                              value="Bs."
                              defaultChecked={
                                selectedPayment.currency === "Bs."
                              }
                            />{" "}
                            Bolívares
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check>
                            <Input
                              type="radio"
                              name="currency"
                              id="other"
                              value="other"
                              defaultChecked={
                                selectedPayment.currency === "other"
                              }
                            />{" "}
                            Otro
                          </Label>
                        </FormGroup>
                      </FormGroup>
                    ) : (
                      <>
                        {selectedPayment.currency === "USD" ? (
                          <span>: Dólar</span>
                        ) : null}
                        {selectedPayment.currency === "Bs." ? (
                          <span>: Bolívares</span>
                        ) : null}
                        {selectedPayment.currency === "other" ? (
                          <span>: Otro</span>
                        ) : null}
                      </>
                    )}
                  </td>
                </tr>

                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Programa</td>
                  <td className="col-9">
                    {editPaymentFlag ? (
                      <>
                        {showList !== undefined &&
                        showList[0] !== undefined &&
                        showList[0] !== null ? (
                          <Select
                            id="show_owner"
                            value={showList.filter(
                              (obj) => obj.value === selectedPayment.show_owner
                            )}
                            options={showGroupOptions}
                            formatGroupLabel={formatGroupLabel}
                            onChange={(e) => {
                              onChange(
                                selectedPayment._id,
                                "show_owner",
                                e.value,
                                selectedPayment
                              );
                            }}
                          />
                        ) : null}
                      </>
                    ) : (
                      `: ${showName}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Pauta de Grabación</td>
                  <td className="col-9">
                    {editPaymentFlag ? (
                      <>
                        {turnsList !== undefined &&
                        turnsList[0] !== undefined &&
                        turnsList[0] !== null ? (
                          <Select
                            id="recording_turn_owner"
                            value={turnsList.filter(
                              (obj) =>
                                obj.value ===
                                selectedPayment.recording_turn_owner
                            )}
                            options={turnsGroupOptions}
                            formatGroupLabel={formatGroupLabel}
                            onChange={(e) => {
                              onChange(
                                selectedPayment._id,
                                "recording_turn_owner",
                                e.value,
                                selectedPayment
                              );
                            }}
                          />
                        ) : null}
                      </>
                    ) : (
                      `: ${turnDescription}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Método de Pago</td>
                  <td className="col-9">
                    {editPaymentFlag ? (
                      <Input
                        type="select"
                        id="method"
                        name="method"
                        onChange={(e) =>
                          onChange(
                            selectedPayment._id,
                            "method",
                            e.target.value,
                            selectedPayment
                          )
                        }
                      >
                        <option value="none" defaultValue="" disabled="">
                          Seleccione
                        </option>
                        <option
                          value="cash"
                          id="cash"
                          selected={selectedPayment.method === "cash"}
                        >
                          Efectivo
                        </option>
                        <option
                          value="zelle"
                          id="zelle"
                          selected={selectedPayment.method === "zelle"}
                        >
                          Zelle
                        </option>
                        <option
                          value="transferBs."
                          id="transferBs."
                          selected={selectedPayment.method === "transferBs."}
                        >
                          Transferencia Bs.
                        </option>
                        <option
                          value="transferUSD"
                          id="transferUSD"
                          selected={selectedPayment.method === "transferUSD"}
                        >
                          Transferencia Dólares
                        </option>
                        <option
                          value="mobile"
                          id="mobile"
                          selected={selectedPayment.method === "mobile"}
                        >
                          Pago Móvil
                        </option>
                        <option
                          value="paypal"
                          id="paypal"
                          selected={selectedPayment.method === "paypal"}
                        >
                          Paypal
                        </option>
                        <option
                          value="other"
                          id="other"
                          selected={selectedPayment.method === "other"}
                        >
                          Otro
                        </option>
                      </Input>
                    ) : (
                      `: ${selectedPayment.method}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Fecha de Pago</td>
                  <td className="col-5">
                    {editPaymentFlag ? (
                      <Input
                        type="date"
                        name="payment_date"
                        id="date"
                        value={dateFormat(
                          selectedPayment.payment_date,
                          "yyyy-mm-dd"
                        )}
                        onChange={(e) =>
                          onChange(
                            selectedPayment._id,
                            "payment_date",
                            e.target.value,
                            selectedPayment
                          )
                        }
                      />
                    ) : (
                      `: ${dateFormat(
                        selectedPayment.payment_date,
                        "dd-mm-yyyy"
                      )} `
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Conciliado</td>
                  <td className="col-9">
                    {editPaymentFlag && isUserAdmin ? (
                      <FormGroup
                        tag="fieldset"
                        onChange={(e) =>
                          onChange(
                            selectedPayment._id,
                            "reconciled",
                            e.target.id,
                            selectedPayment
                          )
                        }
                      >
                        <FormGroup check>
                          <Label check>
                            <Input
                              type="radio"
                              name="reconciled"
                              id={"true"}
                              value={"true"}
                              defaultChecked={
                                selectedPayment.reconciled === true
                              }
                            />{" "}
                            Sí
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check>
                            <Input
                              type="radio"
                              name="reconciled"
                              id={"false"}
                              value={"false"}
                              defaultChecked={
                                selectedPayment.reconciled === false
                              }
                            />{" "}
                            No
                          </Label>
                        </FormGroup>
                      </FormGroup>
                    ) : (
                      <>
                        {selectedPayment.reconciled === true ? (
                          <span>: Sí</span>
                        ) : null}
                        {selectedPayment.reconciled === false ? (
                          <span>: No</span>
                        ) : null}
                      </>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Recibos</td>
                  <td className="col-9"></td>
                </tr>
              </tbody>
            </Table>
            <Media>
              {editPaymentFlag ? (
                <MyFilesDropzone
                  isEditPayment
                  layout={"user"}
                  onDelete={onDelete}
                  setReceiptFiles={setReceiptFiles}
                  disabled={false}
                  showName={showName}
                  id={selectedPayment._id}
                  selectedPayment={selectedPayment}
                  receiptFiles={receiptFiles}
                  onChange={onChange}
                ></MyFilesDropzone>
              ) : (
                <MyFilesDropzone
                  layout={"user"}
                  setReceiptFiles={setReceiptFiles}
                  disabled={true}
                  showName={showName}
                  onDelete={onDelete}
                  id={selectedPayment._id}
                  selectedPayment={selectedPayment}
                  receiptFiles={receiptFiles}
                  isEditPayment
                  onChange={onChange}
                ></MyFilesDropzone>
              )}
            </Media>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un pago para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

PaymentDetails.prototype = {
  selectedPayment: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      amount: PropTypes.number,
      owner_id: PropTypes.string,
      show_owner: PropTypes.number,
      concept: PropTypes.string,
      currency: PropTypes.string,
      payment_date: PropTypes.string,
      receipts: PropTypes.array,
      reconciled: PropTypes.bool,
      isActive: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
};
export default PaymentDetails;
