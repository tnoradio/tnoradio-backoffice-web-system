import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

const PaginationComponent = ({
  currentPage,
  prevPage,
  totalPages,
  handlePageClick,
  handlePrevPageClick,
  handleNextPageClick,
  handleDotsClick,
  fabricatePaginationArray,
}) => {
  return (
    <Pagination
      aria-label="Posts page navigation"
      disabled={currentPage <= 0}
      className="d-flex justify-content-center"
    >
      <PaginationItem disabled={prevPage === 0}>
        <PaginationLink
          previous
          onClick={(e) => handlePrevPageClick(e)}
          href="#"
        ></PaginationLink>
      </PaginationItem>
      <PaginationItem active={1 === currentPage}>
        <PaginationLink onClick={(e) => handlePrevPageClick(e, 1)} href="#">
          1
        </PaginationLink>
      </PaginationItem>
      {currentPage >= 6 && (
        <PaginationItem>
          <PaginationLink onClick={(e) => handleDotsClick(e)} href="#">
            ...
          </PaginationLink>
        </PaginationItem>
      )}
      {fabricatePaginationArray(currentPage).map((page, i) => (
        <PaginationItem active={page === currentPage} key={i}>
          <PaginationLink onClick={() => handlePageClick(page)} href="#">
            {page}
          </PaginationLink>
        </PaginationItem>
      ))}
      <PaginationItem disabled={currentPage === totalPages}>
        <PaginationLink
          onClick={(e) => handleNextPageClick(e)}
          next
          href="#"
        ></PaginationLink>
      </PaginationItem>
    </Pagination>
  );
};

export default PaginationComponent;
