import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import "../../assets/scss/views/components/extra/upload.scss";
import { toastr } from "react-redux-toastr";

export default function MyDropzone(props) {
  const [files, setFiles] = useState([]);
  const [isSelectedImage, setIsSelectedImage] = useState(false);
  const { layout } = props;
  const noDisplayStyle = {
    display: "none",
    visibility: "hidden",
  };
  const userStyle = {
    padding: "0px",
  };
  const displayStyle = {
    display: "flex",
    visibility: "visible",
    maxWidth: layout === "post" ? "40%" : "100%",
  };
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      setIsSelectedImage(true);
      props.setImageFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
    },
    onDropRejected: (rejected) => {
      toastr.error("Sólo se admiten imágenes");
    },
  });

  const thumbs = files.map((file) => (
    <div key={file.name}>
      <div>
        <img
          alt={file.name}
          src={file.preview}
          className={
            props.layout === "user"
              ? "rounded-circle img-thumbnail"
              : "img-thumbnail"
          }
        />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <section className={"container " + layout === "user" ? userStyle : ""}>
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} disabled={props.disabled} />
        <img
          style={isSelectedImage ? noDisplayStyle : displayStyle}
          src={props.image}
          className={
            props.layout === "user" ? "img-thumbnail" : "img-thumbnail"
          }
          alt="Imagen"
        />
        {!props.disabled && (
          <p>
            Click aquí o arrastre la nueva imagen y será actualizada
            automáticamente.
          </p>
        )}
        <aside>{thumbs}</aside>
      </div>
    </section>
  );
}
