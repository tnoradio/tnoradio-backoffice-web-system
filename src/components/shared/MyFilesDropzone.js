import React, { useCallback, useMemo } from "react";
import { useDropzone } from "react-dropzone";
import "../../assets/scss/views/components/extra/upload.scss";
import { toastr } from "react-redux-toastr";
import { Trash } from "react-feather";
import { uploadReceiptFile } from "../../redux/actions/payments";
import { Progress } from "reactstrap";
import { Link } from "react-router-dom";

const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "5px",
  //borderWidth: 2,
  //borderRadius: 2,
  //borderColor: "#eeeeee",
  //borderStyle: "dashed",
  color: "success",
  outline: "none",
  textDecoration: "underline",
  transition: "border .24s ease-in-out",
  cursor: "pointer",
};

const focusedStyle = {
  //borderColor: "#2196f3",
};

const acceptStyle = {
  // borderColor: "#00e676",
};

export default function MyFilesDropzone(props) {
  const [loaded, setLoaded] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(false);
  const {
    onDelete,
    disabled,
    setReceiptFiles,
    isEditPayment,
    selectedPayment,
    showName,
    onChange,
    saveFiles,
    setSaveFiles,
    files,
  } = props;

  const Dropzone = () => {
    const onDrop = useCallback((acceptedFiles) => {
      acceptedFiles.forEach((file) => {
        if (isEditPayment) {
          saveFile(file);
        } else {
          var files = saveFiles;
          files.push(file);
          setSaveFiles([...saveFiles, file]);
          setSaveFiles(files);
        }
      });
    }, []);
    const onDropRejected = useCallback((rejected) => {
      toastr.error("No se admite archivo");
    }, []);
    const { getRootProps, getInputProps, isFocused, isDragAccept } =
      useDropzone({
        onDrop,
        onDropRejected,
      });

    const style = useMemo(
      () => ({
        ...baseStyle,
        ...(isFocused ? focusedStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
      }),
      [isFocused, isDragAccept]
    );

    const saveFile = async (file) => {
      progressValue();
      setIsLoading(true);
      const receipt = await uploadReceiptFile(selectedPayment, file, showName);
      setLoaded(100);
      setIsLoading(false);
      var files = files;
      files.push(receipt);
      setReceiptFiles(files);
      onChange(selectedPayment._id, "receipts", files, selectedPayment);
    };

    const progressValue = () => {
      let width = 1;
      return setInterval(() => {
        if (width < 100) {
          width++;
          setLoaded(width);
        }
      }, 60);
    };

    return (
      <div>
        {isEditPayment ? (
          <section className="container">
            <aside className="post-list-group-item">
              {files &&
                files.length >= 0 &&
                files.map((file, index) => (
                  <li key={file.fileId}>
                    <a href={file.fileUrl} target="_blank" rel="noreferrer">
                      Recibo {index + 1}
                    </a>
                    <Trash
                      size={15}
                      hidden={disabled}
                      style={{
                        color: "#FF586B",
                        marginBottom: "0.3rem",
                        marginLeft: "1rem",
                        cursor: "pointer",
                      }}
                      onClick={() => onDelete(file.fileId)}
                    />
                  </li>
                ))}
            </aside>
            <aside>
              <Progress
                animated
                color="info"
                value={loaded}
                hidden={!isLoading}
              />
              <div className="text-center" hidden={!isLoading}>
                {loaded}%
              </div>
            </aside>

            <div {...getRootProps({ style })} hidden={disabled}>
              <input {...getInputProps()} disabled={props.disabled} />
              <p>Haga click aquí para agregar un archivo</p>
            </div>
          </section>
        ) : (
          <section className="container pb-4">
            <aside className="post-list-group-item">
              {files &&
                files.length >= 0 &&
                files?.map((file) => (
                  <li key={file.fileId}>
                    <Link to={{ pathname: file.fileUrl }} target="_blank">
                      {file.fileName}
                    </Link>
                  </li>
                ))}
            </aside>
          </section>
        )}
      </div>
    );
  };

  return <Dropzone onDrop={(acceptedFiles) => console.log(acceptedFiles)} />;
}
