import React, { Component } from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import Select from "react-select";
import "react-datetime/css/react-datetime.css";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { AtSign, Trash } from "react-feather";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const tagsFormSchema = Yup.object().shape({
  userName: Yup.string().required("Debes escribir un nombre."),
});

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

export default class Tag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tag: "",
      tagsState: this.props.tags,
    };

    const tagsGroupOptions = [
      {
        label: "Etiquetas",
        options: props.tags,
      },
    ];
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs={6}>
            <ListGroup>
              {this.props.tags.map((tag) => {
                return (
                  <ListGroupItem key={tag._id + tag.tag}>
                    <Row>
                      <Col xs={10}>
                        <span>{tag.tag}</span>
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            this.props.onDeleteTagClick(tag);
                            this.setState({ tagsState: this.props.tags });
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={6}></Col>
        </Row>
        <Row>
          <Col xs={12}>
            <br></br>
            <Button
              className="btn-square"
              size="md"
              outline
              color="primary"
              onClick={() => this.setState({ tagsModal: true })}
            >
              <AtSign size={16} color="#009DA0" />
              {"   "}
              Agregar Red Social
            </Button>
          </Col>
        </Row>
        <Modal
          isOpen={this.state.tagsModal}
          toggle={() => this.setState({ tagsModal: !this.state.tagsModal })}
        >
          <ModalHeader>
            <Label>Agregar Red Social</Label>
          </ModalHeader>
          <Formik
            initialValues={{
              userName: "",
              url: "",
              tagNetwork: "",
            }}
            validationSchema={tagsFormSchema}
            onSubmit={(values) => {
              ////console.log(values);
              // //console.log(this.state.tagNetwork);
              this.props.addSocialSubmit(
                values.userName,
                this.state.tagNetwork,
                this.state.tagUrl
              );
              ////console.log(this.props.tags);
              this.setState({
                tagsModal: false,
                tagsState: this.props.tags,
              });
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <ModalBody>
                  {this.props.tags !== undefined &&
                  tagNetworks[0] !== undefined &&
                  tagNetworks[0] !== null ? (
                    <Row>
                      <Col xs={12}>
                        <FormGroup>
                          <Select
                            defaultValue={tagNetworks[0].label}
                            options={tagsGroupOptions}
                            formatGroupLabel={formatGroupLabel}
                            value={tagNetworks.find(
                              (obj) => obj === this.state.tagsState
                            )}
                            onChange={(e) => {
                              //console.log(e);
                              this.setState({ tagNetwork: e.label });
                              this.setState({ tagUrl: e.url });
                              //console.log(this.state.tagNetwork);
                            }}
                            id="tagNetwork"
                            name="tagNetwork"
                          />
                          <Label for="name">Nombre de Usuario</Label>
                          <Field
                            className={`form-control ${
                              errors.userName &&
                              touched.userName &&
                              "is-invalid"
                            }`}
                            name="userName"
                            id="userName"
                            required
                          />
                          {errors.userName && touched.userName ? (
                            <div className="invalid-feedback">
                              {errors.userName}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                  ) : (
                    <Row></Row>
                  )}
                </ModalBody>
                <ModalFooter>
                  <Button
                    className="btn-square"
                    outline
                    color="primary"
                    //  onClick={(e) => {
                    //console.log(e)
                    //   }}
                    type="submit"
                  >
                    Agregar
                  </Button>{" "}
                </ModalFooter>
              </Form>
            )}
          </Formik>
        </Modal>
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ tagsState: nextProps.tags });
  }
}
