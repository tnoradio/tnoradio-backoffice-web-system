import React from "react";
import { PulseLoader } from "react-spinners";
import "../../../src/assets/scss/components/spinner/_spinner.scss";

const SpinnerLoader = ({ loading }) => {
  return (
    <div className="loadingSpinner" hidden={!loading}>
      <PulseLoader loading={loading} color="#009da0" />
    </div>
  );
};

export default SpinnerLoader;
