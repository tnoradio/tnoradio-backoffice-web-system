import React, { Component } from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import Select from "react-select";

import "react-datetime/css/react-datetime.css";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

import { AtSign, Trash } from "react-feather";

const socialNetworks = [
  { value: "Facebook", label: "Facebook", url: "https://www.facebook.com/" },
  { value: "Twitter", label: "Twitter", url: "https://www.twitter.com/" },
  { value: "Instagram", label: "Instagram", url: "https://www.instagram.com/" },
  { value: "Snapchat", label: "Snapchat", url: "" },
  { value: "Tik Tok", label: "Tik Tok", url: "https://www.tiktok.com/@" },
  { value: "Youtube", label: "Youtube", url: "" },
  { value: "Other", label: "Otra", url: "" },
];

const socialsGroupOptions = [
  {
    label: "Redes Sociales",
    options: socialNetworks,
  },
];

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const socialsFormSchema = Yup.object().shape({
  userName: Yup.string().required("Debes escribir un nombre."),
});

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

export default class SocialNetwork extends Component {
  constructor(props) {
    super(props);
    //console.log(props);
    this.state = {
      socialsModal: false,
      socialNetwork: "",
      socialUrl: "",
      socialsState: this.props.socials,
    };
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs={6}>
            <ListGroup>
              {this.props.socials.map((social) => {
                //console.log(this.props.socials);
                return (
                  <ListGroupItem key={social.socialNetwork + social.userName}>
                    <Row>
                      <Col xs={10}>
                        <span>{social.socialNetwork}</span>
                        {"                "}
                        <span>@{social.userName}</span>{" "}
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            this.props.onDeleteSocialClick(social);
                            this.setState({ socialsState: this.props.socials });
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={6}></Col>
        </Row>
        <Row>
          <Col xs={12}>
            <br></br>
            <Button
              className="btn-square"
              size="md"
              outline
              color="primary"
              onClick={() => this.setState({ socialsModal: true })}
            >
              <AtSign size={16} color="#009DA0" />
              {"   "}
              Agregar Red Social
            </Button>
          </Col>
        </Row>
        <Modal
          isOpen={this.state.socialsModal}
          toggle={() =>
            this.setState({ socialsModal: !this.state.socialsModal })
          }
        >
          <ModalHeader>
            <Label>Agregar Red Social</Label>
          </ModalHeader>
          <Formik
            initialValues={{
              userName: "",
              url: "",
              socialNetwork: "",
            }}
            validationSchema={socialsFormSchema}
            onSubmit={(values) => {
              ////console.log(values);
              // //console.log(this.state.socialNetwork);
              this.props.addSocialSubmit(
                values.userName,
                this.state.socialNetwork,
                this.state.socialUrl
              );
              ////console.log(this.props.socials);
              this.setState({
                socialsModal: false,
                socialsState: this.props.socials,
              });
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <ModalBody>
                  {this.props.socials !== undefined &&
                  socialNetworks[0] !== undefined &&
                  socialNetworks[0] !== null ? (
                    <Row>
                      <Col xs={12}>
                        <FormGroup>
                          <Select
                            defaultValue={socialNetworks[0].label}
                            options={socialsGroupOptions}
                            formatGroupLabel={formatGroupLabel}
                            value={socialNetworks.find(
                              (obj) => obj === this.state.socialsState
                            )}
                            onChange={(e) => {
                              //console.log(e);
                              this.setState({ socialNetwork: e.label });
                              this.setState({ socialUrl: e.url });
                              //console.log(this.state.socialNetwork);
                            }}
                            id="socialNetwork"
                            name="socialNetwork"
                          />
                          <Label for="name">Nombre de Usuario</Label>
                          <Field
                            className={`form-control ${
                              errors.userName &&
                              touched.userName &&
                              "is-invalid"
                            }`}
                            name="userName"
                            id="userName"
                            required
                          />
                          {errors.userName && touched.userName ? (
                            <div className="invalid-feedback">
                              {errors.userName}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Col>
                    </Row>
                  ) : (
                    <Row></Row>
                  )}
                </ModalBody>
                <ModalFooter>
                  <Button
                    className="btn-square"
                    outline
                    color="primary"
                    //  onClick={(e) => {
                    //console.log(e)
                    //   }}
                    type="submit"
                  >
                    Agregar
                  </Button>{" "}
                </ModalFooter>
              </Form>
            )}
          </Formik>
        </Modal>
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ socialsState: nextProps.socials });
  }
}
