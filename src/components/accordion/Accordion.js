import React, { Fragment, Children, cloneElement, useState } from "react";

const Accordion = ({ children }) => {
  const [selectedAccordionItem, setSelectedAccordionItem] = useState(-1);

  const toggle = (accordionIndex) => {
    setSelectedAccordionItem(
      selectedAccordionItem === accordionIndex ? -1 : accordionIndex
    );
  };

  const Nodes = Children.map(children, (child, index) => {
    if (child.type === AccordionItem) {
      return cloneElement(child, {
        index,
        toggle,
        selected: index === selectedAccordionItem,
        ...child.props,
      });
    }
  });

  return <div className="accordion">{Nodes}</div>;
};

const AccordionItem = ({ children, toggle, index, selected, render }) => (
  <Fragment>
    <div className="accordion-item" onClick={() => toggle(index)}>
      {render && render()} {/* Can pass state and do extra stuff */}
    </div>
    {selected && children}
  </Fragment>
);

Accordion.AccordionItem = AccordionItem;

export default Accordion;
