import PropTypes from "prop-types";
import Expense from "./expense";
import { useDispatch } from "react-redux";
import { updateExpense } from "../../redux/actions/expenses";
import { useState } from "react";
import { Table } from "reactstrap";

const ExpenseList = (props) => {
  const {
    active,
    expenses,
    toggleStarredExpense,
    deleteExpense,
    expenseDetails,
  } = props;
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  console.log(expenses);
  return (
    <div className="post-app-list">
      <div id="expenses-list">
        <Table responsive>
          <thead>
            <tr>
              <th>índice</th>
              <th>Fecha</th>
              <th>Concepto</th>
              <th>Mon</th>
              <th>Monto</th>
            </tr>
          </thead>
          <tbody>
            {expenses.map((expense, index) => {
              return (
                <Expense
                  index={index}
                  key={expense._id}
                  active={active}
                  date={expense.date}
                  {...expense}
                  onStarredClick={() => {
                    dispatch(
                      updateExpense(
                        expense._id,
                        "starred",
                        !expense.starred,
                        expense
                      )
                    );
                    toggleStarredExpense(expense._id);
                  }}
                  onDeleteClick={() => deleteExpense(expense._id)}
                  isOpen={isOpen}
                  toggle={toggle}
                  onExpenseClick={() => {
                    expenseDetails(expense._id);
                    toggle();
                  }}
                />
              );
            })}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

ExpenseList.prototype = {
  expenses: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      amountBs: PropTypes.number,
      amountUsd: PropTypes.number,
      amountEuro: PropTypes.number,
      amountCrypto: PropTypes.number,
      owner_id: PropTypes.string,
      show_owner: PropTypes.number,
      receipts: PropTypes.array,
      date: PropTypes.string,
      concept: PropTypes.string,
      currecy: PropTypes.string,
      reconciled: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
      method: PropTypes.string,
      recording_turn_owner: PropTypes.string,
    }).isRequired
  ).isRequired,
  toggleStarredExpense: PropTypes.func.isRequired,
  deleteExpense: PropTypes.func.isRequired,
  expenseDetails: PropTypes.func.isRequired,
};
export default ExpenseList;
