import React, { Component, Fragment } from "react";
import {
  Row,
  Modal,
  ModalHeader,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import classnames from "classnames";
import { expenseVisibilityFilter } from "../../redux/actions/expenses";

class ExpenseTabs extends Component {
  state = {
    activeTab: "0",
  };

  toggleTab = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  render() {
    const { onClick } = this.props;

    return (
      <Nav tabs className="nav-border-bottom">
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "0",
            })}
            onClick={() => {
              this.toggleTab("0");
              onClick(expenseVisibilityFilter.SHOW_ALL);
            }}
          >
            Todo
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "1",
            })}
            onClick={() => {
              this.toggleTab("1");
              onClick(expenseVisibilityFilter.USD_EXPENSE);
            }}
          >
            USD
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "2",
            })}
            onClick={() => {
              this.toggleTab("2");
              onClick(expenseVisibilityFilter.BS_EXPENSE);
            }}
          >
            Bs.
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "3",
            })}
            onClick={() => {
              this.toggleTab("3");
              onClick(expenseVisibilityFilter.EURO_EXPENSE);
            }}
          >
            Euro
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "4",
            })}
            onClick={() => {
              this.toggleTab("4");
              onClick(expenseVisibilityFilter.CRYPTO_EXPENSE);
            }}
          >
            Crypto
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: this.state.activeTab === "5",
            })}
            onClick={() => {
              this.toggleTab("5");
              onClick(expenseVisibilityFilter.OTHER_EXPENSE);
            }}
          >
            Otro
          </NavLink>
        </NavItem>
      </Nav>
    );
  }
}

export default ExpenseTabs;
