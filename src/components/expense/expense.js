import React from "react";
import { Row, Col, Modal, ModalHeader } from "reactstrap";
import PropTypes from "prop-types";
import {
  ChevronDown,
  ChevronUp,
  ChevronsDown,
  ChevronsRight,
  DollarSign,
  PlusCircle,
  Star,
  Trash,
} from "react-feather";
import ExpenseDetails from "../../containers/expense/visibleExpenseDetails";
import { Table } from "reactstrap";
import dateFormat from "dateformat";

const baseStyle = {
  flexDirection: "column",
  alignItems: "center",
};

const Expense = ({
  onExpenseClick,
  onStarredClick,
  onDeleteClick,
  concept,
  active,
  _id,
  deleted,
  currency,
  starred,
  amountUsd,
  amountBs,
  amountOther,
  amountCrypto,
  amountEuro,
  date,
  expense_type,
  isOpen,
  toggle,
  index,
}) => {
  let newDate = new Date(date);
  newDate.setMinutes(newDate.getMinutes() + newDate.getTimezoneOffset());

  return (
    <tr>
      <td>{index}</td>
      <td className="col-2 text-nowrap font-small-2">
        {dateFormat(newDate, "dd-mm-yyyy")}
      </td>
      <td className="col-5" onClick={onExpenseClick}>
        <p className="mb-0 text-truncate">
          {active ? <span>{concept}</span> : <span>{concept} </span>}
        </p>
      </td>
      <td className="col-1 font-small-2">{currency}</td>
      <td className="col-2 font-small-2">
        {currency === "USD" ? <>{amountUsd}</> : null}
        {currency === "crypto" ? <>{amountCrypto}</> : null}
        {currency === "Euro" ? <>{amountEuro}</> : null}
        {currency === "Bs" ? <>{amountBs}</> : null}
        {currency === "other" ? <>{amountOther}</> : null}
      </td>
      <td className="col-2 font-small-2">
        <Trash
          size={15}
          className="danger"
          onClick={onDeleteClick}
          style={{ color: active ? "#FF586B" : "#495057" }}
        />
      </td>
      <Modal
        isOpen={isOpen}
        toggle={toggle}
        // className={this.props.className}
        size="lg"
      >
        <ModalHeader toggle={toggle}>Gasto</ModalHeader>
        <ExpenseDetails />
      </Modal>
    </tr>
  );
};

Expense.propTypes = {
  _id: PropTypes.string,
  concept: PropTypes.string,
  amountUsd: PropTypes.number,
  amountBs: PropTypes.number,
  amountOther: PropTypes.number,
  amountCrypto: PropTypes.number,
  amountEuro: PropTypes.number,
  owner_id: PropTypes.string,
  currency: PropTypes.string,
  starred: PropTypes.bool,
  deleted: PropTypes.bool,
  index: PropTypes.number,
  expense_type: PropTypes.string,
  onStarredClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onExpenseClick: PropTypes.func,
};

export default Expense;
