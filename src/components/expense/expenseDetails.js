import React, { Fragment, useState, useEffect } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  FormGroup,
  Label,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import MyFilesDropzone from "../shared/MyFilesDropzone";
import dateFormat from "dateformat";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { destroyFile } from "../../redux/actions/expenses";
import { fetchRecordingTurns } from "../../redux/actions/recordingTurns";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

const ExpenseDetails = ({
  selectedExpense,
  onEditClick,
  editExpenseFlag,
  onChange,
}) => {
  let turnsList = [];
  let turnsGroupOptions = [];
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  const recordingTurns = state.recordingTurnsApp.recordingTurns;
  const [receiptFiles, setReceiptFiles] = useState([]);
  const [turnDescription, setTurnDescription] = useState("");
  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  if (recordingTurns) {
    recordingTurns.forEach((recordingTurn) => {
      if (recordingTurn.owner_id === state.session?.user._id)
        turnsList.push({
          value: recordingTurn._id,
          label: recordingTurn.description,
        });
    });

    turnsGroupOptions = [
      {
        label: "Pautas",
        options: turnsList,
      },
    ];
  } else {
    turnsList = [];
  }
  useEffect(() => {}, [receiptFiles]);

  useEffect(() => {
    dispatch(fetchRecordingTurns());
  }, []);

  useEffect(() => {
    fetchTurnDescription();
  }, [selectedExpense]);

  useEffect(() => {
    if (
      selectedExpense &&
      selectedExpense.receipts &&
      selectedExpense.receipts.length !== 0
    )
      setReceiptFiles(selectedExpense.receipts);
  }, [selectedExpense]);

  const fetchTurnDescription = () => {
    if (
      selectedExpense &&
      selectedExpense.recording_turn_owner &&
      recordingTurns !== undefined
    ) {
      const description = recordingTurns.filter(
        (turn) => turn._id === selectedExpense.recording_turn_owner
      )[0]?.description;
      setTurnDescription(description);
    }
  };

  const onDelete = (fileId) => {
    try {
      var filtered = receiptFiles.filter((file) => file.fileId !== fileId);
      onChange(selectedExpense._id, "receipts", filtered, selectedExpense);
      setReceiptFiles(filtered);
      destroyFile(fileId);
    } catch (e) {}
  };

  const getAmount = () => {
    switch (selectedExpense.currency) {
      case "USD":
        return selectedExpense.amountUsd;
      case "Bs":
        return selectedExpense.amountBs;
      case "OTHER":
        return selectedExpense.amountOther;
      case "CRYPTO":
        return selectedExpense.amountCrypto;
      case "EURO":
        return selectedExpense.amountEuro;
      default:
        return selectedExpense.amount;
    }
  };

  const getMethod = () => {
    switch (selectedExpense.method) {
      case "cash":
        return "Efectivo";
      case "zelle":
        return "Zelle";
      case "transferBs.":
        return "Transferencia Bs.";
      case "transferUSD":
        return "Transferencia en USA";
      case "transferPan":
        return "Transferencia Panamá";
      case "cardPan":
        return "Débito Panamá";
      case "mobile":
        return "Pago Móvil";
      case "paypal":
        return "Paypal";
      case "binance":
        return "Binance";
      default:
        return selectedExpense.method;
    }
  };

  const getAmountCurrencyFieldName = () => {
    switch (selectedExpense.currency) {
      case "USD":
        return "amountUsd";
      case "Bs":
        return "amountBs";
      case "OTHER":
        return "other";
      case "CRYPTO":
        return "amountCrypto";
      case "EURO":
        return "amountEuro";
      default:
        return "amount";
    }
  };

  let date = new Date(selectedExpense.date);
  date.setMinutes(date.getMinutes() + date.getTimezoneOffset());

  return (
    <Fragment>
      {selectedExpense ? (
        <div className="contact-app-content-detail pl-4 pr-4 pt-0">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editExpenseFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media heading>{selectedExpense.concept}</Media>
            <Table responsive borderless size="sm" className="mt-4">
              <tbody>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Concepto</td>
                  <td className="col-9">
                    {editExpenseFlag ? (
                      <Input
                        type="text"
                        name="concept"
                        id="concept"
                        defaultValue={selectedExpense.concept}
                        onChange={(e) =>
                          onChange(
                            selectedExpense._id,
                            "concept",
                            e.target.value,
                            selectedExpense
                          )
                        }
                      />
                    ) : (
                      ": " + selectedExpense.concept
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Monto</td>
                  <td className="col-9">
                    {editExpenseFlag ? (
                      <Input
                        type="text"
                        name={getAmountCurrencyFieldName()}
                        id={getAmountCurrencyFieldName()}
                        defaultValue={getAmount()}
                        onChange={(e) =>
                          onChange(
                            selectedExpense._id,
                            getAmountCurrencyFieldName(),
                            e.target.value,
                            selectedExpense
                          )
                        }
                      />
                    ) : (
                      `: ${getAmount()}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Moneda</td>
                  <td className="col-9">
                    {editExpenseFlag ? (
                      <FormGroup
                        tag="fieldset"
                        onChange={(e) =>
                          onChange(
                            selectedExpense._id,
                            "currency",
                            e.target.id,
                            selectedExpense
                          )
                        }
                      >
                        <FormGroup check>
                          <Label check for="USD">
                            <Input
                              type="radio"
                              name="currency"
                              id="USD"
                              value="USD"
                              defaultChecked={
                                selectedExpense.currency === "USD"
                              }
                            />{" "}
                            Dólares
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check for="Bs">
                            <Input
                              type="radio"
                              name="currency"
                              id="Bs"
                              value="Bs"
                              defaultChecked={selectedExpense.currency === "Bs"}
                            />{" "}
                            Bolívares
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check for="EURO">
                            <Input
                              type="radio"
                              name="currency"
                              id="EURO"
                              value="EURO"
                              defaultChecked={
                                selectedExpense.currency === "EURO"
                              }
                            />{" "}
                            Euro
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check for="CRYPTO">
                            <Input
                              type="radio"
                              name="currency"
                              id="CRYPTO"
                              value="CRYPTO"
                              defaultChecked={
                                selectedExpense.currency === "CRYPTO"
                              }
                            />{" "}
                            Crypto
                          </Label>
                        </FormGroup>
                        <FormGroup check>
                          <Label check for="other">
                            <Input
                              type="radio"
                              name="currency"
                              id="other"
                              value="other"
                              defaultChecked={
                                selectedExpense.currency === "other"
                              }
                            />{" "}
                            Otro
                          </Label>
                        </FormGroup>
                      </FormGroup>
                    ) : (
                      <>
                        {selectedExpense.currency === "USD" ? (
                          <span>: Dólar</span>
                        ) : null}
                        {selectedExpense.currency === "Bs" ? (
                          <span>: Bolívares</span>
                        ) : null}
                        {selectedExpense.currency === "EURO" ? (
                          <span>: Euro</span>
                        ) : null}
                        {selectedExpense.currency === "CRYPTO" ? (
                          <span>: Crypto</span>
                        ) : null}
                        {selectedExpense.currency === "OTHER" ? (
                          <span>: Otro</span>
                        ) : null}
                      </>
                    )}
                  </td>
                </tr>
                {selectedExpense.recording_turn_owner &&
                  selectedExpense.recording_turn_owner !== "" && (
                    <tr className="d-flex">
                      <td className="col-3 text-bold-400">Pauta/Evento</td>
                      <td className="col-9">
                        {editExpenseFlag ? (
                          <>
                            {turnsList !== undefined &&
                            turnsList[0] !== undefined &&
                            turnsList[0] !== null ? (
                              <Select
                                id="recording_turn_owner"
                                value={turnsList.filter(
                                  (obj) =>
                                    obj.value ===
                                    selectedExpense.recording_turn_owner
                                )}
                                options={turnsGroupOptions}
                                formatGroupLabel={formatGroupLabel}
                                onChange={(e) => {
                                  onChange(
                                    selectedExpense._id,
                                    "recording_turn_owner",
                                    e.value,
                                    selectedExpense
                                  );
                                }}
                              />
                            ) : null}
                          </>
                        ) : (
                          `: ${turnDescription}`
                        )}
                      </td>
                    </tr>
                  )}
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Método de Pago</td>
                  <td className="col-lg-3 col-sm-4">
                    {editExpenseFlag ? (
                      <Input
                        type="select"
                        id="method"
                        name="method"
                        onChange={(e) =>
                          onChange(
                            selectedExpense._id,
                            "method",
                            e.target.value,
                            selectedExpense
                          )
                        }
                      >
                        <option value="none" defaultValue="" disabled="">
                          Seleccione
                        </option>
                        {selectedExpense.currency !== "crypto" && (
                          <option
                            value="cash"
                            id="cash"
                            selected={selectedExpense.method === "cash"}
                          >
                            Efectivo
                          </option>
                        )}
                        {selectedExpense.currency === "USD" && (
                          <option
                            value="zelle"
                            id="zelle"
                            selected={selectedExpense.method === "zelle"}
                          >
                            Zelle
                          </option>
                        )}
                        {selectedExpense.currency === "Bs" && (
                          <option
                            value="transferBs."
                            id="transferBs."
                            selected={selectedExpense.method === "transferBs."}
                          >
                            Transferencia Bs.
                          </option>
                        )}
                        {selectedExpense.currency === "USD" && (
                          <option
                            value="transferUSD"
                            id="transferUSD"
                            selected={selectedExpense.method === "transferUSD"}
                          >
                            Transferencia en USA
                          </option>
                        )}
                        {selectedExpense.currency === "USD" && (
                          <option
                            value="transferPan"
                            id="transferPan"
                            selected={selectedExpense.method === "transferPan"}
                          >
                            Transferencia Panamá
                          </option>
                        )}
                        {selectedExpense.currency === "USD" ||
                          (selectedExpense.currency === "Bs" && (
                            <option
                              value="cardPan"
                              id="cardPan"
                              selected={selectedExpense.method === "cardPan"}
                            >
                              Débito Panamá
                            </option>
                          ))}
                        {selectedExpense.currency === "Bs" && (
                          <option
                            value="mobile"
                            id="mobile"
                            selected={selectedExpense.method === "mobile"}
                          >
                            Pago Móvil
                          </option>
                        )}
                        {selectedExpense.currency === "USD" && (
                          <option
                            value="paypal"
                            id="paypal"
                            selected={selectedExpense.method === "paypal"}
                          >
                            Paypal
                          </option>
                        )}
                        {selectedExpense.currency === "crypto" && (
                          <option
                            value="binance"
                            id="binance"
                            selectd={selectedExpense.method === "binance"}
                          >
                            Binance
                          </option>
                        )}
                        <option
                          value="other"
                          id="other"
                          selected={selectedExpense.method === "other"}
                        >
                          Otro
                        </option>
                      </Input>
                    ) : (
                      `: ${getMethod()}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Fecha de Pago</td>
                  <td className="col-lg-3 col-sm-4">
                    {editExpenseFlag ? (
                      <Input
                        type="date"
                        name="date"
                        id="date"
                        value={dateFormat(date, "yyyy-mm-dd")}
                        onChange={(e) =>
                          onChange(
                            selectedExpense._id,
                            "date",
                            e.target.value,
                            selectedExpense
                          )
                        }
                      />
                    ) : (
                      `: ${dateFormat(date, "dd-mm-yyyy")}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Recibos</td>
                  <td className="col-9"></td>
                </tr>
              </tbody>
            </Table>
            <Media>
              {editExpenseFlag ? (
                <MyFilesDropzone
                  isEditExpense
                  layout={"user"}
                  onDelete={onDelete}
                  setReceiptFiles={setReceiptFiles}
                  disabled={false}
                  showName={null}
                  id={selectedExpense._id}
                  selectedExpense={selectedExpense}
                  receiptFiles={receiptFiles}
                  files={receiptFiles}
                  onChange={onChange}
                ></MyFilesDropzone>
              ) : (
                <MyFilesDropzone
                  layout={"user"}
                  setReceiptFiles={setReceiptFiles}
                  showName={null}
                  onDelete={onDelete}
                  id={selectedExpense._id}
                  selectedExpense={selectedExpense}
                  files={receiptFiles}
                  isEditExpense
                  onChange={onChange}
                />
              )}
            </Media>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione un pago para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

ExpenseDetails.prototype = {
  selectedExpense: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      amountBs: PropTypes.number,
      amountEuro: PropTypes.number,
      amountCrypto: PropTypes.number,
      amountUsd: PropTypes.number,
      amountOther: PropTypes.number,
      owner_id: PropTypes.string,
      concept: PropTypes.string,
      currency: PropTypes.string,
      date: PropTypes.string,
      receipts: PropTypes.array,
      reconciled: PropTypes.bool,
      isActive: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
      method: PropTypes.string,
      recording_turn_owner: PropTypes.string,
    }).isRequired
  ).isRequired,
};
export default ExpenseDetails;
