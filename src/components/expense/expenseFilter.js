import React from "react";
import FilterLink from "../../containers/expense/expenseFilterLink";
import { expenseVisibilityFilter } from "../../redux/actions/expenses";
import * as Icon from "react-feather";

class ExpenseFilter extends React.Component {
  render() {
    return (
      <div className="post-app-sidebar float-left d-none d-xl-block">
        <div className="post-app-sidebar-content">
          <div className="post-app-menu">
            <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
              Flitrar
            </h6>
            <ul className="list-group list-group-messages">
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.SHOW_ALL}
              >
                <Icon.DollarSign size={18} className="mr-1" /> Gastos
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.DELETED_EXPENSE}
              >
                <Icon.Trash
                  size={18}
                  style={{ color: "#FF586B" }}
                  className="mr-1"
                />{" "}
                Eliminados
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.STARRED_EXPENSE}
              >
                <Icon.Star
                  size={18}
                  style={{ color: "#FFC107" }}
                  className="mr-1"
                />{" "}
                Destacados
              </FilterLink>
            </ul>
            <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
              Conceptos
            </h6>
            <ul className="list-group list-group-messages">
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.PRODUCTION_EXPENSE}
                genreValue="PRODUCTION"
              >
                <Icon.Circle size={18} className="mr-1 danger" />
                Producción
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.POST_PROD_EXPENSE}
                genreValue="POST PRODUCCIÓN"
              >
                <Icon.Circle size={18} className="mr-1 info" />
                Post Producción
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.TAX_EXPENSE}
                genreValue="TAX"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Pago de Impuestos
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.PAYROLL_EXPENSE}
                genreValue="PAYROLL"
              >
                <Icon.Circle size={18} className="mr-1 warning" />
                Nómina
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.PARKING_EXPENSE}
                genreValue="PARKING"
              >
                <Icon.Circle size={18} className="mr-1 info" />
                Estacionamiento
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.TRANSPORTATION_EXPENSE}
                genreValue="TRANSPORTATION"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Transporte
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.MARKETING_EXPENSE}
                genreValue="MARKETING"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Marketing
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.RENT_EXPENSE}
                genreValue="RENT"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Alquiler
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.REPRESENTATION_EXPENSE}
                genreValue="REPRESENTATION"
              >
                <Icon.Circle size={18} className="mr-1 warning" />
                Representación
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.EDUCATION_EXPENSE}
                genreValue="EDUCATION"
              >
                <Icon.Circle size={18} className="mr-1 warning" />
                Representación
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.FEE_EXPENSE}
                genreValue="FEE"
              >
                <Icon.Circle size={18} className="mr-1 info" />
                Honorarios
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.SUPPLIES_EXPENSE}
                genreValue="SUPPLIES"
              >
                <Icon.Circle size={18} className="mr-1 info" />
                Artículos de Oficina
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.EVENT_EXPENSE}
                genreValue="EVENT"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Eventos / Pautas
              </FilterLink>
              <FilterLink
                className="cursor-pointer"
                filter={expenseVisibilityFilter.RECORDING_EXPENSE}
                genreValue="RECORDING"
              >
                <Icon.Circle size={18} className="mr-1 success" />
                Grabación
              </FilterLink>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default ExpenseFilter;
