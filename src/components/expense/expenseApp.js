import React from "react";

import VisibleExpenseList from "../../containers/expense/visibleExpenseList";

const App = () => (
  <div>
    <VisibleExpenseList />
  </div>
);

export default App;
