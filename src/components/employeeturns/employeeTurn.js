import React from "react";
import { Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import PropTypes from "prop-types";
import { Plus, Trash } from "react-feather";
import moment from "moment";
import { Modal, ModalHeader, ModalBody } from "reactstrap";

const EmployeeTurn = ({
  onDeleteClick,
  startTime,
  endTime,
  date,
  activities,
}) => {
  const [modal, setModal] = React.useState(false);

  const toggle = () => setModal(!modal);

  const onDeleteActivityClick = (activity) => {
    const indexOfDelete = activities.indexOf(activity);
    activities.splice(indexOfDelete, 1);
  };

  const calculateActivityHours = () => {
    let total = 0;
    activities.forEach((activity) => {
      total += activity.duration;
    });
    return (total / 60).toFixed(2);
  };

  return (
    <li className={"list-group-item list-group-item-action no-border "}>
      <Row>
        <Col xs={3}>
          <p className="mb-0 text-center">
            {moment(date).format("DD/MM/YYYY")}
          </p>
        </Col>
        <Col xs={3}>
          <p className="mb-0 text-center">
            {moment(startTime).format("HH:mm:ss")}
          </p>
        </Col>
        <Col xs={3}>
          <p className="mb-0 text-muted text-center">
            {" "}
            {moment(endTime).format("HH:mm:ss")}
          </p>
        </Col>
        <Col xs={1}>
          <Trash
            size={18}
            onClick={onDeleteClick}
            className="text-center mt-1 mb-2 width-25 d-block cursor-pointer"
            style={{ color: "#FF586B" }}
          />
        </Col>
        <Col xs={2} className="d-flex primary cursor-pointer" onClick={toggle}>
          <div>Detalles</div>
          <div>
            <Plus
              size={18}
              className="text-center mt-1 mb-2 width-25 d-block"
            />
          </div>
        </Col>
      </Row>
      <>
        <Modal isOpen={modal} toggle={toggle} size="md">
          <ModalHeader toggle={toggle}>Asistencia</ModalHeader>
          <ModalBody>
            <Row>
              <Col xs={12}>
                <label>Actividades</label>
                <ListGroup>
                  {activities &&
                    activities.length > 0 &&
                    activities.map((activity) => {
                      return (
                        <ListGroupItem>
                          <Row key={activity._id}>
                            <Col xs={8}>
                              <span>{activity.name}</span>
                            </Col>
                            <Col xs={3}>
                              <span>{activity.duration} mins</span>
                            </Col>
                            <Col xs={1}>
                              <Trash
                                size={18}
                                className="float-right mt-1 mb-2 width-25 d-block"
                                style={{
                                  color: "#FF586B",
                                  "&:hover": {
                                    cursor: "pointer",
                                  },
                                }}
                                onClick={() => {
                                  onDeleteActivityClick(activity);
                                }}
                              />
                            </Col>
                          </Row>
                        </ListGroupItem>
                      );
                    })}
                </ListGroup>
              </Col>
            </Row>
            <br></br>
            <Row>
              <Col xs={12}>
                <label>{`Horas Trabajadas: ${moment(endTime).diff(
                  moment(startTime),
                  "hours"
                )}`}</label>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <label>{`Suma de Actividades: ${calculateActivityHours()}`}</label>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </>
    </li>
  );
};

EmployeeTurn.propTypes = {
  onDeleteClick: PropTypes.func.isRequired,
};

export default EmployeeTurn;
