import React from "react";
import FilterLink from "../../containers/employeeturns/employeeTurnsFilterLink";
import { Modal, ModalHeader } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";
import { connect } from "react-redux";
import { fetchUsers } from "../../redux/actions/users";
import AddEmployeeTurn from "../../containers/employeeturns/addEmployeeTurn";

class EmployeeTurnsFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
    this.state.modal === false && this.props.seed();
  }

  render() {
    const { userId, employees, className } = this.props;
    return (
      <div
        className="contact-app-sidebar float-left d-none d-xl-block"
        style={{ minHeight: "70vh" }}
      >
        <PerfectScrollbar>
          <div className="contact-app-sidebar-content">
            <div className="contact-app-menu">
              <div className="form-group form-group-compose text-center">
                <button
                  type="button"
                  className="btn btn-raised btn-danger btn-block my-2 shadow-z-2"
                  onClick={this.toggle}
                >
                  <Icon.UserPlus size={18} className="mr-1" /> Nuevo Turno
                </button>
              </div>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Filtro
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={userId}>
                  <Icon.Users size={18} className="mr-1" /> Mis Turnos
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Equipo
              </h6>
              <ul className="list-group list-group-messages">
                {employees.map((employee) => (
                  <FilterLink filter={employee._id}>
                    <Icon.Circle size={18} className="mr-1 success" />
                    {employee.name} {employee.lastName}
                  </FilterLink>
                ))}
              </ul>
            </div>
          </div>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={className}
            size="lg"
          >
            <ModalHeader toggle={this.toggle}>
              Agregar entrada de Asistencia
            </ModalHeader>
            <AddEmployeeTurn owner_id={userId} toggle={this.toggle} />
          </Modal>
        </PerfectScrollbar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // Mapping employeeTurns object and Visiblityfilter state to Object
    userId: state.session?.user._id,
    employees: state.userApp.users.filter(
      (user) =>
        user.roles.some((role) => role.role === "EMPLOYEE") && user.isActive
    ),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: dispatch(fetchUsers()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeTurnsFilter);
