import React from "react";
import EmployeeTurn from "./employeeTurn";
import PerfectScrollbar from "react-perfect-scrollbar";
import { connect, useDispatch, useSelector } from "react-redux";
import {
  deleteEmployeeTurn,
  fetchEmployeeTurns,
} from "../../redux/actions/emoployeeTurns";
import { Row, Col, Collapse, Table } from "reactstrap";
import { ChevronRight, ChevronDown } from "react-feather";

const EmployeeTurnsList = ({
  employeeTurns,
  userId,
  isUserAdmin,
  deleteEmployeeTurn,
}) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [collapseJan, setCollapseJan] = React.useState(false);
  const [collapseFeb, setCollapseFeb] = React.useState(false);
  const [collapseMar, setCollapseMar] = React.useState(false);
  const [collapseApr, setCollapseApr] = React.useState(false);
  const [collapseMay, setCollapseMay] = React.useState(false);
  const [collapseJun, setCollapseJun] = React.useState(false);
  const [collapseJul, setCollapseJul] = React.useState(false);
  const [collapseAug, setCollapseAug] = React.useState(false);
  const [collapseSep, setCollapseSep] = React.useState(false);
  const [collapseOct, setCollapseOct] = React.useState(false);
  const [collapseNov, setCollapseNov] = React.useState(false);
  const [collapseDec, setCollapseDec] = React.useState(false);

  const toggleJan = () => setCollapseJan(!collapseJan);
  const toggleFeb = () => setCollapseFeb(!collapseFeb);
  const toggleMar = () => setCollapseMar(!collapseMar);
  const toggleApr = () => setCollapseApr(!collapseApr);
  const toggleMay = () => setCollapseMay(!collapseMay);
  const toggleJun = () => setCollapseJun(!collapseJun);
  const toggleJul = () => setCollapseJul(!collapseJul);
  const toggleAug = () => setCollapseAug(!collapseAug);
  const toggleSep = () => setCollapseSep(!collapseSep);
  const toggleOct = () => setCollapseOct(!collapseOct);
  const toggleNov = () => setCollapseNov(!collapseNov);
  const toggleDec = () => setCollapseDec(!collapseDec);

  const monthStyles = {
    overflowX: "hidden",
    backgroundColor: "white",
    paddingTop: "2rem",
    paddingBottom: "1rem",
    borderBottom: "solid 1px rgb(0 157 160)",
  };

  const renderMonths = (month) => {
    return (
      <PerfectScrollbar>
        {/*{month === 0 && 
         <div>
          <div className="d-flex cursor-pointer" onClick={toggleJan}>
              <h2 className="col-10">Enero</h2>
              {!collapseJan ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseJan}>
              {renderEmployeeTurns(0)}
            </Collapse>
          </div>}
          {month === 1 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleFeb}>
              <h2 className="col-10">Febrero</h2>
              {!collapseFeb ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseFeb}>
              {renderEmployeeTurns(1)}
            </Collapse>
          </div>}
          {month === 2 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleMar}>
              <h2 className="col-10">Marzo</h2>
              {!collapseMar ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseMar}>
              {renderEmployeeTurns(2)}
            </Collapse>
          </div>}
          {month === 3 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleApr}>
              <h2 className="col-10">Abril</h2>
              {!collapseApr ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseApr}>
              {renderEmployeeTurns(3)}
            </Collapse>
          </div>}
          {month === 4 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleMay}>
              <h2 className="col-10">Mayo</h2>
              {!collapseMay ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseMay}>
              {renderEmployeeTurns(4)}
            </Collapse>
          </div>}
          {month === 5 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleJun}>
              <h2 className="col-10">Junio</h2>
              {!collapseJun ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseJun}>
              {renderEmployeeTurns(5)}
            </Collapse>
          </div>}
          {month === 6 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleJul}>
              <h2 className="col-10">Julio</h2>
              {!collapseJul ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseJul}>
              {renderEmployeeTurns(6)}
            </Collapse>
          </div>}
          {month === 7 &&
          <div>
            <div className="d-flex cursor-pointer" onClick={toggleAug}>
              <h2 className="col-10">Agosto</h2>
              {!collapseAug ? <ChevronRight className="col-2"/> : <ChevronDown className="col-2"/>}
            </div>
            <Collapse isOpen={collapseAug}>
              {renderEmployeeTurns(7)}
            </Collapse>
          </div>}*/}
        {month === 8 && (
          <div style={monthStyles}>
            <div className="d-flex cursor-pointer" onClick={toggleSep}>
              <h2 className="col-10">Septiembre</h2>
              {!collapseSep ? (
                <ChevronRight className="col-2" />
              ) : (
                <ChevronDown className="col-2" />
              )}
            </div>
            <Collapse isOpen={collapseSep}>{renderEmployeeTurns(8)}</Collapse>
          </div>
        )}
        {month === 9 && (
          <div style={monthStyles}>
            <div className="d-flex cursor-pointer" onClick={toggleOct}>
              <h2 className="col-10">Octubre</h2>
              {!collapseOct ? (
                <ChevronRight className="col-2" />
              ) : (
                <ChevronDown className="col-2" />
              )}
            </div>
            <Collapse isOpen={collapseOct}>{renderEmployeeTurns(9)}</Collapse>
          </div>
        )}
        {month === 10 && (
          <div style={monthStyles}>
            <div className="d-flex cursor-pointer" onClick={toggleNov}>
              <h2 className="col-10">Noviembre</h2>
              {!collapseNov ? (
                <ChevronRight className="col-2" />
              ) : (
                <ChevronDown className="col-2" />
              )}
            </div>
            <Collapse isOpen={collapseNov}>{renderEmployeeTurns(10)}</Collapse>
          </div>
        )}
        {month === 11 && (
          <div style={monthStyles}>
            <div className="d-flex cursor-pointer" onClick={toggleDec}>
              <h2 className="col-10">Diciembre</h2>
              {!collapseDec ? (
                <ChevronRight className="col-2" />
              ) : (
                <ChevronDown className="col-2" />
              )}
            </div>
            <Collapse isOpen={collapseDec}>
              {renderEmployeeTurns(month)}
            </Collapse>
          </div>
        )}
      </PerfectScrollbar>
    );
  };

  const renderEmployeeTurns = (month) => {
    return (
      <div>
        <Row>
          <Col xs={3}>
            <p className="text-muted font-small-3 text-bold-600 text-uppercase my-3 text-center">
              Fecha del Turno
            </p>
          </Col>
          <Col xs={3}>
            <p className="text-muted font-small-3 text-bold-600 text-uppercase my-3 text-center">
              Hora de entrada
            </p>
          </Col>
          <Col xs={3}>
            <p className="text-muted font-small-3 text-bold-600 text-uppercase my-3 text-center">
              {" "}
              Hora de salida
            </p>
          </Col>
          <Col xs={3}></Col>
        </Row>
        <ul className="list-group">
          {employeeTurns !== undefined &&
            employeeTurns?.map(
              (employeeTurn) =>
                new Date(employeeTurn.date).getMonth() === month && (
                  <EmployeeTurn
                    onDeleteClick={() => deleteEmployeeTurn(employeeTurn._id)}
                    key={employeeTurn.id}
                    {...employeeTurn}
                  />
                )
            )}
        </ul>
        <Row>
          <div className="col-1"></div>
          <div className="col-10">
            <h5>
              Horas trabajadas en el mes: {calculateMonthActivityHours(month)}
            </h5>
          </div>
          <div className="col-1"></div>
        </Row>
      </div>
    );
  };

  var employee =
    isUserAdmin === false ||
    state.employeeTurnsApp.employeeTurnsVisibilityFilter === "SHOW_ALL"
      ? userId
      : state.employeeTurnsApp.employeeTurnsVisibilityFilter;

  React.useEffect(() => {
    dispatch(fetchEmployeeTurns(employee));
  }, [employee]);

  const calculateMonthActivityHours = (month) => {
    let total = 0;
    employeeTurns &&
      employeeTurns.length > 0 &&
      employeeTurns.forEach((employeeTurn) => {
        employeeTurn.activities.forEach((activity) => {
          new Date(employeeTurn.date).getMonth() === month &&
            (total += activity.duration);
        });
      });
    return (total / 60).toFixed(2);
  };

  return (
    <ul className="list-group">
      {renderMonths(0)}
      {renderMonths(1)}
      {renderMonths(2)}
      {renderMonths(3)}
      {renderMonths(4)}
      {renderMonths(5)}
      {renderMonths(6)}
      {renderMonths(7)}
      {renderMonths(8)}
      {renderMonths(9)}
      {renderMonths(10)}
      {renderMonths(11)}
    </ul>
  );
};

const mapStateToProps = (state, ownProps) => {
  console.log(state.session?.user._id);
  return {
    // Mapping employeeTurns object and Visiblityfilter state to Object
    userId: state.session?.user._id,
    employeeTurns: state.employeeTurnsApp.employeeTurn.employeeTurnsList,
    isUserAdmin: state.session?.user.roles.some(
      (role) => role.role === "ADMIN"
    ),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    deleteEmployeeTurn: (id) => dispatch(deleteEmployeeTurn(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeTurnsList);
