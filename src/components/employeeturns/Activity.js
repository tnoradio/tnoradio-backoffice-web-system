import React, { useState, useEffect } from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import Select from "react-select";
import "react-datetime/css/react-datetime.css";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { AtSign, PlusCircle, Trash } from "react-feather";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const Activity = ({
  employeeTasks,
  activities,
  onDeleteActivityClick,
  addActivitySubmit,
  turnId,
}) => {
  const [activityName, setActivityName] = useState("");
  const [manualName, setManualName] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [taskId, setTaskId] = useState("");
  let employeeTasksOptions = [];

  const formSchema = Yup.object().shape({
    duration: Yup.number().required("Debes indicar la duración."),
    name: Yup.string(),
  });

  const formatGroupLabel = (data) => (
    <div style={groupStyles}>
      <span>{data.label}</span>
      <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
  );

  const setTaskOptions = () => {
    employeeTasks &&
      employeeTasks.length > 0 &&
      employeeTasks.forEach((todo) => {
        employeeTasksOptions.push({
          label: todo.task,
          value: todo,
        });
      });
  };

  const groupedOptions = [
    {
      label: "Tareas",
      options: employeeTasksOptions,
    },
  ];

  setTaskOptions();

  return (
    <div>
      {activities && activities.length > 0 && (
        <Row>
          <Col xs={8}>
            <Label>Descripción de la Actividad</Label>
          </Col>
          <Col xs={3}>
            <Label>Duración</Label>
          </Col>
          <Col xs={1} />
        </Row>
      )}
      <Row>
        <Col xs={12}>
          <ListGroup>
            {activities &&
              activities.length > 0 &&
              activities.map((activity) => {
                return (
                  <ListGroupItem>
                    <Row key={activity._id}>
                      <Col xs={8}>
                        <span>{activity.name}</span>
                      </Col>
                      <Col xs={3}>
                        <span>{activity.duration} mins</span>
                      </Col>
                      <Col xs={1}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            onDeleteActivityClick(activity);
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
          </ListGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <br></br>
          <Button
            className="btn-square"
            size="md"
            outline
            color="primary"
            onClick={() => setIsOpen(true)}
          >
            Agregar Actividad
          </Button>
        </Col>
      </Row>
      <Modal isOpen={isOpen} toggle={() => setIsOpen((isOpen) => !isOpen)}>
        <Formik
          initialValues={{
            name: "",
            duration: 0,
          }}
          validationSchema={formSchema}
          onSubmit={(values) => {
            addActivitySubmit(
              values.duration,
              activityName !== "" ? activityName : manualName,
              taskId,
              turnId
            );
            setIsOpen(false);
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <ModalHeader>
                <Label>Agregar una Actividad</Label>
              </ModalHeader>
              <ModalBody>
                <Row>
                  <Col xs={12}>
                    <h4 for="name">Descripción de la actividad:</h4>
                  </Col>
                  {employeeTasks !== undefined &&
                    employeeTasks.length > 0 &&
                    manualName === "" && (
                      <Col xs={12}>
                        <Label for="name">
                          Seleccionar de la lista de tareas
                        </Label>
                        <FormGroup>
                          <Select
                            defaultValue={employeeTasksOptions[0]}
                            options={groupedOptions}
                            formatGroupLabel={formatGroupLabel}
                            value={employeeTasksOptions.filter(
                              (obj) => obj.label === activityName
                            )}
                            onChange={(e) => {
                              setActivityName(e.value.task);
                              setManualName("");
                              setTaskId(e.value._id);
                            }}
                          />
                          <div className="text-right">
                            <span
                              onClick={() => {
                                setActivityName("");
                                setTaskId("");
                              }}
                              className="primary"
                            >
                              Reset
                            </span>
                          </div>
                        </FormGroup>
                      </Col>
                    )}
                  {activityName === "" && (
                    <Col xs={12}>
                      <FormGroup>
                        <Label for="name">Agregar manualmente</Label>
                        <Field
                          className={`form-control ${
                            activityName === "" && errors.name && "is-invalid"
                          }`}
                          type="text"
                          name="name"
                          id="name"
                          value={manualName}
                          onChange={(e) => {
                            setManualName(e.target.value);
                            setActivityName("");
                            setTaskId("");
                          }}
                        />
                      </FormGroup>
                    </Col>
                  )}
                </Row>
                <Row>
                  <Col xs={12}>
                    <Label for="duration">
                      Indique la duración de la actividad (minutos)
                    </Label>
                  </Col>
                  <Col xs={3}>
                    <FormGroup>
                      <Field
                        className={`form-control ${
                          touched.duration && errors.duration && "is-invalid"
                        }`}
                        type="number"
                        name="duration"
                        id="duration"
                      />
                    </FormGroup>
                    {errors.duration && touched.duration ? (
                      <div className="invalid-feedback">{errors.duration}</div>
                    ) : null}
                  </Col>
                  <Col xs={9}></Col>
                </Row>
              </ModalBody>
              <ModalFooter>
                <Button
                  className="btn-square"
                  outline
                  color="primary"
                  type="submit"
                >
                  Agregar
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>
    </div>
  );
};

export default Activity;
