import React from "react";
import PropTypes from "prop-types";
import RecordingTurn from "./recordingTurn";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useDispatch } from "react-redux";
import { updateRecordingTurn } from "../../redux/actions/recordingTurns";

const RecordingTurnList = (props) => {
  const {
    active,
    recordingTurns,
    deleteRecordingTurn,
    recordingTurnDetails,
    toggleSuspendRecordingTurn,
  } = props;
  const dispatch = useDispatch();
  return (
    <div className="post-app-list">
      <PerfectScrollbar>
        <div id="recordingTurns-list">
          <ul className="list-group">
            {recordingTurns?.map((recordingTurn) => {
              return (
                <RecordingTurn
                  key={recordingTurn._id}
                  active={active}
                  {...recordingTurn}
                  onSuspendClick={() => {
                    dispatch(
                      updateRecordingTurn(
                        recordingTurn._id,
                        "isSuspended",
                        !recordingTurn.isSuspended,
                        recordingTurn
                      )
                    );
                    toggleSuspendRecordingTurn(recordingTurn._id);
                  }}
                  onDeleteClick={() => deleteRecordingTurn(recordingTurn._id)}
                  onRecordingTurnClick={() =>
                    recordingTurnDetails(recordingTurn._id)
                  }
                />
              );
            })}
          </ul>
        </div>
      </PerfectScrollbar>
    </div>
  );
};

RecordingTurnList.prototype = {
  recordingTurns: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      price: PropTypes.number,
      description: PropTypes.string,
      date: PropTypes.string,
      startTime: PropTypes.string,
      endTime: PropTypes.string,
      status: PropTypes.string,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
  toggleSuspendRecordingTurn: PropTypes.func.isRequired,
  deleteRecordingTurn: PropTypes.func.isRequired,
  recordingTurnDetails: PropTypes.func.isRequired,
};
export default RecordingTurnList;
