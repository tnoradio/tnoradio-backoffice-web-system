import React, { useState } from "react";
import { Row, Col, Tooltip } from "reactstrap";
import PropTypes from "prop-types";
import { CheckCircle, Trash } from "react-feather";
import moment from "moment";

const RecordingTurn = ({
  onRecordingTurnClick,
  onSuspendClick,
  onDeleteClick,
  date,
  _id,
  starred,
  status,
  startTime,
  active,
  isSuspended,
}) => {
  let dateFormat = moment(date).format("DD/MM/yyyy");
  let timeFormat = moment(startTime).format("hh:mm a");
  const [suspendTooltipOpen, setSuspendTooltipOpen] = useState(false);
  const [deleteTooltipOpen, setDeleteTooltipOpen] = useState(false);
  return (
    <li
      className={
        "list-group-item list-group-item-action no-border " +
        (active === _id ? "bg-grey bg-lighten-4" : "")
      }
    >
      <Row className="post-list-group-item">
        <Col xs={1}>
          <CheckCircle
            size={20}
            onClick={onSuspendClick}
            style={{ color: !isSuspended ? "green" : "red" }}
            id={`suspend-${_id}-Toggler`}
          />{" "}
          <Tooltip
            placement="bottom"
            isOpen={suspendTooltipOpen}
            target={`suspend-${_id}-Toggler`}
            toggle={() =>
              setSuspendTooltipOpen((suspendTooltipOpen) => !suspendTooltipOpen)
            }
          >
            Suspender la pauta
          </Tooltip>
        </Col>
        <Col xs={4} onClick={onRecordingTurnClick}>
          <p className="mb-0 text-truncate">{status}</p>
        </Col>
        <Col xs={3} onClick={onRecordingTurnClick}>
          <p className="mb-0 text-truncate">
            {active ? <span>{dateFormat}</span> : <span>{dateFormat} </span>}
          </p>
        </Col>
        <Col xs={3} onClick={onRecordingTurnClick}>
          <p className="mb-0 text-truncate">{timeFormat}</p>
        </Col>
        <Col xs={1}>
          <Trash
            size={20}
            onClick={onDeleteClick}
            style={{ color: active ? "#FF586B" : "#495057" }}
            id={`delete-${_id}-Toggler`}
          />
          <Tooltip
            placement="bottom"
            isOpen={deleteTooltipOpen}
            target={`delete-${_id}-Toggler`}
            toggle={() =>
              setDeleteTooltipOpen((deleteTooltipOpen) => !deleteTooltipOpen)
            }
          >
            Eliminar la pauta para siempre
          </Tooltip>
        </Col>
      </Row>
    </li>
  );
};

RecordingTurn.propTypes = {
  _id: PropTypes.string,
  userData: PropTypes.string,
  date: PropTypes.string,
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  price: PropTypes.number,
  owner_id: PropTypes.string,
  status: PropTypes.string,
  starred: PropTypes.bool,
  isSuspended: PropTypes.bool,
  deleted: PropTypes.bool,
  onStarredClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  onRecordingTurnClick: PropTypes.func,
};

export default RecordingTurn;
