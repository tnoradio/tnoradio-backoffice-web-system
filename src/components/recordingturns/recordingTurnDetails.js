import React, { Fragment, useState, useEffect } from "react";
import {
  Row,
  Col,
  Media,
  Table,
  Button,
  Input,
  FormGroup,
  ListGroup,
  ListGroupItem,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  Modal,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import * as Icon from "react-feather";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { useDispatch, useSelector } from "react-redux";
import DateTime from "react-datetime";
import Select from "react-select";
import moment from "moment";
import { fetchPayments } from "../../redux/actions/payments";
import AddPayment from "../../containers/payment/addPayment";
import { useHistory } from "react-router-dom";
import { fetchUsersList } from "../../redux/actions/users/fetchUsers";

const RecordingTurnDetails = ({
  selectedRecordingTurn,
  onEditClick,
  editRecordingTurnFlag,
  onChange,
}) => {
  const state = useSelector((state) => state);
  const [start, setStartTime] = useState(new Date());
  const [end, setEndTime] = useState(new Date());
  const [date, setDate] = useState(new Date());
  const [paymentsModal, setPaymentsModal] = React.useState(false);
  const [turnPayments, setTurnPayments] = React.useState([]);
  const [userPayments, setUserPayments] = React.useState([]);
  const [paymentsData, setPaymentsData] = React.useState([]);
  const [turnTeam, setTurnTeam] = React.useState([]);
  const [teamModal, setTeamModal] = React.useState(false);
  const [errorTeamMember, setErrorTeamMember] = React.useState(false);
  const [usersList, setUsersList] = React.useState([]);
  const [teamMember, setTeamMember] = React.useState({});
  const [errorUserTeamMember, setErrorUserTeamMember] = React.useState(false);
  const [errorDuplicateTeamMember, setErrorDuplicateTeamMember] =
    React.useState(false);
  const dispatch = useDispatch();
  let history = useHistory();
  let users = [];
  let userGroupOptions = [];

  const options = [
    { value: "PENDING", label: "Pendiente" },
    { value: "RECONCILED", label: "Conciliado" },
    { value: "DONE", label: "Listo" },
  ];
  const groupStyles = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  };

  const groupBadgeStyles = {
    backgroundColor: "#EBECF0",
    borderRadius: "2em",
    color: "#172B4D",
    display: "inline-block",
    fontSize: 12,
    fontWeight: "normal",
    lineHeight: "1",
    minWidth: 1,
    padding: "0.16666666666667em 0.5em",
    textAlign: "center",
  };
  useEffect(() => {
    load();
    async function load() {
      const res = await fetchUsersList();
      setUsersList(res);
    }
  }, []);

  usersList.forEach((user) => {
    if (user.roles.some((role) => role.role === "EMPLOYEE"))
      users.push({
        label: user.name + " " + user.lastName,
        value: user,
      });
  });

  userGroupOptions = [
    {
      label: "Usuarios",
      options: users,
    },
  ];

  const handleDateChange = (selectedDate) => {
    onChange(
      selectedRecordingTurn._id,
      "date",
      selectedDate.toDate(),
      selectedRecordingTurn
    );
  };
  const handleStartChange = (time) => {
    onChange(
      selectedRecordingTurn._id,
      "startTime",
      time.toDate(),
      selectedRecordingTurn
    );
  };
  const handleEndChange = (time) => {
    onChange(
      selectedRecordingTurn._id,
      "endTime",
      time.toDate(),
      selectedRecordingTurn
    );
  };

  const handleStatusChange = (status) => {
    onChange(
      selectedRecordingTurn._id,
      "status",
      status,
      selectedRecordingTurn
    );
  };

  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  const formatGroupLabel = (data) => (
    <div style={groupStyles}>
      <span>{data.label}</span>
      <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
  );

  const addPaymentSubmit = (paymentId) => {
    var payments = turnPayments;
    payments.push(paymentId);
    setPaymentsModal(false);
    setTurnPayments(payments);
    onChange(selectedRecordingTurn._id, "payments", payments);
  };

  const onDeletePaymentClick = (payment) => {
    var paymentsAux = [];
    turnPayments.map((item) => {
      if (item._id !== payment._id) {
        paymentsAux.push(item);
      }
    });
    setTurnPayments(paymentsAux);
    onChange(selectedRecordingTurn._id, "payments", paymentsAux);
  };
  useEffect(() => {
    dispatch(fetchPayments());
    // Safe to add dispatch to the dependencies array
  }, [dispatch]);
  useEffect(() => {
    let startTime, endTime, date, payments;
    if (selectedRecordingTurn?.startTime) {
      startTime = new Date(selectedRecordingTurn?.startTime);
      setStartTime(startTime);
    }
    if (selectedRecordingTurn?.endTime) {
      endTime = new Date(selectedRecordingTurn?.endTime);
      setEndTime(endTime);
    }
    if (selectedRecordingTurn?.date) {
      date = new Date(selectedRecordingTurn?.date);
      setDate(date);
    }
    if (selectedRecordingTurn?.payments) {
      payments = new Date(selectedRecordingTurn?.payments);
      setTurnPayments(payments);
    }
    if (selectedRecordingTurn?.payments) {
      let userPaymentsList = state.paymentApp.payments.filter(
        (payment) => payment.owner_id === selectedRecordingTurn.owner_id
      );

      setUserPayments(userPaymentsList);

      let paymentsDataList = userPayments.filter((userPayment) =>
        selectedRecordingTurn.payments.find(
          (payment) => userPayment._id === payment
        )
      );

      setPaymentsData(paymentsDataList);
    }
  }, [selectedRecordingTurn]);

  const verifyUserExistOnList = (id, list) => {
    var found = false;
    list.map((element) => {
      if (element.userId === id) {
        found = true;
      }
    });
    return found;
  };

  const addTeamMember = (teamMember) => {
    let teamMembers = turnTeam;
    console.log(teamMember);
    if (teamMember.value._id !== undefined) {
      if (!verifyUserExistOnList(teamMember.value._id, teamMembers)) {
        teamMembers.push(teamMember.value._id);
        setTurnTeam(teamMembers);
        console.log(turnTeam);
        onChange(selectedRecordingTurn._id, "turnTeam", teamMembers);
        setTeamModal(false);
        setErrorTeamMember(false);
        setErrorUserTeamMember(false);
        setErrorDuplicateTeamMember(false);
      } else {
        setErrorDuplicateTeamMember(true);
      }
    } else {
      setErrorUserTeamMember(true);
    }
  };

  const onDeleteTeamMemberClick = (userId) => {
    console.log(turnTeam);
    let teamMembers = turnTeam.filter((memberId) => memberId !== userId);
    console.log(teamMembers);
    setTurnTeam(teamMembers);
    console.log(turnTeam);
    onChange(selectedRecordingTurn._id, "turnTeam", teamMembers);
  };

  return (
    <Fragment>
      {selectedRecordingTurn ? (
        <div className="contact-app-content-detail">
          <Row>
            <Col className="text-right">
              <Button
                className="btn-outline-success mr-1 mt-1"
                size="sm"
                onClick={onEditClick}
              >
                {editRecordingTurnFlag ? (
                  <Icon.Check size={16} />
                ) : (
                  <Icon.Edit2 size={16} />
                )}
              </Button>
            </Col>
          </Row>
          <PerfectScrollbar>
            <Media heading>{selectedRecordingTurn.description}</Media>

            <Table responsive borderless size="md" className="mt-4">
              <tbody style={{ height: "600px" }}>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Descripción</td>
                  <td className="col-9">
                    {editRecordingTurnFlag ? (
                      <Input
                        type="text"
                        name="description"
                        id="description"
                        defaultValue={selectedRecordingTurn.description}
                        closeOnSelect
                        onChange={(e) =>
                          onChange(
                            selectedRecordingTurn._id,
                            "description",
                            e.target.value,
                            selectedRecordingTurn
                          )
                        }
                      />
                    ) : (
                      ": " + selectedRecordingTurn.description
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Monto</td>
                  <td className="col-5">
                    {editRecordingTurnFlag ? (
                      <Input
                        type="text"
                        name="price"
                        id="price"
                        defaultValue={selectedRecordingTurn.price}
                        onChange={(e) =>
                          onChange(
                            selectedRecordingTurn._id,
                            "price",
                            e.target.value,
                            selectedRecordingTurn
                          )
                        }
                      />
                    ) : (
                      `: ${selectedRecordingTurn.price}`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Fecha de Reserva</td>
                  <td className="col-5">
                    {editRecordingTurnFlag ? (
                      <DateTime
                        onChange={handleDateChange}
                        value={date}
                        dateFormat
                        timeFormat={false}
                      />
                    ) : (
                      moment(date).format("DD/MM/yyyy")
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Hora de Inicio</td>
                  <td className="col-5">
                    {editRecordingTurnFlag ? (
                      <DateTime
                        dateFormat={false}
                        onChange={handleStartChange}
                        value={start}
                      />
                    ) : (
                      `: ${moment(start).format("hh:mm a")} `
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Hora Fin</td>
                  <td className="col-5">
                    {editRecordingTurnFlag ? (
                      <DateTime
                        dateFormat={false}
                        timeFormat
                        onChange={(e) => handleEndChange(e)}
                        //    initialValue={selectedRecordingTurn.endTime}
                        value={end}
                      />
                    ) : (
                      `: ${moment(end).format("hh:mm a")} `
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Estatus</td>
                  <td className="col-5">
                    {editRecordingTurnFlag ? (
                      <Select
                        options={options}
                        disabled={!isUserAdmin}
                        onChange={(e) => handleStatusChange(e.value)}
                        value={options.filter(
                          (option) =>
                            option.value === selectedRecordingTurn.status
                        )}
                      />
                    ) : (
                      `: ${
                        options.filter(
                          (option) =>
                            option.value === selectedRecordingTurn.status
                        )[0]?.label
                      }`
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Pagos</td>
                  <td className={editRecordingTurnFlag ? "col-5" : "col-7"}>
                    {editRecordingTurnFlag ? (
                      <>
                        <Row>
                          <Col>
                            <ListGroup>
                              {paymentsData?.map((payment) => {
                                return (
                                  <ListGroupItem key={payment._id}>
                                    <Row>
                                      <Col xs={10}>
                                        <span>
                                          {moment(payment.payment_date).format(
                                            "DD/MM/yyyy"
                                          )}
                                        </span>
                                        {"  "}
                                        <span>{payment.amount}</span>{" "}
                                      </Col>
                                      <Col xs={2}>
                                        <Icon.Trash
                                          size={18}
                                          className="float-right mt-1 mb-2 width-25 d-block"
                                          style={{
                                            color: "#FF586B",
                                            cursor: "pointer",
                                          }}
                                          onClick={() => {
                                            onDeletePaymentClick(payment);
                                          }}
                                        />
                                      </Col>
                                    </Row>
                                  </ListGroupItem>
                                );
                              })}
                            </ListGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs={12}>
                            <Button
                              className="btn-square"
                              size="sm"
                              outline
                              color="primary"
                              onClick={() => history.push("/payments")}
                            >
                              <Icon.DollarSign size={16} color="#009DA0" />
                              {"   "}
                              Agregar Pago
                            </Button>
                          </Col>
                        </Row>
                        <Modal
                          isOpen={paymentsModal}
                          toggle={() => setPaymentsModal(!paymentsModal)}
                        >
                          <ModalHeader>
                            <Label>Agregar Pago</Label>
                          </ModalHeader>
                          <ModalBody>
                            <AddPayment></AddPayment>
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              className="btn-square"
                              outline
                              color="primary"
                              onClick={addPaymentSubmit}
                              type="submit"
                            >
                              Agregar
                            </Button>{" "}
                          </ModalFooter>
                        </Modal>
                      </>
                    ) : (
                      <Row>
                        <Col>
                          <ListGroup>
                            {paymentsData.map((payment) => {
                              return (
                                <ListGroupItem key={payment._id}>
                                  <Row>
                                    <Col xs={10}>
                                      <span>
                                        Fecha:{"       "}
                                        {moment(payment.payment_date).format(
                                          "DD/MM/yyyy"
                                        )}
                                      </span>
                                      {"       "}|{"       "}
                                      <span>Monto: {payment.amount}</span>{" "}
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              );
                            })}
                          </ListGroup>
                        </Col>
                      </Row>
                    )}
                  </td>
                </tr>
                <tr className="d-flex">
                  <td className="col-3 text-bold-400">Equipo</td>
                  <td className={editRecordingTurnFlag ? "col-5" : "col-7"}>
                    {editRecordingTurnFlag && isUserAdmin ? (
                      <>
                        <Row>
                          <Col>
                            <ListGroup flush>
                              {selectedRecordingTurn.turnTeam.map(
                                (teamMemberId) => {
                                  return (
                                    <ListGroupItem
                                      key={"teamMember " + teamMemberId}
                                    >
                                      <Row>
                                        <Col xs={10}>
                                          {users.map((user) => {
                                            if (
                                              user.value._id === teamMemberId
                                            ) {
                                              return (
                                                <div
                                                  key={
                                                    "label_" + user.value._id
                                                  }
                                                >
                                                  <span>{user.label}</span>
                                                </div>
                                              );
                                            } else return "";
                                          })}
                                        </Col>
                                        <Col xs={2}>
                                          <Icon.Trash
                                            size={18}
                                            className="float-right mt-1 mb-2 width-25 d-block"
                                            style={{
                                              color: "#FF586B",
                                              cursor: "pointer",
                                            }}
                                            onClick={() => {
                                              onDeleteTeamMemberClick(
                                                teamMemberId
                                              );
                                            }}
                                          />
                                        </Col>
                                      </Row>
                                    </ListGroupItem>
                                  );
                                }
                              )}
                            </ListGroup>
                          </Col>
                          <Col xs={12}>
                            <Button
                              className="btn-square"
                              size="sm"
                              outline
                              color="primary"
                              onClick={() => setTeamModal(true)}
                            >
                              <Icon.User size={16} color="#009DA0" />
                              {"  "}
                              Armar Equipo
                            </Button>
                          </Col>
                          <Col xs={2}></Col>
                          {errorTeamMember ? (
                            <div
                              className="invalid-feedback"
                              style={{ display: "block", textAlign: "center" }}
                            >
                              {"Debe indicar al menos un miembro"}
                            </div>
                          ) : null}
                        </Row>
                        <Modal
                          isOpen={teamModal}
                          toggle={() => setTeamModal((state) => !state)}
                        >
                          <ModalHeader>
                            <Label>Agregar Equipo de Trabajo a la Pauta</Label>
                          </ModalHeader>
                          <ModalBody>
                            <Row>
                              {usersList !== undefined &&
                              usersList[0] !== undefined &&
                              usersList[0] !== null ? (
                                <Col xs={12}>
                                  <FormGroup>
                                    <Select
                                      defaultValue={users[0].value}
                                      options={userGroupOptions}
                                      formatGroupLabel={formatGroupLabel}
                                      value={users.find((obj) => {
                                        return obj === teamMember;
                                      })}
                                      onChange={(e) => {
                                        console.log(e);
                                        setTeamMember(e);
                                      }}
                                    />
                                  </FormGroup>
                                </Col>
                              ) : (
                                <Col xs={11}></Col>
                              )}
                              {errorUserTeamMember ? (
                                <div
                                  className="invalid-feedback"
                                  style={{
                                    display: "block",
                                    textAlign: "center",
                                  }}
                                >
                                  {"Debe seleccionar a un Locutor"}
                                </div>
                              ) : null}
                              {errorDuplicateTeamMember ? (
                                <div
                                  className="invalid-feedback"
                                  style={{
                                    display: "block",
                                    textAlign: "center",
                                  }}
                                >
                                  {"El miembro ya existe para esta pauta"}
                                </div>
                              ) : null}
                            </Row>
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              className="btn-square"
                              outline
                              color="primary"
                              onClick={() => addTeamMember(teamMember)}
                            >
                              Agregar
                            </Button>{" "}
                          </ModalFooter>
                        </Modal>
                      </>
                    ) : (
                      <ListGroup flush>
                        {selectedRecordingTurn.turnTeam.map((teamMemberId) => {
                          return (
                            <ListGroupItem key={"teamMember " + teamMemberId}>
                              <Row>
                                <Col>
                                  {users.map((user) => {
                                    if (user.value._id === teamMemberId) {
                                      return (
                                        <div key={user.value._id}>
                                          {user.label}
                                          <br></br>
                                        </div>
                                      );
                                    } else return "";
                                  })}
                                </Col>
                              </Row>
                            </ListGroupItem>
                          );
                        })}
                      </ListGroup>
                    )}
                  </td>
                </tr>
              </tbody>
            </Table>
          </PerfectScrollbar>
        </div>
      ) : (
        <div className="row h-100">
          <div className="col-sm-12 my-auto">
            <div className="text-center">
              <Icon.MessageSquare size={84} color="#ccc" className="pb-3" />
              <h4>Seleccione una pauta para mostrar los detalles.</h4>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

RecordingTurnDetails.prototype = {
  selectedRecordingTurn: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      amount: PropTypes.number,
      owner_id: PropTypes.string,
      show_owner: PropTypes.number,
      concept: PropTypes.string,
      currency: PropTypes.string,
      recordingTurn_date: PropTypes.string,
      receipts: PropTypes.array,
      reconciled: PropTypes.bool,
      isActive: PropTypes.bool,
      starred: PropTypes.bool,
      deleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
};
export default RecordingTurnDetails;
