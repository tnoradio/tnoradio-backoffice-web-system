import React from "react";
import { Modal, ModalHeader } from "reactstrap";
import FilterLink from "../../containers/recordingturns/recordingTurnFilterLink";
import { recordingTurnVisibilityFilter } from "../../redux/actions/recordingTurns";
import AddRecordingTurn from "../../containers/recordingturns/addRecordigTurn";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as Icon from "react-feather";

class RecordingTurnFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  render() {
    console.log(this.props);
    return (
      <div className="post-app-sidebar float-left d-none d-xl-block">
        <PerfectScrollbar>
          <div className="post-app-sidebar-content">
            <div className="post-app-menu">
              <div className="form-group form-group-compose text-center">
                <button
                  type="button"
                  className="btn btn-raised btn-danger btn-block my-2 shadow-z-2"
                  onClick={this.toggle}
                >
                  <Icon.Edit size={18} className="mr-1" /> Reservar Pauta
                </button>
              </div>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Flitrar
              </h6>
              <ul className="list-group list-group-messages">
                <FilterLink filter={recordingTurnVisibilityFilter?.SHOW_ALL}>
                  <Icon.Disc size={18} className="mr-1" /> Todas las Pautas
                </FilterLink>
                <FilterLink
                  filter={recordingTurnVisibilityFilter?.DELETED_RECORDING_TURN}
                >
                  <Icon.Trash
                    size={18}
                    style={{ color: "#FF586B" }}
                    className="mr-1"
                  />{" "}
                  Eliminados
                </FilterLink>
                <FilterLink
                  filter={
                    recordingTurnVisibilityFilter?.SUSPENDED_RECORDING_TURN
                  }
                >
                  <Icon.CheckCircle
                    size={18}
                    style={{ color: "red" }}
                    className="mr-1"
                  />{" "}
                  Suspendidas
                </FilterLink>
                <FilterLink
                  filter={
                    recordingTurnVisibilityFilter?.RECONCILED_RECORDING_TURN
                  }
                  genreValue="CONCILIADO"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Pago Conciliado
                </FilterLink>
                <FilterLink
                  filter={recordingTurnVisibilityFilter?.DONE}
                  genreValue="DONE"
                >
                  <Icon.Circle size={18} className="mr-1 success" />
                  Culminado
                </FilterLink>
              </ul>
              <h6 className="text-muted font-small-3 text-bold-600 text-uppercase my-3">
                Conceptos
              </h6>
              <ul className="list-group list-group-messages"></ul>
            </div>
          </div>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={this.props.className}
            size="lg"
          >
            <ModalHeader toggle={this.toggle}>
              Reservar una Pauta de Grabación
            </ModalHeader>
            <AddRecordingTurn toggle={this.toggle} />
          </Modal>
        </PerfectScrollbar>
      </div>
    );
  }
}

export default RecordingTurnFilter;
