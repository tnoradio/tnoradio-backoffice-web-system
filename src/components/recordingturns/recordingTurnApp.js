import React from "react";

import VisibleRecordingTurnList from "../../containers/recordingturns/visibleRecordingTurnList";

const App = () => (
  <div>
    <VisibleRecordingTurnList />
  </div>
);

export default App;
