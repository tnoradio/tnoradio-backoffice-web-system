// test-utils.js

export const storeInitialState = {
  calender: {},
  emailApp: {},
  contactApp: {},
  userApp: {},
  todoApp: {},
  toastr: {}, // <- Mounted at toastr.
  chatApp: {},
  customizer: {},
  login: {},
  session: { user: { name: "testName", lastname: "testLastName" } },
};
