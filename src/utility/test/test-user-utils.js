import { User } from "../../core/users/domain/User";
import { UserEmail } from "../../core/users/domain/UserEmail";

export const mockUser = User.create(
  "mock_user_id",
  "testName",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  false,
  false,
  ""
);

export const mockUser1 = User.create(
  "mock_user_id_1",
  "testName",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  false,
  false,
  ""
);

export const mockStarredUser = User.create(
  "mock_user_id_2",
  "testName",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  true,
  false,
  ""
);

export const mockStarredUserBefore = User.create(
  "mock_user_id_2",
  "testName",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  false,
  false,
  ""
);

export const mockDeleteUserBefore = User.create(
  "mock_user_id_2",
  "testName",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  false,
  false,
  ""
);

export const mockDeletedUser = User.create(
  "mock_user_id_2",
  "testName",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  false,
  true,
  ""
);

export const mockUser1Updated = User.create(
  "mock_user_id_1",
  "testName Updated",
  "testLastName",
  "https://randomuser.me/api/portraits/lego/1.jpg",
  1245678,
  new UserEmail("testEmail@email.com"),
  true,
  "testPassword",
  "testPassword",
  "O",
  "AUDIENCE",
  "testAddress",
  "This is a test user for unit testing",
  "tetsCompany",
  "TestDepartment",
  "55-555-555-5555",
  false,
  false,
  ""
);

export const mockUserRequest = {
  _id: undefined,
  name: "testUserName",
  lastName: "testLastName",
  image: "https://randomuser.me/api/portraits/lego/1.jpg",
  dni: 1245678,
  email: { value: "testEmail@email.com" },
  isActive: true,
  password: "testPassword",
  passwordConfirmation: "testPassword",
  gender: "O",
  role: "AUDIENCE",
  address: "testAddress",
  notes: "This is a test user for unit testing",
  company: "tetsCompany",
  department: "TestDepartment",
  phone: "55-555-555-5555",
  isStarred: false,
  isDeleted: false,
  updatedBy: "",
};

export const addUserResponse = {
  data: mockUser1,
};

export const starredUserResponse = {
  data: mockStarredUser,
};

export const mockUsersList = [mockUser, mockUser1, mockStarredUserBefore];

export const mockUsersBeforeDeleteList = [
  mockUser,
  mockUser1,
  mockDeleteUserBefore,
];

export const mockUsersDeletedList = [mockUser, mockUser1, mockDeletedUser];

export const mockUsersUpdatedList = [
  mockUser,
  mockUser1Updated,
  mockStarredUserBefore,
];

export const mockUserStarredList = [mockUser, mockUser1, mockStarredUser];

export const updateUserStoreInitialState = {
  calender: {},
  emailApp: {},
  contactApp: {},
  userApp: { users: mockUsersList },
  todoApp: {},
  toastr: {}, // <- Mounted at toastr.
  chatApp: {},
  customizer: {},
  login: {},
  session: { user: { name: "testName", lastname: "testLastName" } },
};

export const mockUserUpdate = {
  _id: "mock_user_id",
  field: "name",
  value: "testName Updated",
};

export const mockToggleStarredUSerRequest = {
  _id: "mock_user_id_2",
  field: "isStarred",
  value: true,
};

export const mockUserLoginRequest = {
  email: {
    value: "testEmail@email.com",
  },
  password: "testPassword",
};

export const mockSession = {
  user: mockUser,
  token: "mockToken",
};

export const mockUserEmail = new UserEmail("testEmail@email.com");
