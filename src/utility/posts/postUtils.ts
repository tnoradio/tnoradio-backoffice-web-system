import UserRole from "../../core/users/domain/UserRole";

export function translateRole(roles: UserRole[]) {
  //console.log(typeof(roles))
  let userRole = "";

  if (roles !== null && roles !== undefined) {
    roles.forEach((role) => {
      if (userRole !== "") {
        if (role.role === "EMPLOYEE") userRole = "Empleado";
        if (role.role === "AUDIENCE") userRole = userRole + ", Audiencia";
        if (role.role === "ADMIN") userRole = userRole + ", Administrador";
        if (role.role === "POTENCIAL_CLIENT")
          userRole = userRole + ", Cliente Potencial";
        if (role.role === "CLIENT") userRole = userRole + ", Cliente";
        if (role.role === "HOST") userRole = userRole + ", Locutor";
        if (role.role === "PRODUCER") userRole = userRole + ", Productor";
        if (role.role === "PRODUCTION_ASSISTANT")
          userRole = userRole + ", Asistente de Producción";
      } else {
        if (role.role === "EMPLOYEE") userRole = "Empleado";
        if (role.role === "AUDIENCE") userRole = "Audiencia";
        if (role.role === "ADMIN") userRole = "Administrador";
        if (role.role === "POTENCIAL_CLIENT") userRole = "Cliente Potencial";
        if (role.role === "CLIENT") userRole = "Cliente";
        if (role.role === "HOST") userRole = "Locutor";
        if (role.role === "PRODUCER") userRole = "Productor";
        if (role.role === "PRODUCTION_ASSISTANT")
          userRole = "Asistente de Producción";
      }
    });
    //  console.log(userRole);
    return userRole;
  } else {
    //console.log("null");
    return "Por definir";
  }
}

export const validateIfPostHasTag = (postTags, tag): boolean => {
  let postHasTag = false;

  if (postTags !== null && postTags !== undefined && Array.isArray(postTags)) {
    postTags.forEach((postTag) => {
      // console.log(userRole.role);

      if (postHasTag !== true) {
        if (postTag === tag) postHasTag = true;
        //   console.log("Es TRUE");
        // console.log(userHasRole);
        return true;
      }
    });
    //  console.log(userHasRole);
    return postHasTag;
  } else {
    // console.log("null");
    return false;
  }
};
