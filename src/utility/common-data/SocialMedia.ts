export const SocialNetworks = [
  { value: "FACEBOOK", label: "Facebook", url: "https://www.facebook.com/" },
  { value: "TWITTER", label: "Twitter", url: "https://www.twitter.com/" },
  {
    value: "INSTAGRAM",
    label: "Instagram",
    url: "https://www.instagram.com/",
  },
  { value: "SNAPCHAT", label: "Snapchat", url: "" },
  { value: "TIK_TOK", label: "Tik Tok", url: "https://www.tiktok.com/@" },
  { value: "YOUTUBE", label: "Youtube", url: "" },
  { value: "OTHER", label: "Otra", url: "" },
];
