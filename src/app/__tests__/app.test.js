import React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import configureStore from "redux-mock-store";
import App from "../app";
import * as utils from "../../utility/test/test-utils";

const mockStore = configureStore([]);
let component;
let store;

beforeEach(() => {
  const initialState = utils.storeInitialState;
  store = mockStore(initialState);
});

//store.dispatch = jest.fn();

xit("should render the App component", () => {
  component = renderer.create(
    <Provider store={store}>
      <App />
    </Provider>
  );
  expect(component.toJSON()).toMatchSnapshot();
});
