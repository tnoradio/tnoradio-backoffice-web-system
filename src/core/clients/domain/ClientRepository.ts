import Client from "./Client";
import Agency from "./Agency";

export interface ClientRepository {
  save(payment: Client): Promise<Client>;
  getClientById(_id: string): Promise<Client>;
  getAllAgencies(): Promise<Agency[] | Error>;
  index(): Promise<Client[] | Error>;
  getClientsPage(pageSize: number, page: number): Promise<Client[] | Error>;
  update(id: string, field: string, value: any): Promise<Client | Error>;
  destroy(paymentId: string): Promise<any | Error>;
  updateIsUserStatus(id: string): Promise<Client | Error>;
}
