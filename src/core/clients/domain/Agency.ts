export default class Agency {
  id: string;
  name: string;
  name_screen: string;
  location: string;
  description: string;
  followers_count: string;
  verified: Boolean;
  url_twitter: string;
  created_at: string;
  updated_at: string;
}
