export default class Client {
  pcustomer_id: string;
  pcustomer_user_id: string;
  pcustomer_isUser: Boolean;
  pcustomer_username: string;
  pcustomer_frequency: string;
  pcustomer_frequency_percent: string;
  pcustomer_verify: string;
  pcustomer_bio: string;
  pcustomer_followers: string;
  pcustomer_followers_percent: string;
  pcustomer_social: string;
  pcustomer_total: string;
  tpc_id: string;
  tpc_twitter_id: string;
  tpc_label: string;
  tpc_createdAt: string;
  tpc_updatedAt: string;

  constructor(
    pcustomer_id: string,
    pcustomer_bio: string,
    pcustomer_followers: string,
    pcustomer_followers_percent: string,
    pcustomer_frequency: string,
    pcustomer_frequency_percent: string,
    pcustomer_social: string,
    pcustomer_total: string,
    pcustomer_user_id: string,
    pcustomer_username: string,
    pcustomer_verify: string,
    tpc_createdAt: string,
    tpc_id: string,
    tpc_label: string,
    tpc_twitter_id: string,
    tpc_updatedAt: string,
    pcustomer_isUser: Boolean
  ) {
    this.pcustomer_username = pcustomer_username;
    this.pcustomer_id = pcustomer_id;
    this.tpc_createdAt = tpc_createdAt;
    this.tpc_updatedAt = tpc_updatedAt;
    this.tpc_label = tpc_label;
    this.tpc_twitter_id = tpc_twitter_id;
    this.pcustomer_bio = pcustomer_bio;
    this.pcustomer_followers = pcustomer_followers;
    this.pcustomer_followers_percent = pcustomer_followers_percent;
    this.pcustomer_frequency = pcustomer_frequency;
    this.pcustomer_frequency_percent = pcustomer_frequency_percent;
    this.pcustomer_social = pcustomer_social;
    this.pcustomer_total = pcustomer_total;
    this.pcustomer_user_id = pcustomer_user_id;
    this.pcustomer_verify = pcustomer_verify;
    this.tpc_id = tpc_id;
    this.pcustomer_isUser = pcustomer_isUser;
  }

  static create(
    pcustomer_id: string,
    pcustomer_bio: string,
    pcustomer_followers: string,
    pcustomer_followers_percent: string,
    pcustomer_frequency: string,
    pcustomer_frequency_percent: string,
    pcustomer_social: string,
    pcustomer_total: string,
    pcustomer_user_id: string,
    pcustomer_username: string,
    pcustomer_verify: string,
    tpc_createdAt: string,
    tpc_id: string,
    tpc_label: string,
    tpc_twitter_id: string,
    tpc_updatedAt: string,
    pcustomer_isUser: Boolean
  ) {
    return new Client(
      pcustomer_id,
      pcustomer_bio,
      pcustomer_followers,
      pcustomer_followers_percent,
      pcustomer_frequency,
      pcustomer_frequency_percent,
      pcustomer_social,
      pcustomer_total,
      pcustomer_user_id,
      pcustomer_username,
      pcustomer_verify,
      tpc_createdAt,
      tpc_id,
      tpc_label,
      tpc_twitter_id,
      tpc_updatedAt,
      pcustomer_isUser
    );
  }
}
