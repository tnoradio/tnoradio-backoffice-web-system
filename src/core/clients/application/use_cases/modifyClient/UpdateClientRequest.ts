export type UpdateClientRequest = {
  id: string;
  field: string;
  value: any;
};
