import Client from "../../../domain/Client";
import { ClientRequest } from "../../ClientRequest";
import { ClientRepository } from "../../../domain/ClientRepository";

export class ClientCreator {
  private repository: ClientRepository;

  constructor(repository: ClientRepository) {
    this.repository = repository;
  }

  async run(request: ClientRequest): Promise<Client> {
    const client = Client.create(
      request.pcustomer_id,
      request.pcustomer_bio,
      request.pcustomer_followers,
      request.pcustomer_followers_percent,
      request.pcustomer_frequency,
      request.pcustomer_frequency_percent,
      request.pcustomer_social,
      request.pcustomer_total,
      request.pcustomer_user_id,
      request.pcustomer_username,
      request.pcustomer_verify,
      request.tpc_createdAt,
      request.tpc_id,
      request.tpc_label,
      request.tpc_twitter_id,
      request.tpc_updatedAt,
      request.pcustomer_isUser
    );

    return await this.repository.save(client);
  }
}
