export type ClientRequest = {
  pcustomer_id: string;
  pcustomer_user_id: string;
  pcustomer_username: string;
  pcustomer_frequency: string;
  pcustomer_frequency_percent: string;
  pcustomer_verify: string;
  pcustomer_bio: string;
  pcustomer_followers: string;
  pcustomer_followers_percent: string;
  pcustomer_social: string;
  pcustomer_total: string;
  tpc_id: string;
  tpc_twitter_id: string;
  tpc_label: string;
  tpc_createdAt: string;
  tpc_updatedAt: string;
  pcustomer_isUser: Boolean;
};
