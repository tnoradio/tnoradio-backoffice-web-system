export class Image {
  id: string;
  name: string;
  category: string;
  file: Buffer;
  type: string;

  constructor(
    id: string,
    name: string,
    category: string,
    file: Buffer,
    type: string
  ) {
    this.id = id;
    this.name = name;
    this.category = category;
    this.file = file;
    this.type = type;
  }
}
