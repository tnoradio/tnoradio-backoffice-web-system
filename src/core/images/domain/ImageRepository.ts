import { Image } from "./Image";

export interface ImageRepository {
  uploadImage(
    imageFile: File,
    category: string,
    name: string,
    type: string,
    location: string
  ): Promise<string>;
  getImage(
    category: string,
    type: string,
    name: string,
    location: string
  ): Promise<Image>;
  updateImage(
    imageFile: File,
    category: string,
    type: string,
    name: string,
    location: string
  ): Promise<string>;
}
