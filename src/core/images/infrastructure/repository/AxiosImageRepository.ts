import { ImageRepository } from "../../domain/ImageRepository";
import { Image } from "../../domain/Image";
import axios from "axios";
import { apiUrl, filesUrl } from "../../../../config";

const baseURL = "http://localhost:5555/api/images/";

export class AxiosImageRepository implements ImageRepository {
  public async uploadImage(
    imageFile: File,
    category: string,
    type: string,
    name: string,
    location: string
  ): Promise<string> {
    try {
      const formData = new FormData();

      const fileExtension = imageFile.name.split(".").pop();

      const newFileName = `${name}.${fileExtension}`;

      const renamedFile = new File([imageFile], newFileName, {
        type: imageFile.type,
        lastModified: imageFile.lastModified,
      });

      formData.set("file", renamedFile);

      await axios.post(
        `${baseURL}upload/${category}/${type}/${location}/${name}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return renamedFile.name;
    } catch (e) {
      console.error(e.response);
      return e;
    }
  }
  public async getImage(
    category: string,
    type: string,
    name: string,
    location: string
  ): Promise<Image> {
    try {
      const response = await axios.get(
        `${baseURL}/${category}/${type}/${location}/${name}`
      );
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async updateImage(
    imageFile,
    category,
    type,
    name,
    location
  ): Promise<string> {
    try {
      await axios.delete(
        `${baseURL}delete/${category}/${type}/${location}/${name}`
      );

      const response = await this.uploadImage(
        imageFile,
        category,
        type,
        name,
        location
      );
      return response;
    } catch (e) {
      if (e.response.data.ftp_code === 550) {
        const response = await this.uploadImage(
          imageFile,
          category,
          type,
          name,
          location
        );
        return response;
      } else {
        throw e;
      }
    }
  }
}
