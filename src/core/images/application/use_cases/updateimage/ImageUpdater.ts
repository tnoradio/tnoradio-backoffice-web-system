import { ImageRepository } from "../../../domain/ImageRepository";

export class ImageUpdater {
  private repository: ImageRepository;

  constructor(repository: ImageRepository) {
    this.repository = repository;
  }

  async run(
    file: File,
    category: string,
    type: string,
    name: string,
    location: string
  ): Promise<string> {
    return await this.repository.updateImage(
      file,
      category,
      type,
      name,
      location
    );
  }
}
