import { ImageRepository } from "../../../domain/ImageRepository";
import { Image } from "../../../domain/Image";

export class ImageGetter {
  private repository: ImageRepository;

  constructor(repository: ImageRepository) {
    this.repository = repository;
  }

  async run(
    category: string,
    type: string,
    name: string,
    location: string
  ): Promise<Image> {
    return await this.repository.getImage(category, type, name, location);
  }
}
