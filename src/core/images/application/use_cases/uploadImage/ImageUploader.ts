import { ImageRepository } from "../../../domain/ImageRepository";

export class ImageUploader {
  private repository: ImageRepository;

  constructor(repository: ImageRepository) {
    this.repository = repository;
  }

  async run(
    imageFile: File,
    category: string,
    name: string,
    type: string,
    location: string
  ): Promise<string> {
    return await this.repository.uploadImage(
      imageFile,
      category,
      name,
      type,
      location
    );
  }
}
