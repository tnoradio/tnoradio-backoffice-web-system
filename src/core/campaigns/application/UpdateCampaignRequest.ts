export type UpdateCampaignRequest = {
  _id: string;
  field: string;
  value: any;
};
