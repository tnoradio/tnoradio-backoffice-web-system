import Channel from "../domain/Channel";

export type CampaignRequest = {
  _id: string;
  name: string;
  price: Number;
  owner_id: string; // user id of owner, person that added the campaign
  start_date: Date;
  end_date: Date;
  channel: Channel[];
  starred: Boolean;
  deleted: Boolean;
  isActive: Boolean;
  owner: string;
  payments: string[];
};
