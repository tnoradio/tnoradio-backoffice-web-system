import Campaign from "../../domain/Campaign";
import { CampaignRepository } from "../../domain/CampaignRepository";

export class CampaignByIdGetter {
  private repository: CampaignRepository;

  constructor(repository: CampaignRepository) {
    this.repository = repository;
  }

  async run(_id: string): Promise<Campaign> {
    return await this.repository.getCampaignById(_id);
  }
}
