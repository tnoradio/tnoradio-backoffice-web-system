export default interface BuyerPerson {
  _id: string;
  age_range: string[];
  target: string[];
  brand_type: string; //Product or Service
  geolocalization: string[]; // Cities, states, countries
  interest_topics: string[];
  genders: string[];
}
