import Channel from "./Channel";

export default class SiteBanner extends Channel {
  dimensions: string;

  constructor(dimensions: string) {
    super();
    this.dimensions = dimensions;
  }

  static create(dimensions: string) {
    return new SiteBanner(dimensions);
  }
}
