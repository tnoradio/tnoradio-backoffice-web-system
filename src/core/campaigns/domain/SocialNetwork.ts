import Channel from "./Channel";

export default class SocialNetwork extends Channel {
  name: string;
  qty: number;

  constructor(name: string, qty: number) {
    super();
    this.name = name;
    this.qty = qty;
  }

  static create(name: string, qty: number) {
    return new SocialNetwork(name, qty);
  }
}
