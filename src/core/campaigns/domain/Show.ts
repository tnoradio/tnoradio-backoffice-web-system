import Channel from "./Channel";

export default class Show extends Channel {
  show_id: string;

  constructor(show_id: string) {
    super();
    this.show_id = show_id;
  }

  static create(show_id: string) {
    return new Show(show_id);
  }
}
