import Channel from "./Channel";
import Schedule from "./Schedule";

export default class Rotary extends Channel {
  schedules: Schedule[];

  constructor(schedules: Schedule[]) {
    super();
    this.schedules = schedules;
  }

  static create(schedules: Schedule[]) {
    return new Rotary(schedules);
  }
}
