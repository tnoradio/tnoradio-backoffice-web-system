import { UpdateCampaignRequest } from "../application/UpdateCampaignRequest";
import BuyerPerson from "./BuyerPerson";
import Campaign from "./Campaign";

export interface CampaignRepository {
  save(Campaign: Campaign): Promise<Campaign>;
  saveBuyerPerson(buyerPerson: BuyerPerson): Promise<BuyerPerson>;
  getCampaignById(_id: string): Promise<Campaign>;
  index(): Promise<Campaign[]>;
  getCampaignsPage(pageSize: number, page: number): Promise<Campaign[]>;
  update(updateRequest: UpdateCampaignRequest): Promise<Campaign>;
  destroy(CampaignId: string): Promise<any>;
  delete(CampaignId: string): Promise<Campaign>;
}
