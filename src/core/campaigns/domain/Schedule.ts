export default class Schedule {
  weekday: string;
  start_time: Date;
  end_time: Date;

  constructor(weekday: string, start_time: Date, end_time: Date) {
    this.weekday = weekday;
    this.start_time = start_time;
    this.end_time = end_time;
  }

  static create(weekday: string, start_time: Date, end_time: Date) {
    return new Schedule(weekday, start_time, end_time);
  }
}
