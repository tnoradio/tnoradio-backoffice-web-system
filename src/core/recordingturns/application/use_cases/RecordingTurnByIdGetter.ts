import DomainRecordingTurn from "../../domain/RecordingTurn";
import { RecordingTurnRepository } from "../../domain/RecordingTurnRepository";

export class RecordingTurnByIdGetter {
  private repository: RecordingTurnRepository;

  constructor(repository: RecordingTurnRepository) {
    this.repository = repository;
  }

  async run(_id: string): Promise<DomainRecordingTurn> {
    return await this.repository.getRecordingTurnById(_id);
  }
}
