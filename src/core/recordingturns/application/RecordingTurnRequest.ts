export type RecordingTurnRequest = {
  _id: string;
  owner_id: string; // user id of owner
  price: Number;
  description: string;
  date: Date;
  startTime: Date;
  endTime: Date;
  starred: Boolean;
  deleted: Boolean;
  status: string;
  payments: string[]; // associated payments ids
  createdAt: Date;
  turnTeam: string[];
  approved: Boolean;
  advertisers: string[];
  expenses: string[];
};
