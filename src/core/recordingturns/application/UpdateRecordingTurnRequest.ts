export type UpdateRecordingTurnRequest = {
  _id: string;
  field: string;
  value: any;
};
