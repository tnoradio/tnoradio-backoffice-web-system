import { UpdateRecordingTurnRequest } from "../application/UpdateRecordingTurnRequest";
import RecordingTurn from "./RecordingTurn";

export interface RecordingTurnRepository {
  save(recordingTurn: RecordingTurn): Promise<RecordingTurn>;
  getRecordingTurnById(_id: string): Promise<RecordingTurn>;
  index(): Promise<RecordingTurn[]>;
  getRecordingTurnsPage(
    pageSize: number,
    page: number
  ): Promise<RecordingTurn[]>;
  update(updateRequest: UpdateRecordingTurnRequest): Promise<RecordingTurn>;
  destroy(recordingTurnId: string): Promise<any>;
  delete(recordingTurnId: string): Promise<RecordingTurn>;
}
