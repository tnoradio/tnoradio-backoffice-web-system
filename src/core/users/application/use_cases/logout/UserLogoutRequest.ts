import EmployeeTurn from "../../../domain/EmployeeTurn";
import { UserImage } from "../../../domain/UserImage";
import UserRole from "../../../domain/UserRole";
import UserSocial from "../../../domain/UserSocial";

export type UserLogoutRequest = {
  user: User;
  token: string;
};

export type User = {
  _id: string;
  name: string;
  lastName: string;
  bio: string;
  birthdate: Date;
  image: string;
  dni: Number;
  email: {
    value: string;
  };
  isActive: Boolean;
  isWorking: Boolean;
  password: string;
  passwordConfirmation: string;
  socials: UserSocial[];
  gender: string;
  images: UserImage[];
  role: UserRole[];
  address: string;
  notes: string;
  company: string;
  department: string;
  phone: string;
  isStarred: Boolean;
  isDeleted: Boolean;
  updatedBy: string;
  slug: string;
  turns: EmployeeTurn[];
};
