import { UserEmail } from "../../../domain/UserEmail";
import { User } from "../../../domain/User";
import { UserRepository } from "../../../domain/UserRepository";
import { UserLogoutRequest } from "./UserLogoutRequest";
import { UserSession } from "../../../domain/UserSession";

export class UserLogout {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: UserLogoutRequest): Promise<Boolean> {
    const email = new UserEmail(request.user.email.value);
    const user = User.create(
      request.user._id,
      request.user.name,
      request.user.lastName,
      request.user.bio,
      request.user.birthdate,
      request.user.image,
      request.user.dni,
      email,
      request.user.isActive,
      request.user.isWorking,
      request.user.password,
      request.user.passwordConfirmation,
      request.user.socials,
      request.user.gender,
      request.user.images,
      request.user.role,
      request.user.address,
      request.user.notes,
      request.user.company,
      request.user.department,
      request.user.phone,
      request.user.isStarred,
      request.user.isDeleted,
      request.user.updatedBy,
      request.user.slug,
      request.user.turns
    );

    const session = new UserSession(user, request.token);
    return await this.repository.userLogout(session);
  }
}
