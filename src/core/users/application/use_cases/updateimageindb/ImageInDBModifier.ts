import { UserRepository } from "../../../domain/UserRepository";

export class ImageInDBModifier {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(
    image: any,
    name: string,
    type: string,
    slug: string,
    location: string,
    owner: string
  ): Promise<any> {
    return await this.repository.updateImageInDB(
      image,
      name,
      type,
      slug,
      location,
      owner
    );
  }
}
