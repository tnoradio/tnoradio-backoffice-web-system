import { User } from "../../../domain/User";
import { UserRepository } from "../../../domain/UserRepository";

export class ImageUploader {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(image: any, name: string): Promise<any> {
    return await this.repository.uploadImage(image, name);
  }
}
