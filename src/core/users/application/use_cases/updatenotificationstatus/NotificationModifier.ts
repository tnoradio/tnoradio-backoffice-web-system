import Notification from "../../../domain/Notification";
import { UserRepository } from "../../../domain/UserRepository";

export class NotificationModifier {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(updateValues): Promise<Notification> {
    return await this.repository.updateNotificationStatus(updateValues);
  }
}
