import { UserRepository } from "../../../domain/UserRepository";

export class MailSender {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(email: string, subject: string, text: string): Promise<string> {
    return await this.repository.sendEmail(email, subject, text);
  }
}
