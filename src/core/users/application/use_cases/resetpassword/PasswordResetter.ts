import { User } from "../../../domain/User";
import { UserRepository } from "../../../domain/UserRepository";

export class PasswordResetter {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(
    _id: string,
    oldPassword: string,
    password: string,
    passwordConfirmation: string
  ): Promise<User> {
    return await this.repository.resetLoggedInUserPassword(
      _id,
      oldPassword,
      password,
      passwordConfirmation
    );
  }
}
