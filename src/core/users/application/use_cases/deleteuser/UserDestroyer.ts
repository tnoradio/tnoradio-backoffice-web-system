import { UserRepository } from "../../../domain/UserRepository";
//import { DeleteUserRequest } from './DeleteUserRequest'
import { User } from "../../../domain/User";

export class UserDestroyer {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(_id: string): Promise<User | Error> {
    //ALGO EXTRAÑO SUCEDE, EL REQUEST RETORNA DIRECTO EL ID SIN
    //ENTRAR AL CAMPO
    try {
      console.log(_id);

      return await this.repository.destroyUser(_id);
    } catch (err) {
      console.log(err);
      return null;
    }
  }
}
