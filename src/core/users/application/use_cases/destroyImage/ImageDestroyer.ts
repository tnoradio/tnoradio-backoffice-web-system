import { UserRepository } from "../../../domain/UserRepository";

export class ImageDestroyer {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(userSlug: string): Promise<any | Error> {
    //ALGO EXTRAÑO SUCEDE, EL REQUEST RETORNA DIRECTO EL ID SIN
    //ENTRAR AL CAMPO
    try {
      console.log(userSlug);

      return await this.repository.destroyImage(userSlug);
    } catch (err) {
      console.log(err);
      return null;
    }
  }
}
