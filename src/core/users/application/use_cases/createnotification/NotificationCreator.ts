import Notification from "../../../domain/Notification";
import { UserRepository } from "../../../domain/UserRepository";

export class NotificationCreator {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: Notification): Promise<Notification> {
    const notification = Notification.create(
      request._id,
      request.message,
      request.user,
      request.isActive,
      request.type
    );

    return await this.repository.saveUserNotification(notification);
  }
}
