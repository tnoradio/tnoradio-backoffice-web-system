export type GetUserByEmailRequest = {
  email: {
    value: string;
  };
};
