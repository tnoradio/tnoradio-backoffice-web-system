import { UserEmail } from "../../../domain/UserEmail";
import { User } from "../../../domain/User";
import { UserRepository } from "../../../domain/UserRepository";
import { GetUserByEmailRequest } from "./GetUserByEmailRequest";

export class UserByEmailGetter {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: GetUserByEmailRequest): Promise<User | Error> {
    const email = new UserEmail(request.email.value);
    return await this.repository.getUserByEmail(email);
  }
}
