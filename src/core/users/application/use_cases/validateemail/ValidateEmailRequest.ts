export type ValidateEmailRequest = {
  email: string;
};
