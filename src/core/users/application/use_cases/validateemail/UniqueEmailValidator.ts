import { UserEmail } from "../../../domain/UserEmail";
import { UserRepository } from "../../../domain/UserRepository";
import { ValidateEmailRequest } from "./ValidateEmailRequest";

export class UniqueEmailValidator {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: ValidateEmailRequest): Promise<boolean> {
    const email = new UserEmail(request.email);
    return await this.repository.isValidEmail(email.value);
  }
}
