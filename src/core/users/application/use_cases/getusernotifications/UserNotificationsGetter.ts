import { UserRepository } from "../../../domain/UserRepository";
import Notification from "../../../domain/Notification";

export class UserNotificationsGetter {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(userId: string): Promise<Notification[]> {
    return await this.repository.getUserNotifications(userId);
  }
}
