import { User } from "../../../domain/User";
import { UserEmail } from "../../../domain/UserEmail";
import { UserRepository } from "../../../domain/UserRepository";
import { UserRequest } from "../../UserRequest";

export class UserCreator {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: UserRequest): Promise<User> {
    const user = User.create(
      request._id,
      request.name,
      request.lastName,
      request.bio,
      request.birthdate,
      request.image,
      request.dni,
      new UserEmail(request.email.value),
      request.isActive,
      request.isWorking,
      request.password,
      request.passwordConfirmation,
      request.socials,
      request.gender,
      request.images,
      request.roles,
      request.address,
      request.notes,
      request.company,
      request.department,
      request.phone,
      request.isStarred,
      request.isDeleted,
      request.updatedBy,
      request.slug,
      request.turns
    );

    return await this.repository.save(user);
  }
}
