import { UserCreator } from "../UserCreator";
import * as userUtils from "../../../../../../utility/test/test-user-utils";
import { AxiosUserRepository } from "../../../../infrastructure/repository/AxiosUserRepository";
import axios from "axios";

describe("UserCreator use case", () => {
  const axiosUserRepo = new AxiosUserRepository();

  it("should call create user use case", async () => {
    (axios.post as jest.Mock).mockReturnValueOnce(userUtils.addUserResponse);
    let userCreator = new UserCreator(axiosUserRepo);
    const result = await userCreator.run(userUtils.mockUserRequest);
    // console.log(result);
    expect(result).toStrictEqual(userUtils.mockUser1);
  });
});
