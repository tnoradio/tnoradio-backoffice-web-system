import { AllUsersGetter } from "../AllUsersGetter";
import * as userUtils from "../../../../../../utility/test/test-user-utils";
import { AxiosUserRepository } from "../../../../infrastructure/repository/AxiosUserRepository";
import axios from "axios";

describe("Get all users use case", () => {
  const axiosUserRepo = new AxiosUserRepository();

  (axios.get as jest.Mock).mockReturnValueOnce({
    data: userUtils.mockUsersList,
  });

  let allUsersGetter = new AllUsersGetter(axiosUserRepo);

  it("fetches users List", async () => {
    const result = await allUsersGetter.run();
    expect(result).toBe(userUtils.mockUsersList);
  });
});
