import { UserLogin } from "../UserLogin";
import * as userUtils from "../../../../../../utility/test/test-user-utils";
import { AxiosUserRepository } from "../../../../infrastructure/repository/AxiosUserRepository";
import axios from "axios";

describe("Get all users use case", () => {
  const axiosUserRepo = new AxiosUserRepository();

  (axios.post as jest.Mock).mockReturnValueOnce({
    data: userUtils.mockSession,
  });

  let userLogin = new UserLogin(axiosUserRepo);

  it("Logs in an user", async () => {
    const result = await userLogin.run(userUtils.mockUserLoginRequest);

    // console.log(result);
    expect(result).toEqual(userUtils.mockSession);
  });
});
