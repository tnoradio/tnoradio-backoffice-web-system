import { UserEmail } from "../../../domain/UserEmail";
import { UserRepository } from "../../../domain/UserRepository";
import { UserLoginRequest } from "./UserLoginRequest";
import { UserSession } from "../../../domain/UserSession";

export class UserLogin {
  private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: UserLoginRequest): Promise<UserSession> {
    const email = new UserEmail(request.email.value);
    return await this.repository.userLogin(email, request.password);
  }
}
