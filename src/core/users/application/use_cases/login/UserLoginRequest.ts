export type UserLoginRequest = {
  email: {
    value: string;
  };
  password: string;
};
