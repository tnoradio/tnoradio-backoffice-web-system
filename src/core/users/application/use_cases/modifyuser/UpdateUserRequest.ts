export type UpdateUserRequest = {
  _id: string;
  field: string;
  value: any;
};
