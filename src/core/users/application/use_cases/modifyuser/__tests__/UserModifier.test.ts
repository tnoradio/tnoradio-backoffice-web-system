import { UserModifier } from "../UserModifier";
import * as userUtils from "../../../../../../utility/test/test-user-utils";
import { AxiosUserRepository } from "../../../../infrastructure/repository/AxiosUserRepository";
import axios from "axios";

describe("UserCreator use case", () => {
  const axiosUserRepo = new AxiosUserRepository();

  it("should call modify user use case", async () => {
    (axios.get as jest.Mock).mockResolvedValueOnce(userUtils.addUserResponse);
    (axios.patch as jest.Mock).mockResolvedValueOnce(userUtils.addUserResponse);
    let userModifier = new UserModifier(axiosUserRepo);
    const result = await userModifier.run(userUtils.mockUserUpdate);
    // console.log(result);
    expect(result).toStrictEqual(userUtils.mockUser1);
  });
});
