import { User } from "../../../domain/User";
import { UserRepository } from "../../../domain/UserRepository";
import { UpdateUserRequest } from "./UpdateUserRequest";

export class UserModifier {
  private repository: UserRepository;
  request: UpdateUserRequest;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: UpdateUserRequest): Promise<User> {
    const userUpdate = {
      _id: request._id,
      field: request.field,
      value: request.value,
    };

    return await this.repository.updateUser(userUpdate);
  }
}
