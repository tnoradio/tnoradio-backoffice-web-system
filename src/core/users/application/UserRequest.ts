import EmployeeTurn from "../domain/EmployeeTurn";

export type UserRequest = {
  _id: string;
  name: string;
  lastName: string;
  bio: string;
  birthdate: Date;
  image: string;
  dni: number;
  email: {
    value: string;
  };
  isActive: Boolean;
  isWorking: Boolean;
  password: string;
  passwordConfirmation: string;
  socials: [{ socialNetwork: string; url: string; userName: string }];
  gender: string;
  images: any;
  roles: [{ role: string }];
  address: string;
  notes: string;
  company: string;
  department: string;
  phone: string;
  isStarred: Boolean;
  isDeleted: Boolean;
  updatedBy: string;
  slug: string;
  turns: EmployeeTurn[];
};
