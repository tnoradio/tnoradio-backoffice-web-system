import { UserEmail } from "../UserEmail";

export class User {
  readonly _id: string;
  readonly name: string;
  readonly lastName: string;
  readonly image: string;
  readonly dni: Number;
  readonly email: UserEmail;
  readonly isActive: Boolean;
  readonly password: string;
  readonly passwordConfirmation: string;
  readonly gender: string;
  readonly role: string;
  readonly address: string;
  readonly notes: string;
  readonly company: string;
  readonly department: string;
  readonly phone: string;
  readonly isStarred: Boolean;
  readonly isDeleted: Boolean;
  readonly updatedBy: string;

  constructor(
    _id: string,
    name: string,
    lastName: string,
    image: string,
    dni: Number,
    email: UserEmail,
    isActive: Boolean,
    password: string,
    passwordConfirmation: string,
    gender: string,
    role: string,
    address: string,
    notes: string,
    company: string,
    department: string,
    phone: string,
    isStarred: Boolean,
    isDeleted: Boolean,
    updatedBy: string
  ) {
    this._id = _id;
    this.name = name;
    this.lastName = lastName;
    this.image = image;
    this.dni = dni;
    this.email = email;
    this.isActive = isActive;
    this.password = password;
    this.passwordConfirmation = passwordConfirmation;
    this.gender = gender;
    this.role = role;
    this.address = address;
    this.notes = notes;
    this.company = company;
    this.department = department;
    this.phone = phone;
    this.isStarred = isStarred;
    this.isDeleted = isDeleted;
    this.updatedBy = updatedBy;
  }

  static create(
    _id: string,
    name: string,
    lastName: string,
    image: string,
    dni: Number,
    email: UserEmail,
    isActive: Boolean,
    password: string,
    passwordConfirmation: string,
    gender: string,
    role: string,
    address: string,
    notes: string,
    company: string,
    department: string,
    phone: string,
    isStarred: Boolean,
    isDeleted: Boolean,
    updatedBy: string
  ) {
    return new User(
      _id,
      name,
      lastName,
      image,
      dni,
      email,
      isActive,
      password,
      passwordConfirmation,
      gender,
      role,
      address,
      notes,
      company,
      department,
      phone,
      isStarred,
      isDeleted,
      updatedBy
    );
  }

  static createFromUser(user: any) {}
}
