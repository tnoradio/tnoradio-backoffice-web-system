export default class UserSocial {
  socialNetwork: string;
  url: string;
  userName: string;

  constructor(socialNetwork: string, url: string, userName: string) {
    this.socialNetwork = socialNetwork;
    this.url = url;
    this.userName = userName;
  }

  static create(socialNetwork: string, url: string, userName: string) {
    return new UserSocial(socialNetwork, url, userName);
  }
}
