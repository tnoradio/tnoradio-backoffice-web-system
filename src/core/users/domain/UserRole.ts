export default class UserRole {
  role: string;

  constructor(role: string) {
    this.role = role;
  }

  static create(role: string) {
    return new UserRole(role);
  }
}
