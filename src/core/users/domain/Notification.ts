export default class Notification {
  _id: string;
  message: string; // Attribute just for user_history
  isActive: Boolean;
  user: string;
  type: string;

  constructor(
    _id: string,
    message: string,
    user: string,
    isActive: Boolean,
    type: string
  ) {
    this._id = _id;
    this.message = message;
    this.isActive = isActive;
    this.user = user;
    this.type = type;
  }

  static create(
    _id: string,
    message: string,
    user: string,
    isActive: Boolean,
    type: string
  ) {
    return new Notification(_id, message, user, isActive, type);
  }
}
