import EmployeeTurn from "./EmployeeTurn";
import Notification from "./Notification";
import { User } from "./User";
import { UserEmail } from "./UserEmail";
import { UserSession } from "./UserSession";

export interface UserRepository {
  save(user: User): Promise<User>;
  isValidEmail(email: string): Promise<boolean>;
  getUserByEmail(email: UserEmail): Promise<User | Error>;
  userLogin(email: UserEmail, password: string): Promise<UserSession>;
  userLogout(userSession: UserSession): Promise<Boolean>;
  index(): Promise<User[]>;
  getUserNotifications(userId: string): Promise<Notification[]>;
  saveUserNotification(notification: Notification): Promise<Notification>;
  updateNotificationStatus(updateValues): Promise<Notification>;
  updateUser(updateUser: any): Promise<User>;
  deleteUser(_id: string): Promise<User | Error>;
  destroyUser(_id: string): Promise<User>;
  uploadImage(image: any, name: string): Promise<any>;
  uploadImageToDB(image, name, type, slug, location, owner): Promise<any>;
  updateImageInDB(image, name, type, slug, location, owner): Promise<any>;
  getUserImage(name, slug);
  destroyImage(userSlug): Promise<any>;
  sendEmail(email: string, subject: string, text: string): Promise<string>;
  resetLoggedInUserPassword(
    _id: string,
    oldPassword: string,
    password: string,
    passwordConfirmation: string
  ): Promise<User>;
  saveEmployeeTurn(employeeTurn: EmployeeTurn): Promise<EmployeeTurn>;
  updateEmployeeTurn(updateValues): Promise<EmployeeTurn>;
  getEmployeeTurns(userId: string): Promise<EmployeeTurn[]>;
  destroyEmployeeTurn(_id: string): Promise<EmployeeTurn>;
}
