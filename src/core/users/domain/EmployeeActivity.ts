export default class EmployeeActivity {
  _id: string;
  duration: number; //
  name: string;
  taskId: string;
  turnId: string;

  constructor(
    _id: string,
    duration: number, //
    name: string,
    taskId: string,
    turnId: string
  ) {
    this._id = _id;
    this.duration = duration;
    this.name = name;
    this.taskId = taskId;
    this.turnId = turnId;
  }

  static create(
    _id: string,
    duration: number, //
    name: string,
    taskId: string,
    turnId: string
  ) {
    return new EmployeeActivity(_id, duration, name, taskId, turnId);
  }
}
