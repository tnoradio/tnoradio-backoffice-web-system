export class UserImage {
  _id: string;
  imageName: string;
  imageUrl: string;
  file: {
    data: Buffer;
    contentType: string;
  };
  owner: string;

  constructor(
    _id: string,
    imageName: string,
    imageUrl: string,
    file: { data: Buffer; contentType: string },
    owner: string
  ) {
    this._id = _id;
    this.imageName = imageName;
    this.imageUrl = imageUrl;
    this.file = file;
    this.owner = owner;
  }

  static create(
    _id: string,
    imageName: string,
    imageUrl: string,
    file: { data: Buffer; contentType: string },
    owner: string
  ) {
    return new UserImage(_id, imageName, imageUrl, file, owner);
  }
}
