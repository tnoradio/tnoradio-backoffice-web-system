import { Repository } from "../../shared/domain/Repository";
import Payment from "./Payment";

export interface PaymentRepository extends Repository {
  save(payment: Payment, files: [], showName: string): Promise<Payment>;
  getPaymentById(_id: string): Promise<Payment>;
  index(): Promise<Payment[] | Error>;
  getPaymentsPage(pageSize: number, page: number): Promise<Payment[] | Error>;
  update(
    payment: Payment,
    field?: string,
    files?: File[]
  ): Promise<Payment | Error>;
  destroy(paymentId: string): Promise<any | Error>;
}
