export default class Receipt {
  fileId: string;
  fileName: string;
  fileUrl: string;
  owner: string;

  constructor(
    fileId: string,
    fileName: string,
    fileUrl: string,
    owner: string
  ) {
    this.fileId = fileId;
    this.fileName = fileName;
    this.fileUrl = fileUrl;
    this.owner = owner;
  }
}
