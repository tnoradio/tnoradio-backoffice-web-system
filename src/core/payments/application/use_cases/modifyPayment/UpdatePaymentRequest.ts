import Receipt from "../../../domain/Receipt";

export type UpdatePaymentRequest = {
  _id: string;
  amount: Number;
  amountBs: Number;
  amountUsd: Number;
  amountEuro: Number;
  amountCrypto: Number;
  amountOther: Number;
  owner_id: string; // user id of owner, person that added the payment
  show_owner: number; //show id if payment belongs to a show
  recording_turn_owner: string;
  concept: string;
  currency: string;
  receipts: Receipt[];
  payment_date: Date;
  starred: Boolean;
  deleted: Boolean;
  reconciled: Boolean;
  method: string;
  payment_type: string;
};
