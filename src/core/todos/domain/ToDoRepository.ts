import { ToDo } from "./ToDo";

export interface ToDoRepository {
  save(show: ToDo): Promise<ToDo>;
  index(): Promise<ToDo[]>;
  getUserTodos(owner: string): Promise<ToDo[]>;
  updateToDo(updateValues: any): Promise<ToDo>;
  destroyToDo(pro_id: string): Promise<ToDo | Error>;
  uploadImage(image, slug, location, name, showId, imageName): Promise<any>;
  updateImageInDB(image, name, type, slug, location, owner): Promise<any>;
  getToDoImage(name, slug);
  getToDoDetail(slug);
}
