export class ToDoImage {
  _id: string;
  imageName: string;
  imageUrl: string;

  constructor(_id: string, imageName: string, imageUrl: string) {
    this._id = _id;
    this.imageName = imageName;
    this.imageUrl = imageUrl;
  }

  static create(_id: string, imageName: string, imageUrl: string) {
    return new ToDoImage(_id, imageName, imageUrl);
  }
}
