export type DeleteTodoRequest = {
  _id: string;
};
