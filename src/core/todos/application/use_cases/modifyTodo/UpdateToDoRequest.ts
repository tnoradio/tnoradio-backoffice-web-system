import { ToDoImage } from "../../../domain/ToDoImage";

export type UpdateToDoRequest = {
  _id: string;
  toDo_id: string; // Attribute just for toDo_history
  full_name: string;
  task: string;
  owner_id: string; // user id of owner
  project: string;
  image: string;
  images: ToDoImage[];
  start_date: Date;
  due_date: Date;
  tags: string[];
  comments: string;
  priority: Boolean;
  department: string;
  starred: Boolean;
  deleted: Boolean;
  updatedBy: string;
  slug: string;
  completed: Boolean;
  status: string;
};
