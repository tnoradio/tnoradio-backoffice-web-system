export default class TwitterUserClassifiedByTopic {
  proc_user_id_twitter: string;
  proc_label: string;
  proc_bio: string;

  constructor(
    proc_user_id_twitter: string,
    proc_label: string,
    proc_bio: string
  ) {
    this.proc_user_id_twitter = proc_user_id_twitter;
    this.proc_label = proc_label;
    this.proc_bio = proc_bio;
  }
}
