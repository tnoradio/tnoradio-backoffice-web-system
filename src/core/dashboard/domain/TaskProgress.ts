export default class TaskProgress {
  status: string;
  count_status: Number;
  count_percent: Number;

  constructor(status: string, count_status: Number, count_percent: Number) {
    this.status = status;
    this.count_status = count_status;
    this.count_percent = count_percent;
  }
}
