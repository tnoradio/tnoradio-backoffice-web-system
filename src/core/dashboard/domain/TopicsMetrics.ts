export default class TopicMetrics {
  topic_name: string;
  topic_cant: string;
  topic_cant_percent: string;

  constructor(
    topic_name: string,
    topic_cant: string,
    topic_cant_percent: string
  ) {
    this.topic_name = topic_name;
    this.topic_cant = topic_cant;
    this.topic_cant_percent = topic_cant_percent;
  }
}
