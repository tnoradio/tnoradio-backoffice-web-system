export default class PublishedFacebook {
  _id: string;
  id: string;
  message: string;
  story: string;
  created_time: Date;
  post_reaction_like: string;
  post_impressions_unique: string;
  post_impressions_fan: string;
  post_impressions_organic_unique: string;
  createdAt: string;
  updatedAt: string;

  constructor(
    id: string,
    _id: string,
    message: string,
    story: string,
    created_time: Date,
    updated_time: Date,
    post_impressions_organic_unique: string,
    post_impressions_fan: string,
    post_reaction_like: string,
    post_impressions_unique: string,
    createdAt: string,
    updatedAt: string
  ) {
    this._id = _id;
    this.id = id;
    this.message = message;
    this.story = story;
    this.created_time = created_time;
    this.post_reaction_like = post_reaction_like;
    this.post_impressions_unique = post_impressions_unique;
    this.post_impressions_fan = post_impressions_fan;
    this.post_impressions_organic_unique = post_impressions_organic_unique;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
