export default class PublishedInstagram {
  _id: string;
  comments: string;
  likes: string;
  description: string;
  views: Date;
  score: string;
  link: string;
  hashtags: string[];
}
