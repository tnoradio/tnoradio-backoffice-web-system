import TopicMetrics from "../TopicsMetrics";
import PublishedFacebook from "../PublishedFacebook";
import PublishedInstagram from "../PublishedInstagram";
import TaskProgress from "../TaskProgress";
import TaskStatististicsPerPerson from "../TaskStatisticsPerPerson";
import TwitterUserClassifiedByTopic from "../TwitterUserClassifiedByTopic";

export interface DashboardRepository {
  getMetricsByTopic(): Promise<TopicMetrics[]>;
  getPublishedFacebook(): Promise<PublishedFacebook[]>;
  getPublishedInstagram(): Promise<PublishedInstagram[]>;
  getProgressTasks(): Promise<TaskProgress[]>;
  getTasksStatisticsPerPerson(): Promise<TaskStatististicsPerPerson[]>;
  getTwitterUsersClassifiedByTopic(): Promise<TwitterUserClassifiedByTopic[]>;
  updateBioTopic(
    userClassifiedByTopic: TwitterUserClassifiedByTopic,
    newTopic: string
  ): Promise<any>;
}
