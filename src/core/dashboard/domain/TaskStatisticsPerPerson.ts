export default class TaskStatististicsPerPerson {
  owner_id: string;
  total_tasks: number;
  total_completed: number;
  total_completed_percents: number;

  constructor(
    owner_id: string,
    total_tasks: number,
    total_completed: number,
    total_completed_percents: number
  ) {
    this.owner_id = owner_id;
    this.total_tasks = total_tasks;
    this.total_completed = total_completed;
    this.total_completed_percents = total_completed_percents;
  }
}
