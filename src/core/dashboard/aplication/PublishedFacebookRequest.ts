export type PublishedFacebookRequest = {
  _id: string;
  id: string;
  message: string;
  story: string;
  created_time: Date;
  post_reaction_like: string;
  post_impressions_unique: string;
  post_impressions_fan: string;
  post_impressions_organic_unique: string;
  createdAt: string;
  updatedAt: string;
};
