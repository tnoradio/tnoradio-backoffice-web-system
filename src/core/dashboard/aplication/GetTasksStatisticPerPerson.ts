export type GetTasksStatisticsPerPerson = {
  owner_id: string;
  total_task: number;
  total_completed: number;
  total_completed_percents: number;
};
