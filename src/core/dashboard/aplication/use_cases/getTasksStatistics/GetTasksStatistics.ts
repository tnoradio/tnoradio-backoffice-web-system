import { DashboardRepository } from "../../../domain/services/DashboardRepository";
import TaskProgress from "../../../domain/TaskProgress";

export class GetTasksStatistics {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(): Promise<TaskProgress[]> {
    return await this.repository.getProgressTasks();
  }
}
