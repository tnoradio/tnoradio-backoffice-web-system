import { DashboardRepository } from "../../../domain/services/DashboardRepository";
import TaskStatisticsPerPerson from "../../../domain/TaskStatisticsPerPerson";

export class GetTasksStatisticsPerPerson {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(): Promise<TaskStatisticsPerPerson[]> {
    return await this.repository.getTasksStatisticsPerPerson();
  }
}
