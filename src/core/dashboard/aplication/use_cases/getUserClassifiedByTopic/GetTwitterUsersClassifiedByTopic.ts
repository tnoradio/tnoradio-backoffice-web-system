import { DashboardRepository } from "../../../domain/services/DashboardRepository";
import TwitterUserClassifiedByTopic from "../../../domain/TwitterUserClassifiedByTopic";

export class GetUsersClassifiedByTopic {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(): Promise<TwitterUserClassifiedByTopic[]> {
    return await this.repository.getTwitterUsersClassifiedByTopic();
  }
}
