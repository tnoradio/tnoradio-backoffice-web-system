import { DashboardRepository } from "../../../domain/services/DashboardRepository";
import TwitterUserClassifiedByTopic from "../../../domain/TwitterUserClassifiedByTopic";

export class UpdateUserBioTopic {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(
    userClassifiedByTopic: TwitterUserClassifiedByTopic,
    newTopic: string
  ): Promise<any> {
    return await this.repository.updateBioTopic(
      userClassifiedByTopic,
      newTopic
    );
  }
}
