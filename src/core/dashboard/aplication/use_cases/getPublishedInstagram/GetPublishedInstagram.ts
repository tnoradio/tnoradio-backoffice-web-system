import { DashboardRepository } from "../../../domain/services/DashboardRepository";

import PublishedInstagram from "../../../domain/PublishedInstagram";

export class GetPublishedInstagram {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(): Promise<PublishedInstagram[]> {
    return await this.repository.getPublishedInstagram();
  }
}
