import { DashboardRepository } from "../../../domain/services/DashboardRepository";

import PublishedFacebook from "../../../domain/PublishedFacebook";

export class GetPublishedFacebook {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(): Promise<PublishedFacebook[]> {
    return await this.repository.getPublishedFacebook();
  }
}
