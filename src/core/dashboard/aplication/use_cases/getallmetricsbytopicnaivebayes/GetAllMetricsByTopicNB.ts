import { DashboardRepository } from "../../../domain/services/DashboardRepository";
import TopicMetrics from "../../../domain/TopicsMetrics";

export class GetAllTopicMetricsNaiveBayes {
  private repository: DashboardRepository;

  constructor(repository: DashboardRepository) {
    this.repository = repository;
  }
  async run(): Promise<TopicMetrics[]> {
    return await this.repository.getMetricsByTopic();
  }
}
