export type PublishedInstagramRequest = {
  _id: string;
  description: string;
  views: string;
  score: string;
  likes: string;
  comments: string;
  link: string;
};
