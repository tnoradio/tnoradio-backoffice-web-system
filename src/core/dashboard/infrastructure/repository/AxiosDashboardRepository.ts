import TopicMetrics from "../../domain/TopicsMetrics";
import { DashboardRepository } from "../../domain/services/DashboardRepository";
import axios from "axios";
import PublishedFacebook from "../../domain/PublishedFacebook";
import TaskProgress from "../../domain/TaskProgress";
import TaskStatististicsPerPerson from "../../domain/TaskStatisticsPerPerson";
import PublishedInstagram from "../../domain/PublishedInstagram";
import TwitterUserClassifiedByTopic from "../../domain/TwitterUserClassifiedByTopic";
import { apiUrl } from "../../../../config";

const baseURL = `${apiUrl}`;
const facebookBaseURL = `${apiUrl}facebook`;
const taskBaseURL = `${apiUrl}toDo/`;
const instagramBaseURL = `${apiUrl}instagram/`;

export class AxiosDashboardRepository implements DashboardRepository {
  async updateBioTopic(
    userClassifiedByTopic: TwitterUserClassifiedByTopic,
    newTopic: string
  ): Promise<any> {
    const updateObject = {
      userClassifiedByTopic: userClassifiedByTopic,
      newTopic: newTopic,
    };
    console.log(updateObject);
    try {
      const result = await axios.patch(
        baseURL +
          "update-data-training-request/" +
          userClassifiedByTopic.proc_user_id_twitter,
        updateObject
      );
      return result.data;
    } catch (error) {
      console.error(error);
      try {
        return setTimeout(async function () {
          const result = await axios.patch(
            baseURL +
              "update-data-training-request/" +
              userClassifiedByTopic.proc_user_id_twitter,
            updateObject
          );
          console.log(result);
          return result;
        }, 600);
      } catch (error) {
        console.log("otra vez");
        if (error.response) {
          return Promise.reject(error.response);
        } else {
          let errorObject = JSON.parse(JSON.stringify(error));
          if (errorObject.message) {
            return Promise.reject(errorObject);
          } else {
            return Promise.reject(error);
          }
        }
      }
    }
  }

  async getPublishedInstagram(): Promise<PublishedInstagram[]> {
    try {
      console.log(instagramBaseURL + "poststnoradio");
      const response = await axios.get(instagramBaseURL + "poststnoradio");
      console.log(response);
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async getProgressTasks(): Promise<TaskProgress[]> {
    try {
      const response = await axios.get(taskBaseURL + "getstatistics");
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async getPublishedFacebook(): Promise<PublishedFacebook[]> {
    try {
      const response = await axios.get(facebookBaseURL + "poststnoradio");
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async getMetricsByTopic(): Promise<TopicMetrics[]> {
    try {
      const response = await axios.get(baseURL + "get-metric-by-topic");
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async getTasksStatisticsPerPerson(): Promise<TaskStatististicsPerPerson[]> {
    try {
      const response = await axios.get(taskBaseURL + "progressPerPerson");
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async getTwitterUsersClassifiedByTopic(): Promise<
    TwitterUserClassifiedByTopic[]
  > {
    try {
      const response = await axios.get(baseURL + "get-list-classify");
      console.log(response);
      return response.data.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }
}
