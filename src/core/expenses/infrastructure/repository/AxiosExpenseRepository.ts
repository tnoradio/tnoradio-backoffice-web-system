/**
 * Implements Expense repository interface with axios
 * promise based HTTP client.
 */

import Expense from "../../domain/Expense";
import { ExpenseRepository } from "../../domain/ExpenseRepository";
import axios from "axios";
import Receipt from "../../domain/Receipt";
import { apiUrl } from "../../../../config";

//const baseURL = `https://expenses.tnoradio.com/api/expenses/`;
const baseURL = "http://localhost:18000/api/expenses/";

export class AxiosExpenseRepository implements ExpenseRepository {
  public async index(): Promise<Expense[]> {
    try {
      const response = await axios.get(baseURL + "index");
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async save(expense: Expense, files?: []): Promise<Expense> {
    const receiptFiles = this.createFormData(expense, files);
    // receiptFiles.append("expense", JSON.stringify(expense));
    try {
      const response = await axios.post(baseURL + "save", receiptFiles, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      if (response !== undefined && response.data !== undefined) {
        return response.data;
      } else {
        return Promise.resolve(null);
      }
    } catch (error) {
      //  console.error(error);
      if (error.response) {
        console.error(error.response);
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async update(
    updatedExpense: Expense,
    field?: string,
    files?: File[],
    showName?: string
  ): Promise<Expense | Error> {
    try {
      const formData = this.createFormData(updatedExpense, files, showName);
      const result = await axios.patch(
        baseURL + "update/" + updatedExpense._id,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return Promise.resolve(result.data);
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async destroy(id: string): Promise<Expense> {
    try {
      const response = await axios.delete(baseURL + "destroy/" + id);
      return response.data;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async uploadImage(
    file,
    _id,
    location,
    name,
    type,
    expenseId,
    imageName
  ): Promise<any> {
    const formData = new FormData();
    formData.set("image", file, imageName);
    formData.set("_id", _id);
    formData.set("location", location);
    formData.set("type", type);
    formData.set("owner", expenseId);
    formData.set("name", name);

    try {
      const response = await axios.post(baseURL + "uploadtodb", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return response.data;
    } catch (e) {
      console.error(e);
      return e;
    }
  }
  public async expenseImage(
    file,
    _id,
    location,
    name,
    type,
    expenseId,
    imageName
  ): Promise<any> {
    const formData = new FormData();
    formData.set("image", file, imageName);
    formData.set("_id", _id);
    formData.set("location", location);
    formData.set("type", type);
    formData.set("owner", expenseId);
    formData.set("name", name);

    try {
      const response = await axios.post(baseURL + "upload", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return response.data;
    } catch (e) {
      console.error(e);
      return e;
    }
  }

  public async updateFiles(
    image,
    name,
    type,
    _id,
    location,
    imageId,
    fileId
  ): Promise<any> {
    const formData = new FormData();
    formData.set("image", image, _id + ".jpg");
    formData.set("name", name);
    formData.set("type", type);
    formData.set("_id", _id);
    formData.set("location", location);
    formData.set("owner", imageId);
    formData.set("fileId", fileId);

    try {
      const response = await axios.patch(baseURL + "updateimage", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Accept: "multipart/form-data",
        },
      });
      return response.data;
    } catch (e) {
      console.error(e);
      return e;
    }
  }

  public async getExpenseImage(name: string, _id: string) {
    let res;
    try {
      res = await axios.get(baseURL + "imagefromdb/" + name + "/" + _id);
      return res.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getExpenseById(_id: string): Promise<Expense> {
    let res;
    try {
      res = await axios.get(baseURL + "details/" + _id + "/");
      return res.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  async getExpensesPage(
    pageSize: number,
    page: number
  ): Promise<Expense[] | Error> {
    try {
      const response = await axios.get(
        baseURL + "index/" + pageSize + "/" + page
      );
      //console.log(response);
      return response.data;
    } catch (error) {
      // console.log(error);
      if (error.response) {
        //console.log("ERROR.RESPONSE");
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          // console.log("ERROR OBJECT " + errorObject);
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getFile(file_id: string, file_name: string): Promise<File> {
    try {
      const response = await axios.get(
        baseURL + "receipt/" + file_id + "/" + file_name
      );
      return response.data;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  public async createFile(expense, file, showName): Promise<Receipt> {
    var files = [];
    files.push(file);
    try {
      const receiptFiles = this.createFormData(expense, files, showName);
      const response = await axios.post(baseURL + "savereceipt", receiptFiles, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      if (response !== undefined && response.data !== undefined) {
        return response.data;
      } else {
        return Promise.resolve(null);
      }
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  public async deleteFile(fileId: string): Promise<any> {
    try {
      const response = await axios.delete(baseURL + "destroyfile/" + fileId);
      if (response !== undefined && response.data !== undefined) {
        return response.data;
      } else {
        return Promise.resolve(null);
      }
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  createFormData(expense: Expense, files?: any[], showName?: string) {
    var formData = new FormData();
    //NO FILE DATA SHALL GO FIRST
    formData.append("expense", JSON.stringify(expense));
    showName && formData.append("showName", JSON.stringify(showName || ""));

    files &&
      files.length > 0 &&
      files.forEach((file, index) => {
        const filename =
          expense.owner_id +
          "_" +
          index +
          "_" +
          expense.date +
          file.name.substring(file.name.lastIndexOf("."), file.name.length);
        formData.append("receipts", file, filename);
      });
    return formData;
  }

  async fetchFile(fileUrl: string): Promise<any> {
    try {
      const res = await axios({
        method: "get",
        url: fileUrl,
        responseType: "stream",
      });
      return res;
    } catch (e) {
      console.log(e);
    }
  }
}
