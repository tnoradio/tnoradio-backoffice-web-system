import Receipt from "../domain/Receipt";

export type ExpenseRequest = {
  _id: string;
  amountBs: Number;
  amountUsd: Number;
  amountEuro: Number;
  amountCripto: Number;
  amountOther: Number;
  owner_id: string; // user id of owner, person that added the expense
  recording_turn_owner: string; //if is fro a show
  concept: string;
  currency: string;
  receipts: Receipt[];
  date: Date;
  starred: Boolean;
  deleted: Boolean;
  reconciled: Boolean;
  method: string;
  expense_type: string;
};
