import Expense from "../../../domain/Expense";
import { ExpenseRepository } from "../../../domain/ExpenseRepository";
import { UpdateExpenseRequest } from "./UpdateExpenseRequest";

export class ExpenseModifier {
  private repository: ExpenseRepository;
  request: UpdateExpenseRequest;

  constructor(repository: ExpenseRepository) {
    this.repository = repository;
  }

  async run(
    request: UpdateExpenseRequest,
    field?: string,
    files?: File[]
  ): Promise<Expense | Error> {
    const expenseUpdate = Expense.create(
      request._id,
      request.amountBs,
      request.amountUsd,
      request.amountEuro,
      request.amountCripto,
      request.amountOther,
      request.owner_id,
      request.recording_turn_owner,
      request.concept,
      request.receipts,
      request.date,
      request.starred,
      request.deleted,
      request.reconciled,
      request.currency,
      request.method,
      request.expense_type
    );

    return await this.repository.update(expenseUpdate, field, files);
  }
}
