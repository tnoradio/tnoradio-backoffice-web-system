import { Repository } from "../../shared/domain/Repository";
import Expense from "./Expense";

export interface ExpenseRepository extends Repository {
  save(expense: Expense, files: []): Promise<Expense>;
  getExpenseById(_id: string): Promise<Expense>;
  index(): Promise<Expense[] | Error>;
  getExpensesPage(pageSize: number, page: number): Promise<Expense[] | Error>;
  update(
    expense: Expense,
    field?: string,
    files?: File[]
  ): Promise<Expense | Error>;
  destroy(expenseId: string): Promise<any | Error>;
}
