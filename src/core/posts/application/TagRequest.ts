export type TagRequest = {
  _id: string;
  tag: string;
};
