import { TextBlock } from "../../../domain/Post";
import PostImage from "../../../domain/PostImage";

export type UpdatePostRequest = {
  _id: string;
  title: string;
  owner_id: string; // user id of owner
  subtitle: string;
  summary: string;
  image: string;
  images: PostImage[];
  publish_date: Date;
  tags: string[];
  starred: Boolean;
  deleted: Boolean;
  text: string;
  slug: string;
  approved: Boolean;
};
