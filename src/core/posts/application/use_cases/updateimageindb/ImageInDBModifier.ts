import { PostRepository } from "../../../domain/PostRepository";

export class ImageInDBModifier {
  private repository: PostRepository;

  constructor(repository: PostRepository) {
    this.repository = repository;
  }

  async run(
    image: any,
    name: string,
    type: string,
    slug: string,
    location: string,
    owner: string,
    fileId: string
  ): Promise<any> {
    return await this.repository.updateImageInDB(
      image,
      name,
      type,
      slug,
      location,
      owner,
      fileId
    );
  }
}
