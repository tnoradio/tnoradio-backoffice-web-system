import { PostRepository } from "../../../domain/PostRepository";
import { Tag } from "../../../domain/Tag";

export class AllTagsGetter {
  private repository: PostRepository;

  constructor(repository: PostRepository) {
    this.repository = repository;
  }

  async run(): Promise<Tag[]> {
    return await this.repository.indexTags();
  }
}
