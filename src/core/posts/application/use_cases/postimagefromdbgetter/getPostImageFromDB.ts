import { PostRepository } from "../../../domain/PostRepository";
import PostImage from "../../../domain/PostImage";

export class PostImageFromDBGetter {
  private repository: PostRepository;

  constructor(repository: PostRepository) {
    this.repository = repository;
  }

  async run(name: string, slug: string): Promise<PostImage> {
    return await this.repository.getPostImage(name, slug);
  }
}
