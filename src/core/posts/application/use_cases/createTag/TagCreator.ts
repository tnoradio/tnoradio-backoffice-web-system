import { Tag } from "../../../domain/Tag";
import { PostRepository } from "../../../domain/PostRepository";
import { TagRequest } from "../../TagRequest";

export class TagCreator {
  private repository: PostRepository;

  constructor(repository: PostRepository) {
    this.repository = repository;
  }

  async run(request: TagRequest): Promise<Tag> {
    const tag = Tag.create(request._id, request.tag);

    return await this.repository.saveTag(tag);
  }
}
