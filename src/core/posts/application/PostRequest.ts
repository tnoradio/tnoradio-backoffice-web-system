import PostImage from "../../posts/domain/PostImage";
import { TextBlock } from "../domain/Post";

export type PostRequest = {
  _id: string;
  title: string;
  owner_id: string; // user id of owner
  subtitle: string;
  summary: string;
  image: string;
  images: PostImage[];
  publish_date: Date;
  tags: string[];
  starred: Boolean;
  deleted: Boolean;
  text: string;
  slug: string;
  approved: Boolean;
};
