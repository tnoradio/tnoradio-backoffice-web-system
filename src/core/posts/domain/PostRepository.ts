import { Post } from "./Post";
import { Tag } from "./Tag";

export interface PostRepository {
  save(post: Post): Promise<Post>;
  saveTag(tag: Tag): Promise<Tag>;
  index(): Promise<Post[]>;
  indexTags(): Promise<Tag[]>;
  updatePost(updateValues: any): Promise<Post>;
  destroyPost(pro_id: string): Promise<Post | Error>;
  uploadImage(
    file,
    slug,
    location,
    name,
    type,
    postId,
    imageName
  ): Promise<any>;
  updateImageInDB(
    image,
    name,
    type,
    slug,
    location,
    owner,
    fileId
  ): Promise<any>;
  getPostImage(name, slug);
  getPostDetail(slug);
  getPostsPage(pageSize: number, pageNumber: number): Promise<Post[]>;
  getPostsCount(): Promise<number>;
}
