export class Tag {
  _id: string;
  tag: string;

  constructor(_id: string, tag: string) {
    this._id = _id;
    this.tag = tag;
  }

  static create(_id: string, tag: string) {
    return new Tag(_id, tag);
  }
}
