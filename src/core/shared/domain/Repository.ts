export interface Repository {
  getFile(file_id: string, file_name: string): any;
  fetchFile(fileUrl: string): Promise<any>;
  createFile(payment, file, show_name): Promise<any>;
  deleteFile(file_id: string): Promise<any>;
}
