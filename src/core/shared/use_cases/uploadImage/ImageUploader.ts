export class ImageUploader {
  private repository: any;

  constructor(repository: any) {
    this.repository = repository;
  }

  async run(
    file: any,
    slug: string,
    location: string,
    name: string,
    type: string,
    postId: string,
    imageName: string
  ): Promise<any> {
    return await this.repository.postImage(
      file,
      slug,
      location,
      name,
      type,
      postId,
      imageName
    );
  }
}
