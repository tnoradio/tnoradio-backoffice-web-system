export class ImageToDBUploader {
  private repository: any;

  constructor(repository: any) {
    this.repository = repository;
  }

  async run(
    file: any,
    slug: string,
    location: string,
    name: string,
    type: string,
    postId: string,
    imageName: string
  ): Promise<any> {
    return await this.repository.uploadImage(
      file,
      slug,
      location,
      name,
      type,
      postId,
      imageName
    );
  }
}
