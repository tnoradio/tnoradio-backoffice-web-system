export interface ChatService {
  call();
  chatsList();
  sendMessage(chatId, text);
  signIn(code, phone_code_hash, phone);
  sendAuthCode(phone);
  getConfig();
  getUser(userId);
  getChannel(channelId);
  getMessagesHistory(chatId);
  getMe();
}
