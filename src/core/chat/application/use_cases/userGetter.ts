import { ChatService } from "../../domain/ChatService";

export class UserGetter {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(userId): Promise<any> {
    return await this.service.getUser(userId);
  }
}
