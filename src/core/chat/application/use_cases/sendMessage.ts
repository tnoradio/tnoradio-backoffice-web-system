import { ChatService } from "../../domain/ChatService";

export class MessageSender {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(chatId, text): Promise<any> {
    return await this.service.sendMessage(chatId, text);
  }
}
