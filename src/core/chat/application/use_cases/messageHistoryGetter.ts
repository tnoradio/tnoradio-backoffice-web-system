import { ChatService } from "../../domain/ChatService";

export class MessageHistoryGetter {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(chatId): Promise<any> {
    console.log(chatId);
    return await this.service.getMessagesHistory(chatId);
  }
}
