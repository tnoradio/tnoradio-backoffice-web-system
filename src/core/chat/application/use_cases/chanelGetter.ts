import { ChatService } from "../../domain/ChatService";

export class ChannelGetter {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(channelId): Promise<any> {
    return await this.service.getChannel(channelId);
  }
}
