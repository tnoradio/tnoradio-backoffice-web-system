import { ChatService } from "../../domain/ChatService";

export class MeGetter {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(): Promise<any> {
    try {
      return await this.service.getMe();
    } catch (error) {
      throw error;
    }
  }
}
