import { ChatService } from "../../domain/ChatService";

export class AuthCodeSender {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(phone): Promise<any> {
    return await this.service.sendAuthCode(phone);
  }
}
