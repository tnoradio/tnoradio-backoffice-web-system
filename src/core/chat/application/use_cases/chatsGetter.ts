import { ChatService } from "../../domain/ChatService";

export class ChatsGetter {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(): Promise<any> {
    return await this.service.chatsList();
  }
}
