import { ChatService } from "../../domain/ChatService";

export class ServiceSignIn {
  private service: ChatService;

  constructor(service: ChatService) {
    this.service = service;
  }

  async run(code, phone_code_hash, phone?): Promise<any> {
    return await this.service.signIn(code, phone_code_hash, phone);
  }
}
