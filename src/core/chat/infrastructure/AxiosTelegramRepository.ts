import axios from "axios";

import { telegramConfig } from "../../../config.telegram";
import { ChatService } from "../domain/ChatService";
import { apiUrl } from "../../../config";

const baseURL = `${apiUrl}telegram/`;
export class AxiosTelegramRepository implements ChatService {
  call() {
    throw new Error("Method not implemented.");
  }
  getConfig = async () => {};

  getPassword = async () => {};

  chatsList = async () => {
    console.log("chats list service");
    try {
      const res = await axios.get(baseURL + "getdialogs");
      console.log(res);
      return res.data;
    } catch (error) {
      console.log(error);
      return [];
    }
  };

  sendMessage = async (chatId, text) => {
    try {
      const res = await axios.get(baseURL + "sendmessage", {
        params: { chat_id: chatId, text: text },
      });
      console.log(res);
      return res.data;
    } catch (error) {
      console.log(error);
      return error;
    }
  };

  getUser = async (userId) => {
    console.log(userId);
  };

  getChannel = async (channelId) => {
    console.log(channelId);
  };

  sendAuthCode = async () => {
    try {
      const res = await axios.get(baseURL + "getphonehash");
      return res.data;
    } catch (e) {
      console.log(e);
    }
  };

  signIn = async (code?, phone_code_hash?, phone?) => {
    console.log(code);
    console.log(phone_code_hash);
    try {
      const res = await axios.get(baseURL + "signin", {
        params: { phone_code: code, phone_code_hash: phone_code_hash },
      });
      return res.data;
    } catch (e) {
      console.log(e);
      return "error";
    }
  };

  signUp = ({ phone, phone_code_hash }) => {};

  checkPassword = ({ srp_id, A, M1 }) => {};

  getMe = async () => {
    console.log("GET ME");
    try {
      const getMe = await axios.get(baseURL + "getme");
      console.log(getMe);
      return getMe.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  };

  async getMessagesHistory(chatId) {
    try {
      const res = await axios.get(baseURL + "getmessages", {
        params: { chat_id: chatId },
      });
      console.log(res);
      return res.data;
    } catch (error) {
      console.log(error);
      return [];
    }
  }
}
