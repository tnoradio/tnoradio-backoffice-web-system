/*import { telegramConfig } from "../../../config.telegram";
import { ChatService } from "../domain/ChatService";

const MTProto = require("@mtproto/core/envs/browser");

const mtproto = new MTProto({
  api_id: telegramConfig.api_id,
  api_hash: telegramConfig.api_hash,
});
*/
export class TelegramMTProtoService {
  /*
  getMe() {
    throw new Error("Method not implemented.");
  }
  call = () => {
    mtproto.call("help.getNearestDc").then((result) => {
      console.log("country:", result.country);
    });
  };

  getConfig = async () => {
    const res = await mtproto.call("help.getConfig");
    console.log(res);
    return res;
  };

  getPassword = async () => {
    return await mtproto.call("account.getPassword", { dcId: 1 });
  };

  chatsList = async () => {
    console.log("chats list service");
    try {
      await mtproto.setDefaultDc(1);
      //const user = await this.getUser("inputUserSelf");
      //console.log(user);
      // const pass = await this.getPassword();
      //console.log(pass);
    } catch (e) {
      console.log("Error de set dc", e);
    }
    try {
      const dialogs = await mtproto.call("messages.getDialogs", {
        folder_id: 0,
        hash: 9797004008062923167,
        offset_id: 0,
        exclude_pinned: false,
        offset_date: 0,
        limit: 100,
        offset_peer: {
          _: "inputPeerSelf",
        },
      });
      console.log(dialogs);

      return dialogs;
    } catch (e) {
      console.log("error de all chats", e);
      return [];
    }
  };

  sendMessage = () => {
    mtproto.call(
      "messages.sendMessage",
      {
        clear_draft: true,

        peer: {
          _: "inputPeerSelf",
        },
        message: "Hello @mtproto_core",
        entities: [
          {
            _: "messageEntityBold",
            offset: 6,
            length: 13,
          },
        ],

        random_id:
          Math.ceil(Math.random() * 0xffffff) +
          Math.ceil(Math.random() * 0xffffff),
      },
      { dcId: 1 }
    );
  };

  getUser = async (userId) => {
    console.log(userId);
    try {
      const user = await mtproto.call("users.getFullUser", {
        id: userId,
      });
      console.log(user);

      return user;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  getChannel = async (channelId) => {
    console.log(channelId);
    try {
      const channel = await mtproto.call("channels.getFullChannel", {
        inputChannel: {
          channel_id: channelId,
        },
      });
      console.log(channel);

      return channel;
    } catch (error) {
      return null;
    }
  };

  sendAuthCode = async (phone) => {
    const res = await mtproto.call(
      "auth.sendCode",
      {
        phone_number: phone,
        settings: {
          _: "codeSettings",
        },
      },
      { dcId: 1 }
    );
    console.log(res);
    return res;
  };

  signIn = async (code?, phone?, phone_code_hash?) => {
    console.log(phone);
    console.log(code);
    console.log(phone_code_hash);
    try {
      const result = await mtproto.call(
        "auth.signIn",
        {
          phone_code: 70127,
          phone_number: "+584123964576",
          phone_code_hash: "a413e922606615e966",
        },
        { dcId: 1 }
      );
      console.log("paso por result");
      console.log(result);
      return result;
    } catch (error) {
      const { error_code, error_message } = await error;
      console.log(`error de sign in:`, error);
      return error_message;
    }
  };

  signUp = ({ phone, phone_code_hash }) => {
    return mtproto.call("auth.signUp", {
      phone_number: phone,
      phone_code_hash: phone_code_hash,
      first_name: "MTProto",
      last_name: "Core",
    });
  };

  checkPassword = ({ srp_id, A, M1 }) => {
    return mtproto.call("auth.checkPassword", {
      password: {
        _: "inputCheckPasswordSRP",
        srp_id,
        A,
        M1,
      },
    });
  };

  async getMessagesHistory(chatId) {
    const inputPeer = { _: "peerUser", user_id: 777000 };

    const LIMIT_COUNT = 10;
    const allMessages = [];

    try {
      const chats = await mtproto.call("messages.getFullChat", {
        chat_id: 191,
        peer: 777000,
      });
      console.log(chats);

      /*
      
      
      const firstHistoryResult = await mtproto.call("messages.getHistory", {
        peer: {
          _: "inputPeerChannel",
          channel_id: 188,
          access_hash: chat.access_hash,
        },
        max_id: -offsetId,
        offset: -full.length,
        limit,
      });

      const historyCount = firstHistoryResult.count;
      console.log(firstHistoryResult);
      for (let offset = 0; offset < historyCount; offset += LIMIT_COUNT) {
      const history = await mtproto.call("messages.getHistory", {
        peer: inputPeer,
        add_offset: offset,
        limit: LIMIT_COUNT,
      });

      allMessages.push(...history.messages);
    }

      //console.log("allMessages:", allMessages);
      return chats;
    } catch (error) {
      console.log(error);
    }
  }*/
}
