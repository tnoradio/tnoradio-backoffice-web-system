import { ShowAdvertiser } from "../../../domain/ShowAdvertiser";
import { ShowRepository } from "../../../domain/ShowRepository";

export class ShowAdvertiserCreator {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(image, name, adName, showId, website): Promise<ShowAdvertiser> {
    return await this.repository.saveShowAd(
      image,
      name,
      adName,
      showId,
      website
    );
  }
}
