import { ShowGenre } from "../../../domain/ShowGenre";
import { ShowRepository } from "../../../domain/ShowRepository";

export class ShowGenresGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(): Promise<ShowGenre[]> {
    return await this.repository.getShowGenres();
  }
}
