import { ShowRepository } from "../../../domain/ShowRepository";

export class ImageInDBModifier {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(
    image: any,
    name: string,
    type: string,
    slug: string,
    location: string,
    owner: string
  ): Promise<any> {
    return await this.repository.updateImageInDB(
      image,
      name,
      type,
      slug,
      location,
      owner
    );
  }
}
