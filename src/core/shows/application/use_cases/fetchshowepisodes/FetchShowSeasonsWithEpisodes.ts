import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowSeason } from "../../../domain/ShowSeason";

export class ShowSeasonsWithEpisodesGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(showId: number): Promise<ShowSeason[]> {
    return await this.repository.fetchShowSeasonsWithEpisodes(showId);
  }
}
