import { OnDemandRepository } from "../../../domain/OnDemandRepository";

export class ShowFileNamesGetter {
  private repository: OnDemandRepository;

  constructor(repository: OnDemandRepository) {
    this.repository = repository;
  }

  async run(showSlug: string): Promise<any[]> {
    return await this.repository.getShowFileNames(showSlug);
  }
}
