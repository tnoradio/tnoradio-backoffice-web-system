import { ShowEpisode } from "../../../domain/ShowEpisode";
import { ShowRepository } from "../../../domain/ShowRepository";

export class UpdateShowEpisode {
  constructor(private repository: ShowRepository) {}

  async execute(episode: ShowEpisode): Promise<ShowEpisode> {
    return await this.repository.updateShowEpisode(episode);
  }
}
