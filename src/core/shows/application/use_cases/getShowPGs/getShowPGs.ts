import { ShowGenre } from "../../../domain/ShowGenre";
import { ShowPGClassification } from "../../../domain/ShowPGClassification";
import { ShowRepository } from "../../../domain/ShowRepository";

export class ShowPGsGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(): Promise<ShowPGClassification[]> {
    return await this.repository.getShowPGs();
  }
}
