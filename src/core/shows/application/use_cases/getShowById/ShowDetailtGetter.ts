import { ShowRepository } from "../../../domain/ShowRepository";
import { Show } from "../../../domain/Show";

export class ShowByIdGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(_id: number): Promise<Show> {
    return await this.repository.getShowById(_id);
  }
}
