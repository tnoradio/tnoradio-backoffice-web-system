import { ShowEpisode } from "../../../domain/ShowEpisode";
import { ShowRepository } from "../../../domain/ShowRepository";

export class AddTagToEpisode {
  constructor(private readonly showRepository: ShowRepository) {}

  async execute(episodeId: number, tag: string): Promise<ShowEpisode> {
    return await this.showRepository.addTagToEpisode(tag, episodeId);
  }
}
