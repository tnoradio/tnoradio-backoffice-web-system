import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowType } from "../../../domain/ShowType";

export class ShowTypesGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(): Promise<ShowType[]> {
    return await this.repository.getShowTypes();
  }
}
