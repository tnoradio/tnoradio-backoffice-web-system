import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowEpisodeTag } from "../../../domain/ShowEpisodeTag";

export class GetEpisodeTags {
  constructor(private readonly showRepository: ShowRepository) {}

  async execute(): Promise<ShowEpisodeTag[]> {
    return this.showRepository.getEpisodeTags();
  }
}
