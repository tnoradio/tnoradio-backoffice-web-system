import { ShowRepository } from "../../../domain/ShowRepository";
//import { DeleteShowRequest } from './DeleteShowRequest'
import { ShowAdvertiser } from "../../../domain/ShowAdvertiser";

export class ShowAdvertiserDeleter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(id: string): Promise<ShowAdvertiser | Error> {
    //ALGO EXTRAÑO SUCEDE, EL REQUEST RETORNA DIRECTO EL ID SIN
    //ENTRAR AL CAMPO
    try {
      console.log(id);

      return await this.repository.destroyShowAdvertiser(id);
    } catch (err) {
      console.log(err);
      return null;
    }
  }
}
