import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowEpisodeTag } from "../../../domain/ShowEpisodeTag";

export class AddEpisodeTag {
  constructor(private readonly showRepository: ShowRepository) {}

  async execute(tag: ShowEpisodeTag): Promise<ShowEpisodeTag> {
    return await this.showRepository.addEpisodeTag(tag);
  }
}
