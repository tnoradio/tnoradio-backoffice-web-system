import { Show } from "../../../domain/Show";
import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowRequest } from "../ShowRequest";

export class ShowCreator {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(request: ShowRequest): Promise<Show> {
    const show = {
      id: request.id,
      name: request.name,
      genre: request.genre,
      synopsis: request.synopsis,
      type: request.type,
      showSchedules: request.showSchedules,
      email: request.email,
      isEnabled: request.isEnabled,
      isActive: request.isActive,
      pgClassification: request.pgClassification,
      initDate: request.initDate,
      producers: request.producers,
      socials: request.socials,
      hosts: request.hosts,
      advertisers: request.advertisers,
      images: request.images,
      showSlug: request.showSlug,
      colors: request.colors,
      isDeleted: request.isDeleted,
      age_range: request.age_range,
      target: request.target,
      target_gender: request.target_gender,
      seasons: request.seasons,
      genres: request.genres,
      subgenres: request.subgenres,
    };

    return await this.repository.save(show);
  }
}
