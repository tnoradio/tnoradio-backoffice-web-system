import { ShowRepository } from "../../../domain/ShowRepository";
import { Show } from "../../../domain/Show";

export class RecommendedShowsGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(
    genders: string[],
    genres: string[],
    target: string[],
    age_ranges: string[]
  ): Promise<Show[]> {
    return await this.repository.fetchRecommendedShows(
      genders,
      genres,
      target,
      age_ranges
    );
  }
}
