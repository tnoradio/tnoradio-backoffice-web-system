import { ShowAdvertiser } from "../../../domain/ShowAdvertiser";
import { ShowRepository } from "../../../domain/ShowRepository";

export class ShowAdvertisersGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(showId: string): Promise<ShowAdvertiser[]> {
    return await this.repository.getShowAdvertisers(showId);
  }
}
