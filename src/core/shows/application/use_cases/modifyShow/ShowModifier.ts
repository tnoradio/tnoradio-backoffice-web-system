import { Show } from "../../../domain/Show";
import { ShowRepository } from "../../../domain/ShowRepository";
import { UpdateShowRequest } from "./UpdateShowRequest";

export class ShowModifier {
  private repository: ShowRepository;
  request: UpdateShowRequest;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(request: UpdateShowRequest): Promise<Show> {
    const showUpdate = {
      id: request.id,
      field: request.field,
      value: request.value,
    };

    return await this.repository.updateShow(showUpdate);
  }
}
