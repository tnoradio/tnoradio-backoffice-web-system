export type UpdateShowRequest = {
  id: number;
  field: string;
  value: any;
};
