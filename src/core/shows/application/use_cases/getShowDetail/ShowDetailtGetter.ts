import { ShowRepository } from "../../../domain/ShowRepository";
import { Show } from "../../../domain/Show";

export class ShowDetailGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(slug: string): Promise<Show> {
    return await this.repository.getShowDetail(slug);
  }
}
