import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowImage } from "../../../domain/ShowImage";

export class ShowImageFromDBGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(name: string, slug: string): Promise<ShowImage> {
    return await this.repository.getShowImage(name, slug);
  }
}
