import { ShowAdvertiser } from "../../domain/ShowAdvertiser";
import { ShowColor } from "../../domain/ShowColor";
import { ShowGenre } from "../../domain/ShowGenre";
import { ShowHost } from "../../domain/ShowHost";
import { ShowImage } from "../../domain/ShowImage";
import { ShowProducer } from "../../domain/ShowProducer";
import { ShowSchedule } from "../../domain/ShowSchedule";
import { ShowSeason } from "../../domain/ShowSeason";
import { ShowSocialMedia } from "../../domain/ShowSocialMedia";
import { ShowSubGenre } from "../../domain/ShowSubGenre";

export type ShowRequest = {
  id: number;
  name: string;
  genre: string;
  synopsis: string;
  type: string;
  showSchedules: ShowSchedule[];
  email: string;
  isEnabled: Boolean;
  isActive: Boolean;
  pgClassification: string;
  initDate: Date;
  producers: ShowProducer[];
  socials: ShowSocialMedia[];
  hosts: ShowHost[];
  advertisers: ShowAdvertiser[];
  images: ShowImage[];
  colors: ShowColor[];
  isDeleted: Boolean;
  showSlug: string;
  age_range: string[];
  target: string[];
  target_gender: string[];
  seasons: ShowSeason[];
  genres: ShowGenre[];
  subgenres: ShowSubGenre[];
};
