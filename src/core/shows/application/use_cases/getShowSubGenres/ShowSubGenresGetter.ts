import { ShowSubGenre } from "../../../domain/ShowSubGenre";
import { ShowRepository } from "../../../domain/ShowRepository";

export class ShowSubGenresGetter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(): Promise<ShowSubGenre[]> {
    return await this.repository.getShowSubGenres();
  }
}
