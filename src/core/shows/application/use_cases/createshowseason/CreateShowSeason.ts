import { ShowRepository } from "../../../domain/ShowRepository";
import { ShowSeason } from "../../../domain/ShowSeason";

export class CreateShowSeason {
  constructor(private readonly showRepository: ShowRepository) {}

  async execute(season: ShowSeason): Promise<ShowSeason> {
    return await this.showRepository.addSeason(season);
  }
}
