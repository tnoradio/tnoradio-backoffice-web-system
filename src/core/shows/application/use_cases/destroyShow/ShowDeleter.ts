import { ShowRepository } from "../../../domain/ShowRepository";
//import { DeleteShowRequest } from './DeleteShowRequest'
import { Show } from "../../../domain/Show";

export class ShowDeleter {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(id: string): Promise<Show | Error> {
    //ALGO EXTRAÑO SUCEDE, EL REQUEST RETORNA DIRECTO EL ID SIN
    //ENTRAR AL CAMPO
    try {
      console.log(id);

      return await this.repository.destroyShow(id);
    } catch (err) {
      console.log(err);
      return null;
    }
  }
}
