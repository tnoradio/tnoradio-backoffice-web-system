import { ShowRepository } from "../../../domain/ShowRepository";

export class ImageUploader {
  private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(image, slug, location, name, showId, imageName): Promise<Boolean> {
    return await this.repository.uploadImage(
      image,
      slug,
      location,
      name,
      showId,
      imageName
    );
  }
}
