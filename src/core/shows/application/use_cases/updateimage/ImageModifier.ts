import { ImageRepository } from "../../../../images/domain/ImageRepository";

export class ImageModifier {
  private repository: ImageRepository;

  constructor(repository: ImageRepository) {
    this.repository = repository;
  }

  async run(
    imageFile: any, //Buffer
    name: string,
    location: string, //carpeta del show
    type: string, //el tipo de imagen
    category: string //categoria de la imagen
  ): Promise<any> {
    return await this.repository.updateImage(
      imageFile,
      category,
      type,
      name,
      location
    );
  }
}
