import { ShowAdvertiser } from "../../../domain/ShowAdvertiser";
import { ShowRepository } from "../../../domain/ShowRepository";
import { UpdateShowAdvertiserRequest } from "./UpdateShowAdvertiserRequest";

export class ShowAdvertiserModifier {
  private repository: ShowRepository;
  request: UpdateShowAdvertiserRequest;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(request: UpdateShowAdvertiserRequest): Promise<ShowAdvertiser> {
    const showAdvertiserUpdate = {
      id: request.id,
      field: request.field,
      value: request.value,
    };

    return await this.repository.updateShowAdvertiser(showAdvertiserUpdate);
  }
}
