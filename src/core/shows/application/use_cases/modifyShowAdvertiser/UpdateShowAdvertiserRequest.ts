export type UpdateShowAdvertiserRequest = {
  id: string;
  field: string;
  value: any;
};
