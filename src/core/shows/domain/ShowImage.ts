export class ShowImage {
  id: string;
  imageName: string;
  imageUrl: string;
  file: {
    data: Buffer;
    contentType: string;
  };
  owner: string;

  constructor(
    id: string,
    imageName: string,
    imageUrl: string,
    file: { data: Buffer; contentType: string },
    owner: string
  ) {
    this.id = id;
    this.imageName = imageName;
    this.imageUrl = imageUrl;
    this.file = file;
    this.owner = owner;
  }

  static create(
    id: string,
    imageName: string,
    imageUrl: string,
    file: { data: Buffer; contentType: string },
    owner: string
  ) {
    return new ShowImage(id, imageName, imageUrl, file, owner);
  }
}
