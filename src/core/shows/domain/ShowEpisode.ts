import { ShowEpisodeTag } from "./ShowEpisodeTag";

export class ShowEpisode {
  title: string | "";
  description: string | "";
  url: string | "";
  miniature: string | "";
  episode_number: number | 0;
  id?: number;
  tags: ShowEpisodeTag[] | [];
  season: number | 0;
}
