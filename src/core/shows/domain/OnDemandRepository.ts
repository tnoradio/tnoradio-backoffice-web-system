export interface OnDemandRepository {
  getShowFileNames(showSlug: string): Promise<any[]>;
}
