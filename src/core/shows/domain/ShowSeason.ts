import { ShowEpisode } from "./ShowEpisode";

export class ShowSeason {
  id?: number;
  season_number: number | 0;
  showId: number | 0;
  episodes: ShowEpisode[] | [];
}
