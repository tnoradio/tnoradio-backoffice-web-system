import { Show } from "./Show";
import { ShowAdvertiser } from "./ShowAdvertiser";
import { ShowEpisode } from "./ShowEpisode";
import { ShowEpisodeTag } from "./ShowEpisodeTag";
import { ShowGenre } from "./ShowGenre";
import { ShowPGClassification } from "./ShowPGClassification";
import { ShowSeason } from "./ShowSeason";
import { ShowSubGenre } from "./ShowSubGenre";
import { ShowType } from "./ShowType";

export interface ShowRepository {
  save(show: Show): Promise<Show>;
  index(): Promise<Show[]>;
  updateShow(updateValues: any): Promise<Show>;
  updateShowAdvertiser(updateValues: any): Promise<ShowAdvertiser>;
  destroyShow(pro_id: string): Promise<Show | Error>;
  destroyShowAdvertiser(pro_id: string): Promise<ShowAdvertiser | Error>;
  getShowGenres(): Promise<ShowGenre[]>;
  getShowSubGenres(): Promise<ShowSubGenre[]>;
  getShowAdvertisers(showId: string): Promise<ShowAdvertiser[]>;
  getShowPGs(): Promise<ShowPGClassification[]>;
  getShowTypes(): Promise<ShowType[]>;
  uploadImage(image, slug, location, name, showId, imageName): Promise<any>;
  saveShowAd(image, name, adName, showId, website): Promise<ShowAdvertiser>;
  updateImageInDB(
    image: any,
    name: string,
    type: string,
    slug: string,
    location: string,
    owner: string
  ): Promise<any>;
  getShowImage(name: string, slug: string): Promise<any>;
  getShowDetail(slug: string): Promise<Show>;
  getShowById(_id: number): Promise<Show>;
  updateImage(
    image: any,
    name: string,
    slug: string,
    location: string,
    showId: number
  ): Promise<any>;
  fetchRecommendedShows(
    genders: string[],
    genres: string[],
    target: string[],
    age_ranges: string[]
  ): Promise<Show[]>;
  fetchShowsByGenre(genre: string): Promise<Show[]>;
  fetchShowsBySubGenre(subGenre: string): Promise<Show[]>;
  fetchShowSeasonsWithEpisodes(showId: number): Promise<ShowSeason[]>;
  updateShowEpisode(episode: ShowEpisode): Promise<ShowEpisode>;
  addTagToEpisode(tag: string, episodeId: number): Promise<ShowEpisode>;
  getEpisodeTags(): Promise<ShowEpisodeTag[]>;
  addSeason(season: ShowSeason): Promise<ShowSeason>;
  addEpisodeTag(tag: ShowEpisodeTag): Promise<ShowEpisodeTag>;
}
