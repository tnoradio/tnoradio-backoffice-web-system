export class ShowPGClassification {
  id: number;
  showPGClassification: string;

  constructor(id: number, showPGClassification: string) {
    this.id = id;
    this.showPGClassification = showPGClassification;
  }

  static create(id: number, showPGClassification: string) {
    return new ShowPGClassification(id, showPGClassification);
  }

  static createFromShowPGClassification(showshowPGClassification: any) {
    return new ShowPGClassification(
      showshowPGClassification.id,
      showshowPGClassification.showPGClassification
    );
  }
}
