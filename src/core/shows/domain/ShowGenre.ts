export class ShowGenre {
  id: Number;
  genre: string;

  constructor(id: Number, genre: string) {
    this.id = id;
    this.genre = genre;
  }

  static create(id: Number, genre: string) {
    return new ShowGenre(id, genre);
  }

  static createFromShowGenre(showGenre: any) {
    return new ShowGenre(showGenre.id, showGenre.genre);
  }
}
