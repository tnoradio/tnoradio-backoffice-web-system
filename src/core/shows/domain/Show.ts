import { ShowAdvertiser } from "./ShowAdvertiser";
import { ShowColor } from "./ShowColor";
import { ShowGenre } from "./ShowGenre";
import { ShowHost } from "./ShowHost";
import { ShowImage } from "./ShowImage";
import { ShowProducer } from "./ShowProducer";
import { ShowSchedule } from "./ShowSchedule";
import { ShowSeason } from "./ShowSeason";
import { ShowSocialMedia } from "./ShowSocialMedia";
import { ShowSubGenre } from "./ShowSubGenre";

export class Show {
  id?: number;
  name: string | "";
  genre: string | "";
  synopsis: string | "";
  type: string | "";
  showSchedules: ShowSchedule[] | [];
  email: string | "";
  isEnabled: Boolean | false;
  isActive: Boolean | false;
  pgClassification: string | "";
  initDate: Date | "";
  producers: ShowProducer[] | [];
  showSlug: string | "";
  socials: ShowSocialMedia[] | [];
  hosts: ShowHost[] | [];
  advertisers: ShowAdvertiser[] | [];
  images: ShowImage[] | [];
  colors: ShowColor[] | [];
  isDeleted: Boolean | false;
  age_range: string[] | [];
  target: string[] | [];
  target_gender: string[] | [];
  seasons: ShowSeason[] | [];
  genres: ShowGenre[] | [];
  subgenres: ShowSubGenre[] | [];
}
