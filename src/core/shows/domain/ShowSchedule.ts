export class ShowSchedule {
  id: Number;
  startTime: Date;
  endTime: Date;
  weekDay: string;

  constructor(id: Number, startTime: Date, endTime: Date, weekDay: string) {
    this.id = id;
    this.startTime = startTime;
    this.endTime = endTime;
    this.weekDay = weekDay;
  }

  static create(id: Number, startTime: Date, endTime: Date, weekDay: string) {
    return new ShowSchedule(id, startTime, endTime, weekDay);
  }

  static createFromShowSchedule(show_schedule: any) {
    return new ShowSchedule(
      show_schedule.id,
      show_schedule.startTime,
      show_schedule.endTime,
      show_schedule.weekDay
    );
  }
}
