import { User } from "../../users/domain/User";

export class ShowProducer {
  id: Number;
  userId: Number;
  user: User;

  constructor(id: Number, user: User) {
    this.id = id;
    this.user = user;
  }

  static create(id: Number, user: User) {
    return new ShowProducer(id, user);
  }

  static createFromShowProducer(show: any) {
    return new ShowProducer(show.id, show.user);
  }
}
