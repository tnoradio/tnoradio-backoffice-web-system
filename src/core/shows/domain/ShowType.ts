export class ShowType {
  id: number;
  showType: string;

  constructor(id: number, showType: string) {
    this.id = id;
    this.showType = showType;
  }

  static create(id: number, showType: string) {
    return new ShowType(id, showType);
  }

  static createFromShowType(showshowType: any) {
    return new ShowType(showshowType.id, showshowType.showType);
  }
}
