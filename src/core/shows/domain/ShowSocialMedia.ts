export class ShowSocialMedia {
  id: Number;
  showId: Number;
  name: string;
  url: string;
  userName: string;

  constructor(
    id: Number,
    showId: Number,
    name: string,
    url: string,
    userName: string
  ) {
    (this.id = id), (this.showId = showId);
    this.name = name;
    this.url = url;
    this.userName = userName;
  }

  static create(
    id: Number,
    showId: Number,
    name: string,
    url: string,
    userName: string
  ) {
    return new ShowSocialMedia(id, showId, name, url, userName);
  }

  static createFromShowSocialMedia(showScocial: any) {
    return new ShowSocialMedia(
      showScocial.id,
      showScocial.showId,
      showScocial.name,
      showScocial.url,
      showScocial.userName
    );
  }
}
