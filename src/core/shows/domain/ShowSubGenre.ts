export class ShowSubGenre {
  id: Number;
  subgenre: string;

  constructor(id: Number, genre: string) {
    this.id = id;
    this.subgenre = genre;
  }

  static create(id: Number, subgenre: string) {
    return new ShowSubGenre(id, subgenre);
  }

  static createFromShowSubGenre(showGenre: any) {
    return new ShowSubGenre(showGenre.id, showGenre.subgenre);
  }
}
