export class ShowColor {
  id: Number;
  color: string;
  number: Number;

  constructor(id: Number, color: string, number: Number) {
    this.id = id;
    this.color = color;
    this.number = number;
  }

  static create(id: Number, color: string, number: Number) {
    return new ShowColor(id, color, number);
  }

  static createFromShowColor(show: any) {
    return new ShowColor(show.id, show.color, show.number);
  }
}
