import { User } from "../../users/domain/User";

export class ShowHost {
  id: Number;
  userId: Number;
  user: User;

  constructor(id: Number, user: User) {
    this.id = id;
    this.user = user;
  }

  static create(id: Number, user: User) {
    return new ShowHost(id, user);
  }

  static createFromShowHost(show: any) {
    return new ShowHost(show.id, show.user);
  }
}
