export class ShowAdvertiser {
  id: Number;
  name: string;
  image: Buffer;
  website: string;

  constructor(id: Number, name: string, image: Buffer, website: string) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.website = website;
  }

  static create(id: Number, name: string, image: Buffer, website: string) {
    return new ShowAdvertiser(id, name, image, website);
  }
}
