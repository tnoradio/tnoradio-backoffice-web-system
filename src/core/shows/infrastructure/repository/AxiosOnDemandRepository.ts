import axios from "axios";
import { OnDemandRepository } from "../../domain/OnDemandRepository";

const baseUrl = process.env.API_CDN_URL || "https://cdn.tnoradio.com/";

export class AxiosOnDemandRepository implements OnDemandRepository {
  async getShowFileNames(showSlug: String): Promise<any[]> {
    try {
      const response = await axios.get(`${baseUrl}get_shows/${showSlug}`);
      return response.data;
    } catch (error) {
      console.log("AxiosOnDemandRepository", error);
      throw error;
    }
  }
}
