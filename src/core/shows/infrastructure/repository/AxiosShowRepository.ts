/**
 * Implements User repository interface with axios
 * promise based HTTP client.
 */

import { Show } from "../../domain/Show";
import { ShowRepository } from "../../domain/ShowRepository";
import axios from "axios";
import { ShowGenre } from "../../domain/ShowGenre";
import { ShowPGClassification } from "../../domain/ShowPGClassification";
import { ShowType } from "../../domain/ShowType";
import { ShowAdvertiser } from "../../domain/ShowAdvertiser";
import { showsUrl } from "../../../../config";
import { ShowSubGenre } from "../../domain/ShowSubGenre";
import { ShowSeason } from "../../domain/ShowSeason";
import { ShowEpisode } from "../../domain/ShowEpisode";
import { ShowEpisodeTag } from "../../domain/ShowEpisodeTag";

const baseURL = showsUrl;

export class AxiosShowRepository implements ShowRepository {
  async fetchShowSeasonsWithEpisodes(showId: number): Promise<ShowSeason[]> {
    try {
      const response = await axios.get(`${baseURL}/seasons/${showId}`);
      if (!response.data) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.data;
    } catch (error) {
      console.log("AxiosShowRepository", error);
      throw error;
    }
  }

  async getShowSubGenres(): Promise<ShowSubGenre[]> {
    try {
      const response = await axios.get(`${baseURL}/subgenres`);
      return response.data;
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  async fetchShowsByGenre(genre: string): Promise<Show[]> {
    try {
      const response = await axios.get(`${baseURL}/genres/${genre}`);
      return response.data;
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  async fetchShowsBySubGenre(subGenre: string): Promise<Show[]> {
    try {
      const response = await axios.get(`${baseURL}/subgenres/${subGenre}`);
      return response.data;
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  async fetchRecommendedShows(
    genders: string[],
    genres: string[],
    target: string[],
    age_ranges: string[]
  ): Promise<Show[]> {
    try {
      const buyerPerson = {
        genders: genders,
        genres: genres,
        target: target,
        age_ranges: age_ranges,
      };
      const response = await axios.get(`${baseURL}/recomendedshows`, {
        params: {
          genders: genders,
          genres: genres,
          target: target,
          age_ranges: age_ranges,
        },
      });
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }
  public async index(): Promise<Show[]> {
    try {
      const response = await axios.get(baseURL);
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getShowGenres(): Promise<ShowGenre[]> {
    try {
      const response = await axios.get(`${baseURL}/genres`);
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getShowAdvertisers(showId: string): Promise<ShowAdvertiser[]> {
    try {
      const response = await axios.get(`${baseURL}/advertisers/${showId}`);
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getShowPGs(): Promise<ShowPGClassification[]> {
    try {
      const response = await axios.get(`${baseURL}/pgs`);
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getShowTypes(): Promise<ShowType[]> {
    try {
      const response = await axios.get(`${baseURL}/types`);
      //
      return response.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async save(show: Show): Promise<Show> {
    try {
      const response = await axios.post(`${baseURL}/save`, show);

      if (response !== undefined && response.data !== undefined) {
        return response.data;
      } else {
        return Promise.resolve(null);
      }
    } catch (error) {
      if (error.response) {
        console.error(error.response);
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async saveShowAd(
    image,
    name,
    adName,
    showId,
    website
  ): Promise<ShowAdvertiser> {
    const formData = new FormData();
    formData.set("image", image, name);
    formData.set("name", adName);
    formData.set("showId", showId);
    formData.set("website", website);

    try {
      const response = await axios.post(`${baseURL}/saveShowAd`, formData);
      if (response !== undefined && response.data !== undefined) {
        return response.data;
      } else {
        return Promise.resolve(null);
      }
    } catch (error) {
      if (error.response) {
        console.error(error.response);
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async updateShow(updateValues: any): Promise<Show> {
    try {
      const result = await axios.put(`${baseURL}/update`, updateValues);
      return Promise.resolve(result.data);
    } catch (error) {
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async destroyShow(id: string): Promise<Show> {
    try {
      const response = await axios.delete(`${baseURL}/destroy/${id}`);
      return response.data;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async updateShowAdvertiser(
    updateValues: any
  ): Promise<ShowAdvertiser> {
    try {
      const result = await axios.put(
        `${baseURL}/update/advertiser`,
        updateValues
      );
      return Promise.resolve(result.data);
    } catch (error) {
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async destroyShowAdvertiser(id: string): Promise<ShowAdvertiser> {
    try {
      const response = await axios.delete(
        `${baseURL}/advertiser/destroy/${id}`
      );
      return response.data;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async uploadImage(
    image,
    slug,
    location,
    name,
    showId,
    imageName
  ): Promise<any> {
    const formData = new FormData();
    formData.set("image", image, `${name}_tnoradio_${slug}`);
    formData.set("slug", slug);
    formData.set("location", location);
    formData.set("name", imageName);
    formData.set("showId", showId);
    try {
      const response = await axios.post(`${baseURL}/upload`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return response;
    } catch (e) {
      console.error(e);
      return e;
    }
  }

  public async updateImageInDB(
    image,
    name,
    type,
    slug,
    location,
    imageId
  ): Promise<any> {
    const formData = new FormData();
    formData.set("image", image, image.name);
    formData.set("name", name);
    formData.set("type", type);
    formData.set("slug", slug);
    formData.set("location", location);
    formData.set("imageId", imageId);
    try {
      const response = await axios.patch(
        `${baseURL}/updateimageindb/`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return response.data;
    } catch (e) {
      console.error(e);
      return e;
    }
  }

  /**
   * Uploads image to repository and deletes it from db
   * @param image
   * @param name
   * @param slug
   * @param location
   * @param showId
   * @returns
   */
  public async updateImage(image, name, slug, location, showId): Promise<any> {
    const formData = new FormData();
    formData.set("image", image, `${name}_tnoradio_${slug}`);
    formData.set("name", name);
    formData.set("slug", slug);
    formData.set("location", location);
    formData.set("showId", showId);
    try {
      const response = await axios.post(`${baseURL}/upload`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return response.data;
    } catch (e) {
      console.error(e);
      return e;
    }
  }

  public async getShowImage(name, slug) {
    let res;
    try {
      res = await axios.get(`${baseURL}/imagefromdb/${name}/${slug}`);
      return res.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async getShowDetail(slug) {
    let res;
    try {
      res = await axios.get(`${baseURL}/details/${slug}/`);
      return res.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }
  public async getShowById(_id: number) {
    try {
      const res = await axios.get(`${baseURL}/id/${_id}`);
      console.log("byid ", res.data);
      return res.data;
    } catch (error) {
      console.error(error);
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }

  public async updateShowEpisode(episode: ShowEpisode): Promise<ShowEpisode> {
    try {
      const response = await axios.patch(`${baseURL}/episode`, episode);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  public async addTagToEpisode(
    tag: string,
    episodeId: number
  ): Promise<ShowEpisode> {
    try {
      const response = await axios.post(`${baseURL}/episode/tag`, {
        tag: tag,
        episodeId: episodeId,
      });
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  public async getEpisodeTags(): Promise<ShowEpisodeTag[]> {
    try {
      const response = await axios.get(`${baseURL}/episode/tags`);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  public async addSeason(season: ShowSeason): Promise<ShowSeason> {
    try {
      const response = await axios.post(`${baseURL}/season`, season);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  public async addEpisodeTag(tag: ShowEpisodeTag): Promise<ShowEpisodeTag> {
    try {
      const response = await axios.post(`${baseURL}/episode/tags`, tag);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}
