import React, { useState } from "react";
import { Col, Row, FormGroup, Label, CustomInput } from "reactstrap";

const genderOptions = ["Femenino", "Masculino", "Otro"];

const GenderSelector = ({ updateStore, getStore }) => {
  const [selectedGenders, setSelectedGenders] = useState(
    getStore().genders || []
  );

  const handleGenderChange = (gender) => {
    setSelectedGenders((prevGenders) => {
      const newGenders = [...prevGenders];
      const index = newGenders.indexOf(gender);
      if (index !== -1) {
        newGenders.splice(index, 1);
      } else {
        newGenders.push(gender);
      }
      updateStore({ genders: newGenders });
      return newGenders;
    });
  };

  return (
    <FormGroup>
      <Label for="gender">Géneros</Label>
      <Row>
        <Col xs={6}>
          {genderOptions.slice(0, 2).map((option) => (
            <FormGroup check className="px-0" key={option}>
              <CustomInput
                type="checkbox"
                id={option}
                label={option}
                value={selectedGenders}
                checked={selectedGenders.includes(option)}
                onChange={() => handleGenderChange(option)}
              />
            </FormGroup>
          ))}
        </Col>
        <Col xs={6}>
          {genderOptions.slice(2).map((option) => (
            <FormGroup check className="px-0" key={option}>
              <CustomInput
                type="checkbox"
                id={option}
                label={option}
                value={selectedGenders}
                checked={selectedGenders.includes(option)}
                onChange={() => handleGenderChange(option)}
              />
            </FormGroup>
          ))}
        </Col>
      </Row>
    </FormGroup>
  );
};

export default GenderSelector;
