import { connect } from "react-redux";
import { setEditShowFlag, updateShow } from "../../redux/actions/shows";
import ShowDetails from "../../components/shows/showDetails";

const mapStateToProps = (state) => {
  return {
    selectedShow: state.showApp.shows.find(
      (show) => show.id === state.showApp.showDetails
    ),
    editShowFlag: state.showApp.editShow,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditShowFlag()),
  onChange: (id, field, value) => dispatch(updateShow(id, field, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ShowDetails);
