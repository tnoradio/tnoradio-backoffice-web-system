import React, { Fragment, useEffect, useState } from "react";
import { Card, CardBody, CardTitle, Row, Col } from "reactstrap";
import StepZilla from "./StepZilla";
import Step1 from "./addShow-Step1";
import Step2 from "./addShow-Step2";
import Step3 from "./addShow-Step3";
import Step4 from "./addShow-Step4";
import Step5 from "./addShow-Step5";
import Step5AndHalf from "./addShow-Step5AndHalf";
import Step6 from "./addShow-Step6";
import Step7 from "./addShow-Step7";
import "../../../assets/scss/views/form/wizard.scss";
import { connect, useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchShowTypes,
  fetchShowGenres,
  fetchShowPGClass,
} from "../../../redux/actions/shows";
import { fetchUsers } from "../../../redux/actions/users";
import fetchShowSubGenres from "../../../redux/actions/shows/fetchShowSubGenres";

const AddShowWizard = () => {
  const dispatch = useDispatch();
  const [step, setStep] = useState(0);
  const { showGenres, showPGs, showTypes, users, showSubGenres } = useSelector(
    (state) => ({
      showGenres: state.showApp.showGenres,
      showSubGenres: state.showApp.showSubGenres,
      showPGs: state.showApp.showPGs,
      showTypes: state.showApp.showTypes,
      users: state.userApp.users,
    })
  );

  const state = useSelector((state) => state);

  const [initStore, setInitStoreUpdated] = useState({
    name: "",
    email: "",
    genre: "",
    socials: [],
    producers: [],
    images: [],
    hosts: [],
    advertisers: [],
    type: "",
    pg: "",
    showSchedules: [],
    showSlug: "",
    synopsis: "",
    isEnabled: false,
    colors: [],
    imageFiles: [],
    isDeleted: false,
    age_range: [],
    target: [],
    genders: [],
    seasons: [],
    genres: [],
    subgenres: [],
    savedToCloud: false,
  });
  useEffect(() => {
    dispatch(fetchShowGenres());
    dispatch(fetchShowPGClass());
    dispatch(fetchShowTypes());
    dispatch(fetchShowSubGenres());
    dispatch(fetchUsers());
  }, [dispatch]);

  function getStore() {
    return initStore;
  }

  function updateStore(update) {
    setInitStoreUpdated({
      ...initStore,
      ...update,
    });
  }

  let steps = [
    {
      name: "Datos del Programa",
      component: (
        <Step1
          getStore={() => getStore()}
          updateStore={(u) => {
            console.log(u);
            updateStore(u);
          }}
          showGenres={showGenres}
          showTypes={showTypes}
          showSubGenres={showSubGenres}
          showPGs={showPGs}
          initStore={initStore}
          getStoreSteps={() => getStoreSteps()}
        />
      ),
    },
    {
      name: "Horarios",
      component: (
        <Step2
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Productores y Locutores",
      component: (
        <Step3
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          users={users}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Redes Sociales",
      component: (
        <Step4
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
        />
      ),
    },
    {
      name: "Imágenes",
      component: (
        <Step5
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Patrocinadores",
      component: (
        <Step5AndHalf
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Validar y Guardar",
      component: (
        <Step6
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          showGenres={showGenres}
          showTypes={showTypes}
          showPGs={showPGs}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Fin",
      component: (
        <Step7
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
        />
      ),
    },
  ];

  function getStoreSteps() {
    return steps;
  }

  function updateStoreSteps(update) {
    steps = [...steps, ...update];
  }

  return (
    <Fragment>
      <Row>
        <Col sm="12">
          <Card>
            <CardBody>
              <CardTitle>Agregar un Programa Nuevo </CardTitle>
              <div className="example">
                <div className="step-progress">
                  <StepZilla
                    steps={steps}
                    preventEnterSubmission={true}
                    nextTextOnFinalActionStep={"Save"}
                    hocValidationAppliedTo={[]}
                    /* startAtStep={
                                        !isNaN(parseInt(window.sessionStorage.getItem("step")))
                                            ? parseFloat(window.sessionStorage.getItem("step"))
                                            : 0
                                        } */
                    startAtStep={step}
                    /* onStepChange={step =>
                                            window.sessionStorage.setItem("step", step)
                                        } */
                    onStepChange={(step) => setStep(step)}
                    getStore={() => getStore()}
                    updateStore={(u) => {
                      updateStore(u);
                    }}
                    showGenres={showGenres}
                    showSubGenres={showSubGenres}
                    showTypes={showTypes}
                    showPGs={showPGs}
                  />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Fragment>
  );
};

export default AddShowWizard;
