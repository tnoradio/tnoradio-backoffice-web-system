import React, { Component } from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import Select from "react-select";
//import { Formik, Field, Form } from "formik";
//import * as Yup from "yup";
import { store } from "../../../redux/storeConfig/store";
import { Trash, Mic, User } from "react-feather";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

export default class Step3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hostModal: false,
      producerModal: false,
      hostsState: [],
      producersState: [],
      host: {},
      producer: {},
      updateStore: this.props.updateStore,
      isErrorHost: false,
      isErrorProducer: false,
      isErrorUserHost: false,
      isErrorUserProducer: false,
      isErrorDuplicatedHost: false,
      isErrorDuplicatedProducer: false,
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState({
        isErrorHost: store.getState().showAddFieldsErrors.isErrorHost,
        isErrorProducer: store.getState().showAddFieldsErrors.isErrorProducer,
      });
    });
  }

  verifyUserExistOnList = (id, list) => {
    var found = false;
    list.map((element) => {
      if (element.userId === id) {
        found = true;
      }
    });
    return found;
  };

  render() {
    // console.log(this.props.getStore());
    //let state ={};
    let usersList = [];
    let userGroupOptions = [];
    let users = store.getState().userApp.users;
    //  console.log("Users", users);
    if (users !== undefined) {
      users.forEach((user) => {
        usersList.push({ value: user, label: user.name + " " + user.lastName });
      });

      userGroupOptions = [
        {
          label: "Usuarios",
          options: usersList,
        },
      ];
    } else {
      //console.log("son undefined");
      usersList = [];
    }

    const addHost = (host) => {
      let hosts = this.state.hostsState;
      // console.log(e);
      if (host._id !== undefined) {
        if (!this.verifyUserExistOnList(host._id, hosts)) {
          hosts.push({
            userId: host._id,
            user: host,
          });
          this.setState({
            hostsState: hosts,
            hostModal: false,
            isErrorHost: false,
            isErrorUserHost: false,
            isErrorDuplicatedHost: false,
          });
          this.props.updateStore({ hosts: hosts });
        } else {
          this.setState({ isErrorDuplicatedHost: true });
        }
      } else {
        this.setState({ isErrorUserHost: true });
      }
    };

    const onDeleteHostClick = (userId) => {
      let hosts = this.state.hostsState.filter(
        (host) => host.userId !== userId
      );
      this.setState({ hostsState: hosts });
      this.props.updateStore({ hosts: hosts });
    };

    const addProducer = (producer) => {
      let producers = this.state.producersState;
      // console.log(e);
      if (producer._id !== undefined) {
        if (!this.verifyUserExistOnList(producer._id, producers)) {
          producers.push({
            userId: producer._id,
            user: producer,
          });
          console.log(producers);
          this.setState({
            producersState: producers,
            producerModal: false,
            isErrorProducer: false,
            isErrorUserProducer: false,
            isErrorDuplicatedProducer: false,
          });
          this.props.updateStore({ producers: producers });
        } else {
          this.setState({ isErrorDuplicatedProducer: true });
        }
      } else {
        this.setState({ isErrorUserProducer: true });
      }
    };

    const onDeleteProducerClick = (userId) => {
      let producers = this.state.producersState.filter(
        (producer) => producer.userId !== userId
      );
      this.setState({ producersState: producers });
      this.props.updateStore({ producers: producers });
    };

    return (
      <div className="step step3">
        <Row>
          <Col xs={2}></Col>
          <Col xs={8}>
            <Label>Locutores</Label>
            <ListGroup>
              {this.props.getStore().hosts.map((host) => {
                return (
                  <ListGroupItem key={"host " + host.userId}>
                    <Row>
                      <Col xs={10}>
                        <span>{host.user.name}</span>{" "}
                        <span>{host.user.lastName}</span>{" "}
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            onDeleteHostClick(host.userId);
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={2}></Col>
        </Row>
        <Row>
          <Col xs={2}></Col>
          <Col xs={8}>
            <br></br>
            <Button
              className="btn-square float-right"
              size="sm"
              outline
              color="primary"
              onClick={() => this.setState({ hostModal: true })}
            >
              <Mic size={16} color="#009DA0" />
              {"  "}
              Agregar Locutor
            </Button>
          </Col>
          <Col xs={2}></Col>
          {this.state.isErrorHost ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe indicar al menos un Locutor"}
            </div>
          ) : null}
        </Row>
        <Modal
          isOpen={this.state.hostModal}
          toggle={() => this.setState({ hostModal: !this.state.hostModal })}
        >
          <ModalHeader>
            <Label>Agregar Locutor</Label>
          </ModalHeader>
          <ModalBody>
            <Row>
              {usersList !== undefined &&
              usersList[0] !== undefined &&
              usersList[0] !== null ? (
                <Col xs={12}>
                  <FormGroup>
                    <Select
                      defaultValue={usersList[0].value}
                      options={userGroupOptions}
                      formatGroupLabel={formatGroupLabel}
                      value={usersList.find((obj) => {
                        return obj === this.state.hostsState;
                      })}
                      onChange={(e) => {
                        this.setState({ host: e.value });
                      }}
                    />
                  </FormGroup>
                </Col>
              ) : (
                <Col xs={11}></Col>
              )}
              {this.state.isErrorUserHost ? (
                <div
                  className="invalid-feedback"
                  style={{ display: "block", textAlign: "center" }}
                >
                  {"Debe seleccionar a un Locutor"}
                </div>
              ) : null}
              {this.state.isErrorDuplicatedHost ? (
                <div
                  className="invalid-feedback"
                  style={{ display: "block", textAlign: "center" }}
                >
                  {"El locutor ya existe para este Programa"}
                </div>
              ) : null}
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button
              className="btn-square"
              outline
              color="primary"
              onClick={() => addHost(this.state.host)}
            >
              Agregar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Row>
          <Col xs={2}></Col>
          <Col xs={8}>
            <Label>Productores</Label>
            <ListGroup>
              {this.props.getStore().producers.map((producer) => {
                return (
                  <ListGroupItem key={"producer " + producer.userId}>
                    <Row>
                      <Col xs={10}>
                        <span>{producer.user.name}</span>{" "}
                        <span>{producer.user.lastName}</span>{" "}
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            onDeleteProducerClick(producer.userId);
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={2}></Col>
        </Row>
        <Row>
          <Col xs={2}></Col>
          <Col xs={8}>
            <br></br>
            <Button
              className="btn-square float-right"
              size="sm"
              outline
              color="primary"
              onClick={() => this.setState({ producerModal: true })}
            >
              <User size={16} color="#009DA0" />
              {"  "}
              Agregar Productor
            </Button>
          </Col>
          <Col xs={2}></Col>
          {this.state.isErrorProducer ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe indicar al menos un Productor"}
            </div>
          ) : null}
        </Row>
        <Modal
          isOpen={this.state.producerModal}
          toggle={() =>
            this.setState({ producerModal: this.state.producerModal })
          }
        >
          <ModalHeader>
            <Label>Agregar Productor</Label>
          </ModalHeader>
          <ModalBody>
            <Row>
              {usersList !== undefined &&
              usersList[0] !== undefined &&
              usersList[0] !== null ? (
                <Col xs={12}>
                  <FormGroup>
                    <Select
                      defaultValue={usersList[0].value}
                      options={userGroupOptions}
                      formatGroupLabel={formatGroupLabel}
                      value={usersList.find((obj) => {
                        return obj === this.state.producersState;
                      })}
                      onChange={(e) => {
                        this.setState({ producer: e.value });
                      }}
                    />
                  </FormGroup>
                </Col>
              ) : (
                <Col xs={11}></Col>
              )}
              {this.state.isErrorDuplicatedProducer ? (
                <div
                  className="invalid-feedback"
                  style={{ display: "block", textAlign: "center" }}
                >
                  {"El productor ya existe para este Programa"}
                </div>
              ) : null}
              {this.state.isErrorUserProducer ? (
                <div
                  className="invalid-feedback"
                  style={{ display: "block", textAlign: "center" }}
                >
                  {"Debe seleccionar a un Productor"}
                </div>
              ) : null}
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button
              className="btn-square"
              outline
              color="primary"
              onClick={() => addProducer(this.state.producer)}
            >
              Agregar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
