/* eslint-disable import/no-anonymous-default-export */
import React, { Component } from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import Dropzone from "react-dropzone";

import "react-datetime/css/react-datetime.css";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

import { store } from "../../../redux/storeConfig/store";
import { Trash } from "react-feather";

const advertiserFormSchema = Yup.object().shape({
  name: Yup.string().required("Debes escribir un nombre."),
  webSite: Yup.string().required("Debes escribir un sitio web."),
  imageUrl: Yup.string().required("Debes indicar uan imagen."),
});

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      advertiserModal: false,
      advertisersState: [],
      isErrorName: false,
      advertiserName: "",
      isErrorWebSite: false,
      advertiserWebSite: "",
      files: [],
      images: [],
      isErrorImage: false,
      isErrorAdvertiser: false,
    };
    // console.log(props);
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState({
        isErrorAdvertiser:
          store.getState().showAddFieldsErrors.isErrorAdvertiser,
      });
    });
  }

  validateResolution = (imageFile) => {
    var response = false;
    var reader = new FileReader();
    //Read the contents of Image File.
    reader.readAsDataURL(imageFile[0]);
    reader.onload = async function (e) {
      //Initiate the JavaScript Image object.
      var image = new Image();

      //Set the Base64 string return from FileReader as source.
      image.src = e.target.result;
      //var res = ()=>{response = true}
      //Validate the File Height and Width.
      image.onload = async function (res) {
        var height = this.height;
        var width = this.width;
        // console.log("Ancho -> ", width);
        // console.log("Alto -> ", height);
        if (width === 300 && height === 288) {
          res();
          // this.setState({isErrorImage: false})
        } else {
          // this.setState({isErrorImage: true})
        }
      };
    };
    return response;
  };

  setImage = (image) => {
    var imageFile = [];
    var images = [];
    if (image[0].type === "image/jpg" || image[0].type === "image/jpeg") {
      imageFile = image.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      );
      images.push({
        name: "advertiser",
        imageUrl:
          "advertisers/advertiser_" + this.state.advertiserName + ".jpg",
      });
      // console.log("Image file -> {0}", imageFile);
      // console.log("Image -> {0}", images);
      this.setState({ isErrorImage: false, files: imageFile, images: images });
    }
  };

  addAdvertiserSubmit = () => {
    if (this.state.advertiserName !== "") {
      if (this.state.advertiserWebSite !== "") {
        if (this.state.files.length > 0) {
          var advertisers = this.state.advertisersState;
          advertisers.push({
            name: this.state.advertiserName,
            website: this.state.advertiserWebSite,
            imageFile: this.state.files[0],
            imageUrl: this.state.images[0].imageUrl,
          });
          // console.log("Patrocinantes: {0}", advertisers);
          this.setState({
            advertisersState: advertisers,
            advertiserName: "",
            advertiserWebSite: "",
            files: [],
            images: [],
            advertiserModal: false,
          });
          this.props.updateStore({ advertisers: advertisers });
        } else {
          this.setState({
            isErrorImage: true,
            isErrorWebSite: false,
            isErrorName: false,
          });
        }
      } else {
        this.setState({
          isErrorWebSite: true,
          isErrorName: false,
          isErrorImage: false,
        });
      }
    } else {
      this.setState({
        isErrorName: true,
        isErrorWebSite: false,
        isErrorImage: false,
      });
    }
  };

  onDeleteAdvertiserClick = (advertiser) => {
    var advertisers = this.state.advertisersState;

    var found = advertisers.map((ad) => {
      return advertiser === ad;
    });

    var newAdvertisers = [];

    found.map((adv, index) => {
      if (!adv) {
        newAdvertisers.push(advertisers[index]);
      }
    });

    this.setState({ advertisersState: newAdvertisers });
    this.props.updateStore({ advertisers: newAdvertisers });

    // console.log("A eliminar ->", found);
  };

  render() {
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 150,
      height: 100,
      padding: 4,
      boxSizing: "border-box",
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden",
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%",
    };

    const thumbs = this.state.files.map((file) => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img src={file.preview} style={img} alt={file.name} />
        </div>
      </div>
    ));

    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16,
    };

    return (
      <div>
        <Row>
          <Col xs={2}></Col>
          <Col xs={8} style={{ marginLeft: "auto", marginRight: "auto" }}>
            <Label>Anunciantes</Label>
            <ListGroup>
              {this.props.getStore().advertisers.map((advertiser, index) => {
                return (
                  <ListGroupItem key={advertiser.name + index}>
                    <Row>
                      <Col xs={6}>
                        <p>Nombre: {advertiser.name}</p>
                        {"  "}
                        <p>Sitio: {advertiser.website}</p>{" "}
                      </Col>
                      <Col xs={4}>
                        <div style={thumbsContainer}>
                          <div style={thumb} key={advertiser.imageFile.name}>
                            <div style={thumbInner}>
                              <img
                                src={advertiser.imageFile.preview}
                                style={img}
                              />
                            </div>
                          </div>
                        </div>
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            this.onDeleteAdvertiserClick(advertiser);
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={2}></Col>
        </Row>
        <Row>
          <Col xs={2}></Col>
          {this.state.isErrorAdvertiser ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe indicar un Anunciante"}
            </div>
          ) : null}
          <Col xs={8}>
            <br></br>
            <Button
              className="btn-square float-right"
              size="sm"
              outline
              color="primary"
              onClick={() => this.setState({ advertiserModal: true })}
            >
              Agregar Anunciante
            </Button>
          </Col>
          <Col xs={2}></Col>
        </Row>
        <Modal
          isOpen={this.state.advertiserModal}
          toggle={() =>
            this.setState({ advertiserModal: !this.state.advertiserModal })
          }
        >
          <ModalHeader>
            <Label>Agregar Anunciante</Label>
          </ModalHeader>
          <Formik
            initialValues={{
              name: "",
              webSite: "",
              imageUrl: "",
            }}
            validationSchema={advertiserFormSchema}
            // onSubmit={
            //    values => {
            //    addSocialSubmit(values);
            //    // console.log(values);
            // }}
          >
            {({}) => (
              <Form>
                <ModalBody>
                  <Row>
                    <Col xs={12}>
                      <FormGroup>
                        <Label for="name">Nombre del Anunciante</Label>
                        <Field
                          className={`form-control ${
                            this.state.isErrorName ? "is-invalid" : ""
                          }`}
                          type="text"
                          onChange={(e) => {
                            this.setState({
                              advertiserName: e.currentTarget.value,
                            });
                          }}
                          value={this.state.advertiserName}
                          name="name"
                          required
                        />
                        {this.state.isErrorName ? (
                          <div className="invalid-feedback">
                            Debes escribir un nombre para el anunciante.
                          </div>
                        ) : null}
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12}>
                      <FormGroup>
                        <Label for="name">Sitio Web del Anunciante</Label>
                        <Field
                          className={`form-control ${
                            this.state.isErrorWebSite ? "is-invalid" : ""
                          }`}
                          type="text"
                          onChange={(e) => {
                            this.setState({
                              advertiserWebSite: e.currentTarget.value,
                            });
                          }}
                          value={this.state.advertiserWebSite}
                          name="name"
                          required
                        />
                        {this.state.isErrorWebSite ? (
                          <div className="invalid-feedback">
                            Debes escribir sitio web para el anunciante.
                          </div>
                        ) : null}
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12}>
                      <FormGroup>
                        <Row>
                          <Col xs={2}></Col>
                          <Col xs={8}>
                            <Label>Imagen del Anunciante</Label>
                          </Col>
                          <Col xs={2}></Col>
                        </Row>
                        <Row>
                          <Col xs={2}></Col>
                          <Col xs={8}>
                            <Dropzone
                              onDrop={(acceptedFiles) =>
                                this.setImage(acceptedFiles)
                              }
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    {thumbs.length > 0 ? (
                                      <div></div>
                                    ) : (
                                      <p>
                                        Arrastra la imagen aquí o haz click en
                                        este espacio para seleccionarla.
                                      </p>
                                    )}
                                    <div style={thumbsContainer}>{thumbs}</div>
                                  </div>
                                </section>
                              )}
                            </Dropzone>
                          </Col>
                          <Col xs={2}></Col>
                        </Row>
                        {this.state.isErrorImage ? (
                          <div
                            className="invalid-feedback"
                            style={{ display: "block", textAlign: "center" }}
                          >
                            {"Debe seleccionar una imagen para el Patrocinador"}
                          </div>
                        ) : null}
                      </FormGroup>
                    </Col>
                  </Row>
                </ModalBody>
                <ModalFooter>
                  <Button
                    className="btn-square"
                    outline
                    color="primary"
                    onClick={() => this.addAdvertiserSubmit()}
                    type="submit"
                  >
                    Agregar
                  </Button>{" "}
                </ModalFooter>
              </Form>
            )}
          </Formik>
        </Modal>
      </div>
    );
  }
}
