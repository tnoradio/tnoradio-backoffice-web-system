import React, { useState, useEffect } from "react";
import { Col, Row, FormGroup, Label, CustomInput } from "reactstrap";
import Select from "react-select";
import { Textarea } from "react-formik-ui";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";
import groupedOptions from "../../../components/common/groupedOptions";
import formatGroupLabel from "../../../components/common/formatGroupLabel";
import Buttons from "../../../components/common/buttons";
import TargetSelector from "../TargetSelector";
import AgeRangeSelector from "./AgeRangeSelector";
import GenderSelector from "../GenderSelector";

const formSchema = Yup.object().shape({
  name: Yup.string().required("Debes escribir un nombre para el programa."),
  synopsis: Yup.string().required("Debes escribir una sinopsis del programa."),
  genre: Yup.string().required("Debes seleccionar un género."),
  genres: Yup.array().required("Debes seleccionar un género."),
  subgenres: Yup.array().required("Debes seleccionar un sub género."),
  type: Yup.string().required("Debes eleccionar un tipo."),
  age_range: Yup.array().required(
    "Debes seleccionar al menis un rango de edades."
  ),
  target: Yup.array().required(
    "Debes seleccionar al menis un target objetivo."
  ),
  gender: Yup.array().required("Debes seleccionar un género."),
  pg: Yup.string().required("Debes seleccionar una clasificación."),
  email: Yup.string()
    .email("Correo Inválido")
    .required("Debes escribir un email."),
});

const customStyles = {
  menuPortal: (base) => ({ ...base, zIndex: 9999 }),
  control: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    zIndex: 900,
  }),
  option: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    backgroundColor: state.isSelected ? "#009DA0" : "#fff",
    color: state.isSelected ? "#fff" : "#000",
  }),
};

export default function Step1({
  getStore,
  updateStore,
  showGenres,
  showTypes,
  showSubGenres,
  showPGs,
}) {
  const [showSelectedGenres, setShowSelectedGenres] = useState(
    getStore().genres || []
  );
  const [showSelectedSubGenres, setShowSelectedSubGenres] = useState(
    getStore().subgenres || []
  );

  let genre = "";
  let subGenre = "";
  let showType = "";
  let pg = "";

  const addGenre = (genre) => {
    const updatedGenres = [...showSelectedGenres];
    const existingGenreIndex = updatedGenres.findIndex(
      (existingGenre) => existingGenre.id === genre.value
    );

    if (existingGenreIndex === -1) {
      updatedGenres.push({ id: genre.value, genre: genre.label });
    }

    setShowSelectedGenres(updatedGenres);
    updateStore({
      genres: updatedGenres,
      genre: genre.label,
    });
  };

  const addSubGenre = (subGenre) => {
    const updatedSubGenres = [...showSelectedSubGenres];
    const existingSubGenreIndex = updatedSubGenres.findIndex(
      (existingSubGenre) => existingSubGenre.id === subGenre.value
    );

    if (existingSubGenreIndex === -1) {
      updatedSubGenres.push({ id: subGenre.value, subgenre: subGenre.label });
    }

    setShowSelectedSubGenres(updatedSubGenres);
    updateStore({ subgenres: updatedSubGenres });
  };

  const onDeleteGenreClick = (genre) => {
    const updatedGenres = showSelectedGenres.filter((item) => {
      return item.id !== genre.value;
    });
    setShowSelectedGenres(updatedGenres);
    updateStore({ subgenres: updatedGenres });
  };

  const onDeleteSubGenreClick = (subGenre) => {
    const updatedSubGenres = showSelectedSubGenres.filter((item) => {
      return item.id !== subGenre.value;
    });
    setShowSelectedSubGenres(updatedSubGenres);
    updateStore({ subgenres: updatedSubGenres });
  };

  const genresList = () => {
    const newGenres = showSelectedGenres.map((genre) => {
      return {
        value: genre.id,
        label: showGenres.find((g) => g.id === genre.id)?.genre,
      };
    });
    return newGenres;
  };

  const subGenresList = () =>
    showSelectedSubGenres.map((subGenre) => ({
      value: subGenre.id,
      label: showSubGenres.find((g) => g.id === subGenre.id)?.subgenre,
    }));

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          name: getStore().name || "",
          email: getStore().email || "",
          genres: getStore().genres || [],
          subgenres: getStore().subgenres || [],
          age_range: getStore().age_range || [],
          target: getStore().target || [],
          genders: getStore().genders || [],
          genre: getStore().genre || "",
          pg: getStore().pg || "Todo Público",
          type: getStore().type || "Mixto",
          synopsis: getStore().synopsis || "",
        }}
        validationSchema={formSchema}
      >
        {({ errors, touched, values, handleChange }) => {
          return (
            <Form>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Label for="name">Nombre del Programa</Label>
                    <Field
                      className={`form-control ${
                        errors.name ? "is-invalid" : ""
                      }`}
                      type="text"
                      onChange={(e) => {
                        handleChange(e);
                        updateStore({
                          name: e.currentTarget.value,
                          showSlug: e.currentTarget.value
                            .toLowerCase()
                            .replace(/\s/g, "")
                            .normalize("NFD")
                            .replace(
                              /([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,
                              "$1"
                            )
                            .normalize(),
                        });
                      }}
                      value={getStore().name}
                      name="name"
                      required
                    />
                    {errors.name && touched.name ? (
                      <div className="invalid-feedback">
                        Debes escribir un nombre para el programa.
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Label for="synopsis">Sinopsis del Programa</Label>
                    <Textarea
                      className={`form-control ${
                        errors.synopsis ? "is-invalid" : ""
                      }`}
                      name="synopsis"
                      id="synopsis"
                      value={getStore().synopsis}
                      onChange={(e) => {
                        handleChange(e);
                        updateStore({
                          synopsis: e.currentTarget.value,
                        });
                      }}
                      required
                    />
                    {errors.synopsis && touched.synopsis ? (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block" }}
                      >
                        Debes escribir una sinopsis del programa.
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <Label for="genres">Géneros</Label>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Select
                      defaultValue={
                        groupedOptions(showGenres, "Géneros")[0].options[0]
                      }
                      styles={customStyles}
                      options={groupedOptions(showGenres, "Géneros")}
                      formatGroupLabel={formatGroupLabel}
                      value={showGenres.find((obj) => obj.genre === genre)}
                      onChange={(e) => {
                        genre = e.label;
                        addGenre(e);
                      }}
                      id="genres"
                      menuPortalTarget={document.body}
                    />
                  </FormGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <Buttons
                    options={genresList()}
                    onDeleteOption={onDeleteGenreClick}
                    editFlag={true}
                  />
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <Label for="subgenres">Sub Géneros</Label>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Select
                      defaultValue={
                        groupedOptions(showSubGenres, "Sub Géneros")[0]
                          .options[0]
                      }
                      styles={customStyles}
                      options={groupedOptions(showSubGenres, "Sub Géneros")}
                      formatGroupLabel={formatGroupLabel}
                      value={showSubGenres.find(
                        (obj) => obj.subGenre === subGenre
                      )}
                      onChange={(e) => {
                        subGenre = e.label;
                        addSubGenre(e);
                      }}
                      id="subgenres"
                      menuPortalTarget={document.body}
                    />
                  </FormGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <Buttons
                    options={subGenresList()}
                    onDeleteOption={onDeleteSubGenreClick}
                    editFlag={true}
                  />
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <Label for="pg">Clasificación</Label>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Select
                      defaultValue={
                        groupedOptions(showPGs, "Clasificación")[0].options[0]
                      }
                      options={groupedOptions(showPGs, "Clasificación")}
                      formatGroupLabel={formatGroupLabel}
                      value={showPGs.find((obj) => obj.value === pg)}
                      onChange={(e) => {
                        pg = e.label;
                      }}
                      id="pg"
                      menuPortalTarget={document.body}
                      styles={customStyles}
                    />
                  </FormGroup>
                </Col>

                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <Label for="type">Tipo</Label>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Select
                      defaultValue={
                        groupedOptions(showTypes, "Tipo")[0].options[0]
                      }
                      options={groupedOptions(showTypes, "Tipo")}
                      formatGroupLabel={formatGroupLabel}
                      value={showTypes.find((obj) => obj.value === showType)}
                      onChange={(e) => {
                        showType = e.label;
                        updateStore({
                          type: e.label,
                        });
                      }}
                      id="type"
                      menuPortalTarget={document.body}
                      styles={customStyles}
                    />
                  </FormGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <AgeRangeSelector
                    updateStore={updateStore}
                    getStore={getStore}
                  />
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <TargetSelector
                    updateStore={updateStore}
                    getStore={getStore}
                  />
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <GenderSelector
                    updateStore={updateStore}
                    getStore={getStore}
                  />
                </Col>
                <Col xs={2}></Col>
              </Row>
              <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                  <FormGroup>
                    <Label for="email">Email</Label>
                    <Field
                      className={`form-control ${errors.email && "is-invalid"}`}
                      type="email"
                      value={getStore().email === "" ? "" : getStore().email}
                      onChange={(e) => {
                        handleChange(e);
                        updateStore({
                          email: e.currentTarget.value,
                        });
                      }}
                      name="email"
                      id="email"
                      required
                    />
                    {errors.email ? (
                      <div className="invalid-feedback">Correo Inválido</div>
                    ) : null}
                  </FormGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>
            </Form>
          );
        }}
      </Formik>
    </React.Fragment>
  );
}
