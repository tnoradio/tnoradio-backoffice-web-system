import React, { Fragment } from "react";
import { Table } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 610,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

const Step6 = (props) => {
  const thumbs = props.getStore().imageFiles.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} />
      </div>
    </div>
  ));

  return (
    <Fragment>
      <div className="contact-app-content-detail">
        <PerfectScrollbar>
          <Table responsive borderless size="sm" className="mt-4">
            <tbody>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Nombre</td>
                <td className="col-9">{props.getStore().name}</td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Sinopsis</td>
                <td className="col-9">{props.getStore().synopsis}</td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Género</td>
                <td className="col-9">
                  {props.getStore().genre === ""
                    ? props.showGenres[0].genre
                    : props.getStore().genre}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Géneros</td>
                <td className="col-9">
                  {props.getStore().genres.map((genre, index) => {
                    return (
                      <tr
                        className="d-flex"
                        key={`genre${genre.genre}${index}`}
                      >
                        <td className="col-3 text-bold-400">{genre.genre}</td>
                      </tr>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Sub Géneros</td>
                <td className="col-9">
                  {props.getStore().subgenres.map((subGenre, index) => {
                    return (
                      <tr
                        className="d-flex"
                        key={`genre${subGenre.subgenre}${index}`}
                      >
                        <td className="col-3 text-bold-400">
                          {subGenre.subgenre}
                        </td>
                      </tr>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Tipo</td>
                <td className="col-9">
                  {props.getStore().type === ""
                    ? props.showTypes[0].showType
                    : props.getStore().type}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Clasificación</td>
                <td className="col-9">
                  {props.getStore().pgClassification === ""
                    ? props.showPGs[0].showPGClassification
                    : props.getStore().pgClassification}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Correo</td>
                <td className="col-9">{props.getStore().email}</td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Productor(es)</td>
                <td className="col-9">
                  {props.getStore().producers.map((producer) => {
                    return (
                      <div key={"producer" + producer.userId}>
                        {producer.user.name} {producer.user.lastName}
                      </div>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Locutor(es)</td>
                <td className="col-9">
                  {props.getStore().hosts.map((host) => {
                    return (
                      <div key={"host" + host.userId}>
                        {host.user.name} {host.user.lastName}
                      </div>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Horario</td>
                <td className="col-9">
                  {props.getStore().showSchedules.map((showSchedule, index) => {
                    return (
                      <div key={`{showSchedule}${index}`}>
                        {showSchedule.weekDay} {showSchedule.startTime}{" "}
                        {showSchedule.endTime}
                      </div>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Redes Sociales</td>
                <td className="col-9">
                  {props.getStore().socials.map((social) => {
                    return (
                      <div key={"social" + social.name + social.userName}>
                        {social.name} {social.userName}
                      </div>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Patrocinantes</td>
                <td className="col-9">
                  {props.getStore().advertisers.map((advertiser, index) => {
                    return (
                      <tr
                        className="d-flex"
                        key={"advertiser" + advertiser.name + index}
                      >
                        <td className="col-3 text-bold-400">
                          {advertiser.name} {advertiser.website}
                        </td>
                        <td className="col-9">
                          <div style={thumbsContainer}>
                            <div style={thumb} key={advertiser.name}>
                              <div style={thumbInner}>
                                <img
                                  src={advertiser.imageFile.preview}
                                  style={img}
                                />
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-3 text-bold-400">Imágenes</td>
                <td className="col-9">
                  <div style={thumbsContainer}>{thumbs}</div>
                </td>
              </tr>
            </tbody>
          </Table>
        </PerfectScrollbar>
      </div>
    </Fragment>
  );
};

export default Step6;
