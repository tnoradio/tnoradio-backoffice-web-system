import { store } from "../../../redux/storeConfig/store";

/**
 * Verify if it can go to the next step
 * @param {Number} step Step to verify
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
export function verifyStep(step, storeLocal) {
  switch (step) {
    case 0:
      return verifyStepOne(storeLocal);
    case 1:
      return verifyStepTwo(storeLocal);
    case 2:
      return verifyStepThree(storeLocal);
    case 3:
      return verifyStepFour(storeLocal);
    case 4:
      return verifyStepFive(storeLocal);
    default:
      return true;
  }
}

/**
 * Verify the step one when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepOne(storeLocal) {
  if (
    storeLocal.name !== "" &&
    storeLocal.synopsis !== "" &&
    storeLocal.email !== ""
  ) {
    store.dispatch({
      type: "UPDATE",
      name: storeLocal.name,
      synopsis: storeLocal.synopsis,
      email: storeLocal.email,
    });
    return true;
  }
  return false;
}

/**
 * Verify the step two when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepTwo(storeLocal) {
  if (storeLocal.showSchedules.length > 0) {
    return true;
  }
  return false;
}

/**
 * Verify the step three when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepThree(storeLocal) {
  if (storeLocal.producers.length > 0 && storeLocal.hosts.length > 0) {
    return true;
  }
  return false;
}

/**
 * Verify the step four when registering program
 * @returns true if can go farther, otherwise false
 */
function verifyStepFour() {
  return true;
}

/**
 * Verify the step five when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepFive(storeLocal) {
  if (storeLocal.imageFiles.length >= 0) {
    return true;
  }
  return true;
}

function verifyStepSix(storeLocal) {
  if (storeLocal.advertisers.length > 0) {
    return true;
  }
  return true;
}

export function isEmptyField(element) {
  if (element !== "") {
    return false;
  }
  return true;
}

export function isValidEmail(element) {
  let pattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

  if (pattern.exec(element) === null) {
    return false;
  }
  return true;
}

export function notifyErrors(step, storeLocal) {
  // Primer paso
  if (step === 0) {
    if (isEmptyField(storeLocal.name)) {
      store.dispatch({ type: "UPDATE", isErrorName: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorName: false });
    }
    if (isEmptyField(storeLocal.synopsis)) {
      store.dispatch({ type: "UPDATE", isErrorSynopsis: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorSynopsis: false });
    }
    if (isEmptyField(storeLocal.email) || !isValidEmail(storeLocal.email)) {
      store.dispatch({ type: "UPDATE", isErrorEmail: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorEmail: false });
    }
  }

  // Segundo paso
  if (step === 1) {
    if (storeLocal.showSchedules.length === 0) {
      store.dispatch({ type: "UPDATE", isErrorNotScheduleSelected: true });
    }
  }

  // Tercer paso
  if (step === 2) {
    if (storeLocal.producers.length === 0) {
      store.dispatch({ type: "UPDATE", isErrorProducer: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorProducer: false });
    }
    if (storeLocal.hosts.length === 0) {
      store.dispatch({ type: "UPDATE", isErrorHost: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorHost: false });
    }
  }
}
