import React, { Component } from "react";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";
import {
  Col,
  Row,
  FormGroup,
  Input,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  CustomInput,
  ModalHeader,
} from "reactstrap";

import DateTime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";
import { Trash, Clock } from "react-feather";
import { store } from "../../../redux/storeConfig/store";

const formSchema = Yup.object().shape({
  start: Yup.date().required("Debes seleccionar una hora de inicio."),
  end: Yup.date().required("Debes seleccionar una hora de finalización."),
});

const weekdays = [
  "lunes",
  "martes",
  "miercoles",
  "jueves",
  "viernes",
  "sabado",
  "domingo",
];

export default class Step2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      start: null,
      end: null,
      modal: false,
      isErrorNotScheduleSelected: false,
      isErrorNotSelectedHours: false,
      weekDay: "",
      scheduleState: this.props.getStore().showSchedules,
      aDayIsSelected: false,
      isErrorCrash: false,
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState({
        isErrorNotScheduleSelected:
          store.getState().showAddFieldsErrors.isErrorNotScheduleSelected,
      });
    });
  }

  verifySchedule = (schedules, newShedule) => {
    var pass = true;
    var weekDay = false;
    for (let i = 0; i < schedules.length; i++) {
      var element = schedules[i];
      if (element.weekDay === newShedule.weekDay) {
        weekDay = true;
        if (newShedule.startDate < element.startDate) {
          if (newShedule.endDate > element.startDate) {
            pass = false;
            break;
          }
        }
        if (newShedule.startDate > element.startDate) {
          if (newShedule.startDate < element.endDate) {
            pass = false;
            break;
          } else {
            if (newShedule.startDate < element.endDate) {
              if (newShedule.endDate > element.endDate) {
                pass = false;
                break;
              }
            }
          }
        }
      }
    }
    if (!weekDay || schedules.length === 0) {
      pass = true;
    }
    return pass;
  };

  render() {
    const { getStore, updateStore } = this.props;
    const { start, end, weekDay } = this.state;

    const resetScheduleForm = () => {
      this.setState({
        weekDay: "",
        start: null,
        end: null,
        modal: false,
        aDayIsSelected: false,
      });
    };

    const verifySchedule = (schedules, newSchedule) => {
      if (schedules.length === 0) return true;

      return !schedules.some(
        (schedule) =>
          schedule.weekDay === newSchedule.weekDay &&
          isScheduleConflict(schedule, newSchedule)
      );
    };

    const isScheduleConflict = (existingSchedule, newSchedule) => {
      return (
        (newSchedule.startDate < existingSchedule.endDate &&
          newSchedule.endDate > existingSchedule.startDate) ||
        (newSchedule.startDate <= existingSchedule.startDate &&
          newSchedule.endDate >= existingSchedule.endDate)
      );
    };

    const hasConflict = verifySchedule(getStore().showSchedules, {
      weekDay: weekDay,
      startTime: start,
      endTime: end,
    });

    const addSchedule = () => {
      const { start, end, weekDay } = this.state;

      if (start !== null && end !== null) {
        const newSchedule = { weekDay, startTime: start, endTime: end }; // Create new schedule object

        updateStore({
          showSchedules: [...getStore().showSchedules, newSchedule],
        });

        resetScheduleForm();
      } else {
        this.setState({ isErrorNotSelectedHours: true }); // Set error state for missing times
      }
    };

    const handleDateTimeChange = (timeType, time) => {
      this.setState({ [timeType]: time });
    };

    const onDeleteSchedule = (showSchedule) => {
      const filteredSchedules = getStore().showSchedules.filter(
        (scheduleItem) =>
          scheduleItem.weekDay !== showSchedule.weekDay ||
          scheduleItem.startTime !== showSchedule.startTime ||
          scheduleItem.endTime !== showSchedule.endTime
      );
      updateStore({ showSchedules: filteredSchedules });
    };

    const handleChangeRadioInput = (weekday) => {
      this.setState({
        weekDay: weekday,
        noDayIsSelected: false,
      });
    };

    const isInvalidTime = (start, end) => {
      const startTime = moment(start);
      const endTime = moment(end);
      return startTime.isAfter(endTime) && !hasConflict;
    };

    return (
      <div>
        <Formik
          initialValues={{
            start: null,
            end: null,
          }}
          validationSchema={formSchema}
          onSubmit={(values) => {
            // same shape as initial values
            // console.log(values);
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                  <Label>Horarios</Label>
                  <ListGroup>
                    {getStore().showSchedules.map((showSchedule) => {
                      return (
                        <ListGroupItem
                          key={`${showSchedule.weekDay}-${showSchedule.startTime}-${showSchedule.endTime}`}
                        >
                          <Row>
                            <Col xs={10}>
                              <span>{showSchedule.weekDay}</span>{" "}
                              <span>de {showSchedule.startTime}</span>{" "}
                              <span>a {showSchedule.endTime}</span>
                            </Col>
                            <Col xs={2}>
                              <Trash
                                size={18}
                                onClick={() => {
                                  onDeleteSchedule(showSchedule);
                                }}
                                className="float-center mt-1 mb-2 width-25 d-block"
                                style={{ color: "#FF586B", cursor: "pointer" }}
                              />
                            </Col>
                          </Row>
                        </ListGroupItem>
                      );
                    })}
                  </ListGroup>
                </Col>
                <Col xs={3}></Col>
              </Row>
              <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                  <br></br>
                  <Button
                    className="btn-square float-right"
                    size="sm"
                    outline
                    color="primary"
                    onClick={() => this.setState({ modal: true })}
                  >
                    <Clock size={16} color="#009DA0" />
                    {"  "}
                    Agregar Horario
                  </Button>
                  <br></br>
                </Col>
                <Col xs={3}></Col>
              </Row>
              {this.state.isErrorNotScheduleSelected ? (
                <div
                  className="invalid-feedback"
                  style={{ display: "block", textAlign: "center" }}
                >
                  {"Debe indicar al menos un horario para el Programa"}
                </div>
              ) : null}

              <Modal
                isOpen={this.state.modal}
                toggle={() => this.setState({ modal: !this.state.modal })}
              >
                <ModalHeader>
                  <Label>Agregar Horario</Label>
                </ModalHeader>
                <ModalBody>
                  <FormGroup tag="fieldset">
                    <h6>Seleccione día de la semana</h6>
                    <Row>
                      {weekdays.map((day) => (
                        <Col xs={6} key={day}>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="radio"
                              id={day}
                              name="customRadio"
                              label={day}
                              onClick={() => handleChangeRadioInput(day)}
                            />
                          </FormGroup>
                        </Col>
                      ))}
                    </Row>
                  </FormGroup>
                  <Row>
                    <Col xs={4}>
                      <div>
                        <Label>Hora Inicio</Label>
                        <Field name="start">
                          {({ field, form, meta }) => (
                            <div className="position-relative has-icon-left">
                              <Input
                                id="start"
                                name="start"
                                placeholder="00:00"
                                onChange={(e) =>
                                  handleDateTimeChange("start", e.target.value)
                                }
                                type="time"
                              />
                            </div>
                          )}
                        </Field>
                      </div>
                    </Col>
                    <Col xs={4}>
                      <div>
                        <Label>Hora Fin</Label>
                        <Field name="end">
                          {({ field, form, meta }) => (
                            <div className="position-relative has-icon-left">
                              <Input
                                id="end"
                                name="end"
                                placeholder="00:00"
                                onChange={(e) =>
                                  handleDateTimeChange("end", e.target.value)
                                }
                                type="time"
                              />
                            </div>
                          )}
                        </Field>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    {this.state.isErrorNotSelectedHours ? (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block", textAlign: "center" }}
                      >
                        {"Debe indicar una hora de inicio y fin válida"}
                      </div>
                    ) : null}
                    {this.state.isErrorCrash ? (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block", textAlign: "center" }}
                      >
                        {
                          "Debe indicar una hora de inicio y fin válida. Existe choque de horarios"
                        }
                      </div>
                    ) : null}
                  </Row>
                </ModalBody>
                <ModalFooter>
                  <Button
                    className="btn-square"
                    outline
                    color="primary"
                    onClick={addSchedule}
                    size="sm"
                    disabled={isInvalidTime(start, end)}
                  >
                    Agregar
                  </Button>{" "}
                </ModalFooter>
              </Modal>
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}
