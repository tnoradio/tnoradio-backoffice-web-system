import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { Col, Row, Label, FormGroup } from "reactstrap";
import "../../../assets/scss/views/components/extra/upload.scss";
import { store } from "../../../redux/storeConfig/store";

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

let images = [];
let imageFiles = [];
let imageFile;

const verifyImage = (name, list) => {
  let found = false;
  list.map((element) => {
    if (element.name === name) {
      found = true;
    }
  });
  return true;
};

const deleteImage = (name, list) => {
  let response = [];
  list.map((element) => {
    if (element.name !== name) {
      response.push(element);
    }
  });
  return response;
};

const deleteImageFile = (name, list, listFile) => {
  let response = [];
  let nameFile = list.map((element) => {
    if (element.name === name) {
      return element.originalName;
    }
  });
  response = deleteImage(nameFile[0], listFile);
  return response;
};

const getImage = (name, props) => {
  var response = null;
  props.getStore().images.map((image) => {
    if (image.name === name) {
      var storeLocal = props.getStore();
      storeLocal.imageFiles.map((imageF) => {
        if (image.originalName === imageF.name) {
          response = imageF;
        }
      });
    }
  });
  // console.log([response]);
  return [response];
};

const Step5 = (props) => {
  console.log(props);
  let state = {};
  const [filesBigHome, setBigHomeFiles] = useState(
    images.length === 0 ? [] : getImage("showBanner", props)
  );
  const [filesResponsive, setResponsiveFiles] = useState(
    images.length === 0 ? [] : getImage("responsiveBanner", props)
  );
  const [filesMini, setMiniFiles] = useState(
    images.length === 0 ? [] : getImage("miniBanner", props)
  );
  const [filesMiniSite, setMiniSiteFiles] = useState(
    images.length === 0 ? [] : getImage("miniSiteBanner", props)
  );
  const [filesResponsiveMiniSite, setResponsiveMiniSiteFiles] = useState(
    images.length === 0 ? [] : getImage("responsiveMiniSiteBanner", props)
  );
  const [isErrorBigExtension, setisErrorBigExtension] = useState(false);
  const [isErrorResponsiveExtension, setisErrorResponsiveExtension] =
    useState(false);
  const [isErrorMiniExtension, setisErrorMiniExtension] = useState(false);
  const [isErrorMiniSiteExtension, setisErrorMiniSiteExtension] =
    useState(false);
  const [
    isErrorResponsiveMiniSiteExtension,
    setisErrorResponsiveMiniSiteExtension,
  ] = useState(false);
  const [imagesState, setImagesState] = useState(images);
  const [isErrorBigBanner, setIsErrorBigBannerState] = useState(false);
  const [isErrorResponsiveBanner, setIsErrorResponsiveBannerState] =
    useState(false);
  const [isErrorMiniBanner, setIsErrorMiniBannerState] = useState(false);
  const [isErrorSiteBanner, setIsErrorSiteBannerState] = useState(false);
  const [
    isErrorResponsiveMiniSiteBanner,
    setIsErrorResponsiveMiniSiteBannerState,
  ] = useState(false);

  store.subscribe(() => {
    setIsErrorBigBannerState(
      store.getState().showAddFieldsErrors.isErrorBigBanner
    );
    setIsErrorResponsiveBannerState(
      store.getState().showAddFieldsErrors.isErrorResponsiveBanner
    );
    setIsErrorMiniBannerState(
      store.getState().showAddFieldsErrors.isErrorMiniBanner
    );
    setIsErrorSiteBannerState(
      store.getState().showAddFieldsErrors.isErrorSiteBanner
    );
    setIsErrorResponsiveMiniSiteBannerState(
      store.getState().showAddFieldsErrors.isErrorResponsiveMiniSiteBanner
    );
  });

  const BannerDropZone = () => {
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 610,
      height: 100,
      padding: 4,
      boxSizing: "border-box",
    };

    const { getRootProps, getInputProps } = useDropzone({
      accept: "image/*",
      multiple: false,
      onDrop: (acceptedFiles) => {
        setisErrorBigExtension(false);
        setBigHomeFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        );
        imageFile = acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        );

        if (verifyImage("showBanner", images)) {
          imageFiles = deleteImageFile("showBanner", images, imageFiles);
          images = deleteImage("showBanner", images);
        }
        imageFiles.push(imageFile[0]);

        images.push({
          name: "showBanner",
          url: "responsive_mini_site_banners/" + imageFiles[0].name,
          originalName: imageFiles[0].name,
        });

        setImagesState((imagesState) => (imagesState = images));
        props.updateStore({ imageFiles: imageFiles, images: images });
      },
      onError: setisErrorBigExtension(true),
    });

    const thumbs = filesBigHome.map((file) => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img src={file.preview} style={img} />
        </div>
      </div>
    ));

    useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        // filesBigHome.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [filesBigHome]
    );

    return (
      <div>
        <FormGroup>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <Label>Banner Grande para Home</Label>
            </Col>
            <Col xs={2}></Col>
          </Row>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <div {...getRootProps({ className: "dropzone" })}>
                <input {...getInputProps()} />
                {thumbs.length > 0 ? (
                  <div></div>
                ) : (
                  <p>
                    Arrastra la imagen aquí o haz click en este espacio para
                    seleccionarla.
                  </p>
                )}
                <div style={thumbsContainer}>{thumbs}</div>
              </div>
            </Col>
            <Col xs={2}></Col>
          </Row>
          {!verifyImage("showBanner", images) ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Banner Grande para Home"}
            </div>
          ) : null}
          {isErrorBigExtension ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Banner Grande para Home tipo JPG"}
            </div>
          ) : null}
        </FormGroup>
      </div>
    );
  };

  const ResponsiveBannerDropZone = () => {
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 330,
      height: 200,
      padding: 4,
      boxSizing: "border-box",
    };
    // const [files, setFiles] = useState([]);
    const { getRootProps, getInputProps } = useDropzone({
      accept: "image/*",
      onDrop: (acceptedFiles) => {
        setisErrorResponsiveExtension(false);
        setResponsiveFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        );

        if (verifyImage("responsiveBanner", images)) {
          imageFiles = deleteImageFile("responsiveBanner", images, imageFiles);
          images = deleteImage("responsiveBanner", images);
        }

        imageFiles.push(acceptedFiles[0]);

        images.push({
          name: "responsiveBanner",
          url: "responsive_mini_site_banners/" + imageFiles[0].name,
          originalName: imageFiles[0].name,
        });

        setImagesState((imagesState) => (imagesState = images));
        props.updateStore({ imageFiles: imageFiles, images: images });
      },
      onError: setisErrorResponsiveExtension(true),
    });

    const thumbs = filesResponsive.map((file) => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img src={file.preview} style={img} />
        </div>
      </div>
    ));

    useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        // files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [filesResponsive]
    );

    return (
      <div>
        <FormGroup>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <Label>Banner Responsive para Home</Label>
            </Col>
            <Col xs={2}></Col>
          </Row>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <div {...getRootProps({ className: "dropzone" })}>
                <input {...getInputProps()} />
                {thumbs.length > 0 ? (
                  <div></div>
                ) : (
                  <p>
                    Arrastra la imagen aquí o haz click en este espacio para
                    seleccionarla.
                  </p>
                )}
                <div style={thumbsContainer}>{thumbs}</div>
              </div>
            </Col>
            <Col xs={2}></Col>
          </Row>
          {!verifyImage("responsiveBanner", images) ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Banner Responsive para Home"}
            </div>
          ) : null}
          {isErrorResponsiveExtension ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {
                "Debe seleccionar una imagen Banner Responsive para Home tipo JPG"
              }
            </div>
          ) : null}
        </FormGroup>
      </div>
    );
  };

  const MiniBannerDropZone = () => {
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 270,
      height: 135,
      padding: 4,
      boxSizing: "border-box",
    };
    // const [files, setFiles] = useState([]);
    const { getRootProps, getInputProps } = useDropzone({
      accept: "image/*",
      onDrop: (acceptedFiles) => {
        setisErrorMiniExtension(false);
        setMiniFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        );

        if (verifyImage("miniBanner", images)) {
          imageFiles = deleteImageFile("miniBanner", images, imageFiles);
          images = deleteImage("miniBanner", images);
        }

        imageFiles.push(acceptedFiles[0]);

        images.push({
          name: "miniBanner",
          url: "responsive_mini_site_banners/" + imageFiles[0].name,
          originalName: imageFiles[0].name,
        });

        setImagesState((imagesState) => (imagesState = images));
        props.updateStore({ imageFiles: imageFiles, images: images });
      },
      onError: setisErrorMiniExtension(true),
    });

    const thumbs = filesMini.map((file) => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img src={file.preview} style={img} />
        </div>
      </div>
    ));

    useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        // files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [filesMini]
    );

    return (
      <div>
        <FormGroup>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <Label>Mini Banner para Programación</Label>
            </Col>
            <Col xs={2}></Col>
          </Row>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <div {...getRootProps({ className: "dropzone" })}>
                <input {...getInputProps()} />
                {thumbs.length > 0 ? (
                  <div></div>
                ) : (
                  <p>
                    Arrastra la imagen aquí o haz click en este espacio para
                    seleccionarla.
                  </p>
                )}
                <div style={thumbsContainer}>{thumbs}</div>
              </div>
            </Col>
            <Col xs={2}></Col>
          </Row>
          {!verifyImage("miniBanner", images) ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Mini Banner para Programación"}
            </div>
          ) : null}
          {isErrorMiniExtension ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {
                "Debe seleccionar una imagen Mini Banner para Programación tipo JPG"
              }
            </div>
          ) : null}
        </FormGroup>
      </div>
    );
  };

  const MiniSiteBannerDropZone = () => {
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 600,
      height: 300,
      padding: 4,
      boxSizing: "border-box",
    };

    const { getRootProps, getInputProps } = useDropzone({
      accept: "image/*",
      onDrop: (acceptedFiles) => {
        setisErrorMiniSiteExtension(false);
        setMiniSiteFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        );
        imageFiles.push(acceptedFiles[0]);

        if (verifyImage("miniSiteBanner", images)) {
          imageFiles = deleteImageFile("miniSiteBanner", images, imageFiles);
          images = deleteImage("miniSiteBanner", images);
        }

        images.push({
          name: "miniSiteBanner",
          url:
            "mini_site_banners/miniSiteBanner_tnoradio_" +
            props.getStore().showSlug +
            ".jpg",
          originalName: imageFiles[0].name,
        });
        setImagesState((imagesState) => (imagesState = images));
        props.updateStore({ imageFiles: imageFiles, images: images });
      },
      onError: setisErrorMiniSiteExtension(true),
    });

    const thumbs = filesMiniSite.map((file) => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img src={file.preview} style={img} />
        </div>
      </div>
    ));

    useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        // files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [filesMiniSite]
    );

    return (
      <div>
        <FormGroup>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <Label>Banner para Mini Site</Label>
            </Col>
            <Col xs={2}></Col>
          </Row>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <div {...getRootProps({ className: "dropzone" })}>
                <input {...getInputProps()} />
                {thumbs.length > 0 ? (
                  <div></div>
                ) : (
                  <p>
                    Arrastra la imagen aquí o haz click en este espacio para
                    seleccionarla.
                  </p>
                )}
                <div style={thumbsContainer}>{thumbs}</div>
              </div>
            </Col>
            <Col xs={2}></Col>
          </Row>
          {!verifyImage("miniSiteBanner", images) ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Banner para Mini Site"}
            </div>
          ) : null}
          {isErrorMiniSiteExtension ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Banner para Mini Site tipo JPG"}
            </div>
          ) : null}
        </FormGroup>
      </div>
    );
  };

  const ResponsiveMiniSiteBannerDropZone = () => {
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 600,
      height: 300,
      padding: 4,
      boxSizing: "border-box",
    };
    const { getRootProps, getInputProps } = useDropzone({
      accept: "image/*",
      onDrop: (acceptedFiles) => {
        console.log(acceptedFiles);

        imageFiles.push(acceptedFiles[0]);

        setisErrorResponsiveMiniSiteExtension(false);
        setResponsiveMiniSiteFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        );

        if (verifyImage("responsiveMiniSiteBanner", images)) {
          imageFiles = deleteImageFile(
            "responsiveMiniSiteBanner",
            images,
            imageFiles
          );
          images = deleteImage("responsiveMiniSiteBanner", images);
        }

        images.push({
          name: "responsiveMiniSiteBanner",
          url: "responsive_mini_site_banners/" + imageFiles[0].name,
          originalName: imageFiles[0].name,
        });

        setImagesState((imagesState) => (imagesState = images));
        props.updateStore({ imageFiles: imageFiles, images: images });
      },
      onError: setisErrorMiniSiteExtension(true),
    });

    const thumbs = filesResponsiveMiniSite.map((file) => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img src={file.preview} style={img} />
        </div>
      </div>
    ));

    useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        // files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [filesResponsiveMiniSite]
    );

    return (
      <div>
        <FormGroup>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <Label>Banner Responsive para Mini Site</Label>
            </Col>
            <Col xs={2}></Col>
          </Row>
          <Row>
            <Col xs={2}></Col>
            <Col xs={8}>
              <div {...getRootProps({ className: "dropzone" })}>
                <input {...getInputProps()} />
                {thumbs.length > 0 ? (
                  <div></div>
                ) : (
                  <p>
                    Arrastra la imagen aquí o haz click en este espacio para
                    seleccionarla.
                  </p>
                )}
                <div style={thumbsContainer}>{thumbs}</div>
              </div>
            </Col>
            <Col xs={2}></Col>
          </Row>
          {!verifyImage("responsiveMiniSiteBanner", images) ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {"Debe seleccionar una imagen Banner Responsive para Mini Site"}
            </div>
          ) : null}
          {isErrorMiniSiteExtension ? (
            <div
              className="invalid-feedback"
              style={{ display: "block", textAlign: "center" }}
            >
              {
                "Debe seleccionar una imagen Banner para el Mini Site Responsive tipo JPG o Webp"
              }
            </div>
          ) : null}
        </FormGroup>
      </div>
    );
  };

  return (
    <>
      <BannerDropZone></BannerDropZone>
      <ResponsiveBannerDropZone></ResponsiveBannerDropZone>
      <MiniBannerDropZone></MiniBannerDropZone>
      <ResponsiveMiniSiteBannerDropZone></ResponsiveMiniSiteBannerDropZone>
      <MiniSiteBannerDropZone></MiniSiteBannerDropZone>
    </>
  );
};

export default Step5;
