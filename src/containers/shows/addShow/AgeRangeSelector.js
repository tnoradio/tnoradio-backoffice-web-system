import React, { useState } from "react";
import { Col, Row, FormGroup, Label, CustomInput } from "reactstrap";

const ageRangeOptions = ["18-25", "26-35", "36-45", "46 and more"];

const AgeRangeSelector = ({ updateStore, getStore }) => {
  const [selectedAgeRanges, setSelectedAgeRanges] = useState(
    getStore().age_range || []
  );

  const handleAgeRangeChange = (ageRange) => {
    setSelectedAgeRanges((prevAgeRanges) => {
      const newAgeRanges = [...prevAgeRanges];
      const index = newAgeRanges.indexOf(ageRange);
      if (index !== -1) {
        newAgeRanges.splice(index, 1);
      } else {
        newAgeRanges.push(ageRange);
      }
      updateStore({ age_range: newAgeRanges });
      return newAgeRanges;
    });
  };

  return (
    <FormGroup>
      <Label for="age_range">Rango de Edad Objetivo</Label>
      <Row>
        <Col xs={6}>
          {ageRangeOptions.slice(0, 2).map((option) => (
            <FormGroup check className="px-0" key={option}>
              <CustomInput
                type="checkbox"
                id={option}
                label={option}
                value={selectedAgeRanges}
                checked={selectedAgeRanges.includes(option)}
                onChange={() => handleAgeRangeChange(option)}
              />
            </FormGroup>
          ))}
        </Col>
        <Col xs={6}>
          {ageRangeOptions.slice(2).map((option) => (
            <FormGroup check className="px-0" key={option}>
              <CustomInput
                type="checkbox"
                id={option}
                label={option}
                value={selectedAgeRanges}
                checked={selectedAgeRanges.includes(option)}
                onChange={() => handleAgeRangeChange(option)}
              />
            </FormGroup>
          ))}
        </Col>
      </Row>
    </FormGroup>
  );
};

export default AgeRangeSelector;
