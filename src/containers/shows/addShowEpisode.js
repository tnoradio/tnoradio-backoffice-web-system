import React, { useState, useEffect } from "react";
import {
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  ModalBody,
  ModalFooter,
  Button,
} from "reactstrap";
import Select from "react-select";
import formatGroupLabel from "../../components/common/formatGroupLabel";
import CustomMenuList from "../../components/shows/MenuList";
import addEpisodeTag from "../../redux/actions/shows/addEpisodeTag";
import TagsManager from "../../components/shows/tagsManager";
import fetchShowFileNames from "../../redux/actions/shows/fetchShowEpisodesFileNames";
import { getEpisodeTags } from "../../redux/actions/shows/getEpisodeTags";

const AddShowEpisode = ({
  onAddEpisode,
  seasons,
  episodeTags,
  toggleModalEpisodes,
  onAddSeason,
  showSlug,
}) => {
  const [episode, setEpisode] = useState({
    episode_number: "",
    title: "",
    description: "",
    url: "",
    miniature: "",
    tags: [],
  });
  const [season, setSeason] = useState({
    season_number: "",
    episodes: [],
  });
  const [selectedFileName, setSelectedFileName] = useState("");
  const [selectedSeason, setSelectedSeason] = useState({});
  const [selectedTags, setSelectedTags] = useState([]);
  const [isAddTagModalOpen, setIsAddTagModalOpen] = useState(false);
  const [fileNames, setFileNames] = useState([]);
  const [tagsForEpisodesState, setTagsForEpisodesState] = useState(
    episodeTags || []
  );

    useEffect(() => {
    if (episode) {
      const fetchEpisodeData = async () => {
        const [fileNames] = await Promise.all([
          fetchShowFileNames(showSlug),
          getEpisodeTags(showSlug),
        ]);
        setFileNames(fileNames);
      };

      fetchEpisodeData();
    }
  }, [showSlug, episode, season]);

  const toggleAddTagModal = () => {
    setIsAddTagModalOpen(!isAddTagModalOpen);
  };

  const onEpisodeNumberChange = (e) => {
    setEpisode({ ...episode, episode_number: e.target.value });
  };

  const onEpisodeTitleChange = (e) => {
    setEpisode({ ...episode, title: e.target.value });
  };

  const onEpisodeDescriptionChange = (e) => {
    setEpisode({ ...episode, description: e.target.value });
  };

  const onEpisodeUrlChange = (e) => {
    setSelectedFileName(e.value);
    setEpisode({ ...episode, url: e.value });
  };

  const onEpisodeMiniatureChange = (e) => {
    setEpisode({ ...episode, miniature: e.target.value });
  };

  const onSeasonNumberChange = (e) => {
    setSelectedSeason(e);
  };

  const onAddTag = (tag) => {
    setSelectedTags([...selectedTags, tag]);
    setEpisode({ ...episode, tags: selectedTags });
  };

  const onRemoveTag = (tag) => {
    setSelectedTags(selectedTags.filter((t) => t.id !== tag));
    setEpisode({ ...episode, tags: selectedTags });
  };

  const handleAddSeason = () => {
    // /onAddSeason(season);
  };

  const onEpisodeAdd = () => {
    onAddEpisode(episode);
    //setSeason({ ...season, episodes: [...season.episodes, episode] });
    setEpisode({
      episode_number: "",
      title: "",
      description: "",
      url: "",
      miniature: "",
      tags: [],
    });
  };

  return (
    <div className="row h-100">
      <div className="col-sm-12 my-auto">
        <div className="">
          <ModalBody>
            <ListGroup>
              <ListGroupItem>
                <Row>
                  <Col xs={6}>
                    <span className="w-100">
                      <label htmlFor="seasonNumber">Temporada</label>

                      <Select
                        defaultValue={{
                          label: seasons[0]?.season_number || 0,
                          value: seasons[0]?.id || 0,
                        }}
                        options={seasons?.map((season) => ({
                          label: season.season_number,
                          value: season.id,
                        }))}
                        formatGroupLabel={formatGroupLabel}
                        value={selectedSeason}
                        onChange={onSeasonNumberChange}
                        id="seasonNumber"
                      />
                    </span>
                    <Button
                      className="px-0"
                      color="link"
                      onClick={handleAddSeason}
                    >
                      Agregar Temporada
                    </Button>
                  </Col>
                  <Col xs={2}>
                    <span className="w-100">
                      <label htmlFor="episodeNumber">Episodio Nº</label>
                      <input
                        type="number"
                        value={episode.episode_number}
                        onChange={onEpisodeNumberChange}
                        name="episodeNumber"
                        id="episodeNumber"
                        className="w-100"
                        style={{ height: "44px" }}
                      />
                    </span>
                  </Col>
                  <Col xs={12}>
                    <span className="w-100">Título</span>
                    <input
                      className="w-100"
                      type="text"
                      value={episode.title}
                      onChange={onEpisodeTitleChange}
                      name="episodeTitle"
                      id="episodeTitle"
                    />
                  </Col>
                  <Col xs={12}>
                    <span className="w-100">Descripción </span>
                    <input
                      className="w-100"
                      type="text"
                      value={episode.description}
                      onChange={onEpisodeDescriptionChange}
                      name="episodeDescription"
                      id="episodeDescription"
                    />
                  </Col>
                  <Col xs={12}>
                    <span className=" w-100">Enlace </span>
                    {Array.isArray(fileNames) && fileNames.length > 0 ? (
                      <Select
                        defaultValue={{
                          label: fileNames[0]?.File_Name,
                          value: fileNames[0]?.File_Name,
                        }}
                        options={fileNames?.map((fileName) => ({
                          label: fileName.File_Name,
                          value: fileName.File_Name,
                        }))}
                        formatGroupLabel={formatGroupLabel}
                        value={{
                          label: selectedFileName,
                          value: selectedFileName,
                        }}
                        onChange={onEpisodeUrlChange}
                        id="fileNames"
                      />
                    ) : (
                      <span className="danger">No se encontraron archivos</span>
                    )}
                  </Col>
                  <Col xs={12}>
                    <span className="w-100">Enlace a Miniatura </span>
                    <input
                      type="text"
                      value={episode.miniature}
                      onChange={onEpisodeMiniatureChange}
                      name="episodeMiniature"
                      id="episodeMiniature"
                      className="w-100"
                    />
                  </Col>
                  <Col xs={12}>
                    <span className="w-100">Etiquetas </span>
                    <TagsManager
                      tags={selectedTags}
                      onAddTag={onAddTag}
                      onRemoveTag={onRemoveTag}
                    />
                  </Col>
                </Row>
              </ListGroupItem>
            </ListGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={onEpisodeAdd}>
              Agregar
            </Button>{" "}
            <Button color="secondary" onClick={toggleModalEpisodes}>
              Cancelar
            </Button>
          </ModalFooter>
        </div>
      </div>
    </div>
  );
};

export default AddShowEpisode;
