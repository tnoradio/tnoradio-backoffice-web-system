import { connect } from "react-redux";
import { setVisibilityFilter } from "../../redux/actions/shows";
import Link from "../../components/shows/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.showApp.showsVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setVisibilityFilter(ownProps.filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
