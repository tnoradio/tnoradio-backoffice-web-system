import { connect } from "react-redux";
import { showsSearch } from "../../redux/actions/shows";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.showApp.showsSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(showsSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
