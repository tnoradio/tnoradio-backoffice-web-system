import React, { useState } from "react";
import { Col, Row, FormGroup, Label, CustomInput } from "reactstrap";

const targetOptions = ["alto", "medio", "bajo"];

const TargetSelector = ({ updateStore, getStore }) => {
  const [selectedTargets, setSelectedTargets] = useState(
    getStore().target || []
  );

  const handleTargetChange = (target) => {
    setSelectedTargets((prevTargets) => {
      const newTargets = [...prevTargets];
      const index = newTargets.indexOf(target);
      if (index !== -1) {
        newTargets.splice(index, 1);
      } else {
        newTargets.push(target);
      }
      updateStore({ target: newTargets });
      return newTargets;
    });
  };

  return (
    <FormGroup>
      <Label for="target">
        Target (Nivel socio-económico de tu cliente ideal)
      </Label>
      <Row>
        <Col xs={6}>
          {targetOptions.slice(0, 2).map((target) => (
            <FormGroup check className="px-0" key={target}>
              <CustomInput
                type="checkbox"
                id={target}
                label={target}
                value={selectedTargets}
                checked={selectedTargets.includes(target)}
                onChange={() => handleTargetChange(target)}
              />
            </FormGroup>
          ))}
        </Col>
        <Col xs={6}>
          {targetOptions.slice(2).map((target) => (
            <FormGroup check className="px-0" key={target}>
              <CustomInput
                type="checkbox"
                id={target}
                label={target}
                value={selectedTargets}
                checked={selectedTargets.includes(target)}
                onChange={() => handleTargetChange(target)}
              />
            </FormGroup>
          ))}
        </Col>
      </Row>
    </FormGroup>
  );
};

export default TargetSelector;
