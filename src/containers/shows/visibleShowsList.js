import { connect } from "react-redux";
import {
  deleteShow,
  toggleEnabledShow,
  showsVisibilityFilter,
  showDetails,
  fetchShows,
  updateShow,
} from "../../redux/actions/shows";
import showsList from "../../components/shows/showsList";
import toggleActiveShow from "../../redux/actions/shows/toggleActiveShow";

const getVisibleShows = (shows, filter, showsSearch) => {
  switch (filter) {
    case showsVisibilityFilter.SHOW_ALL:
      let visibleShows;
      visibleShows = shows.filter(
        (s) =>
          !s.isDeleted &&
          // console.log(s)
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .includes(showsSearch.toLocaleLowerCase())
      );
      return visibleShows;
    case showsVisibilityFilter.ENABLED_SHOW:
      return shows.filter(
        (s) =>
          s.isEnabled &&
          !s.isDeleted &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );
    case showsVisibilityFilter.ACTIVE_SHOW:
      return shows.filter(
        (s) =>
          s.isActive &&
          !s.isDeleted &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );
    case showsVisibilityFilter.DELETED_SHOW:
      return shows.filter(
        (s) =>
          s.isDeleted &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.MOTIVATIONAL_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Motivacional" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.MUSIC_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Musical" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.CULTURE_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Cultura" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.SPORTS_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Deportes" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.ENTREPRENEURSHIP_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Emprendimiento" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.ENTERTAINMENT_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Entretenimiento" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.MAGAZINE_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Revista" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.HEALTH_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Salud" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    case showsVisibilityFilter.BUSINESS_SHOW:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          s.genre === "Economía y Negocios" &&
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(s.genre.toLocaleLowerCase())
            .includes(showsSearch.toLocaleLowerCase())
      );

    default:
      return shows.filter(
        (s) =>
          !s.isDeleted &&
          // console.log(s)
          s.name
            .toLocaleLowerCase()
            .concat(" ")
            .includes(showsSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    shows: getVisibleShows(
      state.showApp.shows,
      state.showApp.showsVisibilityFilter,
      state.showApp.showsSearch
    ),
    active: state.showApp.showDetails,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { toggleShow } = ownProps;
  return {
    fetchShows: dispatch(fetchShows()),
    deleteShow: (id) => dispatch(deleteShow(id)),
    toggleActiveShow: (id) => dispatch(toggleActiveShow(id)),
    showDetails: (id) => {
      toggleShow();
      return dispatch(showDetails(id));
    },
    toggleEnabledShow: (id) => dispatch(toggleEnabledShow(id)),
    toggleShow: (_id) => dispatch(toggleShow(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(showsList);
