import { connect } from "react-redux";
import fetchAllTopicMetrics from "../../redux/actions/dashboard/fetchAllTopicMetricsAction";
import fetchUsers from "../../redux/actions/users/fetchUsers";
import fetchUsersClassifiedByTopic from "../../redux/actions/dashboard/fetchUsersClassifiedByTopicsAction";
import TwitterDashboard from "../../components/dashboard/topics/twitterDashboard";
import updateTwitterBioTopicAction from "../../redux/actions/dashboard/updateTwitterBioTopic";

const mapStateToProps = (state) => {
  console.log(state);
  return {
    statistics: state.dashboardApp.getAllTopicMetricsNaiveBayesReducer,
    usersClassifiedByTopics:
      state.dashboardApp.GetUsersClassifiedByTopicReducer,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchAllTopicMetrics: dispatch(fetchAllTopicMetrics()),
    fetchAllUsers: dispatch(fetchUsers()),
    fetchUsersClassifiedByTopic: dispatch(fetchUsersClassifiedByTopic()),
    updateTwitterUserBioTopic: (userClassifiedByTopic, newTopic) =>
      dispatch(updateTwitterBioTopicAction(userClassifiedByTopic, newTopic)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TwitterDashboard);
