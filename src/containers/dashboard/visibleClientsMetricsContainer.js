import { connect } from "react-redux";
import ClientsDashboard from "../../components/dashboard/topics/clientsDashboard";
import { fetchClients } from "../../redux/actions/clients";

const calculateConvertionRate = (pCustomersCount, pUserCustomersCount) =>
  (pUserCustomersCount * 100) / pCustomersCount;

const mapStateToProps = (state) => {
  return {
    clients: state.clientApp.clients,
    tasksprogress: state.dashboardApp.GetAllTasksProgressReducer,
    statistics: state.dashboardApp.getAllTopicMetricsNaiveBayesReducer,
    instagram: state.dashboardApp.GetAllPublishedInstagramReducer,
    facebook: state.dashboardApp.GetAllPublishedFacebookReducer,
    tasks: state.dashboardApp.GetAllTasksProgressPerPersonReducer,
    tasks_users: state.dashboardApp.fetchUsersReducer,
    producersConvertionRate: calculateConvertionRate(
      state.clientApp.clients.filter(
        (client) => client.tpc_label === "productor"
      ).length,
      state.clientApp.clients.filter(
        (client) =>
          client.tpc_label === "productor" &&
          (client.pcustomer_isUser === true ||
            client.pcustomer_isUser === "True")
      ).length
    ),
    advertisersConvertionRate: calculateConvertionRate(
      state.clientApp.clients.filter(
        (client) => client.tpc_label === "anunciante"
      ).length,
      state.clientApp.clients.filter(
        (client) =>
          client.tpc_label === "anunciante" &&
          (client.pcustomer_isUser === true ||
            client.pcustomer_isUser === "True")
      ).length
    ),
  };
};
const mapDispatchToProps = async (dispatch) => {
  return {
    fetchPCustomers: await dispatch(fetchClients()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientsDashboard);
