import { connect } from "react-redux";
import fetchTasksProgressAction from "../../redux/actions/dashboard/fetchTasksProgressAction";
import fetchAllTopicMetrics from "../../redux/actions/dashboard/fetchAllTopicMetricsAction";
import fetchPublishedFacebook from "../../redux/actions/dashboard/fetchPublishedFacebookAction";
import fetchPublishedInstagram from "../../redux/actions/dashboard/fetchPublishedInstagramAction";
import fetchUsers from "../../redux/actions/users/fetchUsers";
import fetchTasksProgressPerPersonAction from "../../redux/actions/dashboard/fetchTasksProgressPerPersonAction";
import EmployeesTasksDashboard from "../../components/dashboard/topics/employeesTasksDashboard";

const mapStateToProps = (state) => {
  console.log(state);
  return {
    tasksprogress: state.dashboardApp.GetAllTasksProgressReducer,
    statistics: state.dashboardApp.getAllTopicMetricsNaiveBayesReducer,
    instagram: state.dashboardApp.GetAllPublishedInstagramReducer,
    facebook: state.dashboardApp.GetAllPublishedFacebookReducer,
    tasks: state.dashboardApp.GetAllTasksProgressPerPersonReducer,
    tasks_users: state.dashboardApp.fetchUsersReducer,
  };
};
const mapDispatchToProps = async (dispatch) => {
  return {
    fetchAllTopicMetrics: await dispatch(fetchAllTopicMetrics()),
    fetchAllPublishedFacebook: await dispatch(fetchPublishedFacebook()),
    fetchAllPublishedInstagram: await dispatch(fetchPublishedInstagram()),
    fetchAllTasksProgress: await dispatch(fetchTasksProgressAction()),
    fetchAllTasksProgressPerPerson: await dispatch(
      fetchTasksProgressPerPersonAction()
    ),
    fetchAllUsers: await dispatch(fetchUsers()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeesTasksDashboard);
