import React, { Component } from "react";
import { Form, Formik } from "formik";
import { Col, Row, FormGroup, Label } from "reactstrap";
import ShowDetails from "./recommendedShow";
import "react-datetime/css/react-datetime.css";
import { fetchRecommendedShows } from "../../../redux/actions/shows";
import { connect, useSelector } from "react-redux";
import { getRecomendedShows } from "../../../redux/actions/shows/fetchRecommendedShows";

const Step2 = (props) => {
  const [recommendedShows, setRecommendedShows] = React.useState([]);
  const state = useSelector((state) => state);
  const setSelectedRecommendedShowId = (selectedShow) => {
    props.updateStore({ selectedRecommendedShowId: selectedShow.id });
    props.updateStore({ show: selectedShow });
  };

  React.useEffect(() => {
    getShowsRecommended();
  }, []);

  const getShowsRecommended = async () => {
    var shows = await getRecomendedShows(
      props.initStore.genders,
      props.initStore.interests,
      props.initStore.targets,
      props.initStore.age_ranges
    );
    console.log(shows);
    setRecommendedShows(shows);
  };

  var size = 3;
  return (
    <div>
      <Formik>
        {({ errors, touched }) => (
          <Form>
            <Row>
              <Col xs={1}></Col>
              <Col xs={10}>
                <FormGroup>
                  <FormGroup style={{ zIndex: 1000 }}>
                    <Label for="recomendedShows">Programas Recomendados</Label>
                    {recommendedShows?.map((show) => {
                      console.log(show);
                      return (
                        <ShowDetails
                          key={show.id}
                          selectedShow={show.show}
                          selectRecommendedShow={() =>
                            setSelectedRecommendedShowId(show)
                          }
                          getStore={() => props.initStore}
                        />
                      );
                    })}
                  </FormGroup>
                </FormGroup>
              </Col>
              <Col xs={1}></Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Step2;
