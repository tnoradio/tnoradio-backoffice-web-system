import React, { Fragment, useEffect, useState } from "react";
import { Card, CardBody, CardTitle, Row, Col } from "reactstrap";
import StepZilla from "react-stepzilla";
import Step1 from "./addCampaign-Step1";
import Step2 from "./addCampaign-Step2";
import Step3 from "./addCampaign-Step3";
import Step4 from "./addCampaign-Step4";
import Step5 from "./addCampaign-Step5";
import Step6 from "./addCampaign-Step6";
import "../../../assets/scss/views/form/wizard.scss";
import { connect, useDispatch, useSelector } from "react-redux";
import { fetchShowGenres } from "../../../redux/actions/shows";
import { addCampaign } from "../../../redux/actions/campaigns";
import { addNotification } from "../../../redux/actions/notifications";

const mapStateToProps = (state) => {
  console.log(state);
  if (
    state.showApp.showGenres !== null &&
    state.showApp.showGenres !== undefined &&
    state.userApp.users !== null &&
    state.userApp.users !== undefined
  )
    return {
      id: undefined,
      showGenres: state.showApp.showGenres.showGenres,
      users: state.userApp.users,
      adminUsers: state.userApp.users.filter((user) => {
        if (user.roles.some((role) => role.role === "ADMIN")) return user;
      }),
    };
  else
    return {
      id: undefined,
      showGenres: [],
      users: [],
    };
};

const AddCampaignWizard = ({ users, showGenres }) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [step, setStep] = useState(0);
  const [initStore, setInitStore] = useState({
    price: 0,
    owner_id: "", // user id of owner, person that added the campaign
    start_date: new Date(Date.now()),
    end_date: new Date(Date.now()),
    channels: [],
    name: "",
    show: null,
    brand_type: "",
    starred: false,
    deleted: false,
    isActive: false,
    owner: "",
    payments: [],
    rotarySchedules: [],
    targets: [],
    age_ranges: [],
    interests: [],
    genders: [],
    socials: [],
    geolocalizations: [],
    selectedRecommendedShowId: 0,
    savedToCloud: false,
  });

  function getStore() {
    return initStore;
  }

  function updateStore(update) {
    setInitStore({
      ...initStore,
      ...update,
    });
  }

  useEffect(() => {}, [initStore]);
  useEffect(() => {
    dispatch(fetchShowGenres());
  }, []);

  let steps = [
    {
      name: "A quién se dirije tu campaña?",
      component: (
        <Step1
          topics={showGenres}
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
          getStoreSteps={() => getStoreSteps()}
        />
      ),
    },
    {
      name: "Programas Recomendados",
      component: (
        <Step2
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Pautar en Rotativa",
      component: (
        <Step3
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          users={users}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Pautar en Redes Sociales",
      component: (
        <Step4
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
        />
      ),
    },

    {
      name: "Validar y Guardar",
      component: (
        <Step5
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
          initStore={initStore}
        />
      ),
    },
    {
      name: "Fin",
      component: (
        <Step6
          getStore={() => getStore()}
          updateStore={(u) => {
            updateStore(u);
          }}
        />
      ),
    },
  ];

  function getStoreSteps() {
    return steps;
  }

  function updateStoreSteps(update) {
    steps = [...steps, ...update];
  }

  const handleStepChange = (step) => {
    setStep(step);

    if (step === 5) {
      dispatch(
        addCampaign(
          initStore.name,
          initStore.age_ranges,
          initStore.targets,
          initStore.brand_type,
          initStore.geolocalizations,
          initStore.interests,
          initStore.genders,
          initStore.price,
          state.session?.user._id,
          initStore.start_date,
          initStore.end_date,
          initStore.channels,
          false,
          false,
          false,
          []
        )
      );
      state.adminUsers.forEach((user) =>
        dispatch(
          addNotification(
            undefined,
            state.session?.user._id,
            "Se ha creado una campaña publicitaria",
            true,
            "CAMPAIGNS"
          )
        )
      );
    }
  };
  return (
    <Fragment>
      <Row>
        <Col sm="12">
          <Card>
            <CardBody>
              <CardTitle>INFORMACIÓN REQUERIDA</CardTitle>
              <div className="step-progress">
                <StepZilla
                  steps={steps}
                  preventEnterSubmission={true}
                  nextTextOnFinalActionStep={"Guardar"}
                  hocValidationAppliedTo={[]}
                  startAtStep={step}
                  onStepChange={(step) => handleStepChange(step)}
                  getStore={() => getStore()}
                  updateStore={(u) => {
                    updateStore(u);
                  }}
                />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Fragment>
  );
};

export default connect(mapStateToProps)(AddCampaignWizard);
