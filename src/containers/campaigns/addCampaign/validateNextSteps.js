import { idText } from "typescript";
import { store } from "../../../redux/storeConfig/store";

/**
 * Verify if it can go to the next step
 * @param {Number} step Step to verify
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
export function verifyStep(step, storeLocal) {
  switch (step) {
    case 0:
      return verifyStepOne(storeLocal);
      break;
    case 1:
      return verifyStepTwo(storeLocal);
      break;
    case 2:
      return verifyStepThree(storeLocal);
      break;
    case 3:
      return verifyStepFour(storeLocal);
      break;
    case 4:
      return verifyStepFive(storeLocal);
      break;
    default:
      return true;
      break;
  }
}

/**
 * Verify the step one when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepOne(storeLocal) {
  console.log(storeLocal);
  if (
    storeLocal.targets !== [] &&
    storeLocal.age_range !== [] &&
    storeLocal.interests !== [] &&
    storeLocal.geolocalizations !== [] &&
    storeLocal.brand_type !== ""
  ) {
    store.dispatch({
      type: "UPDATE",
      targets: storeLocal.targets,
      age_range: storeLocal.age_range,
      interests: storeLocal.interests,
      geolocalizations: storeLocal.geolocalizations,
      brand_type: storeLocal.brand_type,
    });
    return true;
  }
  return false;
}

/**
 * Verify the step two when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepTwo(storeLocal) {
  return true;
}

/**
 * Verify the step three when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepThree(storeLocal) {
  return true;
}

/**
 * Verify the step four when registering program
 * @returns true if can go farther, otherwise false
 */
function verifyStepFour() {
  return true;
}

/**
 * Verify the step five when registering program
 * @param {Object} storeLocal Structure with the variables to verify
 * @returns true if can go farther, otherwise false
 */
function verifyStepFive(storeLocal) {
  return true;
}

export function isEmptyField(element) {
  if (element !== "") {
    return false;
  }
  return true;
}

export function isValidEmail(element) {
  let pattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

  if (pattern.exec(element) === null) {
    return false;
  }
  return true;
}

export function notifyErrors(step, storeLocal) {
  // Primer paso
  if (step === 0) {
    if (isEmptyField(storeLocal.targets)) {
      store.dispatch({ type: "UPDATE", isErrorTargets: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorTargets: false });
    }
    if (isEmptyField(storeLocal.age_range)) {
      store.dispatch({ type: "UPDATE", isErrorAgeRanges: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorAgeRanges: false });
    }
    if (isEmptyField(storeLocal.email) || !isValidEmail(storeLocal.email)) {
      store.dispatch({ type: "UPDATE", isErrorEmail: true });
    } else {
      store.dispatch({ type: "UPDATE", isErrorEmail: false });
    }
  }
}
