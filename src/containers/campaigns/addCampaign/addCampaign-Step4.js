import React, { Component } from "react";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  //CustomInput,
  ModalHeader,
} from "reactstrap";
import Select from "react-select";

import "react-datetime/css/react-datetime.css";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

//import { store } from "../../../redux/storeConfig/store";
import { AtSign, Trash } from "react-feather";

const socialNetworks = [
  { value: "FACEBOOK", label: "Facebook", url: "https://www.facebook.com/" },
  { value: "TWITTER", label: "Twitter", url: "https://www.twitter.com/" },
  { value: "INSTAGRAM", label: "Instagram", url: "https://www.instagram.com/" },
  { value: "SNAPCHAT", label: "Snapchat", url: "" },
  { value: "TIK_TOK", label: "Tik Tok", url: "https://www.tiktok.com/@" },
  { value: "YOUTUBE", label: "Youtube", url: "" },
  { value: "OTHER", label: "Otra", url: "" },
];

const socialsGroupOptions = [
  {
    label: "Redes Sociales",
    options: socialNetworks,
  },
];

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const socialsFormSchema = Yup.object().shape({
  qty: Yup.number().required("Debes agregar una qtyidad."),
});

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

let socials = [];

export default class Step4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      socialsModal: false,
      socialNetwork: false,
      socialsState: [],
    };
  }

  render() {
    const onDeleteSocialClick = (social) => {
      socials = socials.filter(
        (deleteSocial) =>
          deleteSocial.qty !== social.qty &&
          deleteSocial.socialNetwork !== social.socialNetwork
      );
      this.setState({ socialsState: socials });
      this.props.updateStore({ socials: socials });
    };

    const addSocialSubmit = (social) => {
      if (social.socialNetwork !== undefined && social.qty !== undefined) {
        socials.push({
          name: this.state.socialNetwork,
          qty: social.qty,
          url: social.url,
        });
        this.setState({ socialsState: socials, socialsModal: false });
        this.props.updateStore({ socials: socials });
      }
    };

    return (
      <div>
        <Row>
          <Col xs={2}></Col>
          <Col xs={8}>
            <Label>Redes Sociales del Programa</Label>
            <ListGroup>
              {this.props.getStore().socials.map((social) => {
                return (
                  <ListGroupItem key={social.name + social.qty}>
                    <Row>
                      <Col xs={1}></Col>
                      <Col xs={4}>
                        <span>{social.name}</span>
                        {"  "}
                      </Col>
                      <Col xs={4}>
                        <span>{social.qty} veces por semana</span>{" "}
                      </Col>
                      <Col xs={2}>
                        <Trash
                          size={18}
                          className="float-right mt-1 mb-2 width-25 d-block"
                          style={{
                            color: "#FF586B",
                            "&:hover": {
                              cursor: "pointer",
                            },
                          }}
                          onClick={() => {
                            onDeleteSocialClick(social);
                          }}
                        />
                      </Col>
                      <Col xs={1}></Col>
                    </Row>
                  </ListGroupItem>
                );
              })}
            </ListGroup>
          </Col>
          <Col xs={2}></Col>
        </Row>
        <Row>
          <Col xs={2}></Col>
          <Col xs={8}>
            <br></br>
            <Button
              className="btn-square float-right"
              size="sm"
              outline
              color="primary"
              onClick={() => this.setState({ socialsModal: true })}
            >
              <AtSign size={16} color="#009DA0" />
              {"   "}
              Agregar Red Social
            </Button>
          </Col>
          <Col xs={2}></Col>
        </Row>
        <Modal
          isOpen={this.state.socialsModal}
          toggle={() =>
            this.setState({ socialsModal: !this.state.socialsModal })
          }
        >
          <ModalHeader>
            <Label>Agregar Red Social</Label>
          </ModalHeader>
          <Formik
            initialValues={{
              qty: 0,
              url: "",
              socialNetwork: "",
            }}
            validationSchema={socialsFormSchema}
            onSubmit={(values) => {
              addSocialSubmit(values);
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <ModalBody>
                  {socials !== undefined &&
                  socialNetworks[0] !== undefined &&
                  socialNetworks[0] !== null ? (
                    <div>
                      <Row>
                        <Col xs={12}>
                          <FormGroup>
                            <Select
                              defaultValue={socialNetworks[0].label}
                              options={socialsGroupOptions}
                              formatGroupLabel={formatGroupLabel}
                              value={socialNetworks.find(
                                (obj) => obj === this.state.socialsState
                              )}
                              onChange={(e) =>
                                this.setState({ socialNetwork: e.label })
                              }
                              id="socialNetwork"
                              name="socialNetwork"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={9}>
                          <Label for="name">
                            Cantidad de transmisiones por semana
                          </Label>
                        </Col>
                        <Col xs={3}>
                          <Field
                            className={`form-control ${
                              errors.qty && touched.qty && "is-invalid"
                            }`}
                            name="qty"
                            id="qty"
                            type="number"
                            required
                          />
                          {errors.qty && touched.qty ? (
                            <div className="invalid-feedback">{errors.qty}</div>
                          ) : null}
                        </Col>
                      </Row>
                    </div>
                  ) : (
                    <Row></Row>
                  )}
                </ModalBody>
                <ModalFooter>
                  <Button
                    className="btn-square"
                    outline
                    color="primary"
                    onClick={addSocialSubmit}
                    type="submit"
                  >
                    Agregar
                  </Button>{" "}
                </ModalFooter>
              </Form>
            )}
          </Formik>
        </Modal>
      </div>
    );
  }
}
