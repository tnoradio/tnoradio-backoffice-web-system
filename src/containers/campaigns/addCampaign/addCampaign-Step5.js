import React, { Fragment, useEffect } from "react";
import { Table, Media } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";

const Step6 = (props) => {
  const setChannels = () => {
    const channels = props.getStore().channels;
    if (props.getStore().show !== null) {
      channels.push({ type: "SHOW", show_id: props.getStore().show.id });
      props.updateStore({ channels: channels });
    }
    if (props.getStore().socials !== []) {
      props.getStore().socials.forEach((social) => {
        channels.push({
          type: "SOCIAL_NETWORK",
          name: social.name,
          qty: social.qty,
        });
      });
    }
    if (props.getStore().rotarySchedules !== null) {
      channels.push({
        type: "ROTARY",
        schedules: props.getStore().rotarySchedules,
      });
    }

    props.updateStore({ channels: channels });
  };

  useEffect(() => {
    setChannels();
  }, []);

  return (
    <Fragment>
      <div className="contact-app-content-detail">
        <PerfectScrollbar>
          <Media
            style={{
              color: "white",
              padding: "1.5rem",
            }}
            className="gradient-purple-bliss"
            heading
          >
            <span> Canales de Comercialización</span>
          </Media>
          <Table responsive borderless size="sm" className="mt-4">
            <tbody>
              <tr className="d-flex">
                <td className="col-3"></td>
                <td className="col-2 text-bold-400">Programa:</td>
                <td className="col-6">
                  <div>{props.getStore().show.name}</div>
                </td>
                <td className="col-2"></td>
              </tr>
              <tr className="d-flex">
                <td className="col-3"></td>
                <td className="col-2 text-bold-400">Redes Sociales:</td>
                <td className="col-6">
                  {props.getStore().socials.map((social) => {
                    return (
                      <div key={"social" + social.name + social.qty}>
                        {social.name} {social.qty} veces por semana
                      </div>
                    );
                  })}
                </td>
                <td className="col-2"></td>
              </tr>
              <tr className="d-flex">
                <td className="col-3"></td>
                <td className="col-2 text-bold-400">Rotativa:</td>
                <td className="col-6">
                  {props.getStore().rotarySchedules.map((rotarySchedule) => {
                    return (
                      <div>
                        <span>{rotarySchedule.weekDay}</span>{" "}
                        <span>de {rotarySchedule.startTime}</span>{" "}
                        <span>a {rotarySchedule.endTime}</span>
                      </div>
                    );
                  })}
                </td>
                <td className="col-3"></td>
              </tr>
            </tbody>
          </Table>
        </PerfectScrollbar>
      </div>
    </Fragment>
  );
};

export default Step6;
