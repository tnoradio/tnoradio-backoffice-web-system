import React, { Component } from "react";
import { Form, Formik, Field } from "formik";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Col,
  Row,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  CustomInput,
  ModalHeader,
} from "reactstrap";

import DateTime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";
import { Trash, Clock } from "react-feather";
import { store } from "../../../redux/storeConfig/store";

const formSchema = Yup.object().shape({
  start: Yup.date().required("Debes seleccionar una hora de inicio."),
  end: Yup.date().required("Debes seleccionar una hora de finalización."),
});

export default class Step2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start: "",
      end: "",
      isErrorNotScheduleSelected: false,
      isErrorNotSelectedHours: false,
      monday: false,
      modal: false,
      tuesday: false,
      wednesday: false,
      startDate: new Date(),
      endDate: new Date(),
      thursday: false,
      friday: false,
      saturday: false,
      sunday: false,
      weekDay: "",
      scheduleState: this.props.getStore().rotarySchedules,
      aDayIsSelected: false,
      isErrorCrash: false,
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState({
        isErrorNotScheduleSelected:
          store.getState().showAddFieldsErrors.isErrorNotScheduleSelected,
      });
    });
  }

  verifySchedule = (schedules, newShedule) => {
    var pass = true;
    var weekDay = false;
    for (let i = 0; i < schedules.length; i++) {
      var element = schedules[i];
      if (element.weekDay === newShedule.weekDay) {
        weekDay = true;
        if (newShedule.startDate < element.startDate) {
          if (newShedule.endDate > element.startDate) {
            pass = false;
            break;
          }
        }
        if (newShedule.startDate > element.startDate) {
          if (newShedule.startDate < element.endDate) {
            pass = false;
            break;
          } else {
            if (newShedule.startDate < element.endDate) {
              if (newShedule.endDate > element.endDate) {
                pass = false;
                break;
              }
            }
          }
        }
      }
    }
    if (!weekDay || schedules.length === 0) {
      pass = true;
    }
    return pass;
  };

  render() {
    let timeInitProps = {
      placeholder: "00:00:00",
    };

    const resetScheduleForm = () => {
      this.setState({
        weekDay: "",
        start: "",
        startDate: new Date(),
        endDate: new Date(),
        end: "",
        monday: false,
        wednesday: false,
        thursday: false,
        tuesday: false,
        friday: false,
        saturday: false,
        sunday: false,
        aDayIsSelected: false,
      });
    };

    const addSchedule = () => {
      let rotarySchedules = this.props.getStore().rotarySchedules;
      if (this.state.start !== "" && this.state.end !== "") {
        store.dispatch({ type: "UPDATE", isErrorNotScheduleSelected: false });
        let schedule = {
          weekDay: this.state.weekDay,
          startTime: this.state.start,
          endTime: this.state.end,
          startDate: this.state.startDate,
          endDate: this.state.endDate,
        };
        if (this.verifySchedule(rotarySchedules, schedule)) {
          rotarySchedules.push(schedule);
          this.setState({ isErrorCrash: false, modal: false });
        } else {
          this.setState({ isErrorCrash: true, modal: true });
        }
        this.setState({ scheduleState: rotarySchedules });
        this.props.updateStore({ rotarySchedules: rotarySchedules });
        resetScheduleForm();
        this.setState({ isErrorNotSelectedHours: false });
      } else {
        this.setState({ isErrorNotSelectedHours: true });
      }
    };

    const handleStartDateTimeChange = (date) => {
      let startTime = moment(date._d).format("hh:mm a");
      this.setState({
        start: startTime,
        aDayIsSelected: false,
        startDate: date._d,
      });
    };

    const handleEndDateTimeChange = (date) => {
      let endTime = moment(date._d).format("hh:mm a");
      this.setState({ end: endTime, aDayIsSelected: false, endDate: date._d });
    };

    const onDeleteScheduleClick = (rotarySchedule) => {
      var schedules = [];
      this.props.getStore().rotarySchedules.map((item) => {
        if (
          item.weekDay !== rotarySchedule.weekDay ||
          item.startTime !== rotarySchedule.startTime ||
          item.endTime !== rotarySchedule.endTime
        ) {
          schedules.push(item);
        }
      });
      // let rotarySchedules = this.props.getStore().rotarySchedules.filter(
      //    scheduleItem => rotarySchedule.startTime !== scheduleItem.startTime &&
      //                   rotarySchedule.endTime !== scheduleItem.endTime &&
      //                   rotarySchedule.weekDay !== scheduleItem.weekDay
      // )
      // this.props.updateStore( {rotarySchedules: this.state.schedulesState});
      this.setState({ schedulesState: schedules });
      this.props.updateStore({ rotarySchedules: schedules });
    };

    const handleChangeRadioInput = (weekday) => {
      switch (weekday) {
        case "lunes":
          this.setState({
            weekDay: weekday,
            monday: true,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        case "martes":
          this.setState({
            weekDay: weekday,
            monday: false,
            tuesday: true,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        case "miercoles":
          this.setState({
            weekDay: weekday,
            monday: false,
            tuesday: false,
            wednesday: true,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        case "jueves":
          this.setState({
            weekDay: weekday,
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: true,
            friday: false,
            saturday: false,
            sunday: false,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        case "viernes":
          this.setState({
            weekDay: weekday,
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: true,
            saturday: false,
            sunday: false,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        case "sabado":
          this.setState({
            weekDay: weekday,
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: true,
            sunday: false,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        case "domingo":
          this.setState({
            weekDay: weekday,
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: true,
            noDayIsSelected: noDayIsSelected(),
          });
          break;
        default:
          break;
      }
    };

    const noDayIsSelected = () => {
      if (
        this.state.sunday === true ||
        this.state.monday === true ||
        this.state.tuesday === true ||
        this.state.wednesday === true ||
        this.state.thursday === true ||
        this.state.friday === true ||
        this.state.saturday === true
      )
        if (this.state.start !== "" && this.state.end !== "") {
          return false;
        } else {
          return true;
        }
      else {
        return true;
      }
    };

    const isInvalidTime = (start, end) => {
      if (start <= end) {
        return false;
      } else {
        return true;
      }
    };

    return (
      <div>
        <Formik
          initialValues={{
            start: new Date(Date.now()),
            end: new Date(Date.now()),
          }}
          validationSchema={formSchema}
        >
          {({ errors, touched }) => (
            <Form>
              <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                  <Label>Horarios de Transmisión del Comercial</Label>
                  <ListGroup>
                    {this.props
                      .getStore()
                      .rotarySchedules.map((rotarySchedule) => {
                        // console.log(rotarySchedule);
                        return (
                          <ListGroupItem key={rotarySchedule.weekDay}>
                            <Row>
                              <Col xs={10}>
                                <span>{rotarySchedule.weekDay}</span>{" "}
                                <span>de {rotarySchedule.startTime}</span>{" "}
                                <span>a {rotarySchedule.endTime}</span>
                              </Col>
                              <Col xs={2}>
                                <Trash
                                  size={18}
                                  onClick={() => {
                                    onDeleteScheduleClick(rotarySchedule);
                                  }}
                                  className="float-center mt-1 mb-2 width-25 d-block"
                                  style={{
                                    color: "#FF586B",
                                    cursor: "pointer",
                                  }}
                                />
                              </Col>
                            </Row>
                          </ListGroupItem>
                        );
                      })}
                  </ListGroup>
                </Col>
                <Col xs={3}></Col>
              </Row>
              <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                  <br></br>
                  <Button
                    className="btn-square float-right"
                    size="sm"
                    outline
                    color="primary"
                    onClick={() => this.setState({ modal: true })}
                  >
                    <Clock size={16} color="#009DA0" />
                    {"  "}
                    Agregar Horario
                  </Button>
                  <br></br>
                </Col>
                <Col xs={3}></Col>
              </Row>
              {this.state.isErrorNotScheduleSelected ? (
                <div
                  className="invalid-feedback"
                  style={{ display: "block", textAlign: "center" }}
                >
                  {"Debe indicar al menos un horario para el Programa"}
                </div>
              ) : null}

              <Modal
                isOpen={this.state.modal}
                toggle={() => this.setState({ modal: !this.state.modal })}
              >
                <ModalHeader>
                  <Label>Agregar Horario</Label>
                </ModalHeader>
                <ModalBody>
                  <FormGroup tag="fieldset">
                    <h6>Seleccione día de la semana</h6>
                    <Row>
                      <Col xs={6}>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="monday"
                            name="customRadio"
                            label="Lunes"
                            onClick={() => handleChangeRadioInput("lunes")}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="tuesday"
                            name="customRadio"
                            label="Martes"
                            onClick={() => handleChangeRadioInput("martes")}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="wednesday"
                            name="customRadio"
                            label="Miércoles"
                            onClick={() => handleChangeRadioInput("miercoles")}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="thursday"
                            name="customRadio"
                            label="Jueves"
                            onClick={() => handleChangeRadioInput("jueves")}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={6}>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="friday"
                            name="customRadio"
                            label="Viernes"
                            onClick={() => handleChangeRadioInput("viernes")}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="saturday"
                            name="customRadio"
                            label="Sábado"
                            onClick={() => handleChangeRadioInput("sabado")}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="radio"
                            id="sunday"
                            name="customRadio"
                            label="Domingo"
                            onClick={() => handleChangeRadioInput("domingo")}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </FormGroup>
                  <Row>
                    <Col xs={6}>
                      <div>
                        <Label>Hora de Inicio</Label>
                        <Field name="start">
                          {({ field, form, meta }) => (
                            <DateTime
                              name="start"
                              className={
                                errors.start && touched.start && "is-invalid"
                              }
                              dateFormat={false}
                              onChange={(e) => handleStartDateTimeChange(e)}
                              value={this.state.start}
                              inputProps={timeInitProps}
                            />
                          )}
                        </Field>
                      </div>
                    </Col>
                    <Col xs={6}>
                      <div>
                        <Label>Hora de Finalización</Label>
                        <Field name="end">
                          {({ field, form, meta }) => (
                            <DateTime
                              name="end"
                              className={""}
                              dateFormat={false}
                              onChange={(e) => handleEndDateTimeChange(e)}
                              value={this.state.end}
                              inputProps={timeInitProps}
                            />
                          )}
                        </Field>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    {this.state.isErrorNotSelectedHours ? (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block", textAlign: "center" }}
                      >
                        {"Debe indicar una hora de inicio y fin válida"}
                      </div>
                    ) : null}
                    {this.state.isErrorCrash ? (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block", textAlign: "center" }}
                      >
                        {
                          "Debe indicar una hora de inicio y fin válida. Existe choque de horarios"
                        }
                      </div>
                    ) : null}
                  </Row>
                </ModalBody>
                <ModalFooter>
                  <Button
                    className="btn-square"
                    outline
                    color="primary"
                    onClick={addSchedule}
                    size="sm"
                    disabled={isInvalidTime(this.state.start, this.state.end)}
                  >
                    Agregar
                  </Button>{" "}
                </ModalFooter>
              </Modal>
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}
