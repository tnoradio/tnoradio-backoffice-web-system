import React, { Component } from "react";
import { Col, Row, FormGroup, Label, CustomInput, Input } from "reactstrap";
import Select from "react-select";
import { Form, Formik } from "formik";
import { store } from "../../../redux/storeConfig/store";
const { getCountries } = require("country-list-spanish");

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

export default class Step1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      age_ranges: [],
      targets: [],
      genders: [],
      geolocalizations: [], // Cities, states, countries
      interests: [],
      ageGroupedOptions: [],
      targetGroupedOptions: [],
      geoGroupedOptions: [],
      interestGroupedOptions: [],
      selectedRecommendedShowId: 0,
      initStore: this.props.initStore,
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState({
        //  isErrorName: store.getState().showAddFieldsErrors.isErrorName,
        // isErrorSynopsis: store.getState().showAddFieldsErrors.isErrorSynopsis,
        // isErrorEmail: store.getState().showAddFieldsErrors.isErrorEmail,
      });
    });
  }

  render() {
    const countries = () => {
      let countries = [];
      try {
        getCountries().forEach(
          (country) =>
            country === "Venezuela" /*||
              country === "España" ||
              country === "Puerto Rico" ||
              country === "Costa Rica" ||
              country === "México" ||
              country === "Chile" ||
              country === "Colombia" ||
              country === "Argentina" ||
              country === "Ecuador" ||
              country === "Perú" ||
              country === "Estados Unidos de América"*/ &&
            countries.push({ label: country, value: country })
        );
      } catch (error) {}

      return countries;
    };

    let countryGroupedOptions = [
      {
        label: "Países",
        options: countries(),
      },
    ];

    let interestGroupedOptions = [
      {
        label: "Tópicos de Interés",
        options: this.props.topics?.map((topic) => {
          return { label: topic.genre, value: topic.genre };
        }),
      },
    ];

    const handleAgeRangeChange = (selected_age_range) => {
      var age_range = this.state.age_ranges;
      switch (selected_age_range) {
        case "18-25":
          age_range.some((ageRange) => ageRange === "18-25")
            ? age_range.pop("18-25")
            : age_range.push("18-25");
          this.setState({ age_range: age_range });
          this.props.updateStore({ age_ranges: age_range });
          break;
        case "26-35":
          age_range.some((ageRange) => ageRange === "26-35")
            ? age_range.pop("26-35")
            : age_range.push("26-35");
          this.setState({ age_range: age_range });
          this.props.updateStore({ age_ranges: age_range });
          break;
        case "36-45":
          age_range.some((ageRange) => ageRange === "36-45")
            ? age_range.pop("36-45")
            : age_range.push("36-45");
          this.setState({ age_range: age_range });
          this.props.updateStore({ age_ranges: age_range });
          break;
        case "46 and more":
          age_range.some((ageRange) => ageRange === 3)
            ? age_range.pop("46 and more")
            : age_range.push("46 and more");
          this.setState({ age_range: age_range });
          this.props.updateStore({ age_ranges: age_range });
          break;
        default:
          break;
      }
    };

    const noAgeRangeIsSelected = () => {
      if (this.state.age_range.length > 0) return false;
      else return true;
    };

    const handleTargetChange = (selected_target) => {
      var targets = this.state.targets;
      switch (selected_target) {
        case "alto":
          targets.some((target) => target === "alto")
            ? targets.pop("alto")
            : targets.push("alto");
          this.props.updateStore({ targets: targets });
          this.setState({ targets: targets });
          break;
        case "medio":
          targets.some((target) => target === "medio")
            ? targets.pop("medio")
            : targets.push("medio");
          this.setState({ targets: targets });
          this.props.updateStore({ targets: targets });
          break;
        case "bajo":
          targets.some((target) => target === "bajo")
            ? targets.pop("bajo")
            : targets.push("bajo");
          this.setState({ targets: targets });
          this.props.updateStore({ targets: targets });
          break;
        default:
          break;
      }
    };

    const noTargetIsSelected = () => {
      if (this.state.targets.length > 0) return false;
      else return true;
    };

    const handleGenderChange = (selected_gender) => {
      var genders = this.state.genders;
      switch (selected_gender) {
        case "male":
          genders.some((gender) => gender === "male")
            ? genders.pop("male")
            : genders.push("male");
          this.setState({ genders: genders });
          this.props.updateStore({ genders: genders });
          break;
        case "female":
          genders.some((gender) => gender === "female")
            ? genders.pop("female")
            : genders.push("female");
          this.setState({ genders: genders });
          this.props.updateStore({ genders: genders });
          break;
        case "other":
          genders.some((gender) => gender === "other")
            ? genders.pop("other")
            : genders.push("other");
          this.setState({ genders: genders });
          this.props.updateStore({ genders: genders });
          break;
        default:
          break;
      }
    };

    const noGenderIsSelected = () => {
      if (this.state.genders.length > 0) return false;
      else return true;
    };

    const handleInterestsChange = (selected_interest) => {
      var interests = selected_interest.map((interest) => interest.value);
      this.setState({ interests: interests });
      this.props.updateStore({ interests: interests });
    };

    const noInterestIsSelected = () => {
      if (this.state.interests.length > 0) return false;
      else return true;
    };

    const handleLocalizationChange = (selected_loc) => {
      var updatedLoc = selected_loc.map((localization) => localization.value);
      this.setState({ geolocalizations: updatedLoc });
      this.props.updateStore({ geolocalizations: updatedLoc });
    };

    const noLocalizationIsSelected = () => {
      if (this.state.geolocalizations.length > 0) return false;
      else return true;
    };

    return (
      <React.Fragment>
        <Formik
          initialValues={{
            age_range: this.props.initStore.age_ranges,
            targets: this.props.initStore.targets,
            genders: this.props.initStore.genders,
            geolocalizations: this.props.initStore.geolocalizations, // Cities, states, countries
            interests: this.props.initStore.interests,
          }}
        >
          {({ errors, touched, values, handleChange }) => {
            return (
              <Form>
                <Row>
                  <Col xs={2}></Col>
                  <Col xs={8}>
                    <FormGroup style={{ zIndex: 1000 }}>
                      <Label for="countries">Nombre de la Campaña</Label>
                      <Input
                        type="text"
                        name="name"
                        id="name"
                        defaultValue={this.props.initStore.name}
                        onChange={(e) =>
                          this.props.updateStore({ name: e.target.value })
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col xs={2}></Col>
                </Row>
                <Row>
                  <Col xs={2}></Col>
                  <Col xs={8}>
                    <FormGroup>
                      <Row>
                        {/*<Col xs={6}>
                          <FormGroup style={{ zIndex: 1000 }}>
                            <Label for="countries">Países</Label>
                            <Select
                              defaultValue={countryGroupedOptions.map((geo) =>
                                this.props
                                  .initStore
                                  .geolocalizations.forEach(
                                    (country) => country === geo.value
                                  )
                              )}
                              options={countryGroupedOptions}
                              formatGroupLabel={formatGroupLabel}
                              isMulti
                              onChange={(e) => handleLocalizationChange(e)}
                            />
                          </FormGroup>
                                  </Col>*/}
                        <Col xs={12}>
                          <FormGroup style={{ zIndex: 1000 }}>
                            <Label for="interests">Intereses</Label>
                            <Select
                              options={interestGroupedOptions}
                              formatGroupLabel={formatGroupLabel}
                              isMulti
                              onChange={(e) => handleInterestsChange(e)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs={2}></Col>
                </Row>
                <Row>
                  <Col xs={2}></Col>
                  <Col xs={8}>
                    <FormGroup>
                      <Label for="age_range">Rango de Edades</Label>
                      <Row>
                        <Col xs={6}>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="18-25"
                              label="18-25"
                              defaultChecked={this.props.initStore.age_ranges.some(
                                (age_range) => age_range === "18-25"
                              )}
                              onClick={() => handleAgeRangeChange("18-25")}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="26-35"
                              label="26-35"
                              onChange={() => handleAgeRangeChange("26-35")}
                              defaultChecked={this.props.initStore.age_ranges.some(
                                (age_range) => age_range === "26-35"
                              )}
                            />
                          </FormGroup>
                        </Col>
                        <Col xs={6}>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="36-45"
                              label="36-45"
                              onChange={() => handleAgeRangeChange("36-45")}
                              defaultChecked={this.props.initStore.age_ranges.some(
                                (age_range) => age_range === "36-45"
                              )}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="46 and more"
                              label="46 and more"
                              onChange={() =>
                                handleAgeRangeChange("46 and more")
                              }
                              defaultChecked={this.props.initStore.age_ranges.some(
                                (age_range) => age_range === "46 and more"
                              )}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs={2}></Col>
                </Row>
                <Row>
                  <Col xs={2}></Col>
                  <Col xs={8}>
                    <FormGroup>
                      <Label for="target">
                        Target (Nivel socio-económico de tu cliente ideal)
                      </Label>
                      <Row>
                        <Col xs={6}>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="alto"
                              label="Alto"
                              onChange={() => handleTargetChange("alto")}
                              defaultChecked={this.props.initStore.targets.some(
                                (target) => target === "alto"
                              )}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="medio"
                              label="Medio"
                              onChange={() => handleTargetChange("medio")}
                              defaultChecked={this.props.initStore.targets.some(
                                (target) => target === "medio"
                              )}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="bajo"
                              label="Bajo"
                              onChange={() => handleTargetChange("bajo")}
                              defaultChecked={this.props.initStore.targets.some(
                                (target) => target === "bajo"
                              )}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs={2}></Col>
                </Row>
                <Row>
                  <Col xs={2}></Col>
                  <Col xs={8}>
                    <FormGroup>
                      <Label for="gender">Géneros</Label>
                      <Row>
                        <Col xs={6}>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="female"
                              label="Femenino"
                              onChange={() => handleGenderChange("female")}
                              defaultChecked={this.props.initStore.genders.some(
                                (gender) => gender === "female"
                              )}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="male"
                              label="Masculino"
                              onChange={() => handleGenderChange("male")}
                              defaultChecked={this.props.initStore.genders.some(
                                (gender) => gender === "male"
                              )}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="checkbox"
                              id="other"
                              label="Otro"
                              onChange={() => handleGenderChange("other")}
                              defaultChecked={this.props.initStore.genders.some(
                                (gender) => gender === "other"
                              )}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs={2}></Col>
                </Row>
                <Row>
                  <Col xs={2}></Col>
                  <Col xs={8}>
                    <FormGroup>
                      <Label for="brand_type">Tipo de Marca</Label>
                      <Row>
                        <Col xs={6}>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="radio"
                              id="PRODUCT"
                              name="customRadio"
                              label="Producto"
                              defaultChecked={
                                this.props.initStore.brand_type === "PRODUCT"
                              }
                              onClick={() => {
                                this.setState({ brand_type: "PRODUCT" });
                                this.props.updateStore({
                                  brand_type: "PRODUCT",
                                });
                              }}
                            />
                          </FormGroup>
                          <FormGroup check className="px-0">
                            <CustomInput
                              type="radio"
                              id="SERVICE"
                              name="customRadio"
                              label="Servicio"
                              defaultChecked={
                                this.props.initStore.brand_type === "SERVICE"
                              }
                              onClick={() => {
                                this.setState({ brand_type: "SERVICE" });
                                this.props.updateStore({
                                  brand_type: "PRODUCT",
                                });
                              }}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs={2}></Col>
                </Row>
              </Form>
            );
          }}
        </Formik>
      </React.Fragment>
    );
  }
}
