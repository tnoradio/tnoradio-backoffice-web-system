import React, { Fragment, useEffect } from "react";
import {
  Media,
  Table,
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  CustomInput,
  Input,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { fetchUsersList } from "../../../redux/actions/users/fetchUsers";

const ShowDetails = ({ selectedShow, selectRecommendedShow, getStore }) => {
  const [usersList, setUsersList] = React.useState([]);
  var users = [];
  console.log(getStore());
  useEffect(() => {
    load();
    async function load() {
      const res = await fetchUsersList();
      setUsersList(res);
    }
  }, [selectedShow]);

  usersList.forEach((user) => {
    users.push({
      label: user.name + " " + user.lastName,
      value: user,
    });
  });

  return (
    <Fragment>
      <div className="contact-app-content-detail">
        <PerfectScrollbar>
          <Media>
            {/*}  <Media className="col-3" left href="#">
                     <Media 
                        object
                        src={selectedUser.image}
                        alt="Generic placeholder image"
                        className="rounded-circle img-thumbnail"
                     />
         </Media>*/}
            <Media
              body
              style={{
                marginBottom: "2rem",
              }}
            >
              <Media
                style={{
                  color: "white",
                  padding: "1.5rem",
                }}
                className="gradient-purple-bliss"
                heading
              >
                <Row>
                  <Col xs={11}>
                    {" "}
                    <span>{selectedShow?.name}</span>
                  </Col>
                  <Col xs={1}>
                    {" "}
                    <CustomInput
                      type="radio"
                      id={`selectedShow-${selectedShow.id}`}
                      name="customRadio"
                      defaultChecked={getStore().show?.id === selectedShow.id}
                      onClick={() => selectRecommendedShow(selectedShow.id)}
                    />
                  </Col>
                </Row>
              </Media>
              <span
                style={{
                  padding: "1rem",
                  fontSize: "1.2rem",
                }}
              >
                {" "}
                Género: {selectedShow.genre}
              </span>
            </Media>
          </Media>
          <Table responsive borderless size="xl">
            <tbody>
              <tr className="d-flex">
                <td className="col-2 text-bold-400">Sinopsis:</td>
                <td className="col-10">{" " + selectedShow.synopsis}</td>
              </tr>
              <tr className="d-flex">
                <td className="col-2 text-bold-400">Clasificación:</td>
                <td className="col-3">{" " + selectedShow.pgClassification}</td>
                <td className="col-1 text-bold-400">Tipo:</td>
                <td className="col-3">{" " + selectedShow.type}</td>
              </tr>
              <tr className="d-flex">
                <td className="col-2 text-bold-400">Locutor(es):</td>
                <td className="col-6">
                  <ListGroup>
                    {selectedShow.hosts?.map((host) => {
                      return (
                        <ListGroupItem key={"host " + host.userId}>
                          <Row>
                            <Col>
                              {usersList?.map((user) => {
                                if (user._id === host.userId) {
                                  return (
                                    <div key={user._id}>
                                      {user.name + " " + user.lastName + "\n"}
                                      <br></br>
                                    </div>
                                  );
                                } else return "";
                              })}
                            </Col>
                          </Row>
                        </ListGroupItem>
                      );
                    })}
                  </ListGroup>
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-2 text-bold-400">Horario(s):</td>
                <td className="col-6">
                  <Row>
                    <Col>
                      <ListGroup>
                        {selectedShow.showSchedules?.map((showSchedule) => {
                          return (
                            <ListGroupItem key={showSchedule.weekDay}>
                              <Row>
                                <Col xs={10}>
                                  <span>{showSchedule.weekDay}</span>{" "}
                                  <span>de {showSchedule.startTime}</span>{" "}
                                  <span>a {showSchedule.endTime}</span>
                                </Col>
                              </Row>
                            </ListGroupItem>
                          );
                        })}
                      </ListGroup>
                    </Col>
                  </Row>
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-2 text-bold-400">Redes Sociales:</td>
                <td className="col-6">
                  <Row>
                    <Col>
                      <ListGroup>
                        {selectedShow.socials?.map((social) => {
                          return (
                            <ListGroupItem
                              key={social.socialNetwork + social.userName}
                            >
                              <Row>
                                <Col xs={10}>
                                  <span>{social.socialNetwork}</span>
                                  {"  "}
                                  <span>@{social.userName}</span>{" "}
                                </Col>
                              </Row>
                            </ListGroupItem>
                          );
                        })}
                      </ListGroup>
                    </Col>
                  </Row>
                </td>
              </tr>
              <tr className="d-flex">
                <td className="col-2 text-bold-400">Correo:</td>
                <td className="col-6`">{" " + selectedShow.email}</td>
              </tr>
            </tbody>
          </Table>
        </PerfectScrollbar>
      </div>
    </Fragment>
  );
};

ShowDetails.prototype = {
  selectedShow: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      synopsis: PropTypes.string,
      showSchedules: PropTypes.array,
      genre: PropTypes.string,
      showSlug: PropTypes.string,
      colors: PropTypes.array,
      type: PropTypes.string,
      initDate: PropTypes.string,
      email: PropTypes.string,
      tags: PropTypes.array,
      pgClassification: PropTypes.string,
      producer: PropTypes.string,
      socials: PropTypes.array,
      advertisers: PropTypes.array,
      hosts: PropTypes.array,
      images: PropTypes.array,
      isStared: PropTypes.bool,
      isDeleted: PropTypes.bool,
    }).isRequired
  ).isRequired,
};
export default ShowDetails;
