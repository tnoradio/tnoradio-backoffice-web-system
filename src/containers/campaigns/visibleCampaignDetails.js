import { connect } from "react-redux";
import {
  setEditCampaignFlag,
  updateCampaign,
} from "../../redux/actions/campaigns";
import CampaignDetails from "../../components/campaigns/campaignDetails";

const mapStateToProps = (state) => {
  return {
    selectedCampaign: state.campaignsApp.campaigns.find(
      (campaign) => campaign._id === state.campaignsApp.campaignDetails
    ),
    editCampaignFlag: state.campaignsApp.editCampaign,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditCampaignFlag()),
  onChange: (_id, field, value) => {
    dispatch(updateCampaign(_id, field, value));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CampaignDetails);
