import { connect } from "react-redux";
import { campaignSearch } from "../../redux/actions/campaigns";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.campaignsApp.campaignSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(campaignSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
