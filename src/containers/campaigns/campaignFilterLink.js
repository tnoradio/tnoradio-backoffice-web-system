import { connect } from "react-redux";
import { setCampaignVisibilityFilter } from "../../redux/actions/campaigns";
import Link from "../../components/campaigns/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.campaignsApp.campaignVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => dispatch(setCampaignVisibilityFilter(ownProps.filter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Link);
