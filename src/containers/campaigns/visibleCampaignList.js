import { connect } from "react-redux";
import {
  toggleStarredCampaign,
  destroyCampaign,
  campaignVisibilityFilter,
  campaignDetails,
  fetchCampaigns,
} from "../../redux/actions/campaigns";
import CampaignList from "../../components/campaigns/campaignList";

const getVisibleCampaigns = (
  campaigns,
  filter,
  campaignSearch,
  isUserAdmin,
  userId
) => {
  switch (filter) {
    case campaignVisibilityFilter.SHOW_ALL:
      return campaigns.filter((p) => {
        return (
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.name
            .toLocaleLowerCase()
            .concat(" ")
            .includes(campaignSearch.toLocaleLowerCase())
        );
      });

    case campaignVisibilityFilter.STARRED_CAMPAIGN:
      return campaigns.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.starred &&
          !p.deleted &&
          p.name
            .toLocaleLowerCase()
            .concat(" ")
            .includes(campaignSearch.toLocaleLowerCase())
      );
    case campaignVisibilityFilter.NOT_ACTIVE_CAMPAIGN:
      return campaigns.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.reconciled &&
          !p.deleted &&
          p.name
            .toLocaleLowerCase()
            .concat(" ")
            .includes(campaignSearch.toLocaleLowerCase())
      );
    case campaignVisibilityFilter.ACTIVE_CAMPAIGN:
      return campaigns.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.reconciled &&
          !p.deleted &&
          p.name
            .toLocaleLowerCase()
            .concat(" ")
            .includes(campaignSearch.toLocaleLowerCase())
      );

    case campaignVisibilityFilter.DELETED_CAMPAIGN:
      return campaigns.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.deleted &&
          p.name
            .toLocaleLowerCase()
            .includes(campaignSearch.toLocaleLowerCase())
      );
    default:
      return campaigns.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.name
            .toLocaleLowerCase()
            .includes(campaignSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state) => {
  return {
    campaigns: getVisibleCampaigns(
      state.campaignsApp.campaigns,
      state.campaignsApp.campaignVisibilityFilter,
      state.campaignsApp.campaignSearch,
      state.session?.user.roles.some((role) => role.role === "ADMIN"),
      state.session?.user._id
    ),
    active: state.campaignsApp.campaignDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCampaigns: dispatch(fetchCampaigns()),
    toggleStarredCampaign: (_id) => dispatch(toggleStarredCampaign(_id)),
    deleteCampaign: (_id) => dispatch(destroyCampaign(_id)),
    campaignDetails: (_id) => dispatch(campaignDetails(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CampaignList);
