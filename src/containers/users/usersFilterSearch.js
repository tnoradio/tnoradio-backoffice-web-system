import { connect } from "react-redux";
import { usersSearch } from "../../redux/actions/users";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.userApp.usersSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(usersSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
