import { connect } from "react-redux";
import { setEditUserFlag, updateUser } from "../../redux/actions/users";
import UserDetails from "../../components/users/userDetails";

const mapStateToProps = (state, ownProps) => {
  return {
    selectedUser: state.userApp.users.find(
      (user) => user._id === state.userApp.userDetails
    ),
    editUserFlag: state.userApp.editUser,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditUserFlag()),
  onChange: (_id, field, value) => dispatch(updateUser(_id, field, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
