import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import "../../assets/scss/views/components/extra/upload.scss";
import { toastr } from "react-redux-toastr";
import {
  Col,
  Row,
  FormGroup,
  Input,
  Label,
  Button,
  ModalBody,
  ModalFooter,
  CustomInput,
} from "reactstrap";
import Select from "react-select";
import { connect } from "react-redux";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { addUser } from "../../redux/actions/users";
import SocialNetwork from "../../components/shared/socialNetwork";

const departments = [
  { value: "PRODUCTION", label: "Producción" },
  { value: "MASTER", label: "Operación" },
  { value: "MARKETING", label: "Mercadeo" },
  { value: "ADMIN", label: "Administración" },
  { value: "IT", label: "Sistemas" },
  { value: "COMMERCIAL", label: "Comercialización" },
  { value: "DIRECTION", label: "Dirección" },
];

const groupedOptions = [
  {
    label: "Departamentos",
    options: departments,
  },
];

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

/*const imageStyle = {
   "&:hover": {
      cursor:"pointer"
   }
}*/

const noDisplayStyle = {
  display: "none",
  visibility: "hidden",
};

const displayStyle = {
  display: "flex",
  visibility: "visible",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 270,
  height: 370,
  padding: 4,
  boxSizing: "border-box",
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

const formSchema = Yup.object().shape({
  name: Yup.string().required("Debes escribir un nombre."),
  lastName: Yup.string().required("Debes escribir un apellido."),
  birthdate: Yup.date(),
  email: Yup.string()
    .email("Correo Inválido")
    .required("Debes escribir un email."),
});

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

const mapStateToProps = (state) => ({
  _id: undefined,
});

function MyDropzone(props) {
  const [files, setFiles] = useState([]);
  const [isSelectedImage, setIsSelectedImage] = useState(false);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/jpeg",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      setIsSelectedImage(true);
      props.setImageFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
    },
    onDropRejected: (rejected) => {
      toastr.error("Sólo se admiten imágenes jpg");
    },
  });

  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks

      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <section className="container">
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <img
          style={isSelectedImage ? noDisplayStyle : displayStyle}
          src={"https://randomuser.me/api/portraits/lego/1.jpg"}
          className="rounded-circle img-fluid"
          alt="Imagen"
        />
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}>
          Click para agregar una foto
        </p>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </div>
    </section>
  );
}

let socials = [];
let roles = [];
let bio = "";

const AddUser = ({ dispatch, toggle }) => {
  const [gender, setGender] = useState("O");
  const [department, setDepartment] = useState("");
  const [potentialClient, setPotentialClient] = useState(false);
  const [employee, setEmployee] = useState(false);
  const [client, setClient] = useState(false);
  const [host, setHost] = useState(false);
  const [audience, setAudience] = useState(true);
  const [instructor, setInstructor] = useState(false);
  const [admin, setAdmin] = useState(false);
  const [superAdmin, setSuperAdmin] = useState(false);
  const [employeeAdmin, setEmployeeAdmin] = useState(false);
  const [producer, setProducer] = useState(false);
  const [intern, setIntern] = useState(false);
  const [productionAssistant, setProductionAssistant] = useState(false);
  const [imageProfileFiles, setProfileImageFiles] = useState([]);
  const [imageTalentFiles, setTalentImageFiles] = useState([]);
  const [socialsState, setSocialsState] = useState([]);
  const [roles, setRoles] = useState([]);

  const roleTypes = {
    AUDIENCE: "AUDIENCE",
    SUPER_ADMIN: "SUPER_ADMIN",
    INTERN: "INTERN",
    EMPLOYEE_ADMIN: "EMPLOYEE_ADMIN",
    INSTRUCTOR: "INSTRUCTOR",
    ADMIN: "ADMIN",
    EMPLOYEE: "EMPLOYEE",
    CLIENT: "CLIENT",
    POTENTIAL_CLIENT: "POTENTIAL_CLIENT",
    HOST: "HOST",
    PRODUCER: "PRODUCER",
    PRODUCTION_ASSISTANT: "PRODUCTION_ASSISTANT",
  };

  const addSocialSubmit = (userName, socialNetwork, socialUrl) => {
    if (socialNetwork !== undefined && userName !== undefined) {
      socials.push({
        socialNetwork: socialNetwork,
        userName: userName,
        url: socialUrl,
      });
      //console.log(socials);
      setSocialsState((socialsState) =>
        socialsState.concat({
          socialNetwork: socialNetwork,
          userName: userName,
          url: socialUrl,
        })
      );
      //console.log(socialsState);
    }
  };

  const onDeleteSocialClick = (social) => {
    //console.log(social);
    const indexOfDelete = socials.indexOf(social);
    socials.splice(indexOfDelete, 1);
    setSocialsState((socialsState) => socialsState.splice(indexOfDelete, 1));
  };

  const handleEmployeeChange = () => {
    setEmployee((employee) => (employee = !employee));
  };

  useEffect(() => {
    const newRoles = Object.entries({
      audience,
      superAdmin,
      instructor,
      admin,
      employeeAdmin,
      employee,
      client,
      potentialClient,
      host,
      producer,
      productionAssistant,
    })
      .filter(([_, value]) => value)
      .map(([key, _]) => ({ role: roleTypes[key] }));

    setRoles(newRoles);
  }, [
    audience,
    superAdmin,
    instructor,
    admin,
    employeeAdmin,
    employee,
    client,
    potentialClient,
    host,
    producer,
    productionAssistant,
  ]);

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          name: "",
          lastName: "",
          bio: "",
          birthdate: "",
          company: "",
          department: "",
          email: "",
          gender: "",
          phone: "",
          address: "",
          notes: "",
          dni: "",
          images: [],
          socials: [],
          roles: [],
        }}
        validationSchema={formSchema}
        onSubmit={(values) => {
          console.log(values);
          const images = imageProfileFiles.concat(imageTalentFiles);
          dispatch(
            addUser(
              values.name,
              values.lastName,
              bio,
              values.birthdate,
              values.email,
              values.address,
              values.notes,
              socials,
              roles,
              gender,
              images, //images
              values.dni,
              values.company,
              department,
              values.phone,
              toggle
            )
          );
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <ModalBody>
              <Row>
                <Col md={1}></Col>
                <Col md={4}>
                  <MyDropzone setImageFiles={setProfileImageFiles}></MyDropzone>
                </Col>
                <Col md={1}></Col>
                <Col md={1}></Col>
                <Col md={4}>
                  <MyDropzone setImageFiles={setTalentImageFiles}></MyDropzone>
                </Col>
                <Col md={1}></Col>
                <Col md={12}>
                  <Row>
                    <Col md={12}>
                      <FormGroup>
                        <Label for="name">Nombre</Label>
                        <Field
                          className={`form-control ${
                            errors.name && touched.name && "is-invalid"
                          }`}
                          type="text"
                          name="name"
                          id="name"
                          required
                        />
                        {errors.name && touched.name ? (
                          <div className="invalid-feedback">{errors.name}</div>
                        ) : null}
                      </FormGroup>
                      <FormGroup>
                        <Label for="lastName">Apellido</Label>
                        <Field
                          className={`form-control ${
                            errors.lastName && touched.lastName && "is-invalid"
                          }`}
                          type="text"
                          name="lastName"
                          id="lastName"
                        />
                        {errors.lastName && touched.lastName ? (
                          <div className="invalid-feedback">
                            {errors.lastName}
                          </div>
                        ) : null}
                      </FormGroup>
                    </Col>
                    <Col md={12}>
                      <FormGroup>
                        <Label for="phone">Teléfono</Label>
                        <Field
                          className="form-control"
                          type="phone"
                          name="phone"
                          id="phone"
                        />
                      </FormGroup>

                      <FormGroup>
                        <Label for="email">Email</Label>
                        <Field
                          className={`form-control ${
                            errors.email && touched.email && "is-invalid"
                          }`}
                          type="email"
                          name="email"
                          id="email"
                          required
                        />
                        {errors.email && touched.email ? (
                          <div className="invalid-feedback">{errors.email}</div>
                        ) : null}
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Label for="notes">Biografía</Label>
                    <Input
                      type="textarea"
                      name="bio"
                      id="bio"
                      onChange={(e) => {
                        //console.log(e.target.value);
                        bio = e.target.value;
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup>
                    <Label for="lastName">Cédula</Label>
                    <Field
                      className="form-control"
                      type="text"
                      name="dni"
                      id="dni"
                    />
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup>
                    <Label for="email">Cumpleaños</Label>
                    <Field
                      className={`form-control ${
                        errors.birthdate && touched.birthdate && "is-invalid"
                      }`}
                      type="date"
                      name="birthdate"
                      id="birthdate"
                      required
                    />
                    {errors.birthdate && touched.birthdate ? (
                      <div className="invalid-feedback">{errors.birthdate}</div>
                    ) : null}
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup tag="fieldset">
                    <Label for="gender">Género</Label>
                    <FormGroup check className="px-0">
                      <CustomInput
                        type="radio"
                        value="F"
                        id="F"
                        name="gender"
                        label="Femenino"
                        checked={gender === "F"}
                        onChange={() => setGender("F")}
                      />
                    </FormGroup>
                    <FormGroup check className="px-0">
                      <CustomInput
                        type="radio"
                        value="M"
                        id="M"
                        name="gender"
                        label="Masculino"
                        checked={gender === "M"}
                        onChange={() => setGender("M")}
                      />
                    </FormGroup>
                    <FormGroup check className="px-0">
                      <CustomInput
                        type="radio"
                        value="O"
                        id="O"
                        label="otro"
                        checked={gender === "O"}
                        onChange={() => setGender("O")}
                      />
                    </FormGroup>
                  </FormGroup>
                  {gender === "" ? (
                    <div className="invalid-feedback">
                      Debes seleccionar un género.{gender}
                    </div>
                  ) : null}
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Label for="address">Dirección</Label>
                    <Field
                      className="form-control"
                      type="textarea"
                      name="address"
                      id="address"
                    />
                  </FormGroup>
                </Col>
                <Col md={12}>
                  <FormGroup>
                    <Label for="notes">Notas</Label>
                    <Field
                      className="form-control"
                      type="textarea"
                      name="notes"
                      id="notes"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <FormGroup tag="fieldset">
                    <h6>Rol en TNO Radio</h6>
                    <Row>
                      <Col xs={6}>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="AUDIENCE"
                            label="Audiencia"
                            defaultChecked
                            onChange={() => {
                              setAudience((audience) => !audience);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="EMPLOYEE"
                            label="Empleado"
                            onChange={handleEmployeeChange}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="CLIENT"
                            label="Cliente"
                            onChange={() => {
                              setClient((client) => !client);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="POTENTIAL_CLIENT"
                            label="Cliente Potencial"
                            onChange={() => {
                              setPotentialClient(
                                (potentialClient) => !potentialClient
                              );
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="INTERN"
                            label="Pasante"
                            onChange={() => {
                              setIntern((intern) => !intern);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="EMPLOYEE_ADMIN"
                            label="Empleado con Permisos de Administrador"
                            onChange={() => {
                              setEmployeeAdmin(
                                (employeeAdmin) => !employeeAdmin
                              );
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={6}>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="HOST"
                            label="Locutor"
                            onChange={() => {
                              setHost((host) => !host);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="PRODUCER"
                            label="Productor"
                            onChange={() => {
                              setProducer((producer) => !producer);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="PRODUCTION_ASSISTANT"
                            label="Asistente de Producción"
                            onChange={() => {
                              setProductionAssistant(
                                (productionAssistant) => !productionAssistant
                              );
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="ADMIN"
                            label="Admin"
                            onChange={() => {
                              setProductionAssistant((admin) => !admin);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="SUPER_ADMIN"
                            label="Super Admin"
                            onChange={() => {
                              setSuperAdmin((superAdmin) => !superAdmin);
                            }}
                          />
                        </FormGroup>
                        <FormGroup check className="px-0">
                          <CustomInput
                            type="checkbox"
                            id="INSTRUCTOR"
                            label="Instructor"
                            onChange={() => {
                              setInstructor((instructor) => !instructor);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </FormGroup>
                </Col>
              </Row>
              <Row style={employee || intern ? displayStyle : noDisplayStyle}>
                <Col>
                  <h6>Departamento</h6>
                </Col>
                <Col xs={1}></Col>
                <Col xs={10}>
                  <FormGroup>
                    <Select
                      defaultValue={departments[1]}
                      options={groupedOptions}
                      formatGroupLabel={formatGroupLabel}
                      value={departments.find((obj) => obj === department)}
                      onChange={(e) => {
                        setDepartment(e.value);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row style={client ? displayStyle : noDisplayStyle}>
                <Col md={6}>
                  <FormGroup>
                    <Label for="company">EMPRESA</Label>
                    <Field
                      className="form-control"
                      type="text"
                      name="company"
                      id="company"
                    />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="department">DEPARTAMENTO</Label>
                    <Field
                      className="form-control"
                      type="text"
                      name="department"
                      id="department"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <FormGroup tag="fieldset">
                    <h6>Redes Sociales</h6>
                    <SocialNetwork
                      addSocialSubmit={addSocialSubmit}
                      onDeleteSocialClick={onDeleteSocialClick}
                      socials={socials}
                    ></SocialNetwork>
                  </FormGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Guardar Usuario
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default connect(mapStateToProps)(AddUser);
