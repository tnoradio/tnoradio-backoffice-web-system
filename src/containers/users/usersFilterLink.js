import { connect } from "react-redux";
import { setVisibilityFilter } from "../../redux/actions/users";
import Link from "../../components/users/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.userApp.usersVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setVisibilityFilter(ownProps.filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
