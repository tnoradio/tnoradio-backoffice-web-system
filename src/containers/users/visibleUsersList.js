import { connect } from "react-redux";
import {
  toggleStarredUser,
  destroyUser,
  userVisibilityFilter,
  userDetails,
  fetchUsers,
} from "../../redux/actions/users";
import UsersList from "../../components/users/usersList";
import * as userUtils from "../../utility/users/userUtils";

const getVisibleUsers = (users, filter, usersSearch) => {
  switch (filter) {
    case userVisibilityFilter.SHOW_ALL:
      return users.filter(
        (u) =>
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.STARRED_USER:
      return users.filter(
        (u) =>
          u.isStarred &&
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.EMPLOYEE_USER:
      return users.filter((u) => {
        //console.log(u.roles)
        return (
          userUtils.validateIfUserHasRole(u.roles, "EMPLOYEE") &&
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
        );
      });
    case userVisibilityFilter.CLIENT_USER:
      return users.filter(
        (u) =>
          userUtils.validateIfUserHasRole(u.roles, "CLIENT") &&
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.POTENTIAL_CLIENT_USER:
      return users.filter(
        (u) =>
          userUtils.validateIfUserHasRole(u.roles, "POTENTIAL_CLIENT") &&
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.DELETED_USER:
      return users.filter(
        (u) =>
          u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.AUDIENCE_USER:
      return users.filter(
        (u) =>
          userUtils.validateIfUserHasRole(u.roles, "AUDIENCE") &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.PRODUCTION_ASSISTANT_USER:
      return users.filter((u) => {
        //console.log(u.roles)
        return (
          userUtils.validateIfUserHasRole(u.roles, "PRODUCTION_ASSISTANT") &&
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
        );
      });
    case userVisibilityFilter.HOST_USER:
      return users.filter(
        (u) =>
          userUtils.validateIfUserHasRole(u.roles, "HOST") &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.PRODUCER_USER:
      return users.filter(
        (u) =>
          userUtils.validateIfUserHasRole(u.roles, "PRODUCER") &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
    case userVisibilityFilter.ADMIN_USER:
      return users.filter((u) => {
        //console.log(u.roles)
        return (
          userUtils.validateIfUserHasRole(u.roles, "ADMIN") &&
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
        );
      });
    default:
      return users.filter(
        (u) =>
          !u.isDeleted &&
          u.name
            .toLocaleLowerCase()
            .concat(" ")
            .concat(u.lastName.toLocaleLowerCase())
            .includes(usersSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    // Mapping users object and Visiblityfilter state to Object
    users: getVisibleUsers(
      state.userApp.users,
      state.userApp.usersVisibilityFilter,
      state.userApp.usersSearch
    ),
    active: state.userApp.userDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: dispatch(fetchUsers()),
    toggleStarredUser: (_id) => dispatch(toggleStarredUser(_id)),
    deleteUser: (_id) => dispatch(destroyUser(_id)),
    userDetails: (_id) => dispatch(userDetails(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
