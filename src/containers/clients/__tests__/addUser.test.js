import React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import configureStore from "redux-mock-store";
import addUser from "../addClient";
import { UserEmail } from "../../../core/users/domain/UserEmail";
import * as testUtils from "../../../utility/test/test-utils";

const mockStore = configureStore([]);

describe("AddUser React-Redux Component", () => {
  let store;
  let component;
  //const middlewares = [thunk];
  //const mockStore = createMockStore<any, ThunkDispatch<any, any, AnyAction>>(middlewares);

  const expectedActions = [
    {
      type: "ADD_USER",
      _id: "mock_user_id",
      name: "testName",
      lastName: "testLastName",
      image: "https://randomuser.me/api/portraits/lego/1.jpg",
      dni: 1245678,
      email: new UserEmail("testEmail@email.com"),
      isActive: true,
      gender: "O",
      role: "AUDIENCE",
      address: "testAddress",
      notes: "This is a test user for unit testing",
      company: "tetsCompany",
      department: "TestDepartment",
      phone: "55-555-555-5555",
      isStarred: false,
      isDeleted: false,
      updatedBy: "",
      message: "Usuario agregado con éxito",
    },
  ];

  beforeEach(() => {
    const initialState = testUtils.storeInitialState;
    store = mockStore(initialState);
  });

  //  store.dispatch = jest.fn();

  it("should render addUser with given state from Redux store", () => {
    component = renderer.create(
      <Provider store={store}>
        <addUser />
      </Provider>
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  /*  it('should dispatch an action on button click', () => {
        renderer.act(() => {
            component.root.findByType('button').props.onClick();
        });
    
        expect(store.dispatch).toHaveBeenCalledTimes(1);
        expect(store.dispatch).toHaveBeenCalledWith(
        myAction(expectedActions)
        );
    });*/
});
