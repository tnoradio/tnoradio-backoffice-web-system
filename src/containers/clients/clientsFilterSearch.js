import { connect } from "react-redux";
import Search from "../../components/shared/Search";
import { clientSearch } from "../../redux/actions/clients";

const mapStateToProps = (state) => ({
  value: state.clientApp.clientSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(clientSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
