import { connect } from "react-redux";
import ClientDetails from "../../components/clients/clientDetails";
import agencyDetails from "../../components/clients/agencyDetails";
import {
  setEditClientFlag,
  updateClient,
  updateIsUserClient,
} from "../../redux/actions/clients";

const isAgency = (filter) => {
  switch (filter) {
    case "AGENCIES":
      return true;

    default:
      return false;
  }
};

const mapStateToProps = (state) => {
  return {
    selectedClient: state.clientApp.clients.find(
      (client) => client.pcustomer_id === state.clientApp.clientDetailsReducer
    ),
    selectedAgency: state.clientApp.agencies.agencies.find(
      (client) => client.id === state.clientApp.agencyDetailsReducer
    ),
    isAgency: isAgency(state.clientApp.clientVisibilityFilter),
    editClientFlag: state.clientApp.editClient,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditClientFlag()),
  onChange: (_id, field, value) => dispatch(updateClient(_id, field, value)),
  onChangeIsUserClient: (id) => dispatch(updateIsUserClient(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ClientDetails);
