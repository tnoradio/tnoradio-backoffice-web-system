import { connect } from "react-redux";
import {
  destroyClient,
  clientVisibilityFilter,
  clientDetails,
  agencyDetails,
  fetchClients,
  updateIsUserClient,
  fetchAgencies,
} from "../../redux/actions/clients";
import ClientsList from "../../components/clients/clientsList";

const getVisibleClients = (clients, agencies, filter, clientSearch) => {
  switch (filter) {
    case clientVisibilityFilter.SHOW_ALL:
      return clients?.filter((u) => {
        return u.pcustomer_username
          .toLocaleLowerCase()
          .includes(clientSearch.toLocaleLowerCase());
      });
    case clientVisibilityFilter.POTENTIAL_PRODUCER:
      return clients?.filter((u) => {
        return (
          u.tpc_label === "productor" &&
          u.pcustomer_username
            .toLocaleLowerCase()
            .includes(clientSearch.toLocaleLowerCase())
        );
      });
    case clientVisibilityFilter.POTENTIAL_ADVERTISER:
      return clients?.filter((u) => {
        return (
          u.tpc_label === "anunciante" &&
          u.pcustomer_username
            .toLocaleLowerCase()
            .includes(clientSearch.toLocaleLowerCase())
        );
      });

    case clientVisibilityFilter.AGENCIES:
      return agencies.agencies;

    case clientVisibilityFilter.IS_CLIENT_USER:
      return clients?.filter((u) => {
        return (
          u.pcustomer_isUser === "True" ||
          (u.pcustomer_isUser === true &&
            u.pcustomer_username
              .toLocaleLowerCase()
              .includes(clientSearch.toLocaleLowerCase()))
        );
      });

    default:
      return clients?.filter((u) => {
        return u.pcustomer_username
          .toLocaleLowerCase()
          .includes(clientSearch.toLocaleLowerCase());
      });
  }
};

const isAgency = (filter) => {
  switch (filter) {
    case clientVisibilityFilter.AGENCIES:
      return true;

    default:
      return false;
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    clients: getVisibleClients(
      state.clientApp.clients,
      state.clientApp.agencies,
      state.clientApp.clientVisibilityFilter,
      state.clientApp.clientSearch
    ),
    agencies: state.clientApp.agencies,
    isAgency: isAgency(state.clientApp.clientVisibilityFilter),
    active: state.clientApp.clientDetails,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchClients: dispatch(fetchClients()),
    fetchAgencies: dispatch(fetchAgencies()),
    deleteClient: (_id) => dispatch(destroyClient(_id)),
    clientDetails: (_id) => dispatch(clientDetails(_id)),
    agencyDetails: (_id) => dispatch(agencyDetails(_id)),
    updateIsUserClient: (_id) => dispatch(updateIsUserClient(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientsList);
