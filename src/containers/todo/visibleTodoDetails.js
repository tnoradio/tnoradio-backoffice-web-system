import { connect } from "react-redux";
import {
  setEditTodoFlag,
  updateTodo,
  toggleStarredTodo,
  togglePriorityTodo,
  toggleCompleteTodo,
} from "../../redux/actions/todo/todoActions";
import { fetchUsers } from "../../redux/actions/users";
import TodoDetails from "../../components/todo/todoDetails";

const mapStateToProps = (state, ownProps) => {
  return {
    selectedTodo: state.todoApp.todo.find(
      (todo) => todo._id === state.todoApp.todoDetails
    ),
    editTodoFlag: state.todoApp.editTodo,
    team: state.userApp.users.filter((user) =>
      user.roles.find(
        (role) =>
          role.role === "PRODUCTION_ASSISTANT" || role.role === "EMPLOYEE"
      )
    ),
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditTodoFlag()),
  onChange: (_id, field, value, selectedTodo) =>
    dispatch(updateTodo(_id, field, value, selectedTodo)),
  toggleStarredTodo: (_id) => dispatch(toggleStarredTodo(_id)),
  toggleCompleteTodo: (_id) => dispatch(toggleCompleteTodo(_id)),
  togglePriorityTodo: (_id) => dispatch(togglePriorityTodo(_id)),
  fetchUsers: dispatch(fetchUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoDetails);
