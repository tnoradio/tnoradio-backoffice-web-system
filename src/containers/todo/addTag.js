import React from "react";
import { ButtonToolbar, Button, ButtonGroup, Row, Col } from "reactstrap";

const AddTag = ({ addTag, setToggleTag }) => {
  return (
    <React.Fragment>
      <ButtonGroup className="me-2" aria-label="First group">
        <Row>
          <Col sm={6} md={12}>
            <Button
              color="primary"
              onClick={() => {
                addTag((tags) => [...tags, "Eventos"]);
                setToggleTag((toggleTag) => (toggleTag = false));
              }}
            >
              Eventos
            </Button>{" "}
            <Button
              color="warning"
              onClick={() => {
                addTag((tags) => [...tags, "Promociones"]);
                setToggleTag((toggleTag) => (toggleTag = false));
              }}
            >
              Promociones
            </Button>{" "}
            <Button
              color="danger"
              onClick={() => {
                addTag((tags) => [...tags, "Edición"]);
                setToggleTag((toggleTag) => (toggleTag = false));
              }}
            >
              Edición
            </Button>{" "}
          </Col>
          <Col sm={6} md={12}>
            <Button
              color="info"
              onClick={() => {
                addTag((tags) => [...tags, "Cobranzas"]);
                setToggleTag((toggleTag) => (toggleTag = false));
              }}
            >
              Cobranzas
            </Button>{" "}
            <Button
              color="success"
              onClick={() => {
                addTag((tags) => [...tags, "Ventas"]);
                setToggleTag((toggleTag) => (toggleTag = false));
              }}
            >
              Ventas
            </Button>{" "}
            <Button
              color="primary"
              onClick={() => {
                addTag((tags) => [...tags, "Servicio Técnico"]);
                setToggleTag((toggleTag) => (toggleTag = false));
              }}
            >
              Servicio Técnico
            </Button>
          </Col>
        </Row>
      </ButtonGroup>
    </React.Fragment>
  );
};

export default AddTag;
