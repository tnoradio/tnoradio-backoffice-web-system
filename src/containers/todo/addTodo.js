import React from "react";
import {
  Col,
  Row,
  Form,
  FormGroup,
  Label,
  Button,
  ModalBody,
  ModalFooter,
  Modal,
  ModalHeader,
} from "reactstrap";
import { connect } from "react-redux";
import { addTodo } from "../../redux/actions/todo/todoActions";
import AddTag from "./addTag";
import { Tag } from "react-feather";
import { addNotification } from "../../redux/actions/notifications";

const mapStateToProps = (state) => {
  return {
    id: state.todoApp.todo.length,
    sessionUser: state.session.user,
    team: state.userApp.users.filter((user) =>
      user.roles.find(
        (role) =>
          role.role === "PRODUCTION_ASSISTANT" || role.role === "EMPLOYEE"
      )
    ),
  };
};

const AddTodo = ({ id, team, sessionUser, dispatch }) => {
  const [toggleTag, setToggleTag] = React.useState(false);
  const [tags, addTag] = React.useState([]);

  let owner, task, start_date, due_date, project, priority, comments;

  React.useEffect(() => {
    console.log(tags);
  }, [tags]);

  return (
    <React.Fragment>
      <Form
        onSubmit={(e) => {
          console.log(priority.value);
          e.preventDefault();
          if (!task.value.trim() || !project.value.trim()) {
            return;
          }
          dispatch(
            addTodo(
              id,
              sessionUser.name + " " + sessionUser.lastName,
              task.value,
              owner.value,
              start_date.value,
              due_date.value,
              project.value,
              priority.value === "on" ? true : false,
              tags,
              comments.value,
              team.find((taskOwner) => taskOwner._id === owner.value).slug
            )
          );
          dispatch(
            addNotification(
              undefined,
              owner.value,
              "Se te ha asignado la tarea: " + task.value,
              true,
              "TASK_ADDED"
            )
          );
          owner.value = "";
          project.value = "";
          task.value = "";
          start_date.value = "";
          due_date.value = "";
          project.value = "";
          priority.value = false;
          comments.value = "";
        }}
      >
        <ModalBody>
          <Row>
            <Col md={12}>
              <FormGroup>
                <Label for="firstName">Tarea</Label>
                <input
                  className="form-control"
                  type="text"
                  name="task"
                  id="task"
                  ref={(node) => (task = node)}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="fullName">Responsable</Label>
                <select
                  className="form-control"
                  type="select"
                  name="owner"
                  id="owner"
                  ref={(node) => (owner = node)}
                  required
                >
                  {team.map((owner) => {
                    return (
                      <option key={owner._id} value={owner._id}>
                        {owner.name} {owner.lastName}
                      </option>
                    );
                  })}
                </select>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="lastName">Área</Label>
                <select
                  className="form-control"
                  type="select"
                  name="project"
                  id="project"
                  ref={(node) => (project = node)}
                  required
                >
                  <option key="PROD" value="PROD">
                    Producción
                  </option>
                  <option key="OPE" value="OPE">
                    Operación
                  </option>
                  <option key="ADMIN" value="ADMIN">
                    Administración
                  </option>
                  <option key="SALES" value="SALES">
                    Mercadeo y Ventas
                  </option>
                </select>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="start-date">Fecha Inicio</Label>
                <input
                  className="form-control"
                  type="date"
                  name="start-date"
                  id="start-date"
                  ref={(node) => (start_date = node)}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="due-date">Fecha Fin</Label>
                <input
                  className="form-control"
                  type="date"
                  name="due-date"
                  id="due-date"
                  ref={(node) => (due_date = node)}
                />
              </FormGroup>
            </Col>
            <Col md={2}>
              <FormGroup>
                <Label for="priority">Urgente</Label>
                <input
                  className="form-control"
                  type="checkbox"
                  name="priority"
                  id="priority"
                  ref={(node) => (priority = node)}
                />
              </FormGroup>
            </Col>
            <Col md={10}>
              <FormGroup>
                <Label for="tag">Tags</Label>
                <Row>
                  <Button
                    className="btn-square"
                    size="md"
                    outline
                    color="primary"
                    onClick={() => setToggleTag((toggleTag) => !toggleTag)}
                  >
                    <Tag size={16} color="#009DA0" />
                    {"  "}
                    Agregar Etiqueta
                  </Button>
                </Row>
                <Modal
                  isOpen={toggleTag}
                  toggle={() => setToggleTag((toggleTag) => !toggleTag)}
                >
                  <ModalHeader>
                    <Label>Agregar Etiqueta</Label>
                  </ModalHeader>
                  <ModalBody>
                    <AddTag addTag={addTag} setToggleTag={setToggleTag} />
                  </ModalBody>
                </Modal>
              </FormGroup>
            </Col>
            <Col md={12}>
              <FormGroup>
                <Label for="comments">Comentarios</Label>
                <input
                  className="form-control"
                  type="comments"
                  name="comments"
                  id="comments"
                  ref={(node) => (comments = node)}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <p className="mb-0 font-small-3">
              {tags.map((tag) => (
                <span
                  className={
                    "badge " +
                    (tag === "Eventos" ? "badge-info" : "") +
                    (tag === "Edición" ? "badge-warning" : "") +
                    (tag === "Cobranzas" ? "badge-success" : "") +
                    (tag === "Ventas" ? "badge-info" : "") +
                    (tag === "Producción" ? "badge-warning" : "") +
                    (tag === "Servicio Técnico" ? "badge-success" : "") +
                    (tag === "Promociones" ? "badge-info" : "") +
                    " mr-1"
                  }
                >
                  {tag}
                </span>
              ))}
            </p>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" type="submit">
            Agregar tarea
          </Button>
        </ModalFooter>
      </Form>
    </React.Fragment>
  );
};

export default connect(mapStateToProps)(AddTodo);
