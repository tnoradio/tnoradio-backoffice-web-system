import { connect } from "react-redux";
import {
  fetchTodos,
  toggleTodo,
  toggleStarredTodo,
  togglePriorityTodo,
  deleteTodo,
  todoVisiblityFilter,
  todoDetails,
} from "../../redux/actions/todo/todoActions";
import TodoList from "../../components/todo/todoList";

const getVisibleTodo = (todo, filter, todoSearch) => {
  switch (filter) {
    case todoVisiblityFilter.SHOW_ALL:
      return todo.filter(
        (c) =>
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );

    // Project Filter
    case todoVisiblityFilter.ADMIN_TODO:
      return todo.filter(
        (c) =>
          c.project === "ADMIN" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.OPE_TODO:
      return todo.filter(
        (c) =>
          c.project === "OPE" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.SALES_TODO:
      return todo.filter(
        (c) =>
          c.tags.includes("Ventas") &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.PROD_TODO:
      return todo.filter(
        (c) =>
          c.project === "PROD" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.COLLECTIONS_TODO:
      return todo.filter(
        (c) =>
          c.tags.includes("Cobranzas") &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.EVENTS_TODO:
      return todo.filter(
        (c) =>
          c.tags.includes("Eventos") &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.POST_PRODUCTION_TODO:
      return todo.filter(
        (c) =>
          c.tags.includes("Edición") &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.PROM_TODO:
      return todo.filter(
        (c) =>
          c.project === "PROM" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.TECHNICAL_SUPPORT_TODO:
      return todo.filter(
        (c) =>
          c.tags.includes("Servicio Técnico") &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );

    // Filters
    case todoVisiblityFilter.STARRED_TODO:
      return todo.filter(
        (c) =>
          c.starred &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.PRIORITY_TODO:
      return todo.filter(
        (c) =>
          c.priority &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.COMPLETED_TODO:
      return todo.filter(
        (c) =>
          c.completed &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );

    // Tag filter
    case todoVisiblityFilter.UIUX_TODO:
      return todo.filter(
        (c) =>
          c.tag === "uiux" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.FRONTEND_TODO:
      return todo.filter(
        (c) =>
          c.tag === "frontend" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    case todoVisiblityFilter.BACKEND_TODO:
      return todo.filter(
        (c) =>
          c.tag === "backend" &&
          !c.deleted &&
          c.task?.toLocaleLowerCase().includes(todoSearch.toLocaleLowerCase())
      );
    default:
      throw new Error("Unknown filter: " + filter);
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    // Mapping todo object and Visiblityfilter state to Object

    todo: getVisibleTodo(
      state.todoApp.todo,
      state.todoApp.todoVisibilityFilter,
      state.todoApp.todoSearch
    ),
    active: state.todoApp.todoDetails,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchTodos: dispatch(fetchTodos()),
  toggleTodo: (_id) => dispatch(toggleTodo(_id)),
  toggleStarredTodo: (_id) => dispatch(toggleStarredTodo(_id)),
  togglePriorityTodo: (_id) => dispatch(togglePriorityTodo(_id)),
  deleteTodo: (_id) => dispatch(deleteTodo(_id)),
  todoDetails: (_id) => dispatch(todoDetails(_id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
