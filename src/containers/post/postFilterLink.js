import { connect } from "react-redux";
import { setPostVisibilityFilter } from "../../redux/actions/posts";
import Link from "../../components/post/Link";

const mapStateToProps = (state, ownProps) => {
  // console.log(ownProps);
  return {
    active: ownProps.filter === state.postApp.postVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setPostVisibilityFilter(ownProps.filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
