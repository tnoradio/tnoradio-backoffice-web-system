import React from "react";
import { Label, ModalBody, Modal, ModalHeader } from "reactstrap";
import Select from "react-select";
import { useDispatch, useSelector } from "react-redux";
import { fetchPostTags } from "../../redux/actions/posts/tags";
var tags = [];

const Tags = ({ toggleTag, setToggleTag, setPostTags, postTags, onChange }) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  React.useEffect(() => {
    dispatch(fetchPostTags());
    tags = [];
    state.postApp.postTags.forEach((tag) => {
      tags.push({
        label: tag.tag,
        value: tag,
      });
    });
  }, [state.showApp.postTags]);

  const [value, setValue] = React.useState(tags[0]);
  const tagGroupOptions = [
    {
      label: "Etiquetas",
      options: tags,
    },
  ];

  const groupStyles = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  };

  const groupBadgeStyles = {
    backgroundColor: "#EBECF0",
    borderRadius: "2em",
    color: "#172B4D",
    display: "inline-block",
    fontSize: 12,
    fontWeight: "normal",
    lineHeight: "1",
    minWidth: 1,
    padding: "0.16666666666667em 0.5em",
    textAlign: "center",
  };

  const formatGroupLabel = (data) => (
    <div style={groupStyles}>
      <span>{data.label}</span>
      <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
  );

  const handleAddTag = (tag) => {
    const tags = postTags;
    tags.push(tag.value.tag);
    setPostTags(tags);
    onChange();
  };
  return (
    <Modal
      isOpen={toggleTag}
      toggle={() => setToggleTag((toggleTag) => !toggleTag)}
    >
      <ModalHeader>
        <Label>Agregar Etiqueta</Label>
      </ModalHeader>
      <ModalBody>
        <Select
          defaultValue={tags[0]?.value}
          options={tagGroupOptions}
          formatGroupLabel={formatGroupLabel}
          value={value}
          onChange={(e) => {
            const value = tags?.find((obj) => {
              return obj.value === e.value;
            });
            setValue(value);
            handleAddTag(value);
          }}
        />
      </ModalBody>
    </Modal>
  );
};

export default Tags;
