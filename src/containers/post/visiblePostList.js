import { connect } from "react-redux";
import {
  toggleStarredPost,
  destroyPost,
  postVisibilityFilter,
  postDetails,
  toggleApprovePost,
  fetchPostsPage,
  fetchPostsCount,
} from "../../redux/actions/posts";
import PostList from "../../components/post/postList";
import * as postUtils from "../../utility/posts/postUtils";
import { fetchPostTags } from "../../redux/actions/posts/tags";

const getVisiblePosts = (posts, filter, postSearch) => {
  switch (filter) {
    case postVisibilityFilter.SHOW_ALL:
      return posts.filter(
        (p) =>
          !p.deleted &&
          p.title
            .toLocaleLowerCase()
            .concat(" ")
            .includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.STARRED_POST:
      return posts.filter(
        (p) =>
          p.starred &&
          !p.deleted &&
          p.title
            .toLocaleLowerCase()
            .concat(" ")
            .includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.DELETED_POST:
      return posts.filter(
        (p) =>
          p.deleted &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.ADMIN_POST:
      return posts.filter(
        (p) =>
          p.tags.includes({ role: "ADMIN" }) &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.EVENTS_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "EVENTOS") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.CULTURE_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "CULTURA") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.ENTERTAINMENT_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "ENTRETENIMIENTO") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.ENTREPRENEURSHIP_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "EMPRENDIMIENTO") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.TECHNOLOGY_POST:
      return posts.filter(
        (p) =>
          (postUtils.validateIfPostHasTag(p.tags, "TECNOLOGIA") ||
            postUtils.validateIfPostHasTag(p.tags, "TECNOLOGÍA")) &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.SPORTS_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "DEPORTE") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.CINEMA_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "CINE") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.HEALTH_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "SALUD") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.FRANCHISE_POST:
      return posts.filter(
        (p) =>
          postUtils.validateIfPostHasTag(p.tags, "FRANQUICIAS") &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.EDUCATION_POST:
      return posts.filter(
        (p) =>
          (postUtils.validateIfPostHasTag(p.tags, "EDUCACION") ||
            postUtils.validateIfPostHasTag(p.tags, "EDUCACIÓN")) &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    case postVisibilityFilter.UNAPPROVED_POST:
      return posts.filter(
        (p) =>
          !p.approved &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
    default:
      return posts.filter(
        (p) =>
          !p.deleted &&
          p.title.toLocaleLowerCase().includes(postSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    postsCount: state.postApp.postsCount,
    posts: getVisiblePosts(
      state.postApp.posts,
      state.postApp.postVisibilityFilter,
      state.postApp.postSearch
    ),
    active: state.postApp.postDetails,
    isUserAdmin: state.session?.user.roles.some(
      (role) => role.role === "ADMIN"
    ),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { togglePost } = ownProps;
  return {
    fetchNextPostPage: (pageSize, pageNumber) =>
      dispatch(fetchPostsPage(pageSize, pageNumber)),
    fetchFirstPostPage: dispatch(fetchPostsPage(100, 0)),
    postTags: dispatch(fetchPostTags()),
    togglePost: (_id) => dispatch(togglePost(_id)),
    toggleStarredPost: (_id) => dispatch(toggleStarredPost(_id)),
    toggleApprovePost: (_id) => dispatch(toggleApprovePost(_id)),
    deletePost: (_id) => dispatch(destroyPost(_id)),
    postDetails: (_id) => {
      togglePost();
      return dispatch(postDetails(_id));
    },
    fetchPostsCount: dispatch(fetchPostsCount()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);
