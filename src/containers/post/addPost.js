import React, { useEffect, useState } from "react";
import fetchAllTopicMetrics from "../../redux/actions/dashboard/fetchAllTopicMetricsAction";
import { useDropzone } from "react-dropzone";
import "../../assets/scss/views/components/extra/upload.scss";
import { toastr } from "react-redux-toastr";
import DefaultPostImage from "../../assets/img/default-post.jpeg";
import { convertToRaw } from "draft-js";
import {
  Col,
  Row,
  FormGroup,
  Input,
  Label,
  Button,
  ModalBody,
  ModalFooter,
  Card,
  CardBody,
  Badge,
} from "reactstrap";
import { connect } from "react-redux";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { addPost } from "../../redux/actions/posts";
import { useSelector, useDispatch } from "react-redux";
import { Tag } from "react-feather";
import Tags from "./tags";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SpinnerLoader from "../../components/shared/LoadingSpinner";

const noDisplayStyle = {
  display: "none",
  visibility: "hidden",
};

const displayStyle = {
  display: "flex",
  visibility: "visible",
  maxWidth: "40%",
};

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
  justifyContent: "center",
  "&:hover": {
    cursor: "pointer",
  },
};

const thumb = {
  display: "inline-flex",
  borderRadius: 0,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 600,
  height: 400,
  padding: 2,
  boxSizing: "border-box",
  "&:hover": {
    cursor: "pointer",
  },
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
  "&:hover": {
    cursor: "pointer",
  },
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
  "&:hover": {
    cursor: "pointer",
  },
};

const formSchema = Yup.object().shape({
  title: Yup.string().required("Debes escribir un título."),
  subtitle: Yup.string().required("Debes escribir un subtítulo."),
});

const mapStateToProps = (state) => {
  return {
    statistics: state.dashboardApp.getAllTopicMetricsNaiveBayesReducer,
    _id: undefined,
  };
};

const mapDispatchToProps = async (dispatch) => ({
  //fetchAllTopicMetrics: await dispatch(fetchAllTopicMetrics()),
});

function MyDropzone(props) {
  const [files, setFiles] = useState([]);
  const [isSelectedImage, setIsSelectedImage] = useState(false);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/jpeg",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      setIsSelectedImage(true);
      props.setImageFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
    },

    onDropRejected: (rejected) => {
      toastr.error("Sólo se admiten imágenes jpg");
    },
  });

  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} alt={"Imagen del Post"} style={img} />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <section className="container">
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <img
          style={isSelectedImage ? noDisplayStyle : displayStyle}
          src={DefaultPostImage}
          className="square img-fluid"
          alt="Imagen"
        />
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}></p>
        <p style={isSelectedImage ? noDisplayStyle : displayStyle}>
          Click para agregar una foto
        </p>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </div>
    </section>
  );
}

let summary = "";

const AddPost = ({ toggle }) => {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const [toggleTag, setToggleTag] = React.useState(false);
  const [postTags, setPostTags] = React.useState([]);
  const [imageFiles, setImageFiles] = useState([]);
  const [tagsState, setTagsState] = useState([]);
  const [editorState, setEditorState] = useState("");
  const [loading, setLoading] = useState(false);

  const onEditorStateChange = (editorState) => {
    setEditorState(editorState);
  };

  const addTagSubmit = (tag) => {
    if (tag !== undefined) {
      postTags.push(tag);
      setTagsState((tagsState) => tagsState.concat(tagsState));
    }
  };

  const onDeleteTagClick = (tag) => {
    const indexOfDelete = postTags.indexOf(tag);
    postTags.splice(indexOfDelete, 1);
    tagsState((tagsState) => tagsState.splice(indexOfDelete, 1));
  };

  React.useEffect(() => {}, [postTags]);
  return (
    <React.Fragment>
      <Formik
        initialValues={{
          title: "",
          owner_id: "",
          subtitle: "",
          summary: "",
          image: "",
          images: [],
          publish_date: "",
          tags: [],
          priority: false,
          starred: false,
          deleted: false,
          text: "",
          approved: false,
          isActive: false,
        }}
        validationSchema={formSchema}
        onSubmit={(values) => {
          setLoading(true);
          dispatch(
            addPost(
              undefined,
              values.title,
              state.session?.user._id,
              values.subtitle,
              summary,
              "",
              imageFiles,
              new Date(Date.now()),
              postTags,
              values.priority,
              values.starred,
              values.deleted,
              JSON.stringify(convertToRaw(editorState.getCurrentContent())),
              values.approved,
              values.isActive,
              toggle
            )
          );
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <SpinnerLoader loading={loading} />
            <ModalBody>
              <Row>
                <Col>
                  <MyDropzone setImageFiles={setImageFiles}></MyDropzone>
                </Col>
                <Col md={12}>
                  <Row>
                    <Col xs={12}>
                      <Label for="title">
                        Temas más destacados en twitter según tus seguidores
                      </Label>
                    </Col>
                    <div
                      style={{
                        padding: "50px",
                        paddingTop: "20px",
                        paddingBottom: "20px",
                      }}
                      className="btn-toolbar"
                    >
                      {state.dashboardApp.getAllTopicMetricsNaiveBayesReducer.topicMetrics.data?.map(
                        (theme) => {
                          return (
                            <Button
                              outline
                              className="mr-1"
                              color={
                                theme.topic_name === "finanzas"
                                  ? "primary"
                                  : theme.topic_name === "salud"
                                  ? "secondary"
                                  : theme.topic_name === "entretenimiento"
                                  ? "success"
                                  : theme.topic_name === "emprendimiento"
                                  ? "info"
                                  : theme.topic_name === "deporte"
                                  ? "warning"
                                  : ""
                              }
                            >
                              {theme.topic_name}{" "}
                              <Badge
                                color={
                                  theme.topic_name === "finanzas"
                                    ? "primary"
                                    : theme.topic_name === "salud"
                                    ? "secondary"
                                    : theme.topic_name === "entretenimiento"
                                    ? "success"
                                    : theme.topic_name === "emprendimiento"
                                    ? "info"
                                    : theme.topic_name === "deporte"
                                    ? "warning"
                                    : ""
                                }
                              >
                                {" "}
                                {Number(theme.topic_cant_percent).toFixed(2)}%
                              </Badge>
                            </Button>
                          );
                        }
                      )}
                    </div>
                    <Col md={12}>
                      <FormGroup>
                        <Label for="title">Título</Label>
                        <Field
                          className={`form-control ${
                            errors.title && touched.title && "is-invalid"
                          }`}
                          type="text"
                          name="title"
                          id="title"
                          required
                        />
                        {errors.title && touched.title ? (
                          <div className="invalid-feedback">{errors.title}</div>
                        ) : null}
                      </FormGroup>
                      <FormGroup>
                        <Label for="subtitle">Subtítulo</Label>
                        <Field
                          className={`form-control ${
                            errors.subtitle && touched.subtitle && "is-invalid"
                          }`}
                          type="text"
                          name="subtitle"
                          id="subtitle"
                        />
                        {errors.subtitle && touched.subtitle ? (
                          <div className="invalid-feedback">
                            {errors.subtitle}
                          </div>
                        ) : null}
                      </FormGroup>
                      <FormGroup>
                        <Label for="summary">Summary</Label>
                        <Input
                          className="form-control"
                          type="textarea"
                          name="summary"
                          id="summary"
                          onChange={(e) => {
                            summary = e.target.value;
                          }}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col md={12} className="post-editor">
                  <Card>
                    <CardBody>
                      <Editor
                        style={{ height: "400px" }}
                        editorState={editorState}
                        toolbarClassName="toolbarClassName"
                        wrapperClassName=""
                        editorClassName="post-editor"
                        onEditorStateChange={onEditorStateChange}
                      />
                    </CardBody>
                  </Card>
                </Col>
              </Row>

              <Row>
                <Col md={12}>
                  <FormGroup>
                    <Label for="tag">Tags</Label>
                    <Row>
                      <Col>
                        {postTags.map((tag) => (
                          <span
                            className={
                              "badge " +
                              (tag === "SALUD" ? "badge-info" : "") +
                              (tag === "EMPRENDIMIENTO"
                                ? "badge-warning"
                                : "") +
                              (tag === "CINE" ? "badge-success" : "") +
                              (tag === "ENTRETENIMIENTO" ? "badge-info" : "") +
                              (tag === "CULTURA" ? "badge-warning" : "") +
                              (tag === "HISTORIA" ? "badge-success" : "") +
                              (tag === "ECONOMIA" ? "badge-info" : "") +
                              (tag === "DEPORTE" ? "badge-warning" : "") +
                              (tag === "TEATRO" ? "badge-success" : "") +
                              (tag === "EDUCACION" ? "badge-info" : "") +
                              (tag === "EDUCACIÓN" ? "badge-info" : "") +
                              (tag === "TECNOLOGÍA" ? "badge-purple" : "") +
                              (tag === "TECNOLOGIA" ? "badge-purple" : "") +
                              (tag === "MATERNIDAD" ? "badge-warning" : "") +
                              (tag === "MUSICA" ? "badge-warning" : "") +
                              " mr-1"
                            }
                          >
                            {tag}
                          </span>
                        ))}
                      </Col>
                    </Row>
                    <br></br>
                    <Row>
                      <Col>
                        <Button
                          className="btn-square"
                          size="md"
                          outline
                          color="primary"
                          onClick={() =>
                            setToggleTag((toggleTag) => !toggleTag)
                          }
                        >
                          <Tag size={16} color="#009DA0" />
                          {"  "}
                          Agregar Etiqueta
                        </Button>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        {" "}
                        <Tags
                          postTags={postTags}
                          setToggleTag={setToggleTag}
                          toggleTag={toggleTag}
                          setPostTags={setPostTags}
                        ></Tags>
                      </Col>
                    </Row>
                  </FormGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Guarda tu Contenido
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default connect(mapStateToProps)(AddPost);
