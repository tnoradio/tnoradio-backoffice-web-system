import { connect } from "react-redux";
import { postSearch } from "../../redux/actions/posts";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.postApp.postsSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(postSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
