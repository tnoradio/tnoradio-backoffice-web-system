import { connect } from "react-redux";
import {
  setEditPostFlag,
  updatePost,
  updateSelectedPost,
} from "../../redux/actions/posts";
import PostDetails from "../../components/post/postDetails";

const mapStateToProps = (state) => {
  return {
    selectedPost: state.postApp.posts.find(
      (post) => post._id === state.postApp.postDetails
    ),
    editPostFlag: state.postApp.editPost,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onEditClick: (selectedPost, editFlag) => {
      editFlag === true && dispatch(updateSelectedPost(selectedPost));
      dispatch(setEditPostFlag());
    },
    onChange: (_id, field, value, selectedPost) =>
      dispatch(updatePost(_id, field, value, selectedPost)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetails);
