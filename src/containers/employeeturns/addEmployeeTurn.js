import React, { useEffect, useState } from "react";
import "../../assets/scss/views/components/extra/upload.scss";
import {
  Col,
  Row,
  FormGroup,
  Input,
  Label,
  Button,
  ModalBody,
  ModalFooter,
  CustomInput,
} from "reactstrap";
import { connect, useDispatch, useSelector } from "react-redux";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { addEmployeeTurn } from "../../redux/actions/emoployeeTurns";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";
import Activity from "../../components/employeeturns/Activity";
import { fetchUserTodos } from "../../redux/actions/todo/todoActions";

const formSchema = Yup.object().shape({
  startTime: Yup.string().required("not valid start time"),
  endTime: Yup.string().required("not valid end time"),
  date: Yup.string().required("not valid date"),
});

const timeInitProps = {
  placeholder: "hh:mm a",
};

let activities = [];

const AddEmployeeTurn = ({ owner_id, toggle }) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [employeeTasks, setEmployeeTasks] = useState([]);
  const today = new Date(Date.now());
  const [startTime, setStartTime] = React.useState(new Date());
  const [endTime, setEndTime] = React.useState(new Date());
  const [date, setDate] = React.useState(today);

  const addEmployeeTasksToState = () => {
    dispatch(fetchUserTodos(owner_id));
    const { userToDos } = state.todoApp;
    setEmployeeTasks(userToDos);
  };

  useEffect(() => {
    addEmployeeTasksToState();
  }, []);

  const addActivitySubmit = (duration, name, taskId, turnId) => {
    if (activities !== undefined) {
      activities.push({
        duration: duration,
        name: name,
        taskId: taskId,
        turnId: turnId,
      });
    }
  };

  const onDeleteActivityClick = (activity) => {
    const indexOfDelete = activities.indexOf(activity);
    activities.splice(indexOfDelete, 1);
  };

  useEffect(() => {
    //renders component everytime activities change
    console.log("useeff", activities);
  }, [activities]);

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          date: today,
          startTime: startTime,
          endTime: endTime,
          activities: [],
        }}
        onSubmit={(values) => {
          console.log("values", startTime);
          console.log("values", endTime);
          dispatch(
            addEmployeeTurn(
              startTime,
              endTime,
              owner_id,
              date,
              activities,
              toggle
            )
          );
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <ModalBody>
              <Row>
                <Col xs={2} />
                <Col xs={3}>
                  <FormGroup>
                    <Label for="date">Fecha</Label>
                    <Field name="date" id="date">
                      {() => (
                        <Datetime
                          name="date"
                          dateFormat="DD/MM/yyyy "
                          initialValue={today}
                          value={date}
                          onChange={(date) => setDate(date._d)}
                        />
                      )}
                    </Field>
                  </FormGroup>
                </Col>
                <Col xs={2}>
                  <Label>Inicio</Label>
                  <Field name="startTime" id="startTime">
                    {() => (
                      <Datetime
                        name="startTime"
                        timeFormat="hh:mm a"
                        dateFormat={false}
                        value={startTime}
                        onChange={(date) => setStartTime(date._d)}
                        timeConstraints={{ minutes: { step: 5 } }}
                      />
                    )}
                  </Field>
                </Col>
                <Col xs={2}>
                  <Label>Fin</Label>
                  <Field name="endTime" id="endTime">
                    {() => (
                      <Datetime
                        name="endTime"
                        timeFormat="hh:mm a"
                        dateFormat={false}
                        value={endTime}
                        onChange={(date) => setEndTime(date._d)}
                        timeConstraints={{ minutes: { step: 5 } }}
                      />
                    )}
                  </Field>
                  {errors.endTime && touched.endTime ? (
                    <div className="invalid-feedback">{errors.endTime}</div>
                  ) : null}
                </Col>
                <Col xs={2} />
              </Row>
              <Row>
                <Col xs={2} />
                <Col xs={8}>
                  <FormGroup tag="fieldset">
                    <h6>Actividades/Tareas</h6>
                    <Activity
                      activities={activities}
                      addActivitySubmit={addActivitySubmit}
                      onDeleteActivityClick={onDeleteActivityClick}
                      employeeTasks={employeeTasks}
                    />
                  </FormGroup>
                </Col>
                <Col xs={2} />
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Guardar Turno
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default AddEmployeeTurn;
