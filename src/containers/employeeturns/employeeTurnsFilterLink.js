import { connect } from "react-redux";
import { setVisibilityFilter } from "../../redux/actions/emoployeeTurns";
import Link from "../../components/employeeturns/Link";

const mapStateToProps = (state, ownProps) => ({
  active:
    ownProps.filter === state.employeeTurnsApp.employeeTurnsVisibilityFilter,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setVisibilityFilter(ownProps.filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
