import { connect } from "react-redux";
import chatList from "../../components/chat/chatList";
import { openChat } from "../../redux/actions/chat/chatActions";
import {
  getChatsList,
  getMessagesHistory,
} from "../../redux/actions/chat/telegramActions";

const filterChats = (chats, chatSearch) => {
  if (chatSearch !== "")
    return chats.filter((t) =>
      (
        t.top_message.from_user?.first_name +
        " " +
        t.top_message.from_user?.last_name
      )
        .toLocaleLowerCase()
        .includes(chatSearch.toLocaleLowerCase())
    );
  else return chats;
};

const mapStateToProps = (state) => {
  return {
    chats: filterChats(
      state.chatApp.fetchTelegramMessages,
      state.chatApp.chatSearch
    ),
    chatList: filterChats(
      state.chatApp.fetchTelegramMessages,
      state.chatApp.chatSearch
    ),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openChat: (id) => dispatch(openChat(id)),
    getChatHistory: (_id) => dispatch(getMessagesHistory(_id)),
    fetchMessages: dispatch(getChatsList()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(chatList);
