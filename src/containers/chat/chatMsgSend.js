import { connect } from "react-redux";
import chatMsgForm from "../../components/chat/chatMsgForm";
import { sendMessage } from "../../redux/actions/chat/telegramActions";

const mapStateToProps = (state, ownprops) => {
  return {
    id: state.chatApp.chatContent,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onClick: (id, chatMsg) => {
    dispatch(sendMessage(id, chatMsg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(chatMsgForm);
