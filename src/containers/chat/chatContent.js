import { connect } from "react-redux";
import chatContent from "../../components/chat/chatContent";

const mapStateToProps = (state) => {
  return {
    chatDetails: state.chatApp.messageHistory,
    messageHistory: state.chatApp.messageHistory,
  };
};

export default connect(mapStateToProps)(chatContent);
