import { connect } from "react-redux";
import {
  toggleStarredExpense,
  destroyExpense,
  expenseVisibilityFilter,
  expenseDetails,
  fetchExpenses,
} from "../../redux/actions/expenses";
import ExpenseList from "../../components/expense/expenseList";

const getVisibleExpenses = (
  expenses,
  filter,
  expenseSearch,
  isUserAdmin,
  userId
) => {
  switch (filter) {
    case expenseVisibilityFilter.SHOW_ALL:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase())
      );

    case expenseVisibilityFilter.STARRED_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.starred &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.NOT_RECONCILED_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.reconciled &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.RECONCILED_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.reconciled &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.POST_PROD_EXPENSE:
      return expenses.filter(
        (p) =>
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase() || "post produccion")
      );
    case expenseVisibilityFilter.PARKING_EXPENSE:
      return expenses.filter(
        (p) =>
          !p.deleted &&
          p.expense_type === "PARKING" &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase() || "estacionamiento")
      );
    case expenseVisibilityFilter.PAYROLL_EXPENSE:
      return expenses.filter(
        (p) =>
          !p.deleted &&
          p.expense_type === "PAYROLL" &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(
              expenseSearch.toLocaleLowerCase() ||
                "sueldo" ||
                "salario" ||
                "nomina"
            )
      );
    case expenseVisibilityFilter.FEE_EXPENSE:
      return expenses.filter(
        (p) =>
          !p.deleted &&
          p.expense_type === "FEE" &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase() || "honorarios")
      );
    case expenseVisibilityFilter.EVENT_EXPENSE:
      return expenses.filter(
        (p) =>
          !p.deleted &&
          p.expense_type === "EVENT" &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(expenseSearch.toLocaleLowerCase() || "evento" || "pauta")
      );
    case expenseVisibilityFilter.DELETED_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.USD_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.currency === "USD" &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.BS_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.currency === "Bs" &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.EURO_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.currency === "EURO" &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.CRYPTO_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.currency === "CRYPTO" &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
    case expenseVisibilityFilter.OTHER_EXPENSE:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.currency === "OTHER" &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
    default:
      return expenses.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .includes(expenseSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state) => {
  return {
    expenses: getVisibleExpenses(
      state.expenseApp.expenses,
      state.expenseApp.expenseVisibilityFilter,
      state.expenseApp.expenseSearch,
      state.session?.user.roles.some((role) => role.role === "ADMIN"),
      state.session?.user._id
    ),
    active: state.expenseApp.expenseDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchExpenses: dispatch(fetchExpenses()),
    toggleStarredExpense: (_id) => dispatch(toggleStarredExpense(_id)),
    deleteExpense: (_id) => dispatch(destroyExpense(_id)),
    expenseDetails: (_id) => dispatch(expenseDetails(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseList);
