import React, { useState, useEffect } from "react";
import "../../assets/scss/views/components/extra/upload.scss";
import {
  Col,
  Row,
  FormGroup,
  Label,
  Button,
  Input,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import Select from "react-select";
import { connect, useDispatch } from "react-redux";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { addExpense } from "../../redux/actions/expenses";
import { useSelector } from "react-redux";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import MyFilesDropzone from "../../components/shared/MyFilesDropzone";
import SpinnerLoader from "../../components/shared/LoadingSpinner";
import { fetchRecordingTurns } from "../../redux/actions/recordingTurns";
//import { addNotification } from "../../redux/actions/notifications";
import { fetchUsers } from "../../redux/actions/users";

const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const formSchema = Yup.object().shape({
  concept: Yup.string().required("Debes escribir un concepto."),
  amountBs: Yup.number().typeError("Debes escribir números."),
  amountUsd: Yup.number().typeError("Debes escribir números."),
  amountEuro: Yup.number().typeError("Debes escribir números."),
  amountCripto: Yup.number().typeError("Debes escribir números."),
  amountOther: Yup.number().typeError("Debes escribir números."),
  date: Yup.date().required("Debes escribir una fecha de pago."),
});

const mapStateToProps = (state) => {
  return {
    _id: undefined,
    recordingTurns: state.recordingTurnsApp.recordingTurns,
    adminUsers: state.userApp.users.filter((user) => {
      if (user.roles.some((role) => role.role === "ADMIN")) return user;
    }),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchRecordingTurns: dispatch(fetchRecordingTurns()),
    fetchUsers: dispatch(fetchUsers()),
  };
};

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

const AddExpense = ({ recordingTurns, toggle }) => {
  const state = useSelector((state) => state);
  const [receiptFiles, setReceiptFiles] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currency, setCurrency] = useState("USD");
  const [type, setType] = useState("PRODUCTION");
  const [recordingTurn, setRecordingTurn] = useState("");
  const [method, setMethod] = useState("cash");
  const dispatch = useDispatch();

  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  const recordingTurnsList = React.useMemo(() => {
    var list = [];
    list.push({ value: "", label: "Seleccione una pauta/evento" });
    if (recordingTurns) {
      recordingTurns.forEach((recordingTurn) => {
        if (recordingTurn.owner_id === state.session?.user._id || isUserAdmin)
          list.push({
            value: recordingTurn._id,
            label: recordingTurn.description,
          });
      });
    }
    return list;
  }, []);

  const recordingTurnsGroupOptions = [
    {
      label: "Pautas/Eventos",
      options: recordingTurnsList,
    },
  ];

  const onDelete = (fileName) => {
    var filtered = receiptFiles.filter((file) => file.name !== fileName);
    setReceiptFiles(filtered);
  };

  useEffect(() => {}, [receiptFiles]);

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          amountBs: "",
          amountUsd: "",
          amountEuro: "",
          amountCripto: "",
          amountOther: "",
          owner_id: "",
          recording_turn_owner: "",
          concept: "",
          receipts: [],
          date: "",
          currency: "",
          starred: false,
          deleted: false,
          reconciled: false,
          method: "",
          expense_type: "",
        }}
        validationSchema={formSchema}
        onSubmit={(values) => {
          setLoading(true);
          dispatch(
            addExpense(
              undefined,
              values.amountBs,
              values.amountUsd,
              values.amountEuro,
              values.amountCripto,
              values.amountOther,
              state.session?.user._id,
              recordingTurn,
              values.concept,
              receiptFiles,
              values.date,
              values.starred,
              false,
              false,
              currency,
              method,
              type,
              toggle
            )
          );
          // eslint-disable-next-line no-lone-blocks
          {
            /*adminUsers.forEach((user) =>
            dispatch(
              addNotification(
                undefined,
                user._id,
                "Se ha registrado un pago",
                true,
                "PAYMENT"
              )
            )
              );*/
          }
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <SpinnerLoader loading={loading} />
            <ModalBody>
              <Row>
                <Col md="4" xs="12">
                  <FormGroup>
                    <Label for="date">Fecha</Label>
                    <Field
                      type="date"
                      name="date"
                      id="date"
                      className={`form-control ${
                        errors.date && touched.date && "is-invalid"
                      }`}
                    />
                    {errors.date && touched.date ? (
                      <div className="invalid-feedback">{errors.date}</div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={12}>
                  <FormGroup>
                    <Label for="concept">Concepto</Label>
                    <Field
                      className={`form-control ${
                        errors.concept && touched.concept && "is-invalid"
                      }`}
                      type="textarea"
                      name="concept"
                      id="concept"
                      required
                    />
                    {errors.concept && touched.concept ? (
                      <div className="invalid-feedback">{errors.concept}</div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Label for="expenseType">Tipo de Gasto</Label>
                </Col>
                <Col xs={12} md={6} lg={3}>
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setType(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="PRODUCTION"
                          value="PRODUCTION"
                          defaultChecked
                        />{" "}
                        Producción
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="RECORDING"
                          value="RECORDING"
                        />{" "}
                        Grabación
                      </Label>
                    </FormGroup>
                    <FormGroup check disabled>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="PAUTA"
                          value="PAUTA"
                        />{" "}
                        Pauta/Evento
                      </Label>
                    </FormGroup>
                    <FormGroup check disabled>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="EDUCATION"
                          value="EDUCATION"
                        />{" "}
                        Formación
                      </Label>
                    </FormGroup>
                    <FormGroup check disabled>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="SUPPLIES"
                          value="SUPPLIES"
                        />{" "}
                        Artículos de Oficina / Materiales
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
                <Col xs={12} md={6} lg={3}>
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setType(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="REPRESENTATION"
                          value="REPRESENTATION"
                        />{" "}
                        Representación
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="TRANSPORT"
                          value="TRANSPORT"
                        />{" "}
                        Transporte / Taxis
                      </Label>
                    </FormGroup>
                    <FormGroup check disabled>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="FOOD"
                          value="FOOD"
                        />{" "}
                        Alimentos / Refrigerios
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
                <Col xs={12} md={6} lg={3}>
                  <FormGroup check>
                    <Label check>
                      <Input
                        type="radio"
                        name="type"
                        id="PAYROLL"
                        value="PAYROLL"
                      />{" "}
                      Nómina
                    </Label>
                  </FormGroup>
                  <FormGroup check>
                    <Label check>
                      <Input type="radio" name="type" id="FEE" value="FEE" />{" "}
                      Honorarios
                    </Label>
                  </FormGroup>
                  <FormGroup check>
                    <Label check>
                      <Input
                        type="radio"
                        name="type"
                        id="PARKING"
                        value="PARKING"
                      />{" "}
                      Estacionamiento
                    </Label>
                  </FormGroup>
                  <FormGroup check>
                    <Label check>
                      <Input
                        type="radio"
                        name="type"
                        id="MARKETING"
                        value="MARKETING"
                      />{" "}
                      Marketing
                    </Label>
                  </FormGroup>
                  <FormGroup check>
                    <Label check>
                      <Input type="radio" name="type" id="RENT" value="RENT" />{" "}
                      Alquiler
                    </Label>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                {type === "PAUTA" &&
                recordingTurnsList !== undefined &&
                recordingTurnsList[0] !== undefined &&
                recordingTurnsList[0] !== null ? (
                  <Col xs={12} md={6}>
                    <Label for="recording_turn_owner">Pautas/Eventos</Label>
                    <FormGroup>
                      <Select
                        id="recording_turn_owner"
                        value={recordingTurnsList.filter(
                          (obj) => obj.value === recordingTurn
                        )}
                        options={recordingTurnsGroupOptions}
                        formatGroupLabel={formatGroupLabel}
                        onChange={(e) => {
                          console.log(e.value);
                          setRecordingTurn(e.value);
                        }}
                      />
                    </FormGroup>
                  </Col>
                ) : null}
              </Row>
              <Row>
                <Col xs={12}>
                  <Label for="currency">Moneda</Label>
                </Col>
                <Col md="3" xs="12">
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setCurrency(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="USD"
                          value="USD"
                          defaultChecked
                        />{" "}
                        Dólares
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="Bs"
                          value="Bs"
                        />{" "}
                        Bolívares
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="Euro"
                          value="Euro"
                        />{" "}
                        Euro
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
                <Col md="5" xs="12">
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setCurrency(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="crypto"
                          value="crypto"
                        />{" "}
                        Cripto (Equivalente en USD)
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="other"
                          value="other"
                        />{" "}
                        Otro
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="4">
                  <FormGroup>
                    <Label for="method">Forma de Pago</Label>
                    <Input
                      type="select"
                      id="method"
                      name="method"
                      onChange={(e) => setMethod(e.target.value)}
                    >
                      <option value="none" defaultValue="" disabled="">
                        Seleccione
                      </option>
                      {currency !== "crypto" && (
                        <option value="cash" id="cash">
                          Efectivo
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="zelle" id="zelle">
                          Zelle
                        </option>
                      )}
                      {currency === "Bs" && (
                        <option value="transferBs." id="transferBs.">
                          Transferencia Bs.
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="transferUSD" id="transferUSD">
                          Transferencia en USA
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="transferPan" id="transferPan">
                          Transferencia Panamá
                        </option>
                      )}
                      {currency === "USD" ||
                        (currency === "Bs" && (
                          <option value="cardPan" id="cardPan">
                            Débito Panamá
                          </option>
                        ))}
                      {currency === "Bs" && (
                        <option value="mobile" id="mobile">
                          Pago Móvil
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="paypal" id="paypal">
                          Paypal
                        </option>
                      )}
                      {currency === "crypto" && (
                        <option value="binance" id="binance">
                          Binance
                        </option>
                      )}
                      <option value="other" id="other">
                        Otro
                      </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md="4" xs="12">
                  <FormGroup>
                    <Label for="amount">Monto</Label>
                    {currency === "USD" && (
                      <FormGroup>
                        <Field
                          name="amountUsd"
                          id="amountUsd"
                          className={`form-control ${
                            errors.amountUsd &&
                            touched.amountUsd &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountUsd && touched.amountUsd ? (
                          <div className="invalid-feedback">
                            {errors.amountUsd}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "Bs" && (
                      <FormGroup>
                        <Field
                          name="amountBs"
                          id="amountBs"
                          className={`form-control ${
                            errors.amountBs && touched.amountBs && "is-invalid"
                          }`}
                        />
                        {errors.amountBs && touched.amountBs ? (
                          <div className="invalid-feedback">
                            {errors.amountBs}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "Euro" && (
                      <FormGroup>
                        <Field
                          name="amountEuro"
                          id="amountEuro"
                          className={`form-control ${
                            errors.amountEuro &&
                            touched.amountEuro &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountEuro && touched.amountEuro ? (
                          <div className="invalid-feedback">
                            {errors.amountEuro}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "crypto" && (
                      <FormGroup>
                        <Field
                          name="amountCripto"
                          id="amountCripto"
                          className={`form-control ${
                            errors.amountCripto &&
                            touched.amountCripto &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountCripto && touched.amountCripto ? (
                          <div className="invalid-feedback">
                            {errors.amountCripto}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "other" && (
                      <FormGroup>
                        <Field
                          name="amountOther"
                          id="amountOther"
                          className={`form-control ${
                            errors.amountOther &&
                            touched.amountOther &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountOther && touched.amountOther ? (
                          <div className="invalid-feedback">
                            {errors.amountOther}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Label for="receipts">Recibos</Label>
                  <MyFilesDropzone
                    layout={"user"}
                    disabled={false}
                    isEditExpense={false}
                    setReceiptFiles={setReceiptFiles}
                    onDelete={onDelete}
                    id={null}
                    selectedExpense={null}
                    receiptFiles={[]}
                    saveFiles={receiptFiles}
                    setSaveFiles={setReceiptFiles}
                  />
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Guardar
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddExpense);
