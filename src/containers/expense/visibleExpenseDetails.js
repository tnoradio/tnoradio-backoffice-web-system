import { connect } from "react-redux";
import {
  setEditExpenseFlag,
  updateExpense,
} from "../../redux/actions/expenses";
import ExpenseDetails from "../../components/expense/expenseDetails";

const mapStateToProps = (state) => {
  return {
    selectedExpense: state.expenseApp.expenses.find(
      (expense) => expense._id === state.expenseApp.expenseDetails
    ),
    editExpenseFlag: state.expenseApp.editExpense,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditExpenseFlag()),
  onChange: (_id, field, value, selectedExpense, files) => {
    dispatch(updateExpense(_id, field, value, selectedExpense, files));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseDetails);
