import { connect } from "react-redux";
import { expenseSearch } from "../../redux/actions/expenses";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.expenseApp.expensesSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(expenseSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
