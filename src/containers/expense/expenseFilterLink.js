import { connect } from "react-redux";
import { setExpenseVisibilityFilter } from "../../redux/actions/expenses";
import Link from "../../components/expense/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.expenseApp.expenseVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setExpenseVisibilityFilter(ownProps.filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
