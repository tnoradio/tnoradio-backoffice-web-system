import { connect } from "react-redux";
import { setExpenseVisibilityFilter } from "../../redux/actions/expenses";
import ExpenseTabs from "../../components/expense/expenseTabs";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.expenseApp.expenseVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onClick: (filter) => dispatch(setExpenseVisibilityFilter(filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseTabs);
