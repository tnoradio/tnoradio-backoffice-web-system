import { connect } from "react-redux";
import { recordingTurnSearch } from "../../redux/actions/recordingTurns";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.recordingTurnsApp.recordingTurnsSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(recordingTurnSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
