import React, { useState, useEffect } from "react";
import DateTime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import {
  Col,
  Row,
  FormGroup,
  Label,
  Button,
  ModalBody,
  ModalFooter,
  ListGroupItem,
  ListGroup,
  Modal,
  ModalHeader,
} from "reactstrap";
import * as Icon from "react-feather";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { addRecordingTurn } from "../../redux/actions/recordingTurns";
import { connect, useDispatch, useSelector } from "react-redux";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SpinnerLoader from "../../components/shared/LoadingSpinner";
import { fetchUsers } from "../../redux/actions/users";
import { addNotification } from "../../redux/actions/notifications";
import { fetchUsersList } from "../../redux/actions/users/fetchUsers";
import { addTodo } from "../../redux/actions/todo/todoActions";
import Select from "react-select";
import moment from "moment";

const formSchema = Yup.object().shape({
  description: Yup.string().required("Debes escribir una descripción."),
  price: Yup.number()
    .required("Debes escribir un monto.")
    .typeError("Debes escribir números"),
  date: Yup.date().required("Debes escribir una fecha de pago."),
});

const AddRecordingTurn = ({ adminUsers, toggle }) => {
  const state = useSelector((state) => state);
  const [loading, setLoading] = useState(false);
  const [start, setStartTime] = useState(new Date());
  const [end, setEndTime] = useState(new Date());
  const [turnTeam, setTurnTeam] = React.useState([]);
  const [teamModal, setTeamModal] = React.useState(false);
  const [errorTeamMember, setErrorTeamMember] = React.useState(false);
  const [usersList, setUsersList] = React.useState([]);
  const [teamMember, setTeamMember] = React.useState({});
  const [errorUserTeamMember, setErrorUserTeamMember] = React.useState(false);
  const [errorDuplicateTeamMember, setErrorDuplicateTeamMember] =
    React.useState(false);
  const dispatch = useDispatch();

  let users = [];
  let userGroupOptions = [];

  const options = [
    { value: "PENDING", label: "Pendiente" },
    { value: "RECONCILED", label: "Conciliado" },
    { value: "DONE", label: "Listo" },
  ];
  const groupStyles = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  };

  const groupBadgeStyles = {
    backgroundColor: "#EBECF0",
    borderRadius: "2em",
    color: "#172B4D",
    display: "inline-block",
    fontSize: 12,
    fontWeight: "normal",
    lineHeight: "1",
    minWidth: 1,
    padding: "0.16666666666667em 0.5em",
    textAlign: "center",
  };
  useEffect(() => {
    load();
    async function load() {
      const res = await fetchUsersList();
      setUsersList(res);
    }
  }, []);

  usersList.forEach((user) => {
    if (user.roles.some((role) => role.role === "EMPLOYEE"))
      users.push({
        label: user.name + " " + user.lastName,
        value: user,
      });
  });

  userGroupOptions = [
    {
      label: "Usuarios",
      options: users,
    },
  ];

  const formatGroupLabel = (data) => (
    <div style={groupStyles}>
      <span>{data.label}</span>
      <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
  );

  const handleStartChange = (time) => {
    setStartTime(time.toDate());
  };

  const handleEndChange = (time) => {
    setEndTime(time.toDate());
  };

  const calculatePrice = () => {
    var diff = (end - start) / 1000;
    diff /= 60 * 60;
    return Math.abs(Math.round(diff)) * 30;
  };
  const verifyUserExistOnList = (id, list) => {
    var found = false;
    list.map((element) => {
      if (element.userId === id) {
        found = true;
      }
    });
    return found;
  };

  const addTeamMember = (teamMember) => {
    let teamMembers = turnTeam;
    console.log(teamMember);
    if (teamMember.value._id !== undefined) {
      if (!verifyUserExistOnList(teamMember.value._id, teamMembers)) {
        teamMembers.push(teamMember.value._id);
        setTurnTeam(teamMembers);
        console.log(turnTeam);
        setTeamModal(false);
        setErrorTeamMember(false);
        setErrorUserTeamMember(false);
        setErrorDuplicateTeamMember(false);
      } else {
        setErrorDuplicateTeamMember(true);
      }
    } else {
      setErrorUserTeamMember(true);
    }
  };

  const onDeleteTeamMemberClick = (userId) => {
    console.log(turnTeam);
    let teamMembers = turnTeam.filter((memberId) => memberId !== userId);
    console.log(teamMembers);
    setTurnTeam(teamMembers);
    console.log(turnTeam);
  };

  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  return (
    <React.Fragment>
      <SpinnerLoader loading={loading} />
      <Formik
        initialValues={{
          owner_id: "",
          price: 0,
          description: "",
          date: "",
          startTime: "",
          endTime: "",
          starred: false,
          deleted: false,
          status: "",
          payments: [],
        }}
        validationSchema={formSchema}
        onSubmit={(values) => {
          setLoading(true);
          dispatch(
            addRecordingTurn(
              undefined,
              state.session?.user._id,
              calculatePrice(),
              values.description,
              values.date,
              start,
              end,
              false,
              false,
              "PENDING",
              values.payments,
              turnTeam
            )
          );

          adminUsers.forEach((user) => {
            dispatch(
              addNotification(
                undefined,
                user._id,
                "Se ha registrado una pauta de grabación",
                true,
                "RECORDING_TURN_ADDED"
              )
            );
          });

          const myTimeout = setTimeout(createTodos, 3000);

          myTimeout();

          function createTodos() {
            turnTeam.forEach((teamMemberId) => {
              dispatch(
                addTodo(
                  undefined,
                  state.session?.user.name + " " + state.session?.user.lastName,
                  "Pauta de Grabación",
                  teamMemberId,
                  values.date,
                  values.date,
                  "PROD",
                  true,
                  ["Producción"],
                  "Hora de la Pauta: " + moment(start).format("HH:mm a"),
                  users.find(
                    (taskOwner) => taskOwner.value._id === teamMemberId
                  ).value.slug
                )
              );

              dispatch(
                addNotification(
                  undefined,
                  teamMemberId,
                  "Se te ha asignado una tarea",
                  true,
                  "TASK_ADDED"
                )
              );
            });
          }
          toggle();
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <ModalBody>
              <Row>
                <Col md={1}></Col>
                <Col md={9}>
                  <FormGroup>
                    <Label for="description">Descripción</Label>
                    <Field
                      className={`form-control ${
                        errors.description &&
                        touched.description &&
                        "is-invalid"
                      }`}
                      type="textarea"
                      name="description"
                      id="description"
                      required
                    />
                    {errors.description && touched.description ? (
                      <div className="invalid-feedback">
                        {errors.description}
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
                <Col md={2}></Col>
              </Row>
              <Row>
                <Col md="1" xs="12"></Col>
                <Col md="3" xs="12">
                  <FormGroup>
                    <Label for="date">Fecha</Label>
                    <Field
                      type="date"
                      name="date"
                      id="date"
                      className={`form-control ${
                        errors.date && touched.date && "is-invalid"
                      }`}
                    />
                    {errors.date && touched.date ? (
                      <div className="invalid-feedback">{errors.date}</div>
                    ) : null}
                  </FormGroup>
                </Col>
                <Col md="3" xs="12">
                  <FormGroup>
                    <Label for="startTime">Hora de Inicio</Label>
                    <DateTime
                      dateFormat={false}
                      timeFormat
                      onChange={(e) => handleStartChange(e)}
                      value={start}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" xs="12">
                  <FormGroup>
                    <Label for="endTime">Hora Fin</Label>
                    <DateTime
                      dateFormat={false}
                      timeFormat
                      onChange={(e) => handleEndChange(e)}
                      value={end}
                    />
                  </FormGroup>
                  <Col md="1" xs="12"></Col>
                </Col>
              </Row>
              <Row>
                <Col md={1}></Col>
                <Col md={9}>
                  <Label for="turnTeam">Asignar un Equipo a la Pauta</Label>
                  {isUserAdmin ? (
                    <>
                      <Row>
                        <Col>
                          <ListGroup flush>
                            {turnTeam.map((teamMemberId) => {
                              return (
                                <ListGroupItem
                                  key={"teamMember " + teamMemberId}
                                >
                                  <Row>
                                    <Col xs={10}>
                                      {users.map((user) => {
                                        if (user.value._id === teamMemberId) {
                                          return (
                                            <div
                                              key={"label_" + user.value._id}
                                            >
                                              <span>{user.label}</span>
                                            </div>
                                          );
                                        } else return "";
                                      })}
                                    </Col>
                                    <Col xs={2}>
                                      <Icon.Trash
                                        size={18}
                                        className="float-right mt-1 mb-2 width-25 d-block"
                                        style={{
                                          color: "#FF586B",
                                          cursor: "pointer",
                                        }}
                                        onClick={() => {
                                          onDeleteTeamMemberClick(teamMemberId);
                                        }}
                                      />
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              );
                            })}
                          </ListGroup>
                        </Col>
                        <Col xs={12}>
                          <Button
                            className="btn-square"
                            size="sm"
                            outline
                            color="primary"
                            onClick={() => setTeamModal(true)}
                          >
                            <Icon.User size={16} color="#009DA0" />
                            {"  "}
                            Armar Equipo
                          </Button>
                        </Col>
                        <Col xs={2}></Col>
                        {errorTeamMember ? (
                          <div
                            className="invalid-feedback"
                            style={{ display: "block", textAlign: "center" }}
                          >
                            {"Debe indicar al menos un miembro"}
                          </div>
                        ) : null}
                      </Row>
                      <Modal
                        isOpen={teamModal}
                        toggle={() => setTeamModal((state) => !state)}
                      >
                        <ModalHeader>
                          <Label>Agregar Equipo de Trabajo a la Pauta</Label>
                        </ModalHeader>
                        <ModalBody>
                          <Row>
                            {usersList !== undefined &&
                            usersList[0] !== undefined &&
                            usersList[0] !== null ? (
                              <Col xs={12}>
                                <FormGroup>
                                  <Select
                                    defaultValue={users[0].value}
                                    options={userGroupOptions}
                                    formatGroupLabel={formatGroupLabel}
                                    value={users.find((obj) => {
                                      return obj === teamMember;
                                    })}
                                    onChange={(e) => {
                                      console.log(e);
                                      setTeamMember(e);
                                    }}
                                  />
                                </FormGroup>
                              </Col>
                            ) : (
                              <Col xs={11}></Col>
                            )}
                            {errorUserTeamMember ? (
                              <div
                                className="invalid-feedback"
                                style={{
                                  display: "block",
                                  textAlign: "center",
                                }}
                              >
                                {"Debe seleccionar a un Locutor"}
                              </div>
                            ) : null}
                            {errorDuplicateTeamMember ? (
                              <div
                                className="invalid-feedback"
                                style={{
                                  display: "block",
                                  textAlign: "center",
                                }}
                              >
                                {"El miembro ya existe para esta pauta"}
                              </div>
                            ) : null}
                          </Row>
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            className="btn-square"
                            outline
                            color="primary"
                            onClick={() => addTeamMember(teamMember)}
                          >
                            Agregar
                          </Button>{" "}
                        </ModalFooter>
                      </Modal>
                    </>
                  ) : null}
                </Col>
              </Row>
              <Row>
                <Col md="8" xs="12"></Col>
                <Col md="2" xs="12">
                  <Label>Precio: </Label>
                  <span> {calculatePrice()}$</span>
                </Col>
                <Col md="2" xs="12"></Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Guardar
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  console.log(state);
  return {
    adminUsers: state.userApp.users.filter((user) => {
      if (user.roles.some((role) => role.role === "ADMIN")) return user;
    }),
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: dispatch(fetchUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddRecordingTurn);
