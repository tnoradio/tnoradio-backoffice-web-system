import { connect } from "react-redux";
import {
  toggleSuspendRecordingTurn,
  destroyRecordingTurn,
  recordingTurnVisibilityFilter,
  recordingTurnDetails,
  fetchRecordingTurns,
} from "../../redux/actions/recordingTurns";
import RecordingTurnList from "../../components/recordingturns/recordingTurnList";

const getVisibleRecordingTurns = (
  recordingTurns,
  filter,
  recordingTurnSearch,
  isUserAdmin,
  userId
) => {
  switch (filter) {
    case recordingTurnVisibilityFilter?.SHOW_ALL:
      return recordingTurns?.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.description
            .toLocaleLowerCase()
            .concat(" ")
            .includes(recordingTurnSearch.toLocaleLowerCase())
      );

    case recordingTurnVisibilityFilter?.SUSPENDED_RECORDING_TURN:
      return recordingTurns?.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.isSuspended &&
          !p.deleted &&
          p.description
            .toLocaleLowerCase()
            .concat(" ")
            .includes(recordingTurnSearch.toLocaleLowerCase())
      );

    case recordingTurnVisibilityFilter?.DELETED_RECORDING_TURN:
      return recordingTurns?.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.deleted &&
          p.description
            .toLocaleLowerCase()
            .includes(recordingTurnSearch.toLocaleLowerCase())
      );

    case recordingTurnVisibilityFilter?.RECONCILED_RECORDING_TURN:
      return recordingTurns?.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.status === "RECONCILED" &&
          p.description
            .toLocaleLowerCase()
            .includes(recordingTurnSearch.toLocaleLowerCase())
      );

    case recordingTurnVisibilityFilter?.DONE:
      return recordingTurns?.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.status === "DONE" &&
          p.description
            .toLocaleLowerCase()
            .includes(recordingTurnSearch.toLocaleLowerCase())
      );

    default:
      return recordingTurns?.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.description
            .toLocaleLowerCase()
            .includes(recordingTurnSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state) => {
  return {
    recordingTurns: getVisibleRecordingTurns(
      state.recordingTurnsApp.recordingTurns,
      state.recordingTurnsApp.recordingTurnVisibilityFilter,
      state.recordingTurnsApp.recordingTurnSearch,
      state.session?.user.roles.some((role) => role.role === "ADMIN"),
      state.session?.user._id
    ),
    active: state.recordingTurnsApp.recordingTurnDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchRecordingTurns: dispatch(fetchRecordingTurns()),
    toggleSuspendRecordingTurn: (_id) =>
      dispatch(toggleSuspendRecordingTurn(_id)),
    deleteRecordingTurn: (_id) => dispatch(destroyRecordingTurn(_id)),
    recordingTurnDetails: (_id) => dispatch(recordingTurnDetails(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RecordingTurnList);
