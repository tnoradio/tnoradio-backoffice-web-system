import { connect } from "react-redux";
import {
  setEditRecordingTurnFlag,
  updateRecordingTurn,
} from "../../redux/actions/recordingTurns";
import RecordingTurnDetails from "../../components/recordingturns/recordingTurnDetails";

const mapStateToProps = (state) => {
  return {
    selectedRecordingTurn: state.recordingTurnsApp.recordingTurns.find(
      (recordingTurn) =>
        recordingTurn._id === state.recordingTurnsApp.recordingTurnDetails
    ),
    editRecordingTurnFlag: state.recordingTurnsApp.editRecordingTurn,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onEditClick: () => dispatch(setEditRecordingTurnFlag()),
  onChange: (_id, field, value) => {
    dispatch(updateRecordingTurn(_id, field, value));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordingTurnDetails);
