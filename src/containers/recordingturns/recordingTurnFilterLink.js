import { connect } from "react-redux";
import { setRecordingTurnVisibilityFilter } from "../../redux/actions/recordingTurns";
import Link from "../../components/recordingturns/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active:
      ownProps.filter === state.recordingTurnsApp.recordingTurnVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => dispatch(setRecordingTurnVisibilityFilter(ownProps.filter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Link);
