import { connect } from "react-redux";
import { setPaymentVisibilityFilter } from "../../redux/actions/payments";
import Link from "../../components/payment/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.paymentApp.paymentVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(setPaymentVisibilityFilter(ownProps.filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
