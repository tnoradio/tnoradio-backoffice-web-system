import { connect } from "react-redux";
import { paymentSearch } from "../../redux/actions/payments";
import Search from "../../components/shared/Search";

const mapStateToProps = (state) => ({
  value: state.paymentApp.paymentsSearch,
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (value) => dispatch(paymentSearch(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
