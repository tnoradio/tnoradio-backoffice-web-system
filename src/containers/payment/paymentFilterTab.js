import { connect } from "react-redux";
import { setPaymentVisibilityFilter } from "../../redux/actions/payments";
import PaymentTabs from "../../components/payment/paymentTabs";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.paymentApp.paymentVisibilityFilter,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onClick: (filter) => dispatch(setPaymentVisibilityFilter(filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentTabs);
