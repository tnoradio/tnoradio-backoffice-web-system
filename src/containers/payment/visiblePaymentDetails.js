import { connect } from "react-redux";
import {
  setEditPaymentFlag,
  updatePayment,
} from "../../redux/actions/payments";
import PaymentDetails from "../../components/payment/paymentDetails";
import { fetchShows } from "../../redux/actions/shows";

const mapStateToProps = (state) => {
  return {
    selectedPayment: state.paymentApp.payments.find(
      (payment) => payment._id === state.paymentApp.paymentDetails
    ),
    editPaymentFlag: state.paymentApp.editPayment,
  };
};

const mapDispatchToProps = (dispatch) => ({
  shows: dispatch(fetchShows()),
  onEditClick: () => dispatch(setEditPaymentFlag()),
  onChange: (_id, field, value, selectedPayment, files) => {
    dispatch(updatePayment(_id, field, value, selectedPayment, files));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentDetails);
