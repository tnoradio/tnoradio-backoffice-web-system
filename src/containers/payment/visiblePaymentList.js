import { connect } from "react-redux";
import {
  toggleStarredPayment,
  destroyPayment,
  paymentVisibilityFilter,
  paymentDetails,
  fetchPayments,
} from "../../redux/actions/payments";
import PaymentList from "../../components/payment/paymentList";

const getVisiblePayments = (
  payments,
  filter,
  paymentSearch,
  isUserAdmin,
  userId
) => {
  console.log(payments);
  switch (filter) {
    case paymentVisibilityFilter.SHOW_ALL:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(paymentSearch.toLocaleLowerCase())
      );

    case paymentVisibilityFilter.STARRED_PAYMENT:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.starred &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(paymentSearch.toLocaleLowerCase())
      );
    case paymentVisibilityFilter.NOT_RECONCILED_PAYMENT:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.reconciled &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(paymentSearch.toLocaleLowerCase())
      );
    case paymentVisibilityFilter.RECONCILED_PAYMENT:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.reconciled &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(paymentSearch.toLocaleLowerCase())
      );
    case paymentVisibilityFilter.POST_PROD_PAYMENT:
      return payments.filter(
        (p) =>
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .concat(" ")
            .includes(paymentSearch.toLocaleLowerCase() || "post produccion")
      );
    case paymentVisibilityFilter.DELETED_PAYMENT:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .includes(paymentSearch.toLocaleLowerCase())
      );
    case paymentVisibilityFilter.SHOW_PAYMENT:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.concept
            .toLocaleLowerCase()
            .includes(paymentSearch.toLocaleLowerCase())
      );
    case paymentVisibilityFilter.RECORDING_STUDIO_PAYMENT:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.show_owner !== 0 &&
          p.concept
            .toLocaleLowerCase()
            .includes(paymentSearch.toLocaleLowerCase() || "estudio")
      );

    default:
      return payments.filter(
        (p) =>
          (isUserAdmin || p.owner_id === userId) &&
          !p.deleted &&
          p.concept
            .toLocaleLowerCase()
            .includes(paymentSearch.toLocaleLowerCase())
      );
  }
};

const mapStateToProps = (state) => {
  return {
    payments: getVisiblePayments(
      state.paymentApp.payments,
      state.paymentApp.paymentVisibilityFilter,
      state.paymentApp.paymentSearch,
      state.session?.user.roles.some((role) => role.role === "ADMIN"),
      state.session?.user._id
    ),
    active: state.paymentApp.paymentDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPayments: dispatch(fetchPayments()),
    toggleStarredPayment: (_id) => dispatch(toggleStarredPayment(_id)),
    deletePayment: (_id) => dispatch(destroyPayment(_id)),
    paymentDetails: (_id) => dispatch(paymentDetails(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentList);
