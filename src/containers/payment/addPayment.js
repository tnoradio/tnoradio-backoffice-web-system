import React, { useState, useEffect } from "react";
import "../../assets/scss/views/components/extra/upload.scss";
import {
  Col,
  Row,
  FormGroup,
  Label,
  Button,
  Input,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import Select from "react-select";
import { connect, useDispatch } from "react-redux";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { addPayment } from "../../redux/actions/payments";
import { useSelector } from "react-redux";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import MyFilesDropzone from "../../components/shared/MyFilesDropzone";
import { fetchShows } from "../../redux/actions/shows";
import SpinnerLoader from "../../components/shared/LoadingSpinner";
import { fetchRecordingTurns } from "../../redux/actions/recordingTurns";
//import { addNotification } from "../../redux/actions/notifications";
import { fetchUsers } from "../../redux/actions/users";
const groupStyles = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles = {
  backgroundColor: "#EBECF0",
  borderRadius: "2em",
  color: "#172B4D",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

const formSchema = Yup.object().shape({
  concept: Yup.string().required("Debes escribir un concepto."),
  amount: Yup.number().typeError("Debes escribir números"),
  payment_date: Yup.date().required("Debes escribir una fecha de pago."),
});

const mapStateToProps = (state) => {
  return {
    _id: undefined,
    shows: state.showApp.shows,
    recordingTurns: state.recordingTurnsApp.recordingTurns,
    adminUsers: state.userApp.users.filter((user) => {
      if (user.roles.some((role) => role.role === "ADMIN")) return user;
    }),
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchShows: dispatch(fetchShows()),
    fetchRecordingTurns: dispatch(fetchRecordingTurns()),
    fetchUsers: dispatch(fetchUsers()),
  };
};

const formatGroupLabel = (data) => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

const AddPayment = ({ shows, recordingTurns, toggle }) => {
  const state = useSelector((state) => state);
  const [receiptFiles, setReceiptFiles] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currency, setCurrency] = useState("USD");
  const [show, setShow] = useState(0);
  const [recordingTurn, setRecordingTurn] = useState("");
  const [method, setMethod] = useState("cash");
  const [type, setType] = useState("");
  const dispatch = useDispatch();

  const isUserAdmin = state.session?.user.roles.some(
    (role) => role.role === "ADMIN"
  );

  const showList = React.useMemo(() => {
    var list = [];

    list.push({ value: 0, label: "Seleccione un programa" });

    if (shows) {
      shows.forEach((show) => {
        show.producers.forEach((producer) => {
          if (producer.userId === state.session?.user._id || isUserAdmin)
            list.push({ value: show.id, label: show.name });
        });
        if (list.some((show) => show.id).length === 0)
          show.hosts.forEach((host) => {
            if (host.userId === state.session?.user._id)
              list.push({ value: show.id, label: show.name });
          });
      });
    }
    return list;
  }, []);

  const recordingTurnsList = React.useMemo(() => {
    var list = [];
    list.push({ value: "", label: "Seleccione una pauta" });
    if (recordingTurns) {
      recordingTurns.forEach((recordingTurn) => {
        if (recordingTurn.owner_id === state.session?.user._id || isUserAdmin)
          list.push({
            value: recordingTurn._id,
            label: recordingTurn.description,
          });
      });
    }
    return list;
  }, []);

  const showGroupOptions = [
    {
      label: "Programas",
      options: showList,
    },
  ];

  const recordingTurnsGroupOptions = [
    {
      label: "Pautas",
      options: recordingTurnsList,
    },
  ];

  const onDelete = (fileName) => {
    var filtered = receiptFiles.filter((file) => file.name !== fileName);
    setReceiptFiles(filtered);
  };

  useEffect(() => {}, [receiptFiles]);

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          amount: "",
          amountUsd: "",
          amountEuro: "",
          amountBs: "",
          amountOther: "",
          amountCrypto: "",
          owner_id: "",
          show_owner: 0,
          recording_turn_owner: "",
          concept: "",
          receipts: [],
          payment_date: "",
          currency: "",
          starred: false,
          deleted: false,
          reconciled: false,
          method: "",
          payment_type: "",
        }}
        validationSchema={formSchema}
        onSubmit={(values) => {
          setLoading(true);
          var showName =
            show !== 0 ? showList.find((obj) => obj.value === show).label : "";
          dispatch(
            addPayment(
              undefined,
              values.amount,
              values.amountBs,
              values.amountUsd,
              values.amountEuro,
              values.amountCrypto,
              values.amountOther,
              state.session?.user._id,
              show,
              recordingTurn,
              values.concept,
              receiptFiles,
              values.payment_date,
              values.starred,
              false,
              false,
              currency,
              method,
              values.payment_type,
              showName,
              toggle
            )
          );
          // eslint-disable-next-line no-lone-blocks
          {
            /*adminUsers.forEach((user) =>
            dispatch(
              addNotification(
                undefined,
                user._id,
                "Se ha registrado un pago",
                true,
                "PAYMENT"
              )
            )
              );*/
          }
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <SpinnerLoader loading={loading} />
            <ModalBody>
              <Row>
                <Col md={4} xs={12}>
                  <FormGroup>
                    <Label for="date">Fecha de Pago</Label>
                    <Field
                      type="date"
                      name="payment_date"
                      id="payment_date"
                      className={`form-control ${
                        errors.payment_date &&
                        touched.payment_date &&
                        "is-invalid"
                      }`}
                    />
                    {errors.payment_date && touched.payment_date ? (
                      <div className="invalid-feedback">
                        {errors.payment_date}
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Label for="expenseType">Concepto</Label>
                </Col>
                <Col xs={12} md={6} lg={3}>
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setType(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="SHOW"
                          value="SHOW"
                        />{" "}
                        Programa
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="PAUTA"
                          value="PAUTA"
                        />{" "}
                        Pauta/Evento
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
                <Col xs={12} md={6} lg={3}>
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setType(e.target.id)}
                  >
                    <FormGroup check disabled>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="RECORDING"
                          value="RECORDING"
                        />{" "}
                        Grabación
                      </Label>
                    </FormGroup>
                    <FormGroup check disabled>
                      <Label check>
                        <Input
                          type="radio"
                          name="type"
                          id="OTHER"
                          value="OTHER"
                        />{" "}
                        Otro
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                {type == "SHOW" &&
                showList !== undefined &&
                showList[0] !== undefined &&
                showList[0] !== null ? (
                  <Col xs={12} md={6}>
                    <Label for="show_owner">Seleccione el Programa</Label>
                    <FormGroup>
                      <Select
                        id="show_owner"
                        value={showList.filter((obj) => obj.value === show)}
                        options={showGroupOptions}
                        formatGroupLabel={formatGroupLabel}
                        onChange={(e) => setShow(e.value)}
                      />
                    </FormGroup>
                  </Col>
                ) : null}
                {type === "PAUTA" &&
                recordingTurnsList !== undefined &&
                recordingTurnsList[0] !== undefined &&
                recordingTurnsList[0] !== null ? (
                  <Col xs={12} md={6}>
                    <Label for="recording_turn_owner">
                      Seleccione la Pauta
                    </Label>
                    <FormGroup>
                      <Select
                        id="recording_turn_owner"
                        value={recordingTurnsList.filter(
                          (obj) => obj.value === recordingTurn
                        )}
                        options={recordingTurnsGroupOptions}
                        formatGroupLabel={formatGroupLabel}
                        onChange={(e) => {
                          console.log(e.value);
                          setRecordingTurn(e.value);
                        }}
                      />
                    </FormGroup>
                  </Col>
                ) : null}
              </Row>
              <Row>
                <Col xs={12} md={12}>
                  <FormGroup>
                    <Label for="concept">Descripción</Label>
                    <Field
                      className={`form-control ${
                        errors.concept && touched.concept && "is-invalid"
                      }`}
                      type="textarea"
                      name="concept"
                      id="concept"
                      required
                    />
                    {errors.concept && touched.concept ? (
                      <div className="invalid-feedback">{errors.concept}</div>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Label for="currency">Moneda</Label>
                </Col>
                <Col md="3" xs="12">
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setCurrency(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="USD"
                          value="USD"
                          defaultChecked
                        />{" "}
                        Dólares
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="Bs"
                          value="Bs"
                        />{" "}
                        Bolívares
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="Euro"
                          value="Euro"
                        />{" "}
                        Euro
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
                <Col md="5" xs="12">
                  <FormGroup
                    tag="fieldset"
                    onChange={(e) => setCurrency(e.target.id)}
                  >
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="crypto"
                          value="crypto"
                        />{" "}
                        Cripto (Equivalente en USD)
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input
                          type="radio"
                          name="currency"
                          id="other"
                          value="other"
                        />{" "}
                        Otro
                      </Label>
                    </FormGroup>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="4">
                  <FormGroup>
                    <Label for="method">Forma de Pago</Label>
                    <Input
                      type="select"
                      id="method"
                      name="method"
                      onChange={(e) => setMethod(e.target.value)}
                    >
                      <option value="none" defaultValue="" disabled="">
                        Seleccione
                      </option>
                      {currency !== "crypto" && (
                        <option value="cash" id="cash">
                          Efectivo
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="zelle" id="zelle">
                          Zelle
                        </option>
                      )}
                      {currency === "Bs" && (
                        <option value="transferBs." id="transferBs.">
                          Transferencia Bs.
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="transferUSD" id="transferUSD">
                          Transferencia en USA
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="transferPan" id="transferPan">
                          Transferencia Panamá
                        </option>
                      )}
                      {currency === "USD" ||
                        (currency === "Bs" && (
                          <option value="cardPan" id="cardPan">
                            Débito Panamá
                          </option>
                        ))}
                      {currency === "Bs" && (
                        <option value="mobile" id="mobile">
                          Pago Móvil
                        </option>
                      )}
                      {currency === "USD" && (
                        <option value="paypal" id="paypal">
                          Paypal
                        </option>
                      )}
                      {currency === "crypto" && (
                        <option value="binance" id="binance">
                          Binance
                        </option>
                      )}
                      <option value="other" id="other">
                        Otro
                      </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md="4" xs="12">
                  <FormGroup>
                    <Label for="amount">Monto</Label>
                    {currency === "USD" && (
                      <FormGroup>
                        <Field
                          name="amountUsd"
                          id="amountUsd"
                          className={`form-control ${
                            errors.amountUsd &&
                            touched.amountUsd &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountUsd && touched.amountUsd ? (
                          <div className="invalid-feedback">
                            {errors.amountUsd}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "Bs" && (
                      <FormGroup>
                        <Field
                          name="amountBs"
                          id="amountBs"
                          className={`form-control ${
                            errors.amountBs && touched.amountBs && "is-invalid"
                          }`}
                        />
                        {errors.amountBs && touched.amountBs ? (
                          <div className="invalid-feedback">
                            {errors.amountBs}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "Euro" && (
                      <FormGroup>
                        <Field
                          name="amountEuro"
                          id="amountEuro"
                          className={`form-control ${
                            errors.amountEuro &&
                            touched.amountEuro &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountEuro && touched.amountEuro ? (
                          <div className="invalid-feedback">
                            {errors.amountEuro}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "crypto" && (
                      <FormGroup>
                        <Field
                          name="amountCripto"
                          id="amountCripto"
                          className={`form-control ${
                            errors.amountCripto &&
                            touched.amountCripto &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountCripto && touched.amountCripto ? (
                          <div className="invalid-feedback">
                            {errors.amountCripto}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                    {currency === "other" && (
                      <FormGroup>
                        <Field
                          name="amountOther"
                          id="amountOther"
                          className={`form-control ${
                            errors.amountOther &&
                            touched.amountOther &&
                            "is-invalid"
                          }`}
                        />
                        {errors.amountOther && touched.amountOther ? (
                          <div className="invalid-feedback">
                            {errors.amountOther}
                          </div>
                        ) : null}
                      </FormGroup>
                    )}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Label for="receipts">Recibos</Label>
                  <MyFilesDropzone
                    layout={"user"}
                    disabled={false}
                    isEditPayment={false}
                    setReceiptFiles={setReceiptFiles}
                    showName={""}
                    onDelete={onDelete}
                    id={null}
                    selectedPayment={null}
                    receiptFiles={[]}
                    saveFiles={receiptFiles}
                    setSaveFiles={setReceiptFiles}
                  />
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Guardar
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPayment);
