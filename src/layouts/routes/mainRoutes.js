// import external modules
import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import internal(own) modules
import MainLayout from "../mainLayout";

const MainLayoutRoute = ({ render, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(matchProps) =>
        rest.authenticated === true ? (
          <MainLayout>{render(matchProps)}</MainLayout>
        ) : (
          <Redirect to="/login"></Redirect>
        )
      }
    />
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.session?.user,
    authenticated: state.session.authenticated,
  };
};

export default connect(mapStateToProps, null)(MainLayoutRoute);
