import React, { useEffect } from "react";
import { useDispatch, useSelector, useStore } from "react-redux";
import {
  Form,
  Collapse,
  Navbar,
  Nav,
  NavItem,
  Media,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import * as userUtils from "../../../utility/users/userUtils";
import {
  Moon,
  Mail,
  Menu,
  MoreVertical,
  Check,
  Bell,
  User,
  AlertTriangle,
  Inbox,
  Phone,
  Calendar,
  Lock,
  X,
  LogOut,
  Key,
  DollarSign,
  Info,
} from "react-feather";
import { Link } from "react-router-dom";
import {
  fetchUserNotifications,
  updateNotification,
} from "../../../redux/actions/notifications";

const Notifications = () => {
  const dispatch = useDispatch();
  const store = useStore();
  const state = useSelector((state) => state);

  const notifications = store.getState().notificationApp;
  // console.log(state);
  useEffect(() => {
    dispatch(fetchUserNotifications(store.getState().session?.user._id));
  }, []);
  useEffect(() => {}, [store.getState().notificationApp]);

  //console.log(notifications);

  return notifications.map((notification) => (
    <Media
      key={notification._id}
      className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3"
    >
      <Media left middle href="#" className="mr-2">
        {notification.type === "PAYMENT" ? (
          <span className="bg-success rounded-circle width-35 height-35 d-block">
            <DollarSign size={30} className="p-1 white margin-left-3" />
          </span>
        ) : (
          <span className="bg-info rounded-circle width-35 height-35 d-block">
            <Info size={30} className="p-1 white margin-left-3" />
          </span>
        )}
      </Media>
      <Media body>
        <h6 className="mb-1 text-bold-500 font-small-3">
          <span
            className={notification.type === "PAYMENT" ? "success" : "info"}
          >
            {notification.message}
          </span>
        </h6>
        {notification.type === "TASK_ADDED" ? (
          <span className="font-small-3 line-height-2">
            <Link to="/todo" className="info font-medium-1">
              Ir a tareas
            </Link>{" "}
            {notification.isActive === true ? (
              <Link
                to=""
                className="text-muted font-small-2"
                onClick={() =>
                  dispatch(
                    updateNotification(notification._id, "isActive", false)
                  )
                }
              >
                Marcar como leído
              </Link>
            ) : null}
          </span>
        ) : (
          <div>
            {notification.type === "PAYMENT" ? (
              <span className="font-small-3 line-height-2">
                <Link to="/payments" className="success font-medium-1">
                  Ir a pagos
                </Link>{" "}
                {notification.isActive === true ? (
                  <Link
                    to=""
                    className="text-muted font-small-2"
                    onClick={() =>
                      dispatch(
                        updateNotification(notification._id, "isActive", false)
                      )
                    }
                  >
                    Marcar como leído
                  </Link>
                ) : null}
              </span>
            ) : (
              <span className="font-small-3 line-height-2">
                <Link to="/recordingturns" className="info font-medium-1">
                  Ir a pautas{" "}
                  {notification.isActive === true ? (
                    <Link
                      to=""
                      className="text-muted font-small-2"
                      onClick={() =>
                        dispatch(
                          updateNotification(
                            notification._id,
                            "isActive",
                            false
                          )
                        )
                      }
                    >
                      Marcar como leído
                    </Link>
                  ) : null}
                </Link>
              </span>
            )}
          </div>
        )}
      </Media>
    </Media>
  ));
};

export default Notifications;
