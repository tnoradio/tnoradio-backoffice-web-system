export const telegramConfig = {
  api_id: process.env.REACT_APP_TELEGRAM_API_ID,
  api_hash: process.env.REACT_APP_TELEGRAM_API_HASH,
  test_dc: process.env.REACT_APP_TEST_DATACENTER,
  prod_dc: process.env.REACT_APP_PROD_DATACENTER,
};
