const ExpenseFiles = (state = [], action) => {
  switch (action.type) {
    case "GET_EXPENSE_RECEIPT_FILES":
      return [...state, action.file];

    default:
      return state;
  }
};

export default ExpenseFiles;
