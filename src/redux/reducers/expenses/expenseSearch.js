const ExpenseSearch = (state = "", action) => {
  switch (action.type) {
    case "FILTER_EXPENSE":
      return action.payload;
    case "SET_VISIBILITY_FILTER":
      return (state = "");
    default:
      return state;
  }
};

export default ExpenseSearch;
