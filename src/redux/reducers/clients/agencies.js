const initialState = {
  agencies: [],
};

const GetAllAgenciesReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ALL_AGENCIES":
      return {
        agencies: action.agencies,
      };
    default:
      return state;
  }
};

export default GetAllAgenciesReducer;
