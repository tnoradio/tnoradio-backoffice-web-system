const clientDetailsReducer = (state = "", action) => {
  switch (action.type) {
    case "CLIENT_DETAILS":
      return action._id;
    default:
      return state;
  }
};

export default clientDetailsReducer;
