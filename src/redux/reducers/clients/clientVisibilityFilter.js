import { clientVisibilityFilter } from "../../actions/clients";

const ClientVisibilityFilter = (
  state = clientVisibilityFilter.SHOW_ALL,
  action
) => {
  switch (action.type) {
    case "SET_VISIBILITY_FILTER":
      return action.filter;
    default:
      return state;
  }
};

export default ClientVisibilityFilter;
