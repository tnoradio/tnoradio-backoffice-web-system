const RecordingTurnDetails = (state = "", action) => {
  switch (action.type) {
    case "RECORDING_TURN_DETAILS":
      return action._id;
    default:
      return state;
  }
};

export default RecordingTurnDetails;
