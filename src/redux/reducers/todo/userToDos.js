const userToDos = (state = [], action) => {
  switch (action.type) {
    case "GET_USER_TODOS":
      return action.userToDos;
    default:
      return state;
  }
};

export default userToDos;
