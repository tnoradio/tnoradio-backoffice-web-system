const UpdateShowImage = (state = { showImage: {} }, action) => {
  switch (action.type) {
    case "UPDATE_SHOW_IMAGE":
      return { ...state, showImage: action.showImage };
    default:
      return state;
  }
};
export default UpdateShowImage;
