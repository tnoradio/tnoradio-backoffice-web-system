const PaymentFiles = (state = [], action) => {
  switch (action.type) {
    case "GET_PAYMENT_RECEIPT_FILES":
      return [...state, action.file];

    default:
      return state;
  }
};

export default PaymentFiles;
