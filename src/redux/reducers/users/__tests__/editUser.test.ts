/**
 * A reducer should return the new state after
 * applying the action to the previous state.
 */

import EditUser from "../editUser";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("edit user reducer", () => {
  const initialState = false;

  it("should return the initial state", () => {
    const reducer = EditUser(undefined, {});

    expect(reducer).toEqual(initialState);
  });

  it("should handle EDIT_USER Reducer", () => {
    const reducer = EditUser(initialState, {
      type: "EDIT_USER",
    });
    expect(reducer).toStrictEqual(true);
  });

  it("should handle EDIT_USER Reducer Initial True", () => {
    const reducer = EditUser(true, {
      type: "EDIT_USER",
    });
    expect(reducer).toStrictEqual(false);
  });

  it("should handle USER_DETAILS Reducer", () => {
    const reducer = EditUser(initialState, {
      type: "USER_DETAILS",
    });

    expect(reducer).toStrictEqual(false);
  });
});
