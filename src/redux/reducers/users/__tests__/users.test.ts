/**
 * A reducer should return the new state after
 * applying the action to the previous state.
 */

import users from "../users";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("update user reducer", () => {
  const initialState = [];
  //const updatedState = userUtils.mockUpdatedList;

  it("should return the initial state", () => {
    const reducer = users(initialState, {});

    expect(reducer).toEqual(initialState);
  });

  it("should handle TOGGLE_STARRED_USER Reducer", () => {
    const reducer = users(userUtils.mockUsersList, {
      type: "TOGGLE_STARRED_USER",
      _id: "mock_user_id_2",
    });

    //        console.log(reducer);
    expect(reducer).toEqual(userUtils.mockUserStarredList);
  });

  it("should handle DELETE_USER Reducer", () => {
    const reducer = users(userUtils.mockUsersBeforeDeleteList, {
      type: "DELETE_USER",
      _id: "mock_user_id_2",
    });

    //        console.log(reducer);
    expect(reducer).toEqual(userUtils.mockUsersDeletedList);
  });
});
