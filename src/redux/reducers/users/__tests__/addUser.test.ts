/**
 * A reducer should return the new state after
 * applying the action to the previous state.
 */

import users from "../users";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("add user reducer", () => {
  const initialState = [];

  it("should return the initial state", () => {
    const reducer = users(undefined, {});

    expect(reducer).toEqual(initialState);
  });

  it("should handle ADD_USER Reducer", () => {
    const reducer = users(initialState, {
      type: "ADD_USER",
      _id: "mock_user_id_1",
      name: "testUserName",
      lastName: "testLastName",
      image: "https://randomuser.me/api/portraits/lego/1.jpg",
      dni: 1245678,
      email: { value: "testEmail@email.com" },
      isActive: true,
      password: "testPassword",
      passwordConfirmation: "testPassword",
      gender: "O",
      role: "AUDIENCE",
      address: "testAddress",
      notes: "This is a test user for unit testing",
      company: "tetsCompany",
      department: "TestDepartment",
      phone: "55-555-555-5555",
      isStarred: false,
      isDeleted: false,
      updatedBy: "",
      message: "Usuario agregado con éxito",
    });

    expect(reducer).toStrictEqual([
      {
        _id: "mock_user_id_1",
        name: "testUserName",
        lastName: "testLastName",
        image: "https://randomuser.me/api/portraits/lego/1.jpg",
        dni: 1245678,
        email: { value: "testEmail@email.com" },
        isActive: true,
        gender: "O",
        role: "AUDIENCE",
        address: "testAddress",
        notes: "This is a test user for unit testing",
        company: "tetsCompany",
        department: "TestDepartment",
        phone: "55-555-555-5555",
        isStarred: false,
        isDeleted: false,
        updatedBy: "",
      },
    ]);
  });
});
