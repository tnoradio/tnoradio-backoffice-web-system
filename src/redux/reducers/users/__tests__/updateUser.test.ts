/**
 * A reducer should return the new state after
 * applying the action to the previous state.
 */

import users from "../users";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("update user reducer", () => {
  const initialState = [];
  //const updatedState = userUtils.mockUpdatedList;

  it("should return the initial state", () => {
    const reducer = users(initialState, {});

    expect(reducer).toEqual(initialState);
  });

  it("should handle UPDATE_USER Reducer", () => {
    const reducer = users(userUtils.mockUsersList, {
      type: "UPDATE_USER",
      _id: "mock_user_id_1",
      field: "name",
      value: "testName Updated",
    });

    //        console.log(reducer);
    expect(reducer).toEqual(userUtils.mockUsersUpdatedList);
  });
});
