/**
 * A reducer should return the new state after
 * applying the action to the previous state.
 */

import fetchUsers from "../fetchUsers";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("fetch users reducer", () => {
  const initialState = {
    users: [],
  };

  it("should return the initial state", () => {
    const reducer = fetchUsers(undefined, {});

    expect(reducer).toEqual(initialState);
  });

  it("should handle GET_ALL_USERS", () => {
    const reducer = fetchUsers(initialState, {
      type: "GET_ALL_USERS",
      usersList: userUtils.mockUsersList,
    });

    expect(reducer).toEqual({ users: userUtils.mockUsersList });
  });
});
