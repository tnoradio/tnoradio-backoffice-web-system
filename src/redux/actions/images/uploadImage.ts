import { ImageRepository } from "../../../core/images/domain/ImageRepository";
import { AxiosImageRepository } from "../../../core/images/infrastructure/repository/AxiosImageRepository";
import { toastr } from "react-redux-toastr";
import { ImageUploader } from "../../../core/images/application/use_cases/uploadImage/ImageUploader";

const imageRepository: ImageRepository = new AxiosImageRepository();
const imageUploader = new ImageUploader(imageRepository);

export default async function uploadImage(
  imageFile: File,
  category: string,
  name: string,
  type: string,
  location: string
) {
  try {
    const res = await imageUploader.run(
      imageFile,
      category,
      name,
      type,
      location
    );
    console.log(res);
    toastr.success("Se guardado exitosamente");
    return res;
  } catch (e) {
    console.log(e);
    toastr.warning("Problem uploading Image Mini Banner");
  }
}
