import { ImageUpdater } from "../../../core/images/application/use_cases/updateimage/ImageUpdater";
import { ImageRepository } from "../../../core/images/domain/ImageRepository";
import { AxiosImageRepository } from "../../../core/images/infrastructure/repository/AxiosImageRepository";
import { toastr } from "react-redux-toastr";

const imageRepository: ImageRepository = new AxiosImageRepository();
const imageModifier = new ImageUpdater(imageRepository);

export default async function updateImage(
  file: File,
  category: string,
  type: string,
  name: string,
  location: string
) {
  try {
    const response = await imageModifier.run(
      file,
      category,
      type,
      name,
      location
    );
    toastr.success("Imagen Actualizada");
    return response;
  } catch (err) {
    console.error(err);
    toastr.error("No se pudo actualizar la imagen");
    return err;
  }
}
