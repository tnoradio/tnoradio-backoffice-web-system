import { ImageRepository } from "../../../core/images/domain/ImageRepository";
import { AxiosImageRepository } from "../../../core/images/infrastructure/repository/AxiosImageRepository";
import { ImageGetter } from "../../../core/images/application/use_cases/getimage/ImageGetter";
import { toastr } from "react-redux-toastr";

const imageRepository: ImageRepository = new AxiosImageRepository();
const imageGetter = new ImageGetter(imageRepository);

export default function fetchImageImage(
  category: string,
  type: string,
  name: string,
  location: string
) {
  return async function (dispatch) {
    try {
      const data = await imageGetter.run(category, type, name, location);
      dispatch({
        type: "GET_IMAGE",
        image: data,
      });
    } catch (err) {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de usuarios";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.status + " " + err.statusText;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_IMAGE",
        image: {},
      });
    }
  };
}
