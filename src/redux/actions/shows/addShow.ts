import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowCreator } from "../../../core/shows/application/use_cases/createShow/ShowCreator";
import { toastr } from "react-redux-toastr";
import moment from "moment";

const showRepository: ShowRepository = new AxiosShowRepository();
const showCreator = new ShowCreator(showRepository);

export default function addShow(
  name: string,
  email: string,
  genre: string,
  synopsis: string,
  pg,
  type,
  schedules,
  hosts,
  advertisers,
  socials,
  images,
  imagesFiles,
  colors,
  producers,
  slug,
  age_range,
  target,
  target_gender,
  seasons,
  genres,
  subgenres
) {
  let showAdvertisers = [];

  return async (dispatch) => {
    let newShow = {
      id: undefined,
      name: name,
      email: email,
      genre: genre,
      synopsis: synopsis,
      pgClassification: pg,
      type: type,
      showSchedules: schedules,
      initDate: new Date(Date.now()),
      producers: producers,
      socials: socials,
      hosts: hosts,
      advertisers: [],
      images: [],
      colors: colors,
      isEnabled: false,
      isDeleted: false,
      isActive: true,
      showSlug: slug,
      age_range: age_range,
      target: target,
      target_gender: target_gender,
      seasons: seasons,
      genres: genres,
      subgenres: subgenres,
    };
    console.log(newShow);
    return showCreator
      .run(newShow)
      .then((res) => {
        console.log(res);
        toastr.success("Se guardado todo exitosamente");
        dispatch({
          type: "ADD_SHOW",
          id: res.id,
          name: res.name ? res.name : "",
          genre: res.genre ? res.genre : "",
          synopsis: res.synopsis ? res.synopsis : "",
          showType: res.type ? res.type : "",
          showSchedules: res.showSchedules ? res.showSchedules : [],
          email: res.email ? res.email : "",
          initDate: res.initDate ? res.initDate : new Date(Date.now()),
          isEnabled: res.isEnabled ? res.isEnabled : false,
          isActive: res.isActive ? res.isActive : true,
          pgClassification: res.pgClassification ? res.pgClassification : "",
          producers: res.producers ? res.producers : "",
          socials: res.socials ? res.socials : [],
          hosts: res.hosts ? res.hosts : [],
          advertisers: showAdvertisers ? showAdvertisers : [],
          images: res.images ? res.images : [],
          colors: res.colors ? res.colors : [],
          showSlug: res.showSlug ? res.showSlug : "",
          isDeleted: res.isDeleted ? res.isDeleted : false,
          age_range: res.age_range ? res.age_range : [],
          target: res.target ? res.target : [],
          target_gender: res.target_gender ? res.target_gender : [],
          seasons: res.seasons ? res.seasons : [],
          genres: res.genres ? res.genres : [],
          subgenres: res.subgenres ? res.subgenres : [],
          message: "Show agregado con éxito",
        });
        dispatch({ type: "UPDATE", isRegister: true });
      })
      .catch((err) => {
        console.log(err);
        let errorMessage = "Hubo un error";
        if (err.data) {
          errorMessage = err.data.error;
        } else {
          if (err.message) {
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "ADD_SHOW",
          message: errorMessage,
          id: "",
          name: "",
          genre: "",
          synopsis: "",
          showType: "",
          showSchedules: [],
          email: "",
          pgClassification: "",
          initDate: null,
          isEnabled: false,
          isActive: false,
          producers: [],
          socials: [],
          hosts: [],
          advertisers: [],
          images: [],
          colors: [],
          showSlug: "",
          isDeleted: false,
          age_range: [],
          target: [],
          target_gender: [],
          seasons: [],
          genres: [],
          subgenres: [],
        });
        dispatch({ type: "UPDATE", isRegister: false });
      });
  };
}
