import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowModifier } from "../../../core/shows/application/use_cases/modifyShow/ShowModifier";
import { store } from "../../storeConfig/store";
import { toastr } from "react-redux-toastr";

const showRepository: ShowRepository = new AxiosShowRepository();
const showModifier = new ShowModifier(showRepository);

export default function toggleActiveShow(id) {
  const shows = store.getState().showApp.shows;
  const activeShow = shows.find((show) => show.id === id);

  const activeShowReq = {
    id: id,
    field: "isActive",
    value: !activeShow.isActive,
  };

  return function (dispatch) {
    console.log("Active SHOW");
    return showModifier
      .run(activeShowReq)
      .then((response) => {
        // console.log("RESPONSE", response);
        dispatch({
          type: "TOGGLE_ACTIVE_SHOW",
          id,
        });
      })
      .catch((err) => {
        console.error(err);
        toastr.error(err);
      });
  };
}
