import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowModifier } from "../../../core/shows/application/use_cases/modifyShow/ShowModifier";
import { store } from "../../storeConfig/store";
import { toastr } from "react-redux-toastr";

const showRepository: ShowRepository = new AxiosShowRepository();
const showModifier = new ShowModifier(showRepository);

export default function toggleEnabledShow(id) {
  const shows = store.getState().showApp.shows;
  const enabledShow = shows.find((show) => show.id === id);

  const enabledShowReq = {
    id: id,
    field: "isEnabled",
    value: !enabledShow.isEnabled,
  };

  return function (dispatch) {
    console.log("Enable SHOW");
    return showModifier
      .run(enabledShowReq)
      .then((response) => {
        // console.log("RESPONSE", response);
        dispatch({
          type: "TOGGLE_ENABLED_SHOW",
          id,
        });
      })
      .catch((err) => {
        console.error(err);
        toastr.error(err);
      });
  };
}
