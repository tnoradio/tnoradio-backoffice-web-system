import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowImageFromDBGetter } from "../../../core/shows/application/use_cases/imagefromdbgetter/getShowImageFromDB";
import { toastr } from "react-redux-toastr";

const showRepository: ShowRepository = new AxiosShowRepository();
const showImageFromDBGetter = new ShowImageFromDBGetter(showRepository);

//let res: ShowImage;

export default function fetchShowImage(name, slug) {
  //console.log("FETCH SHOWS");
  return function (dispatch) {
    return showImageFromDBGetter
      .run(name, slug)
      .then((data) => {
        // console.log(data);

        dispatch({
          type: "GET_SHOW_IMAGE",
          showImage: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_SHOW_IMAGE",
          showImage: {},
        });
      });
  };
}
