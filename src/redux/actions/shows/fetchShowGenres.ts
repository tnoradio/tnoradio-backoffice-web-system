import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowGenresGetter } from "../../../core/shows/application/use_cases/getShowGenres/ShowGenresGetter";

const showRepository: ShowRepository = new AxiosShowRepository();
const fetchGenres = new ShowGenresGetter(showRepository);

export default function fetchShowGenres() {
  return async function (dispatch) {
    try {
      let showGenres = await Promise.resolve(fetchGenres.run());
      dispatch({
        type: "GET_ALL_SHOW_GENRES",
        showGenres: showGenres,
      });
    } catch (err) {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        console.log(err);
        errorMessage = err.data.status;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_ALL_SHOW_GENRES",
        showGenres: [],
      });
    }
  };
}
