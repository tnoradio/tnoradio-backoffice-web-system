import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { RecommendedShowsGetter } from "../../../core/shows/application/use_cases/getShows/RecommendedShowsGetter";

const showRepository: ShowRepository = new AxiosShowRepository();
const indexShows = new RecommendedShowsGetter(showRepository);

export default function fetchRecomendedShows(
  genders: string[],
  genres: string[],
  target: string[],
  age_ranges: string[]
) {
  return function (dispatch) {
    return indexShows
      .run(genders, genres, target, age_ranges)
      .then((data) => {
        console.log(data);
        dispatch({
          type: "GET_RECOMMENDED_SHOWS",
          recommendedShows: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de shows";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.data.status;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_RECOMMENDED_SHOWS",
          recommendedShows: [],
        });
      });
  };
}
export async function getRecomendedShows(
  genders: string[],
  genres: string[],
  target: string[],
  age_ranges: string[]
) {
  console.log("GET RECOMMENDED SHOWS");

  try {
    const data = await indexShows.run(genders, genres, target, age_ranges);
    console.log(data);
    return data;
  } catch (err) {
    console.log("INDEX ERRROR");
    console.log(err);
    let errorMessage = "No se pudo obtener la lista de shows";
    if (err.data) {
      console.log("HAY ERROR DATA");
      console.log(err);
      errorMessage = err.data.status;
    } else {
      if (err.message) {
        console.log("HAY ERROR MESSAGE");
        errorMessage = err.message;
      } else {
        console.log("ERROR GENERICO INDEX");
      }
    }
    toastr.error(errorMessage);
    return [];
  }
}
