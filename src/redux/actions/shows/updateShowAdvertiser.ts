import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowAdvertiserModifier } from "../../../core/shows/application/use_cases/modifyShowAdvertiser/ShowAdvertiserModifier";

const showRepository: ShowRepository = new AxiosShowRepository();
const showAdvertiserModifier = new ShowAdvertiserModifier(showRepository);

export default function updateShowAdvertiser(id, field, value) {
  return function (dispatch) {
    const updatedShowAdvertiser = {
      id: id,
      field: field,
      value: value,
    };

    return showAdvertiserModifier
      .run(updatedShowAdvertiser)
      .then((response) => {
        console.log(response);
        toastr.success("Show Actualizado");
        dispatch({
          type: "UPDATE_SHOW_ADVERTISER",
          id: id,
          field: field,
          value: value,
        });
      })
      .catch((err) => {
        console.error(err);
        let errorMessage = "No se pudo actualizar el anunciante";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err.data.message);
          errorMessage = err.data.message;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
      });
  };
}
