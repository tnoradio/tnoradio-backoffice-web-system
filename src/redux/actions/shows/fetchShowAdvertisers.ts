import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowAdvertisersGetter } from "../../../core/shows/application/use_cases/getShowAdvertisers/ShowAdvertisersGetter";

const showRepository: ShowRepository = new AxiosShowRepository();
const fetchAdvertisers = new ShowAdvertisersGetter(showRepository);

export async function getShowAdvertisers(showId: string) {
  //console.log("FETCH SHOW ADV");

  try {
    let showAdvertisers = await Promise.resolve(fetchAdvertisers.run(showId));
    console.log(showAdvertisers);
    return showAdvertisers;
  } catch (err) {
    console.log("INDEX ERRROR");
    console.log(err);
    let errorMessage = "No se pudo obtener la lista de anunciantes del show";
    if (err.data) {
      console.log("HAY ERROR DATA");
      console.log(err);
      errorMessage = err.data.status;
    } else {
      if (err.message) {
        console.log("HAY ERROR MESSAGE");
        errorMessage = err.message;
      } else {
        console.log("ERROR GENERICO INDEX");
      }
    }
    toastr.error(errorMessage);
    return [];
  }
}

export default function fetchShowAdvertiserss(showId: string) {
  return function (dispatch) {
    return fetchAdvertisers
      .run(showId)
      .then((data) => {
        console.log(data);
        dispatch({
          type: "GET_ALL_SHOW_ADVERTISERS",
          showAdvertisers: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de shows";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.data.status;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_ALL_SHOW_ADVERTISERS",
          showAdvertisers: [],
        });
      });
  };
}
