import { ImageModifier } from "../../../core/shows/application/use_cases/updateimage/ImageModifier";
import { toastr } from "react-redux-toastr";
import { AxiosImageRepository } from "../../../core/images/infrastructure/repository/AxiosImageRepository";

const imageModifier = new ImageModifier(new AxiosImageRepository());

export default async function updateShowImage(
  imageFile,
  category,
  type,
  name,
  location
) {
  try {
    const response = await imageModifier.run(
      imageFile,
      name,
      location,
      type,
      category
    );
    console.log(response);
    toastr.success("Imagen Actualizada");
    return response;
  } catch (err) {
    console.error(err);
    throw err;
  }
}
