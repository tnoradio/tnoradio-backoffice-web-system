import { AddEpisodeTag } from "../../../core/shows/application/use_cases/addepisodetag/AddEpisodeTag";
import { ShowEpisodeTag } from "../../../core/shows/domain/ShowEpisodeTag";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";

const addEpisodeTag = async (tag: ShowEpisodeTag) => {
  try {
    const response = await new AddEpisodeTag(new AxiosShowRepository()).execute(
      tag
    );
    return response;
  } catch (err) {
    console.log(err);
    return null;
  }
};

export default addEpisodeTag;
