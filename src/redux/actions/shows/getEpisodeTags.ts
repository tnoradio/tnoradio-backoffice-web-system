import { GetEpisodeTags } from "../../../core/shows/application/use_cases/getepisodetags/GetEpisodeTags";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";

const episodeTagGetter = new GetEpisodeTags(new AxiosShowRepository());

export const getEpisodeTags = () => {
  try {
    return episodeTagGetter.execute();
  } catch (e) {
    console.log(e);
    throw e;
  }
};
