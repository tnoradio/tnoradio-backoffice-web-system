import { ShowEpisode } from "../../../core/shows/domain/ShowEpisode";
import { UpdateShowEpisode } from "../../../core/shows/application/use_cases/updateshowepisode/UpdateShowEpisode";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";

const episodeUpdater = new UpdateShowEpisode(new AxiosShowRepository());

export const updateShowEpisode = async (episode: ShowEpisode) => {
  try {
    const updatedShow = await episodeUpdater.execute(episode);
    return updatedShow;
  } catch (error) {
    console.error(error);
    throw error;
  }
};
