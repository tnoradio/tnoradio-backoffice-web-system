import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowSubGenresGetter } from "../../../core/shows/application/use_cases/getShowSubGenres/ShowSubGenresGetter";

const showRepository: ShowRepository = new AxiosShowRepository();
const fetchSubGenres = new ShowSubGenresGetter(showRepository);

export default function fetchShowSubGenres() {
  return async function (dispatch) {
    try {
      const showSubGenres = await Promise.resolve(fetchSubGenres.run());
      dispatch({
        type: "GET_ALL_SHOW_SUB_GENRES",
        showSubGenres: showSubGenres,
      });
    } catch (err) {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        console.log(err);
        errorMessage = err.data.status;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_ALL_SHOW_SUB_GENRES",
        showSubGenres: [],
      });
    }
  };
}
