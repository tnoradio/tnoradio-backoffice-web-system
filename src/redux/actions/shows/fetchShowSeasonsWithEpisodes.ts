import { ShowSeasonsWithEpisodesGetter } from "../../../core/shows/application/use_cases/fetchshowepisodes/FetchShowSeasonsWithEpisodes";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";

const showSeasonsWithEpisodesGetter = new ShowSeasonsWithEpisodesGetter(
  new AxiosShowRepository()
);

const fetchShowSeasonsWithEpisodes = async (showId: number) => {
  try {
    const response = await showSeasonsWithEpisodesGetter.run(showId);
    return response;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export default fetchShowSeasonsWithEpisodes;
