import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { AddTagToEpisode } from "../../../core/shows/application/use_cases/addtagtoepisode/AddTagToEpisode";

const tagToEpisodeAdder = new AddTagToEpisode(new AxiosShowRepository());

export default async function addTagToEpisode(tag, episodeId) {
  try {
    return await tagToEpisodeAdder.execute(episodeId, tag);
  } catch (error) {
    console.log(error);
    throw error;
  }
}
