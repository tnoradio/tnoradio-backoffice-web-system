import { CreateShowSeason } from "../../../core/shows/application/use_cases/createshowseason/CreateShowSeason";
import { ShowSeason } from "../../../core/shows/domain/ShowSeason";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";

const seasonCreator = new CreateShowSeason(new AxiosShowRepository());

export const addSeason = async (season: ShowSeason) => {
  try {
    const result = await seasonCreator.execute(season);
    return result;
  } catch (error) {
    console.error(error);
  }
};
