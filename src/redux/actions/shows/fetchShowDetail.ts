import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowDetailGetter } from "../../../core/shows/application/use_cases/getShowDetail/ShowDetailtGetter";
import { ShowByIdGetter } from "../../../core/shows/application/use_cases/getShowById/ShowDetailtGetter";
import { toastr } from "react-redux-toastr";

const showRepository: ShowRepository = new AxiosShowRepository();
const showDetailGetter = new ShowDetailGetter(showRepository);
const showByIdGetter = new ShowByIdGetter(showRepository);

export default function fetchShowDetail(slug: string) {
  return async function (dispatch) {
    try {
      const data = await showDetailGetter.run(slug);
      dispatch({
        type: "GET_SHOW_DETAIL",
        showDetail: data,
      });
    } catch (err) {
      console.log(err);
      let errorMessage = "No se pudo obtener el detalle del show";
      if (err.data) {
        console.log(err);
        errorMessage = err.status + " " + err.statusText;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_SHOW_DETAIL",
        showDetail: {},
      });
    }
  };
}
export async function fetchShowById(_id: number) {
  try {
    return await showByIdGetter.run(_id);
  } catch {
    return {};
  }
}
