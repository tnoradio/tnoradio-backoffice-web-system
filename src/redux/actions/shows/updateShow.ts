import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowModifier } from "../../../core/shows/application/use_cases/modifyShow/ShowModifier";
import { toastr } from "react-redux-toastr";

const showRepository: ShowRepository = new AxiosShowRepository();
const showModifier = new ShowModifier(showRepository);

export default function updateShow(id: number, field: string, value: any) {
  return async function (dispatch) {
    const updatedShow = {
      id: id,
      field: field,
      value: value,
    };

    try {
      await showModifier.run(updatedShow);
      toastr.success("Show Actualizado");
      dispatch({
        type: "UPDATE_SHOW",
        id: id,
        field: field,
        value: value,
      });
    } catch (err) {
      console.error(err);
      let errorMessage = "No se pudo actualizar el show";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err.data.message);
        errorMessage = err.data.message;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
    }
  };
}
