import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowTypesGetter } from "../../../core/shows/application/use_cases/showTypeGetter/getShowTypes";

const showRepository: ShowRepository = new AxiosShowRepository();
const showTypesGetter = new ShowTypesGetter(showRepository);

export default function fetchShowTypes() {
  return async function (dispatch) {
    try {
      const showTypes = await Promise.resolve(showTypesGetter.run());
      return dispatch({
        type: "GET_ALL_SHOW_TYPES",
        showTypes: showTypes,
      });
    } catch (err) {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.data.status;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_ALL_SHOW_TYPES",
        showTypes: [],
      });
    }
  };
}
