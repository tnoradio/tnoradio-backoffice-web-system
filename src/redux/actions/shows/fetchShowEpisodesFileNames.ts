import { ShowFileNamesGetter } from "../../../core/shows/application/use_cases/fetchshowepisodes/FetchShowEpisodesFileNames";
import { AxiosOnDemandRepository } from "../../../core/shows/infrastructure/repository/AxiosOnDemandRepository";

const showFileNamesGetter = new ShowFileNamesGetter(
  new AxiosOnDemandRepository()
);

const fetchShowFileNames = async (showSlug: string) => {
  try {
    const response = await showFileNamesGetter.run(showSlug);
    return response;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export default fetchShowFileNames;
