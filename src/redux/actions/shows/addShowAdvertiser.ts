import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowAdvertiserCreator } from "../../../core/shows/application/use_cases/createAdvertiser/advertiserCreator";

const showRepository: ShowRepository = new AxiosShowRepository();
const advertiserCreator = new ShowAdvertiserCreator(showRepository);

export default function addShowAdvertiser(name, website, image, owner) {
  return async (dispatch) => {
    let advertiser = {
      id: undefined,
      name: name,
      website: website,
      image: image,
      owner: owner,
    };

    console.log(image);

    advertiserCreator
      .run(
        advertiser.image,
        `advertiser_tnoradio${advertiser.name}.jpg`,
        advertiser.name,
        owner,
        advertiser.website
      )
      .then((res) => {
        console.log(res);
        dispatch({
          type: "ADD_SHOW_ADVERTISER",
          id: res.id,
          name: res.name ? res.name : "",
          website: res.website ? res.website : "",
          image: res.image ? res.image : null,
          message: "Se ha agregado con éxito",
        });
        toastr.success("Se guardado el anunciante exitosamente");
      })
      .catch((e) => {
        console.log(e);
        dispatch({
          type: "ADD_SHOW_ADVERTISER",
          id: "",
          name: "",
          website: "",
          image: null,
        });
        toastr.error("No se pudo guardar el anunciante exitosamente");
      });
  };
}
