import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { toastr } from "react-redux-toastr";
import { ShowPGsGetter } from "../../../core/shows/application/use_cases/getShowPGs/getShowPGs";

const showRepository: ShowRepository = new AxiosShowRepository();
const fetchShowPGs = new ShowPGsGetter(showRepository);

export default function fetchShowPGClass() {
  return async function (dispatch) {
    try {
      let showPGs = await Promise.resolve(fetchShowPGs.run());
      return dispatch({
        type: "GET_ALL_SHOW_PG_CLASS",
        showPGs: showPGs,
      });
    } catch (err) {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.data.status;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_ALL_SHOW_PG_CLASS",
        showPGs: [],
      });
    }
  };
}
