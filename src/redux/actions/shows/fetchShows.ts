import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { AllShowsGetter } from "../../../core/shows/application/use_cases/getShows/AllShowsGetter";
import { toastr } from "react-redux-toastr";

const showRepository: ShowRepository = new AxiosShowRepository();
const indexShows = new AllShowsGetter(showRepository);

export default function fetchShows() {
  return async function (dispatch) {
    try {
      const data = await indexShows.run();
      //console.log(data);
      dispatch({
        type: "GET_ALL_SHOWS",
        shows: data,
      });
    } catch (err) {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.data.status;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      dispatch({
        type: "GET_ALL_SHOWS",
        shows: [],
      });
    }
  };
}
