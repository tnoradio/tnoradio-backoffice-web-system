import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowDeleter } from "../../../core/shows/application/use_cases/destroyShow/ShowDeleter";
import { ShowAdvertiserDeleter } from "../../../core/shows/application/use_cases/destroyShowAdvertiser/ShowAdvertiserDeleter";

const showRepository: ShowRepository = new AxiosShowRepository();
const showDeleter = new ShowDeleter(showRepository);
const showAdvertiserDeleter = new ShowAdvertiserDeleter(showRepository);

export const showDetails = (id) => {
  return function (dispatch) {
    dispatch({
      type: "SHOW_DETAILS",
      id,
    });
  };
};

export const setEditShowFlag = (flag) => ({
  type: "EDIT_SHOW",
  flag,
});

export const setVisibilityFilter = (filter) => {
  console.log(filter);
  return {
    type: "SET_VISIBILITY_FILTER",
    filter,
  };
};

export const showsSearch = (searchTerm) => ({
  type: "FILTER_SHOW",
  payload: searchTerm,
});

export const deleteShow = (id) => {
  return function (dispatch) {
    return showDeleter
      .run(id)
      .then((response) => {
        dispatch({
          type: "DELETE_SHOW",
          id,
        });
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: "DELETE_SHOW",
          id,
        });
      });
  };
};

export const deleteShowAdvertiser = (id) => {
  return function (dispatch) {
    return showAdvertiserDeleter
      .run(id)
      .then((response) => {
        dispatch({
          type: "DELETE_SHOW_ADVERTISER",
          id,
        });
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: "DELETE_SHOW_ADVERTISER",
          id,
        });
      });
  };
};

export const showsVisibilityFilter = {
  SHOW_ALL: "SHOW_ALL",
  FREQUENT_SHOW: "FREQUENT_SHOW",
  SEARCH_SHOW: "SEARCH_SHOW",
  DELETED_SHOW: "DELETED_SHOW",
  MOTIVATIONAL_SHOW: "MOTIVATIONAL_SHOW",
  BUSINESS_SHOW: "BUSINESS_VALUE",
  HEALTH_SHOW: "HEALTH_SHOW",
  MAGAZINE_SHOW: "MAGAZINE_SHOW",
  ENTERTAINMENT_SHOW: "ENTERTAINMENT_SHOW",
  SPORTS_SHOW: "SPORTS_SHOW",
  ENABLED_SHOW: "ENABLED_SHOW",
  TALK_SHOW: "TALK_SHOW",
  ENTREPRENEURSHIP_SHOW: "ENTREPRENEURSHIP_SHOW",
  CULTURE_SHOW: "CULTURE_SHOW",
  ECOLOGY_SHOW: "ECOLOGY_SHOW",
  MUSIC_SHOW: "MUSIC_SHOW",
  ACTIVE_SHOW: "ACTIVE_SHOW",
};

export { default as fetchShows } from "./fetchShows";
export { default as fetchRecommendedShows } from "./fetchRecommendedShows";
export { default as addShow } from "./addShow";
export { default as toggleEnabledShow } from "./toggleEnabledShow";
export { default as toggleActiveShow } from "./toggleActiveShow";
export { default as updateShow } from "./updateShow";
export { default as fetchShowGenres } from "./fetchShowGenres";
export { default as fetchShowPGClass } from "./fetchShowPGs";
export { default as fetchShowTypes } from "./fetchShowTypes";
export { default as fetchShowAdvertisers } from "./fetchShowAdvertisers";
export { default as updatedShowAdvertiser } from "./updateShowAdvertiser";
export { default as addShowAdvertiser } from "./addShowAdvertiser";
