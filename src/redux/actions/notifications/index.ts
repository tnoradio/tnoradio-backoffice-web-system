import { UserRepository } from "../../../core/users/domain/UserRepository";
//import { notificationDetailGetter } from '../../../core/notifications/application/use_cases/getnotificationDetail/notificationDetailtGetter'
import { toastr } from "react-redux-toastr";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserNotificationsGetter } from "../../../core/users/application/use_cases/getusernotifications/UserNotificationsGetter";
import { NotificationCreator } from "../../../core/users/application/use_cases/createnotification/NotificationCreator";
import { NotificationModifier } from "../../../core/users/application/use_cases/updatenotificationstatus/NotificationModifier";

const userRepository: UserRepository = new AxiosUserRepository();
const getUserNotifications = new UserNotificationsGetter(userRepository);
const createNotification = new NotificationCreator(userRepository);
const notificationModifier = new NotificationModifier(userRepository);

export const addNotification = (
  _id: string,
  user: string,
  message: string,
  isActive: Boolean,
  type: string
) => {
  let newNotification = {
    _id: undefined,
    user: user ? user : "",
    message: message ? message : "",
    isActive: isActive ? isActive : true,
    type: type ? type : "",
  };

  console.log(newNotification);

  return (dispatch) => {
    return createNotification
      .run(newNotification)
      .then((res) => {
        toastr.success("Se ha enviado una notificación");
        dispatch({
          type: "ADD_USER_NOTIFICATION",
          notification: {
            _id: res._id,
            user: res.user,
            message: res.message,
            isActive: res.isActive,
            type: res.type,
          },
        });
        //  window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        //toastr.error("Hubo un problema");
        dispatch({
          type: "ADD_USER_NOTIFICATION",
          notification: {
            _id: _id ? _id : "",
            user: user ? user : "",
            message: message ? message : "",
            isActive: isActive ? isActive : true,
            type: type ? type : "",
          },
        });
      });
  };
};

export const updateNotification = (_id, field, value) => {
  const updateValues = {
    _id: _id,
    field: field,
    value: value,
  };

  return (dispatch) =>
    notificationModifier
      .run(updateValues)
      .then((res) => {
        toastr.success("Se ha cambiado el estatus de la notificacion");
        dispatch({
          type: "UPDATE_NOTIFICATION_STATE",
          _id: _id,
          field: field,
          value: value,
        });
      })
      .catch((err) => {
        console.log(err);
      });
};

export function fetchUserNotifications(userId: string) {
  return function (dispatch) {
    return getUserNotifications
      .run(userId)
      .then((data) => {
        dispatch({
          type: "GET_USER_NOTIFICATIONS",
          notifications: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de notifications";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
        dispatch({
          type: "GET_USER_NOTIFICATIONS",
          notifications: [],
        });
      });
  };
}
