import { UserLogin } from "../../../core/users/application/use_cases/login/UserLogin";
import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { LOGIN_ACTION_KEY } from "../types/types";
//import { UserLogout } from '../../../service/users/application/use_cases/logout/UserLogout';
import { sessionService } from "redux-react-session";
import { toastr } from "react-redux-toastr";

const userRepository: UserRepository = new AxiosUserRepository();
const userLogin = new UserLogin(userRepository);
//const userLogout = new UserLogout(userRepository);

export const LoginAction = (userLoginRequest) => (dispatch) => {
  return new Promise((resolve, reject) => {
    userLogin
      .run(userLoginRequest)
      .then((response) => {
        if (response) {
          const token = response.token;
          sessionService
            .saveSession(token)
            .then(() => {
              sessionService
                .saveUser(response.user)
                .then(() => {
                  dispatch({ type: LOGIN_ACTION_KEY, payload: response });
                  resolve({ success: true });
                })
                .catch((err) => {
                  console.error(err);
                  resolve({ success: false });
                });
            })
            .catch((err) => {
              console.error(err);
              resolve({ success: false });
            });
        } else {
          console.log("ERRRR");
          resolve({ success: false });
        }
      })
      .catch((err) => {
        //console.error("ACRIO ", err.message);
        if (err.message === "Invalid email!")
          toastr.error("El correo no se encuentra registrado");
        else {
          if (err.message === "Invalid email or password")
            toastr.error("Correo o contraseña inválidos");
          else toastr.error(err.message);
        }
      });
  });
};

/* export const logout = (session, history) => {
    return () => {
      return userLogout.run(session).then(() => {
        sessionService.deleteSession();
        sessionService.deleteUser();
        history.push('/login');
      }).catch(err => {
        throw (err);
      });
    };
  };*/
