import { PostRepository } from "../../../core/posts/domain/PostRepository";
import { AxiosPostRepository } from "../../../core/posts/infrastructure/repository/AxiosPostRepository";
import { PostImageFromDBGetter } from "../../../core/posts/application/use_cases/postimagefromdbgetter/getPostImageFromDB";
import { toastr } from "react-redux-toastr";
import { date } from "yup/lib/locale";

const postRepository: PostRepository = new AxiosPostRepository();
const postImageFromDBGetter = new PostImageFromDBGetter(postRepository);

export default function fetchPostImage(name, slug) {
  console.log(name, slug);
  return function (dispatch) {
    return postImageFromDBGetter
      .run(name, slug)
      .then((data) => {
        // console.log(data);

        dispatch({
          type: "GET_POST_IMAGE",
          postImage: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_POST_IMAGE",
          postImage: {},
        });
      });
  };
}

export function ferchPostImage(name, slug) {
  return function (dispatch) {
    return postImageFromDBGetter
      .run(name, slug)
      .then((data) => {
        return data;
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        return undefined;
      });
  };
}

export function getPostImage(name, slug) {
  console.log("FETCH POST Profile IMAGE");

  return postImageFromDBGetter
    .run(name, slug)
    .then((data) => {
      console.log(data);

      return data;
    })
    .catch((err) => {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de usuarios";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.status + " " + err.statusText;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
    });
}
