import { toastr } from "react-redux-toastr";
import { DashboardRepository } from "../../../core/dashboard/domain/services/DashboardRepository";
import { AxiosDashboardRepository } from "../../../core/dashboard/infrastructure/repository/AxiosDashboardRepository";

import { GetAllTopicMetricsNaiveBayes } from "../../../core/dashboard/aplication/use_cases/getallmetricsbytopicnaivebayes/GetAllMetricsByTopicNB";

const dashboardRepository: DashboardRepository = new AxiosDashboardRepository();

const getAllTopicMetricsNaiveBayes = new GetAllTopicMetricsNaiveBayes(
  dashboardRepository
);

export default function fetchThemesPercentAction() {
  return function (dispatch) {
    return getAllTopicMetricsNaiveBayes
      .run()
      .then((data) => {
        console.log(data);
        dispatch({
          type: "GET_TOPICS_PERCENT",
          themes: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de Temas de post";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_TOPICS_PERCENT",
          themes: [],
        });
      });
  };
}
