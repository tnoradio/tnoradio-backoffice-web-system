import { PostRepository } from "../../../core/posts/domain/PostRepository";
import { AxiosPostRepository } from "../../../core/posts/infrastructure/repository/AxiosPostRepository";
//import { PostDetailGetter } from '../../../core/posts/application/use_cases/getPostDetail/PostDetailtGetter'
import { toastr } from "react-redux-toastr";
import { TagCreator } from "../../../core/posts/application/use_cases/createTag/TagCreator";
import { AllTagsGetter } from "../../../core/posts/application/use_cases/gettags/AllTagsGetter";

const postRepository: PostRepository = new AxiosPostRepository();
const indexTags = new AllTagsGetter(postRepository);
const createTag = new TagCreator(postRepository);

export function fetchPostTags() {
  return function (dispatch) {
    return indexTags
      .run()
      .then((data) => {
        dispatch({
          type: "GET_ALL_POSTS_TAGS",
          postTags: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de etiquetas";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_ALL_POSTS_TAGS",
          postTags: [],
        });
      });
  };
}

export const addPostTag = (_id: string, tag: string) => {
  let newPostTag = {
    _id: undefined,
    tag: tag ? tag : "",
  };

  return (dispatch) => {
    return createTag
      .run(newPostTag)
      .then((res) => {
        toastr.success("Se ha guardado el post");
        dispatch({
          type: "ADD_POST_TAG",
          _id: res._id,
          tag: res.tag,
        });
        window.location.reload();
      })
      .catch((error) => {
        toastr.error("Hubo un problema");
        dispatch({
          type: "ADD_POST_TAG",
          _id: _id ? _id : "",
          title: tag ? tag : "",
        });
      });
  };
};
