import { PostRepository } from "../../../core/posts/domain/PostRepository";
import { AxiosPostRepository } from "../../../core/posts/infrastructure/repository/AxiosPostRepository";
import { ImageInDBModifier } from "../../../core/posts/application/use_cases/updateimageindb/ImageInDBModifier";
import { toastr } from "react-redux-toastr";

const postRepository: PostRepository = new AxiosPostRepository();
const imageInDBModifier = new ImageInDBModifier(postRepository);

export default function updatePostImage(
  image,
  name,
  type,
  slug,
  location,
  owner,
  fileId
) {
  return function (dispatch) {
    return imageInDBModifier
      .run(image, name, type, slug, location, owner, fileId)
      .then((response) => {
        toastr.success("Imagen Actualizada");
        dispatch({
          type: "UPDATE_POST_IMAGE",
          postImage: response,
        });
      })
      .catch((err) => {
        console.error(err);
        let errorMessage = "No se pudo actualizar el usuario";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err.data.message);
          errorMessage = err.data.message;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
      });
  };
}
