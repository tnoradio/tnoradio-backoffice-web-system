import { toastr } from "react-redux-toastr";
import { DashboardRepository } from "../../../core/dashboard/domain/services/DashboardRepository";
import { AxiosDashboardRepository } from "../../../core/dashboard/infrastructure/repository/AxiosDashboardRepository";

import { GetPublishedFacebook } from "../../../core/dashboard/aplication/use_cases/getPublishedFacebook/GetPublishedFacebook";

const dashboardRepository: DashboardRepository = new AxiosDashboardRepository();

const getPublishedFacebook = new GetPublishedFacebook(dashboardRepository);

export default function fetchPublishedFacebook() {
  console.log("Actions: facebook");
  return function (dispatch) {
    return getPublishedFacebook
      .run()
      .then((data) => {
        dispatch({
          type: "FETCH_PUBLISHED_FACEBOOK",
          published: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de publicaciones";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
        dispatch({
          type: "FETCH_PUBLISHED_FACEBOOK",
          published: [],
        });
      });
  };
}
