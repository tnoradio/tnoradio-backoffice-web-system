import { toastr } from "react-redux-toastr";
import { DashboardRepository } from "../../../core/dashboard/domain/services/DashboardRepository";
import { AxiosDashboardRepository } from "../../../core/dashboard/infrastructure/repository/AxiosDashboardRepository";
import { GetUsersClassifiedByTopic } from "../../../core/dashboard/aplication/use_cases/getUserClassifiedByTopic/GetTwitterUsersClassifiedByTopic";

const dashboardRepository: DashboardRepository = new AxiosDashboardRepository();

const getUsersClassifiedByTopic = new GetUsersClassifiedByTopic(
  dashboardRepository
);

export default function fetchUsersClassifiedByTopicAction() {
  return function (dispatch) {
    return getUsersClassifiedByTopic
      .run()
      .then((data) => {
        console.log(data);
        dispatch({
          type: "GET_USERS_CLASSIFIED_BY_TOPIC",
          usersClassified: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage =
          "No se pudo obtener la lista del progreso de las tareas por empleado";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
        dispatch({
          type: "GET_USERS_CLASSIFIED_BY_TOPIC",
          usersClassified: [],
        });
      });
  };
}
