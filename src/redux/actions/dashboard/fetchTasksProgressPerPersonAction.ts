import { toastr } from "react-redux-toastr";
import { DashboardRepository } from "../../../core/dashboard/domain/services/DashboardRepository";
import { AxiosDashboardRepository } from "../../../core/dashboard/infrastructure/repository/AxiosDashboardRepository";

import { GetTasksStatisticsPerPerson } from "../../../core/dashboard/aplication/use_cases/getTasksStatistics/GetTasksStatisticsPerPerson";

const dashboardRepository: DashboardRepository = new AxiosDashboardRepository();

const getTasksProgress = new GetTasksStatisticsPerPerson(dashboardRepository);

export default function fetchTasksProgressPerPersonAction() {
  return function (dispatch) {
    return getTasksProgress
      .run()
      .then((data) => {
        dispatch({
          type: "GET_TASKS_PROGRESS_PP",
          tasks: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage =
          "No se pudo obtener la lista del progreso de las tareas por empleado";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
        dispatch({
          type: "GET_TASKS_PROGRESS_PP",
          tasks: [],
        });
      });
  };
}
