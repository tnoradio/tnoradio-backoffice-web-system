import { toastr } from "react-redux-toastr";
import { DashboardRepository } from "../../../core/dashboard/domain/services/DashboardRepository";
import { AxiosDashboardRepository } from "../../../core/dashboard/infrastructure/repository/AxiosDashboardRepository";

import { GetPublishedInstagram } from "../../../core/dashboard/aplication/use_cases/getPublishedInstagram/GetPublishedInstagram";

const dashboardRepository: DashboardRepository = new AxiosDashboardRepository();

const getPublishedInstagram = new GetPublishedInstagram(dashboardRepository);

export default function fetchPublishedInstagram() {
  return function (dispatch) {
    return getPublishedInstagram
      .run()
      .then((data) => {
        console.log(data);
        dispatch({
          type: "FETCH_PUBLISHED_INSTAGRAM",
          published_instagram: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de publicaciones";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        // toastr.error(errorMessage);
        dispatch({
          type: "FETCH_PUBLISHED_INSTAGRAM",
          published_instagram: [],
        });
      });
  };
}
