import { toastr } from "react-redux-toastr";
import { DashboardRepository } from "../../../core/dashboard/domain/services/DashboardRepository";
import { AxiosDashboardRepository } from "../../../core/dashboard/infrastructure/repository/AxiosDashboardRepository";
import { UpdateUserBioTopic } from "../../../core/dashboard/aplication/use_cases/updateTwitterBioTopic/UpdateTwitterBioTopic";
import fetchUsersClassifiedByTopicAction from "./fetchUsersClassifiedByTopicsAction";

const dashboardRepository: DashboardRepository = new AxiosDashboardRepository();
const updateUserBioTopic = new UpdateUserBioTopic(dashboardRepository);

export default function updateTwitterBioTopicAction(
  userClassifiedByTopic,
  newTopic
) {
  return function (dispatch) {
    return updateUserBioTopic
      .run(userClassifiedByTopic, newTopic)
      .then((data) => {
        console.log(data);
        dispatch(fetchUsersClassifiedByTopicAction());
        toastr.success("Se ha actualizado el tópico");
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage =
          "No se pudo obtener la lista del progreso de las tareas por empleado";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
      });
  };
}
