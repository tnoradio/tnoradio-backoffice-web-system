import axios from "axios";
import createMockStore from "redux-mock-store";
import thunk, { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import addUser from "../addUser";
import { User } from "../../../../core/users/domain/User";
import { UserEmail } from "../../../../core/users/domain/UserEmail";
import * as testUtils from "../../../../utility/test/test-utils";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("Tests User's Redux Actions", () => {
  let store;
  const middlewares = [thunk];
  const mockStore = createMockStore<any, ThunkDispatch<any, any, AnyAction>>(
    middlewares
  );

  const expectedActions = [
    {
      type: "ADD_USER",
      _id: "mock_user_id_1",
      name: "testName",
      lastName: "testLastName",
      image: "https://randomuser.me/api/portraits/lego/1.jpg",
      dni: 1245678,
      email: new UserEmail("testEmail@email.com"),
      isActive: true,
      gender: "O",
      role: "AUDIENCE",
      address: "testAddress",
      notes: "This is a test user for unit testing",
      company: "tetsCompany",
      department: "TestDepartment",
      phone: "55-555-555-5555",
      isStarred: false,
      isDeleted: false,
      updatedBy: "",
      message: "Usuario agregado con éxito",
    },
  ];

  beforeEach(() => {
    const initialState = testUtils.storeInitialState;
    store = mockStore(initialState);
  });

  it("Dispatches ADD_USER action", async () => {
    (
      axios.post as jest.MockedFunction<typeof axios.post>
    ).mockResolvedValueOnce(userUtils.addUserResponse);

    return store
      .dispatch(
        addUser(
          "testName",
          "testLastName",
          "testEmail@email.com",
          "testAddress",
          "This is a test user for unit testing",
          "AUDIENCE",
          "O",
          1245678,
          "tetsCompany",
          "TestDepartment",
          "55-555-555-5555"
        )
      )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
