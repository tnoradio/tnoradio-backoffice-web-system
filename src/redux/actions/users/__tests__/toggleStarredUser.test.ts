import axios from "axios";
import createMockStore from "redux-mock-store";
import thunk, { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import toggleStarredUser from "../toggleStarredUser";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("Tests User's Redux Actions", () => {
  let store;
  const middlewares = [thunk];
  const mockStore = createMockStore<any, ThunkDispatch<any, any, AnyAction>>(
    middlewares
  );

  const expectedActions = [
    {
      type: "UPDATE_USER",
      _id: "mock_user_id_2",
      field: "isStarred",
      value: true,
    },
  ];

  beforeEach(() => {
    const initialState = userUtils.updateUserStoreInitialState;
    store = mockStore(initialState);
  });

  xit("Dispatches TOGGLE_STARRED_USER action", async () => {
    console.log(store.getState().userApp.users);

    (axios.get as jest.Mock).mockResolvedValueOnce(
      userUtils.mockToggleStarredUSerRequest
    );
    (axios.patch as jest.Mock).mockResolvedValueOnce(
      userUtils.starredUserResponse
    );

    return store.dispatch(toggleStarredUser("mock_user_id_2")).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
