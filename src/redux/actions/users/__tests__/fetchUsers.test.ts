import axios from "axios";
import createMockStore from "redux-mock-store";
import thunk, { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import fetchUsers from "../fetchUsers";
import { User } from "../../../../core/users/domain/User";
import { UserEmail } from "../../../../core/users/domain/UserEmail";
import * as testUtils from "../../../../utility/test/test-utils";

describe("Tests User's Redux Actions", () => {
  let store;
  const middlewares = [thunk];
  const mockStore = createMockStore<any, ThunkDispatch<any, any, AnyAction>>(
    middlewares
  );
  const mockUserResponse = User.create(
    "mock_user_id",
    "testName",
    "testLastName",
    "https://randomuser.me/api/portraits/lego/1.jpg",
    1245678,
    new UserEmail("testEmail@email.com"),
    true,
    "testPassword",
    "testPassword",
    "O",
    "AUDIENCE",
    "testAddress",
    "This is a test user for unit testing",
    "tetsCompany",
    "TestDepartment",
    "55-555-555-5555",
    false,
    false,
    ""
  );
  const mockUsersList = [];
  mockUsersList.push(mockUserResponse);
  mockUsersList.push(mockUserResponse);
  const mockedResponse = { data: mockUserResponse };

  const expectedActions = [
    {
      type: "GET_ALL_USERS",
      users: mockUsersList,
    },
  ];

  beforeEach(() => {
    const initialState = testUtils.storeInitialState;
    store = mockStore(initialState);
  });

  it("Dispatches index users action", async () => {
    (axios.get as jest.MockedFunction<typeof axios.get>).mockResolvedValueOnce({
      data: mockUsersList,
    });

    return store.dispatch(fetchUsers()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
