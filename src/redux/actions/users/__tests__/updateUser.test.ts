import axios from "axios";
import createMockStore from "redux-mock-store";
import thunk, { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import updateUser from "../updateUser";
import * as userUtils from "../../../../utility/test/test-user-utils";

describe("Tests User's Redux Actions", () => {
  let store;
  const middlewares = [thunk];
  const mockStore = createMockStore<any, ThunkDispatch<any, any, AnyAction>>(
    middlewares
  );

  const expectedActions = [
    {
      type: "UPDATE_USER",
      _id: "mock_user_id_1",
      field: "name",
      value: "testName",
    },
  ];

  beforeEach(() => {
    const initialState = userUtils.updateUserStoreInitialState;
    store = mockStore(initialState);
  });

  it("Dispatches UPDATE_USER action", async () => {
    //  console.log(store.getState().userApp.users);

    (axios.get as jest.Mock).mockResolvedValueOnce(userUtils.addUserResponse);
    (axios.patch as jest.Mock).mockResolvedValueOnce(userUtils.addUserResponse);

    return store
      .dispatch(updateUser("mock_user_id_1", "name", "testName"))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
