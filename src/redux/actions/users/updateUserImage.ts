import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { ImageInDBModifier } from "../../../core/users/application/use_cases/updateimageindb/ImageInDBModifier";
import { toastr } from "react-redux-toastr";

const userRepository: UserRepository = new AxiosUserRepository();
const imageInDBModifier = new ImageInDBModifier(userRepository);

export default function updateUserImage(
  image,
  name,
  type,
  slug,
  location,
  owner
) {
  console.log("UPDATE USER Image");
  console.log(image);

  return function (dispatch) {
    return imageInDBModifier
      .run(image, name, type, slug, location, owner)
      .then((response) => {
        console.log(response);
        toastr.success("Imagen Actualizada");
        dispatch({
          type: "UPDATE_USER_IMAGE",
          userImage: response,
        });
      })
      .catch((err) => {
        console.error(err);
        let errorMessage = "No se pudo actualizar el usuario";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err.data.message);
          errorMessage = err.data.message;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
      });
  };
}
export function updateUserProfileImage(
  image,
  name,
  type,
  slug,
  location,
  owner
) {
  console.log("UPDATE USER Image");
  console.log(image);

  return function (dispatch) {
    return imageInDBModifier
      .run(image, name, type, slug, location, owner)
      .then((response) => {
        console.log(response);
        toastr.success("Imagen Actualizada");
        dispatch({
          type: "UPDATE_USER_PROFILE_IMAGE",
          userProfileImage: response,
        });
      })
      .catch((err) => {
        console.error(err);
        let errorMessage = "No se pudo actualizar el usuario";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err.data.message);
          errorMessage = err.data.message;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
      });
  };
}
