import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserDestroyer } from "../../../core/users/application/use_cases/deleteuser/UserDestroyer";

const userRepository: UserRepository = new AxiosUserRepository();
const userDestroyer = new UserDestroyer(userRepository);

export const userDetails = (_id) => {
  return function (dispatch) {
    dispatch({
      type: "USER_DETAILS",
      _id,
    });
  };
};

export const setEditUserFlag = (flag) => ({
  type: "EDIT_USER",
  flag,
});

export const setVisibilityFilter = (filter) => ({
  type: "SET_VISIBILITY_FILTER",
  filter,
});

export const usersSearch = (searchTerm) => ({
  type: "FILTER_USER",
  payload: searchTerm,
});

export const destroyUser = (_id) => {
  return function (dispatch) {
    return userDestroyer
      .run(_id)
      .then((response) => {
        dispatch({
          type: "DELETE_USER",
          _id,
        });
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: "DELETE_USER",
          _id,
        });
      });
  };
};

export const userVisibilityFilter = {
  SHOW_ALL: "SHOW_ALL",
  FREQUENT_USER: "FREQUENT_USER",
  STARRED_USER: "STARRED_USER",
  EMPLOYEE_USER: "EMPLOYEE_USER",
  AUDIENCE_USER: "AUDIENCE_USER",
  ADMIN_USER: "ADMIN_USER",
  CLIENT_USER: "CLIENT_USER",
  POTENTIAL_CLIENT_USER: "POTENTIAL_CLIENT_USER",
  PRODUCER_USER: "PRODUCER_USER",
  HOST_USER: "HOST_USER",
  PRODUCTION_ASSISTANT_USER: "PRODUCTION_ASSISTANT_USER",
  SEARCH_USER: "SEARCH_USER",
  DELETED_USER: "DELETED_USER",
};

export { default as fetchUsers } from "./fetchUsers";
export { default as addUser } from "./addUser";
export { default as toggleStarredUser } from "./toggleStarredUser";
export { default as updateUser } from "./updateUser";
