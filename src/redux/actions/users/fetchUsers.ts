import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { AllUsersGetter } from "../../../core/users/application/use_cases/getusers/AllUsersGetter";
import { toastr } from "react-redux-toastr";

const userRepository: UserRepository = new AxiosUserRepository();
const indexUsers = new AllUsersGetter(userRepository);

//let res: User[];

export default function fetchUsers() {
  //console.log("FETCH USERS");
  return function (dispatch) {
    return indexUsers
      .run()
      .then((data) => {
        dispatch({
          type: "GET_ALL_USERS",
          users: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "GET_ALL_USERS",
          users: [],
        });
      });
  };
}

export function fetchUsersList() {
  //console.log("FETCH USERS");

  return indexUsers
    .run()
    .then((data) => data)
    .catch((err) => {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de usuarios";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.status + " " + err.statusText;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
    });
}
