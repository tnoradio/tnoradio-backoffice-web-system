import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserModifier } from "../../../core/users/application/use_cases/modifyuser/UserModifier";
import { store } from "../../storeConfig/store";
import { toastr } from "react-redux-toastr";

const userRepository: UserRepository = new AxiosUserRepository();
const userModifier = new UserModifier(userRepository);

export default function toggleStarredUser(_id) {
  console.log(store.getState());
  const users = store.getState().userApp.users;
  console.log(users);
  const starredUser = users.find((user) => user._id === _id);

  const starredUserReq = {
    _id: _id,
    field: "isStarred",
    value: !starredUser.isStarred,
  };

  return function (dispatch) {
    return userModifier
      .run(starredUserReq)
      .then((response) => {
        console.log("RESPONSE", response);
        dispatch({
          type: "TOGGLE_STARRED_USER",
          _id,
        });
      })
      .catch((err) => {
        console.error(err);
        toastr.error(err);
      });
  };
}
