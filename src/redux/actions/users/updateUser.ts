import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserModifier } from "../../../core/users/application/use_cases/modifyuser/UserModifier";
import { toastr } from "react-redux-toastr";
import { sessionService } from "redux-react-session";

const userRepository: UserRepository = new AxiosUserRepository();
const userModifier = new UserModifier(userRepository);

export default function updateUser(_id, field, value) {
  console.log("UPDATE USER");

  return function (dispatch) {
    const updatedUser = {
      _id: _id,
      field: field,
      value: value,
    };

    return userModifier
      .run(updatedUser)
      .then((response) => {
        toastr.success("Usuario Actualizado");
        dispatch({
          type: "UPDATE_USER",
          _id: _id,
          field: field,
          value: value,
        });
        sessionService.loadUser().then((user) => {
          if (user._id === _id) sessionService.saveUser(response);
        });
      })
      .catch((err) => {
        console.error(err);
        let errorMessage = "No se pudo actualizar el usuario";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err.data.message);
          errorMessage = err.data.message;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
      });
  };
}
