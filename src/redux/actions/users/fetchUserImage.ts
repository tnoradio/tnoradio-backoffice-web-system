import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserImageFromDBGetter } from "../../../core/users/application/use_cases/userimagefromdbgetter/getUserImageFromDB";
//import { toastr } from "react-redux-toastr";

const userRepository: UserRepository = new AxiosUserRepository();
const userImageFromDBGetter = new UserImageFromDBGetter(userRepository);

export default function fetchUserImage(name, slug) {
  return function (dispatch) {
    return userImageFromDBGetter
      .run(name, slug)
      .then((data) => {
        dispatch({
          type: "GET_USER_IMAGE",
          userImage: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
        dispatch({
          type: "GET_USER_IMAGE",
          userImage: {},
        });
      });
  };
}

export function getUserImage(name, slug) {
  return userImageFromDBGetter
    .run(name, slug)
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de usuarios";
      if (err.data) {
        console.log("HAY ERROR DATA");
        console.log(err);
        errorMessage = err.status + " " + err.statusText;
      } else {
        if (err.message) {
          console.log("HAY ERROR MESSAGE");
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      //toastr.error(errorMessage);
      return undefined;
    });
}

export function getUserProfileImage(name, slug) {
  return function (dispatch) {
    return userImageFromDBGetter
      .run(name, slug)
      .then((data) => {
        dispatch({
          type: "GET_USER_PROFILE_IMAGE",
          userProfileImage: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        //toastr.error(errorMessage);
        dispatch({
          type: "GET_USER_PROFILE_IMAGE",
          userProfileImage: {},
        });
      });
  };
}
