import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserCreator } from "../../../core/users/application/use_cases/createuser/UserCreator";
import { toastr } from "react-redux-toastr";
import { UserEmail } from "../../../core/users/domain/UserEmail";
import { ImageToDBUploader } from "../../../core/users/application/use_cases/uploadImage/imageUploadToDB";
import { User } from "../../../core/users/domain/User";

const userRepository: UserRepository = new AxiosUserRepository();
const userCreator = new UserCreator(userRepository);
const imageToDBUploader = new ImageToDBUploader(userRepository);

export default function addUser(
  name,
  lastName,
  bio,
  birthdate,
  email,
  address,
  notes,
  socials,
  roles,
  gender,
  images,
  dni,
  company,
  department,
  phone,
  seed: any,
  password?,
  passwordConfirmation?
) {
  let user: User;
  let slug = name.toLowerCase() + lastName.toLowerCase();

  slug = slug
    .replace(/\s/g, "")
    .normalize("NFD")
    .replace(
      /([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,
      "$1"
    )
    .normalize();

  const userImages = [];

  let newUser = {
    _id: undefined,
    name: name,
    lastName: lastName,
    bio: bio,
    birthdate: birthdate,
    image: "profile_" + slug,
    dni: dni,
    email: { value: email },
    address: address,
    isActive: true,
    isWorking: false,
    password: password ? password : "cambiarContrasena",
    passwordConfirmation: passwordConfirmation
      ? passwordConfirmation
      : "cambiarContrasena",
    socials: socials,
    gender: gender,
    images: userImages,
    roles: roles,
    notes: notes,
    company: company,
    department: department,
    phone: phone,
    isStarred: false,
    isDeleted: false,
    updatedBy: "",
    slug: slug,
    turns: [],
  };

  return (dispatch) => {
    console.log(newUser);
    return userCreator
      .run(newUser)
      .then((res) => {
        console.log(res);
        user = res;
        if (images.length === 2) {
          imageToDBUploader
            .run(
              images[0],
              "profile_" + slug + ".jpg",
              "profile",
              slug,
              "profiles",
              user._id
            )
            .then((res) => {
              imageToDBUploader
                .run(
                  images[1],
                  "talent_" + slug + ".jpg",
                  "talent",
                  slug,
                  "talents",
                  user._id
                )
                .then((res) => {
                  dispatch({
                    type: "ADD_USER",
                    _id: user._id,
                    name: user.name ? user.name : "",
                    lastName: user.lastName ? user.lastName : "",
                    bio: user.bio ? user.bio : "",
                    image: "profiles/profile_" + name + lastName + ".jpg",
                    department: user.department ? user.department : "",
                    dni: user.dni ? user.dni : 0,
                    company: user.company ? user.company : "",
                    phone: user.phone ? user.phone : "",
                    address: user.address ? user.address : "",
                    email: user.email
                      ? new UserEmail(user.email.value)
                      : new UserEmail(""),
                    isActive: user.isActive ? user.isActive : false,
                    isWorking: user.isWorking ? user.isWorking : false,
                    addusers: user.address ? user.address : "",
                    socials: user.socials ? user.socials : [],
                    notes: user.notes ? user.notes : "",
                    roles: user.roles ? user.roles : [],
                    gender: user.gender ? user.gender : "",
                    images: user.images ? user.images : [],
                    isStarred: user.isStarred ? user.isStarred : false,
                    isDeleted: user.isDeleted ? user.isDeleted : false,
                    updatedBy: user.updatedBy ? user.updatedBy : "",
                    slug: slug,
                    message: "Usuario agregado con éxito",
                  });
                  toastr.success("Usuario guardado exitosamente");
                  seed();
                })
                .catch((e) => {
                  console.log("Problema guardando imagen de Talento");
                  console.log(e);
                });
            })
            .catch((e) => {
              console.log("Problema guardando imagen de Perfil");
              console.log(e);
            });
        } else {
          dispatch({
            type: "ADD_USER",
            _id: res._id,
            name: res.name ? res.name : "",
            lastName: res.lastName ? res.lastName : "",
            bio: res.bio ? res.bio : "",
            image: "https://randomuser.me/api/portraits/lego/1.jpg",
            department: res.department ? res.department : "",
            dni: res.dni ? res.dni : 0,
            company: res.company ? res.company : "",
            phone: res.phone ? res.phone : "",
            email: res.email
              ? new UserEmail(res.email.value)
              : new UserEmail(""),
            isActive: res.isActive ? res.isActive : false,
            isWorking: res.isWorking ? res.isWorking : false,
            address: res.address ? res.address : "",
            socials: res.socials ? res.socials : [],
            notes: res.notes ? res.notes : "",
            roles: res.roles ? res.roles : [],
            gender: res.gender ? res.gender : "",
            images: res.images ? res.images : [],
            isStarred: res.isStarred ? res.isStarred : false,
            isDeleted: res.isDeleted ? res.isDeleted : false,
            updatedBy: res.updatedBy ? res.updatedBy : "",
            slug: slug,
            password: res.password ? res.password : "cambiarContrasena",
            passwordConfirmation: res.passwordConfirmation
              ? res.passwordConfirmation
              : "cambiarContrasena",
            message: "Usuario agregado con éxito",
          });
          seed();
        }
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "Hubo un error";
        if (err !== undefined && err.data !== undefined) {
          errorMessage = err.data.error;
          console.log("1" + errorMessage);
        } else {
          if (err !== undefined && err.message !== undefined) {
            errorMessage = err.message;
            console.log("2" + errorMessage);
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "ADD_USER",
          message: errorMessage,
          _id: "",
          name: "",
          lastName: "",
          bio: "",
          image: "",
          department: "",
          company: "",
          phone: "",
          email: {},
          address: "",
          socials: [],
          isActive: false,
          isWorking: false,
          notes: "",
          role: [],
          gender: "",
          images: [],
          isStarred: false,
          isDeleted: false,
          updatedBy: "",
          dni: 0,
          slug: "",
          password: "",
          passwordConfirmation: "",
        });
        seed();
      });
  };
}

export function signUpUser(
  name,
  lastName,
  bio,
  birthdate,
  email,
  address,
  notes,
  socials,
  roles,
  gender,
  images,
  dni,
  company,
  department,
  phone,
  password?,
  passwordConfirmation?,
  turns?
) {
  let user: User;
  let slug = name.toLowerCase() + lastName.toLowerCase();

  slug = slug
    .replace(/\s/g, "")
    .normalize("NFD")
    .replace(
      /([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,
      "$1"
    )
    .normalize();

  const userImages = [];
  console.log(images);
  let newUser = {
    _id: undefined,
    name: name,
    lastName: lastName,
    bio: bio,
    birthdate: birthdate,
    image: "profile_" + slug,
    dni: dni,
    email: { value: email },
    address: address,
    isActive: true,
    isWorking: false,
    password: password ? password : "cambiarContrasena",
    passwordConfirmation: passwordConfirmation
      ? passwordConfirmation
      : "cambiarContrasena",
    socials: socials,
    gender: gender,
    images: userImages,
    roles: roles,
    notes: notes,
    company: company,
    department: department,
    phone: phone,
    isStarred: false,
    isDeleted: false,
    updatedBy: "",
    slug: slug,
    turns: turns,
  };

  return userCreator
    .run(newUser)
    .then((res) => {
      console.log(res);
      user = res;
      return user;
    })
    .catch((err) => {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "Hubo un error";
      if (err !== undefined && err.data !== undefined) {
        errorMessage = err.data.error;
        console.log("1" + errorMessage);
      } else {
        if (err !== undefined && err.message !== undefined) {
          errorMessage = err.message;
          console.log("2" + errorMessage);
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      toastr.error(errorMessage);
      return err;
    });
}
