import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { toastr } from "react-redux-toastr";
import { PasswordResetter } from "../../../core/users/application/use_cases/resetpassword/PasswordResetter";

const userRepository: UserRepository = new AxiosUserRepository();
const passwordResetter = new PasswordResetter(userRepository);

export default function resetLoggedUserPass(
  _id: string,
  oldPassword: string,
  password: string,
  passwordConfirmation: string
) {
  return function (dispatch) {
    return passwordResetter
      .run(_id, oldPassword, password, passwordConfirmation)
      .then((data) => {
        dispatch({
          type: "RESET_USER_PASS",
          pass: data,
        });
      })
      .catch((err) => {
        console.log("INDEX ERRROR");
        console.log(err);
        let errorMessage = "No se pudo obtener la lista de usuarios";
        if (err.data) {
          console.log("HAY ERROR DATA");
          console.log(err);
          errorMessage = err.status + " " + err.statusText;
        } else {
          if (err.message) {
            console.log("HAY ERROR MESSAGE");
            errorMessage = err.message;
          } else {
            console.log("ERROR GENERICO INDEX");
          }
        }
        toastr.error(errorMessage);
        dispatch({
          type: "RESET_USER_PASS",
          pass: "",
        });
      });
  };
}
