import { CallMaker } from "../../../core/chat/application/use_cases/makeCall";
import { ChatsGetter } from "../../../core/chat/application/use_cases/chatsGetter";
import { ChatService } from "../../../core/chat/domain/ChatService";
import { AxiosTelegramRepository } from "../../../core/chat/infrastructure/AxiosTelegramRepository";
import { MessageSender } from "../../../core/chat/application/use_cases/sendMessage";
import { AuthCodeSender } from "../../../core/chat/application/use_cases/sendCode";
import { ServiceSignIn } from "../../../core/chat/application/use_cases/serviceSignIn";
import { ConfigGetter } from "../../../core/chat/application/use_cases/getServiceConfig";
import { toastr } from "react-redux-toastr";
import { UserGetter } from "../../../core/chat/application/use_cases/userGetter";
import { ChannelGetter } from "../../../core/chat/application/use_cases/chanelGetter";
import { MessageHistoryGetter } from "../../../core/chat/application/use_cases/messageHistoryGetter";
import { MeGetter } from "../../../core/chat/application/use_cases/meGetter";

const chatService: ChatService = new AxiosTelegramRepository();
const callMaker = new CallMaker(chatService);
const chatsGetter = new ChatsGetter(chatService);
const messageSender = new MessageSender(chatService);
const authCodeSender = new AuthCodeSender(chatService);
const inSigner = new ServiceSignIn(chatService);
const configGetter = new ConfigGetter(chatService);
const userGetter = new UserGetter(chatService);
const channelGetter = new ChannelGetter(chatService);
const messageHistoryGetter = new MessageHistoryGetter(chatService);
const meGetter = new MeGetter(chatService);

export const call = () => {
  callMaker.run();
};

export const getConfig = async () => {
  const res = await configGetter.run();
  return res;
};

export function getChatsList() {
  console.log("FETCH TELEGRAM");
  return async function (dispatch) {
    try {
      const data = await chatsGetter.run();
      dispatch({
        type: "FETCH_USER_TELEGRAM_MESSAGES",
        telegramMessages: data,
      });
    } catch (err) {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de mensajes de Telegram";
      toastr.error(errorMessage);
      dispatch({
        type: "FETCH_USER_TELEGRAM_MESSAGES",
        telegramMessages: [],
      });
    }
  };
}

export const sendMessage = (chatId, text) => {
  return async (dispatch) => {
    try {
      const msg = await messageSender.run(chatId, text);
      const messageHistory = await messageHistoryGetter.run(chatId);
      dispatch({
        type: "SEND_MSG",
        id: chatId,
        messageHistory: messageHistory.reverse(),
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: "SEND_MSG",
        id: chatId,
        messageHistory: [],
      });
    }
  };
};

export const sendAuthCode = async (phone) => {
  return await authCodeSender.run(phone);
};

export const signIn = async (code, phone_code_hash, phone) => {
  return await inSigner.run(code, phone_code_hash, phone);
};

export const authenticateTelegram = () => {
  return (dispatch) => {
    return dispatch({
      type: "HANDLE_TELEGRAM_AUTHENTICATION",
      isTelegramAuthenticated: true,
    });
  };
};

export const getPeerUser = async (userId) => {
  try {
    return await userGetter.run(userId);
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getFullChannel = async (channelId) => {
  try {
    return await channelGetter.run(channelId);
  } catch (e) {
    console.log(e);
    return null;
  }
};

export function getMessagesHistory(chatId) {
  console.log("FETCH HISTORY");
  return async function (dispatch) {
    try {
      const data = await messageHistoryGetter.run(chatId);
      dispatch({
        type: "FETCH_MESSAGE_HISTORY",
        messageHistory: data.reverse(),
      });
    } catch (err) {
      console.log("INDEX ERRROR");
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de mensajes del chat";
      toastr.error(errorMessage);
      dispatch({
        type: "FETCH_MESSAGE_HISTORY",
        messageHistory: [],
      });
    }
  };
}

export const getMe = async () => {
  try {
    const me = await meGetter.run();
    return me;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
